#!/usr/bin/python

'''
cron script to convert new profiler imm data and 3dgmx data from binary to ASCII.
works by looking at list of buoys (from /home directory), adding deployment name (read from usrlist.txt)
and then looking at the appropriate directories for files.

It will process C*.DAT, E*.DAT from the WFP, 3dgmx data from the instrument pack,

Output data is placed into the same directory as the input data was found.

 History:
     Date       Who     Description
     ------     ---     ---------------------------------------------------
   09/26/2013   JWC     Creation

'''

import os, sys , glob, string, time, urllib, re

debugOutput = 0
cmdExecute  = 1

buoyList = []
deployment = ''

# dictionary list, with value being a list with 
#    [ path, glob_string, binary_extension, ASCII_extension, dumper ]
processList = \
{
    'mmp_E': [ 'imm/mmp/', 'proc_imm/mmp_eng/', 'E*.DAT', 'DAT', 'TXT', 'mmp_unpack' ],
    'mmp_C': [ 'imm/mmp/', 'proc_imm/mmp_ctd/', 'C*.DAT', 'DAT', 'TXT', 'mmp_unpack' ],
    'mmp_A': [ 'imm/mmp/', 'proc_imm/mmp_nrtk/', 'A*.DEC', 'DEC', 'TXT', 'mmp_unpack' ],
    'adcpt': [ 'imm/adcpt/', 'proc_imm/adcp_out/', 'adcp*.DAT', 'DAT', 'txt', 'adcp_dump' ],
    'adcps': [ 'imm/adcps/', 'proc_imm/adcp_out/', 'adcp*.DAT', 'DAT', 'txt', 'adcp_dump' ],
    'adcp' : [ 'imm/adcp/', 'proc_imm/adcp_out/', 'adcp*.DAT', 'DAT', 'txt', 'adcp_dump' ],
    '3dmgx': [ '3dmgx3/', 'proc_3dmgx3/', '*.log', 'log', 'txt', '3dmgx_dump' ],
    '3dmgx': [ 'dcl[0-9][0-9]/mopak/', 'proc_mopak/', '*.log', 'log', 'txt', '3dmgx_dump' ],
}
data_path = 0
out_path = 1
in_name = 2
in_extent = 3
out_extent = 4
unpacker = 5
binPath = '/home/ooiuser/pss_sw/cg_util/sensor_dump'
usrListTxt = '/home/ooiuser/pss_sw/webdata/cgsn/omc/cg_shore/ftp_xfr/usrlist.txt'
cfg_file_name = '/home/ooiuser/pss_sw/cg_util/cgi-bin/pss/SurfaceMoorings.cfg'


def readBuoyList():
    '''
    This module reads the list of buoys (logins under /home that match 
    [cg][aeips][01][0-9][a-z][a-z][a-z][a-z]).
    '''
    buoyList = glob.glob('/home/[cg][aeips][01][0-9][a-z][a-z][a-z][a-z]')
    tmpBuoyList = []
    for buoy in buoyList:
        tmpBuoyList = tmpBuoyList + [ buoy[len('/home/'):], ]
    buoyList = tmpBuoyList
    # sort in place
    buoyList.sort()

    return buoyList

if __name__ == '__main__':
    print 'Processing Packed data at {0} GMT'.format(time.strftime('%d %b %Y %H:%M:%S',time.gmtime()))

    MooringType = {}

    try:
        fd = open(cfg_file_name,'r')
    except:
        print 'Error opening file:  '.format(cfg_file_name)
    else:
        for line in fd:
            if len(line) < 3:
                continue
            (key, val) = line.split('=')
            key = urllib.unquote_plus(key)
            val = urllib.unquote_plus(val)
            key = key.replace('\r','\n')
            val = val.replace('\r','\n')
            key1 = key.rstrip('\s').lower()
            val1 = val.rstrip('\n')
            MooringType[key1] = val1
        fd.close()

    buoyList = readBuoyList()

    lastEfile = ''
    #print "Debug buoy: {0}".format(buoyList)

    for plat in buoyList:
        #print 'DEBUG:: plat: {0}'.format(plat)
	lastEfile = ''
        for key in processList:
            #print '\tkey: {0}, processList: {1}'.format(key,
            #                                            processList[key][0])
            # now get the deployment from usrlist.txt
            fd = open(usrListTxt, 'r')
            for line in fd:
                if line[0] == '#': continue
                #print "DEBUG: {0}".format(line)
                array = line.split(' ')
		#print "DEBUG: {0}".format(array)
                if array[0] == plat:
                    base = array[1].rstrip('\r\n')
	            #print "DEBUG: {0}".format(base)
                    break
            fd.close()

            if base.find(plat) < 0:
                # no match!
                base = ''
                lastEfile = ''
            else:
                # have a match, continue
                if MooringType[plat].find('SURF') > -1:
                   base = base + '/cg_data'
                print "Plat: {0}, base: {1}".format(plat, base)
    
                pathStr = '{0}/{1}'.format(base,
                                           processList[key][data_path])
                file_pathStr = '{0}{1}'.format(pathStr,
                                                 processList[key][in_name])
                # glob the list for matchs to the binary names
                fileList = glob.glob(file_pathStr)
                #print '\t\t||{0}||'.format(file_pathStr)
                # sort to make last file the correct one to compare with 
                # TIMETAGS.TXT
                fileList.sort()
                # now process all the files in fileList and check mod time
                for fileName in fileList:
                    print '         processng: {0}'.format(fileName)

                    if processList[key][out_path].find('SAME') > -1:
                        asciiName = string.replace(fileName, 
                                                   processList[key][in_extent], 
                                                   processList[key][out_extent])
                        outdirPath = pathStr
                    else:
                        # if the cg_data is in the path, remove it
                        baseComPath = string.replace(base, '/cg_data', '')
                        # first get the directory only
                        outdirPath = '{0}/{1}'.format(baseComPath,
                                                  processList[key][out_path])
                        # then is there isn't one, make it
                        if debugOutput:
                            print 'check path: {0}'.format(outdirPath)
                        if not os.path.exists(outdirPath):
                            # make the directory
                            if debugOutput:
                                print 'making path: {0}'.format(outdirPath)
                            if cmdExecute:
                                os.makedirs(outdirPath)

                        asciiName1 = string.replace(fileName, 
                                                    processList[key][in_extent], 
                                                    processList[key][out_extent])
                        # if it is there, remove the /cg_data string from out
                        asciiName2 =  string.replace(asciiName1,
                                                     '/cg_data',
                                                     '')

                        asciiName = re.sub(processList[key][data_path],
                                           processList[key][out_path],
                                           asciiName2)

                    # save this for later.
                    if key == 'mmp_E':
                        lastEfile = fileName
                        lastEfileOutPath = outdirPath

                    # if ASCII file exists
                    if os.path.isfile(asciiName):

                        # if binary file is newer than the ASCII file
                        if ( os.path.getmtime(fileName) >  
                             os.path.getmtime(asciiName) ):
                            # build the command that generates the ASCII file
                            if key == '3dmgx':
                                cmd = '{0}/{1} < {3} >{4}'.format(binPath,
                                                                  processList[key][unpacker],
                                                                  pathStr,
                                                                  fileName,
                                                                  asciiName)
                            else:
                                cmd = '{0}/{1} {2} {3}'.format(binPath,
                                                               processList[key][unpacker],
                                                               outdirPath,
                                                               fileName)
                            # and execute it
                            if cmdExecute:
                                os.system(cmd)
                            if debugOutput:
                                print 'Testing, execute: {0}'.format(cmd)
                            if os.geteuid() == 0:
                                cmd = 'chown -R {0}:{0} {1}'.format(plat, outdirPath)
                                if debugOutput:
                                    print 'A) cmd: {0}'.format(cmd)
                                if cmdExecute:
                                    os.system(cmd)
                    else:
                        # build the command that generates the ASCII file
                        if key == '3dmgx':
                            cmd = '{0}/{1} < {3} >{4}'.format(binPath,
                                                              processList[key][unpacker],
                                                              pathStr,
                                                              fileName,
                                                              asciiName)
                        else:
                            cmd = '{0}/{1} {2} {3}'.format(binPath,
                                                           processList[key][unpacker],
                                                           outdirPath,
                                                           fileName)
                        # and execute it
                        if cmdExecute:
                            os.system(cmd)
                        if debugOutput:
                            print 'Testing, execute: {0}'.format(cmd)
                        if os.geteuid() == 0:
                            cmd = 'chown -R {0}:{0} {1}'.format(plat, outdirPath)
                            if debugOutput:
                                print 'B) cmd: {0}'.format(cmd)
                            if cmdExecute:
                                os.system(cmd)

        # don't forget TIMETAGS.TXT
        if len(lastEfile) > 0:
            # build the command that generates the ASCII file
            cmd = '{0}/{1} {2} {3} -t'.format(binPath,
                                           processList[key][unpacker],
                                           lastEfileOutPath,
                                           '{0}E*.DAT'.format(pathStr))
            # and execute it
            if cmdExecute:
                os.system(cmd)
            if debugOutput:
                print 'Testing, execute: {0}'.format(cmd)
            if os.geteuid() == 0:
                cmd = 'chown -R {0}:{0} {1}'.format(plat, lastEfileOutPath)
                if debugOutput:
                    print 'C) cmd: {0}'.format(cmd)
                if cmdExecute:
                    os.system(cmd)
    # fini!

