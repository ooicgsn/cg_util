Summary:	cg_util
Name:		cg_util
Version:	1.0
Release:	1
License:	tbd
Group:		Applications/System
Source:		cg_util
BuildArch:	noarch
Vendor:		ooicgsn
BuildRoot:      %{_tmppath}/%{name}-root-%(%{__id_u} -n)
AutoReqProv:	no
Requires:       perl
Provides:	cg_util

# global variables
%define install_user ooiuser
%define install_group ooiuser

%description
This package builds and installs cg_util in the %{install_user} home directory.


########################################
%build
cd $RPM_SOURCE_DIR/cg_util/sensor_dump/
make

########################################
# This is where most of the work is done. A "root" folder is created in $RPM_BUILD_ROOT
# and everything is installed there as if it were on a real server. This "installation"
# folder is packaged into an rpm. After creation of the rpm, this folder is deleted by
# the "clean" section.
%install
# Create some helpful variables
src=$RPM_SOURCE_DIR/cg_util
root=$RPM_BUILD_ROOT
home=$root/home/%{install_user}

# Clean any previous build and set up directories.
rm -rf $root
mkdir $root
mkdir -p $home

# copy parts of cg_util the will be deployed
mkdir -p $home/cg_util
cp -r $src/bin $home/cg_util/
cp -r $src/cgi-bin $home/cg_util/
cp -r $src/include $home/cg_util/
cp -r $src/wiki $home/cg_util/
cp $src/README.md $home/cg_util/


########################################
%clean
cd $RPM_SOURCE_DIR/cg_util/sensor_dump/
make clean
rm -rf $RPM_BUILD_ROOT


########################################
# List of all files and directories that are managed (created/erased) by this rpm install/uninstall.
# Note that the paths should be absolute.
%files
%attr(-, %{install_user}, %{install_group}) /home/%{install_user}/cg_util/
