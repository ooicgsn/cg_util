/* ====================================================================== 
** WHOI OOI/CGSN system type definitions for processes and pthreads using
** shared memory (SHM) variables.
**
** Modification History:
**   Date      WHO     Description
** 03/31/2010  SL      create
** 03/17/2011  SL      Moved cpm_sys into mpic_sys
** 04/13/2011  ME      Added to data logger structure
** 05/24/2011  SL      Combined fw_wifi since phsically connected
** 05/25/2011  JKOB/SL Added Irid session structure
** 07/31/2011  SL      Added fb250 session structure
** 08/04/2011  SL      Updated irid_session structure
** 08/12/2011  JKOB    Added mmp session structure
** 05/24/2012  ME      Updated syslog_t - added debug_level and min_byte
** 10/10/2012  ME      Added fields to data logger structure
** 11/09/2012  ME/SL   Removed include serial.h as not needed. Moved contact_i
**                     data struct to Dlog/data_logger.h. Elim thread_pid_t
** 02/06/2013  JZ/SL   Added psc.err_flag3, Fuel remaining changed to fc_level.
** 04/04/2013  JKOB    added file_bytes_sent to irid session type and
**                     added some new items to an imm session type
** 04/10/2013  SL      Added mpic esw and dsl flags
** 05/30/2013  JKOB    added mmp_decimation item in imm_session_t to support new MMP command
** 06/13/2013  SL      Added rda_sys_t
** 08/07/2013  JKOB    Add mmp_reqxxx_delay
** 09/13/2013  SL      Add pwr board fields for DCL firmware update (v2.07)
** 11/01/2013  SL      Added PSC fields for MPEA
** 03/26/2014  ME      Added last communications field (last_comm_t) to inst_status_t
**                     and last_data_t now last time good data rcvd
** 07/03/2014  SL      Increased MAX_IMM_DEVICES to 20
** 11/15/2014  SL      Added fc_status
** 03/19/2015  SL      Added deploy_dir to irid_session_t;
** 03/20/2015  SL      Added PSC fields for DNP aka dock conversion bottle
                       added mpea htl leak detect (htl_ld) and pressure (htl_p) fields
** 03/28/2015  SL      Added auv_dock_t
** 04/10/2017  CC      Removed AUV Dock type
** ======================================================================
*/

#ifndef CG_SYSTYPES_INC
#define CG_SYSTYPES_INC 

#include <sys/time.h>

// typedefs
typedef unsigned char       byte;
typedef unsigned int        uint;
typedef unsigned long       ulong;
typedef unsigned short      ushort;


typedef struct
{
     FILE *fptr;                     // Open file pointer
     char logdir[MAX_FILE_NAME];     // Directory to write to
     char tag[MAX_FILE_NAME];        // Name to put in filename
     int  debug_level;               // Debug level
     int  log_type;                  // LOG_DAILY, LOG_HOURLY, LOG_BYSIZE, LOG_CONT
     int  last_day;                  // for keeping track of day  changes
     int  last_hour;                 // for keeping track of hour changes
     int  last_min;                  // for keeping track of min  changes
     int  log_nmin;                  // for logging every n-minutes
     int  log_szlim;                 // kbytes
     int  min_byte;                  // Bytes or minutes logged to current file
} syslog_t;


typedef struct
{ float main_v, main_c;
  unsigned int override;
  unsigned int err_flag1, err_flag2, err_flag3;
  float bt_v[4], bt_c[4], bt_t[4];
  int pv_state[4], wt_state[2], fc_state[2], wt_shunt[2];
  int swgf_state, cvt_state, cvt_intlck;
  float pv_v[4], pv_c[4];
  float wt_v[2], wt_c[2];
  float fc_v[2], fc_c[2];
  float batt_percent_charged;
  float fc_level; // percent
  int go_state;
  float ext_v, ext_c;
  float int_v, int_c, int_t;
  float swgf_pos, swgf_neg;
  float cvt_hv_v, cvt_hv_c, cvt_t;
  float cvt_lv_v, cvt_lv_c;
  int mpea_cvt_state[7], mpea_aux_state, mpea_aux_intlck;
  float mpea_cvt_v[7], mpea_cvt_c[7], mpea_aux_c;
  float mpea_htl_v, mpea_htl_c, mpea_htl_t, mpea_htl_h, mpea_htl_ld, mpea_htl_p;
  char fc_status[256];
} power_sys_t;

typedef struct
{ float fb250_link;
  float fb250_snr;
  float fb250_link_uptime;
  float fb250_link_allowed_time;
  float fb250_link_time_left;
  char fb250_gps[MAX_GPS_STR + 1];
  float irid_link;
  float irid_snr;
  float wifi_link;
  float wifi_snr;
  float airlink_link;
  float airlink_snr;
} telem_sys_t;


typedef struct
{ char device[20];            // fb1, fb2
  char date[20];              // yyyy/mm/dd
  char time[20];              // hh:mm:ss
  char link_state[20];        // UP, DOWN, No_TCP_Connection
  float snr;                  // Signal noise ratio
  float temp;                 // degC
  float gps_lat, gps_lon;     // decimal degrees
  char  gps_date[20];         // yy/mm/dd
  char  gps_time[20];         // hh:mm:ss
  int   gps_csat;             // current satellite
  int   max_duration;         // max session time (minutes)
  int   linkup_attempts;      // number attempts to bring link up
  unsigned long linkup_stime; // linkup start time (unix time)
  unsigned long linkup_etime; // linkup end   time (unix time)
  unsigned long telem_stime;  // telem  start time (unix time)
  unsigned long telem_etime;  // telem  end   time (unix time)
  int   linkup_elapsed;       // number of secs link has been up
  int   total_elapsed;        // number of total secs
  int   remaining_secs;       // remaining secs from max_duration
} fb250_session_t;


typedef struct
{ char  phone_num[20];          // active number
  char  phone1[20];             // primary phone
  char  phone2[20];             // backup phone
  char  phone3[20];             // backup phone
  char  filename[MAX_FILE_NAME];// current file being transferred
  char  mdm_init_str[128];      // modem init string
  long  filesize;               // current file being transferred size
  int   connected;              // state: 0 or 1
  int   signal;                 // signal strength: csq 1-5
  int   attempts;               // connection attempts
  int   dtr;                    // state: 0 or 1
  int   dcd;                    // state: 0 or 1
  int   rts;                    // state: 0 or 1
  int   cts;                    // state: 0 or 1
  int   pkt;                    // current packet number
  int   tpkts;                  // total packets in file
  int   files_sent;             // number of files sent
  int   files_recv;             // number of files received
  long  file_bytes_sent;        // number of bytes sent for current file
  unsigned long tx_bytes;       // total bytes sent
  unsigned long rx_bytes;       // total bytes received
  float rx_xfr_rate;            // Rx file xfer rate (Bytes/Sec)
  float tx_xfr_rate;            // Tx file xfer rate (Bytes/Sec)
  float tot_rx_xfr_rate;        // total Rx file xfer rate (Bytes/Sec)
  float tot_tx_xfr_rate;        // total Tx file xfer rate (Bytes/Sec)
  float total_rxtx_xfr_rate;    // total combined Rx/Tx file xfer rate (Bytes/Sec)
  float dial_time;              // current dial time seconds holder
  float login_time;             // current login time seconds holder
  float tot_login_time;         // total login time seconds holder
  float connect_time;           // current connect time seconds holder
  float file_time;              // current file time seconds holder
  long  max_isu_time;           // max session time in seconds
  long  start_isu_time;         // start session time in seconds
  int   verbose;                // turn on verbose messaging
  int   linkup_afterxfer;       // leave link up and setup mgetty to receive calls
  int   csq_threshold;          // lowest iridium signal strength quality allowed to attempt a call
  int   dial_attempts;          // number of dial attempts
  int   session_attempts;       // number of times transfer can fail before bailing out
  int   establish_conn_timeout; // max time connection process is allowed to take
  int   AT_cmd_attempts;        // max number attempts for AT commands
  int   mode;                   // 0=9522b modem, 1=serial2serial, 2=TCP/IP(not done yet)
  int   packetsize;             // Tx/Rx packet size default 2048
  char  username[32];           // user name for logging into server
  char  deploy_dir[32];         // shore deploy directory name (eg; D0001)
  char  to_buoy_dir_name[64];   // Directory that iridium looks in to find files to bring on to buoy [default=to_buoy]
  char  from_shore_dir_name[64];// Directory that iridium stores files retrieved from shoreside [default=from_shore]
  char  status_str[128];        // last status message holder
} irid_session_t;

#define MAX_IMM_DEVICES 20
#define MAX_IMM_DEVICE_NAME_LEN 32
typedef struct
{ int deviceID;
  char deviceName[MAX_IMM_DEVICE_NAME_LEN];
  char deviceGroup[MAX_IMM_DEVICE_NAME_LEN];
} imm_device;
  
typedef struct
{ long   max_mmp_time;      // seconds
  long   max_mmp_file_time; // seconds
  long   max_mcat_time;     // seconds
  long   max_uimm_time;     // seconds
  int    max_attempts;
  int    mcat_extra_samples;//number of extra samples to get[default=1]
  int    mmp_decimation;
  int    mmp_reqxxx_delay;  // seconds
  int    verbose;
  char   status_str[128];
  imm_device mcat_device_list[MAX_IMM_DEVICES];
  int        mcat_device_count;
  imm_device uimm_device_list[MAX_IMM_DEVICES];
  int        uimm_device_count;
} imm_session_t;


typedef struct
{ int valid_data;
  int log_raw;
  char  gps_date[15];
  char  gps_time[15];
  char  lat_str[20];
  char  lon_str[20];
  struct timeval nmea_time;
  struct timeval nmea_time_rcvd;
  double lat;  // decimal degrees - southern hem, + northern hem
  double lon;  // decimal degrees - western  hem, + eastern  hem
  float spd;   // knots
  float cog;   // degrees 0-360
  int   fix_q; // from GPGGA
  int   nsat;  // from GPGGA
  float hdop;  // from GPGGA
  float alt;   // from GPGGA (meters)
  char  raw_gprmc[132];
  char  raw_gpgga[132];
  char  gps_msgid[10]; // GPRMC or GPGGA
} gps_sys_t;

typedef struct
{  int   port;
   int   state;
   float voltage;
   float current;
   int   eflag;
   int   voltage_sel;
   int   current_limit;
   int   protocol;
   double  last_updated;
} inst_pstatus_t;


typedef struct
{  int mpic_state;
   unsigned int eflag;
   unsigned int stc_eflag2;           // For stc support cpm & dcl eflags
   float main_v;
   float main_c;
   float bbat_v;
   float bbat_c;
   float t1, t2, t3, t4, t5;          // temperature (degC)
   float humid, press;                // humid (%), press (psi)
   float gf1, gf2, gf3, gf4, ld1, ld2;
   int gf_ena;                        // gnd flt enable (bit mask)
   int ld_ena;                        // leak detect enable (2-bit mask)
   int irid, fw_wifi, gps, sbd, imm;  // 0-off, 1-on
   int sbd_ce_msg;                    // 0-no msg, 1-msg pending
   float irid_v, irid_c, fw_wifi_v, fw_wifi_c;
   unsigned int irid_e, fw_wifi_e;
   unsigned int dcl;                  // on/off hex flag for dcl1-7
   unsigned int esw;                  // on/off hex flag for esw1-2
   unsigned int dsl;                  // on/off for dsl (0-off, 1-on)
   int pps;                           // 0-gps, 1-oscillator
   int hbeat_enable;                  // 0-disable, 1-enable
   int hbeat_dtime;                   // time between hbeats (sec)
   int hbeat_threshold;               // max number of missed hbeats
   int power_off_cpm_nsecs;
   int wake_cpm_nsecs;
   int wsrc;                          // flag for what woke cpm
   float wtc;                         // wake timer to next pwr on (min)
   int wpc;                           // wake power on count
   int pwrbd_state;                   // 1-low pwr 12/24, 2-high pwr 12/24, 3-low&high  pwr 12/24
   int pwrbd_mode;                    // 1-legacy gio mode, 2-I2C mode
   int pwrbd_vsel;                    // 1-low pwr, 2-lower power 12/24, 3-highpwr 12/24
   float pwrbd_vmain;                 // volts
   float pwrbd_imain;                 // ma
   float pwrbd_v12;                   // volts
   float pwrbd_i12;                   // ma
   float pwrbd_v24;                   // volts
   float pwrbd_i24;                   // ma
}  mpic_sys_t;


typedef struct
{  int rda_type;    // 0-ci, 1-nsif, 2-mfn
   unsigned int eflag;
   float main_v;    // bus voltage
   float main_c;    // bus current (ma)
   float gf1, gf2, gf3, gf4; //iso3.3neg, iso3.3vpos, vmain_neg, vmain_pos
   int gf_ena;               // gnd flt enable (bit mask)
   int rda_state;   // 0-off 1-on
   float vlt, curr; // rda volts, ma
   int curr_lim;    // ma
}  rda_sys_t;

typedef struct
{ //cpu_stat_t cpu_status;
  power_sys_t power_status;
  telem_sys_t telem_status;
  gps_sys_t gps_status;
  //ntp_sys_t ntp_status;
  mpic_sys_t mpic;
} cpm_status_t;


typedef struct
{ char *cfg_dir;  //as defined by main.c arg
  char *cfg_name; //as defined by main.c arg
  char *syscfg_file;
  char *vehicle_name;
  char *vehicle_cfg;
  char *cruise_name;
  char *cruise_id;
  char *ship_name;
  char *chief_sci;
  char *location;
  double latitude, longitude; //decimal degrees
  int  utm_zone;
  double utm_x;
  double utm_y;
  double mag_variation;       //degrees
  double power_budget;        //watts
  int cmd_tcp_port;
} system_cfg_t;


typedef struct
{
     unsigned long bytes_xmt;     // Number of bytes sent to the instrument
     unsigned long bytes_rcv;     // Number of bytes read from instrument
     unsigned long bytes_log;     // Number of bytes written to log file(s)
     unsigned long good_cnt;      // Number of good records (to best ability of driver)
     unsigned long bad_cnt;       // Number of bad records  (to best ability of driver)
     unsigned long bytes_bad;     // Number of bytes read that seem bad
     unsigned long last_data_t;   // Last good data acquired timestamp (unix secs)
     unsigned long last_comm_t;   // Last time actually got a response from instrument
} inst_status_t;


#endif // CG_SYSTYPES_INC

