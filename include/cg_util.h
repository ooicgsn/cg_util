/* =======================================================
** WHOI OOI/CGSN Common Utility Header file
**
** History:
**     Date      Who    Description
**    -------    ---    ----------------------------------
**   01/01/2010  SL     Create
**   02/17/2010  SL     Added err_abort, err_print from geomag
**                      debug.h
**   04/08/2010  SL/JW  TERM color macros
**   06/17/2010  SL     Added CFG Files
**   07/11/2011  ME     Added instrument specific processing codes
**   08/01/2010  SL/ME  Added public definition, true/false,
**                      success/failure
**   05/24/2012  ME     Added boolian, MAX_FILE_NAME, CHAR. Add
**                      #ifndef NO_TERM_COLOR wrapper (Default is term color!)
**   10/12/2012  ME     Increase max_inst_buffer from 512 -> 1024, added
**                      DEFAULT_INST_VTIME, BAD_BYTE_THRESHOLD,SLASH_CHAR,DOLL_CHAR
**                      Re-defined init values from 0-3 to 1-4
**   05/02/2013  ME     Added QUOTE_CHAR, COLON_CHAR. Moved several items to dlogger.h
**   09/11/2013  ME     Added NULL_STR
**   09/13/2013  JKOB   added #defines for mmp_req_list.txt and sdir_output.txt
**   11/04/2013  ME     Moved return codes NOTHING and UNKNOWN to data_logger.h
**   07/02/2014  ME/SL  Moved MAX_CFG_LINE to serial.h (MAX_CFG_LINE_SIZE)
**   10/02/2014  SL     Moved SDIR to main_telem_sys.c
** =======================================================
*/

#ifndef CG_UTIL_INC
#define CG_UTIL_INC
#ifndef public
#define public
#endif
#ifndef boolian
#define boolian int
#endif

#define MAX_FILE_NAME       128

#define MAX_INST_PORTS        8
#define MAX_TSTRING_SIZE     30
#define MAX_GPS_STR          80

#define NET_CFG_FILE         "Cfg/cg_net.cfg"
#define CPM_SERIAL_CFG_FILE  "Cfg/cpm_serial.cfg"
#define DCL_SERIAL_CFG_FILE  "Cfg/dcl_serial.cfg"
#define CPM_MISSION_CFG_FILE "Cfg/cpm_mission.cfg"
#define DCL_MISSION_CFG_FILE "Cfg/dcl_mission.cfg"
#define UCMD_SHORT_CUT_FILE  "Cfg/short_cuts.cfg"
#define DATA_DIR             "/data/cg_data"
#define CPM_STATUS_FILE      "syslog/cpm_status.txt"
#define DCL_STATUS_FILE      "syslog/dcl_status.txt"
#define CE_STATUS_FILE       "syslog/ce_status.txt"
#define MMP_REQUEST_FILE     "imm/mmp/mmp_req_list.txt"

// Generic return codes
#define SUCCEED          0
#define FAIL             (-1)
#define FALSE            0           // Boolean
#define TRUE             1           // 


// Data Logger Defines

// BYTE Macros
#define CHAR(x)         ((x) & 0x7F)
#define BYTE(x)         ((x) & 0xFF)
#define SHORT(x)        ((x) & 0xFFFF)

// Character define
#define NUL_CHAR            '\0'
#define NEWLN_CHAR          '\n'
#define CR_CHAR             '\r'
#define HASH_CHAR           '#'
#define DOLL_CHAR           '$'
#define COMMA_CHAR          ','
#define DASH_CHAR           '-'
#define PLUS_CHAR           '+'
#define STAR_CHAR           '*'
#define SPACE_CHAR          '\x20'
#define PERIOD_CHAR         '.'
#define SLASH_CHAR          '/'
#define QUOTE_CHAR          '\"'
#define COLON_CHAR          ':'
#define NULL_STR            ""

// Define debug levels
#define ERR  1
#define WNG  2
#define MSG  3
#define DBG  4
#define DBG1 5
#define DBG2 6

// For logging
#define LOG_DAILY             0   // Write to one file for the day  (Name: 20110915.tag.log)
#define LOG_HOURLY            1   // Write to one file for the hour (Name: 20110915_130531.tag.log)
#define LOG_CONT              2   // Write to one file until  done  (Name: 20110915_130531.tag.log)
#define LOG_NMIN              3   // Write to one file until  done  (Name: 20110915_130531.tag.log)
#define LOG_BYSIZE            4   // Write to one file until  done  (Name: 20110915_130531.tag.log)

// For SysLog
#define S_ERR "ERR"
#define S_WNG "WNG"
#define S_MSG "MSG"
#define S_DAT "DAT"
#define S_ALM "ALM"

// For Alarms
#define A_alert    1 // ITU - Critical
#define A_critical 2 // ITU - Major
#define A_error    3 // ITU - Minor
#define A_warning  4 // ITU - Warning
#define A_notice   5 // ITU - Indeterminate or cleared

#define DPRINTF_ENABLE 1
#if DPRINTF_ENABLE
#define DPRINTF(type,level,msg)  \
   {if (type <= level)           \
     {char timestr[MAX_TSTRING_SIZE + 1];\
      get_timestamp(timestr);    \
      printf("%s ",timestr);     \
      {msg;}                     \
      fflush(stdout);            \
      }                          \
     }
#else
#define DPRINTF(type,level,msg)
#endif

//Same as DPRINF w/o timestamp
#define _DPRINTF(type,level,msg) \
   {if (type <= level)           \
     {{msg;}                     \
      fflush(stdout);            \
      }                          \
     }

//
//Define macros for err_abort and err_print
//
#define err_abort(code,text) \
    {fprintf(stderr, "%s at \"%s\":%d:%s (err=%d, %s)\n", \
	        text, __FILE__, __LINE__, __PRETTY_FUNCTION__, code, strerror(code)); \
     abort(); \
     }
	
#define err_print(code,text) \
    {fprintf(stderr, "%s at \"%s\":%d:%s (err=%d, %s)\n", \
	        text, __FILE__, __LINE__, __PRETTY_FUNCTION__, code, strerror(code)); \
     }

//
//Definitions for terminal screen Escape Codes
//
#ifndef NO_TERM_COLOR
//#define TERM_BLACK   "\033[30;47m"  //black text, bg white
#define TERM_BLACK   "\033[37;40m"    //white text, bg black
#define TERM_RED     "\033[31m"
#define TERM_GREEN   "\033[32m"
#define TERM_YELLOW  "\033[33m"
#define TERM_BLUE    "\033[34m"
#define TERM_MAGENTA "\033[35m"
#define TERM_CYAN    "\033[36m"
#define TERM_WHITE   "\033[30;47m"  //black text, bg white
//#define TERM_WHITE   "\033[37;40m"  //white text, bg black 
#define TERMCOLOR(x) printf(x);
#else
#define TERMCOLOR(x)
#endif

#endif //CG_UTIL_INC
