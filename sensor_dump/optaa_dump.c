/* =======================================================
** WHOI OOI/CGSN OPTAA Dump Program
** 
** Description:
**
** 
** Usage: optaa_dump -o offset -a
**
**        
**
** History:
**     Date      Who    Description
**    -------    ---    ----------------------------------
**   11/20/2013  ME     Based on 3dmgx dumpe
**   07/29/2014  ME/SL  Fixed printing out multiple header
**   09/11/2014  SL     Output NumWaves, CrefN, ArefN, CsigN, AsigN
**   12/12/2014  SL/ME  Fixed print_time
** =======================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>

#include "cg_util.h"
#include "cg_systypes.h"

/**
 **
 ** Defines
 **
 **/

#define VERSION           "v1.0-20140729"
#define BUFFER_SIZE       1024
#define MAX_WAVE          100
#define DATESTR           14
#define A_EXT            -7.10233170e-13
#define B_EXT             7.09341920e-8
#define C_EXT            -3.87065673e-3
#define D_EXT             95.82413970
#define A_INT             0.000931350000
#define B_INT             0.000221631000
#define C_INT             0.000000125741

#define PACKETREG         "\xFF\x00\xFF\00"

//define command results structure

typedef struct  // CB Command = AARM - Acceleration, Angular Rate & Magnetometer
{
     ushort cref;
     ushort aref;
     ushort csig;
     ushort asig;
} wavelength_struct;

typedef struct
{
                              // 4 Bytes Packet registration
     ushort length;           // 2 Bytes
     byte  pkttype;           // 1 Byte
                              // 1 Byte reserved             
     byte  metertype;         // 1 Byte
     byte  serNum[3];         // 3 Bytes
     ushort aref;             // 2 Bytes
     ushort prescnts;         // 2 Bytes
     ushort asig;             // 2 Bytes
     ushort ext_temp;         // 2 Bytes
     ushort int_temp;         // 2 Bytes
     ushort cref;             // 2 Bytes
     ushort csig;             // 2 Bytes
     uint   time;             // 4 Bytes
                              // 1 Byte reserved             
     byte  num_waves;         // 1 Byte
     wavelength_struct wave_len[MAX_WAVE];
     ushort checksum;         // 2 Bytes
} data_record;

static int    csv                  = FALSE; // Comma Seperated Values (DEFAULT)
static int    eng_units            = FALSE; // engineering units
static int    debug                = FALSE; // Debug llevel
static int    stop_on_error        = TRUE ; // Stop processing a bad data file
static time_t epoch                = 0;     // Time to use as the starting point


/********************************************************************************************
 ** 
 ** Function: print_usage
 ** 
 **
 *********************************************************************************************/
static void print_usage(char *argv0)
{
     fprintf(stderr, "[%s]: %s  [%s compiled on %s at %s]\n", argv0, VERSION, __FILE__, __DATE__, __TIME__);
     fprintf(stderr, "Usage: %s [-c] [-D] [-e] [-h] [-i] [-t time]\n", argv0);
     fprintf(stderr, "\t-c - put out just data in SSV format\n");
     fprintf(stderr, "\t-D - debug\n");
     fprintf(stderr, "\t-e - put out just engineering units\n");
     fprintf(stderr, "\t-h - Help prints this message (overrides all others)\n");
     fprintf(stderr, "\t-i - Ignore bad records in data file\n");
     fprintf(stderr, "\t-t datetime - Date and time as start reference for data [yyyymmddhhmmss]\n");
}  // End of printf_usage


/*********************************************************************************************
**
**   Function: corrupted_buffer
**
**   
*********************************************************************************************/
static void corrupted_buffer(char *msg, int fatal)
{
     fprintf(stderr, "Buffer No Good - %s\n", msg);
     fflush(stderr);
     fflush(stdout);
     if (fatal)
          exit(EXIT_FAILURE);
}

/*********************************************************************************************
 **
 **   Function: get_int
 **   
 **   Returns four byte value
 **   
 **   
 *********************************************************************************************/
static uint get_int(byte *pb)
{
     uint val;
     
     val  = BYTE(pb[0]) << 24;
     val += BYTE(pb[1]) << 16;
     val += BYTE(pb[2]) << 8;
     val += BYTE(pb[3]);
     return val;
}

/*********************************************************************************************
 **
 **   Function: get_short
 **   
 **   Returns two byte value
 **   
 **   
 *********************************************************************************************/
static ushort get_short(byte *pb)
{
     ushort val;
     
     val  = BYTE(pb[0]) << 8;
     val += BYTE(pb[1]);
     return val;
}

/*********************************************************************************************
**
**   Function: check_checksum
**   
**   Return
**
**   TRUE if match
**   FALSE if not
**
**
**   
*********************************************************************************************/
static int check_checksum(byte *pb, int len)
{
     int                i = 0;
     int    calc_checksum = 0;
     
     for (i = 0; i < len; i++)
          calc_checksum += pb[i];
     calc_checksum &= 0xFFFF;
     
     return (calc_checksum == get_short(pb + len));
}

/*********************************************************************************************
 **
 **   Function: set_start_time
 **
 **   Returns:
 **
 **   TRUE or FALSE
 **   
 **   
 *********************************************************************************************/
static int set_start_time()
{
     int       i               = 0;
     char      t_buf[5]        = {0};
     char timestr[DATESTR + 1] = {0};
     struct tm *ptm            = NULL;

     strncpy(timestr, optarg, DATESTR);
     for (i = 0; i < DATESTR; i++)
          if (!isdigit(timestr[i]))
          {
               fprintf(stderr, "Time must be of the format yyyymmddhhmmss - not [%s]\n", timestr);
               return FALSE;
          }

     if ((epoch = time(NULL)) == FAIL)
     {
          fprintf(stderr, "Function time returned error [errno=%d]\n", errno);
          return FALSE;
     }
     if ((ptm = gmtime(&epoch)) == NULL)
     {
          fprintf(stderr, "Function gmtime returned error [errno=%d]\n", errno);
          return FALSE;
     }

     strncpy(t_buf, timestr, 4);
     t_buf[4] = NUL_CHAR;
     ptm->tm_year = atoi(t_buf) - 1900;
     i = 4;
     
     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_mon = atoi(t_buf) - 1;
     i += 2;
     
     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_mday = atoi(t_buf);
     i += 2;

     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_hour = atoi(t_buf);
     i += 2;

     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_min = atoi(t_buf);
     i += 2;

     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_sec = atoi(t_buf);

     if ((epoch = timegm(ptm)) == FAIL)
     {
          fprintf(stderr, "Could not generate calendar time from [%s], errno [%d]\n", timestr, errno);
          return FALSE;
     }

     return TRUE;
}

/*********************************************************************************************
 **
 **   Function: print_time
 **
 **   
 **   
 **   
 *********************************************************************************************/
static void print_time(int time)
{
     struct tm *pTm      = NULL;
     int        subsec;
     time_t total_secs = epoch + time/1000;

     subsec = time % 1000;
     
     if ((pTm = gmtime(&total_secs)) == NULL)
     {
          printf("TIME ERROR\n");
          return;
     }
     printf(!csv ? " FromPwr=%04d/%02d/%02d %02d:%02d:%02d.%03d " : ", %04d/%02d/%02d %02d:%02d:%02d.%03d, ",
            (int) pTm->tm_year + 1900,
            (int) pTm->tm_mon + 1,
            (int) pTm->tm_mday,
            (int) pTm->tm_hour,
            (int) pTm->tm_min,
            (int) pTm->tm_sec,
            subsec);
}

/*********************************************************************************************
 **
 **   Function: set_struct
 **
 **   
 **   
 **   
 *********************************************************************************************/
static void set_struct(data_record *pd, byte *pb)
{
     int i = 0;
     
     pb += 4; // Skip 4 byte registration
     pd->length    = get_short(pb); pb += 2;
     pd->pkttype   = *pb++;
     pb++; // Skip reserved
     pd->metertype = *pb++;
     pd->serNum[0] = *pb++; pd->serNum[1] = *pb++; pd->serNum[2] = *pb++;
     pd->aref      = get_short(pb); pb += 2;
     pd->prescnts  = get_short(pb); pb += 2;
     pd->asig      = get_short(pb); pb += 2;
     pd->ext_temp  = get_short(pb); pb += 2;
     pd->int_temp  = get_short(pb); pb += 2;
     pd->cref      = get_short(pb); pb += 2;
     pd->csig      = get_short(pb); pb += 2;
     pd->time      = get_int(pb); pb += 4;
     pb++; // Skip reserved
     pd->num_waves = *pb++;
     for (i = 0; i < pd->num_waves; i++)
     {
          pd->wave_len[i].cref = get_short(pb); pb += 2;
          pd->wave_len[i].aref = get_short(pb); pb += 2;
          pd->wave_len[i].csig = get_short(pb); pb += 2;
          pd->wave_len[i].asig = get_short(pb); pb += 2;
     }
     pd->checksum  = get_short(pb); pb += 2;
}


/*********************************************************************************************
 **
 **   Function: process_buffer
 **
 **   
 **   
 **   
 *********************************************************************************************/
static void process_buffer(byte *pb, int len)
{
     data_record  dr  = {0};
     double dt        = 0.0;
     static int first = TRUE;
     int i;
     

     if (!check_checksum(pb, len))
          corrupted_buffer("Bad Checksum", stop_on_error);
     set_struct(&dr, pb);
     if (csv && first)
     {    first = FALSE;
          printf("PType, MType, SerNum, RawExTemp, RawInTemp, Time(ms)");
          if (eng_units)
          {
               if (epoch)
                    printf(", Date Time"); // printf(", FromPwr");
               printf(", ExtTemp(C), IntTemp(C), NumWaves");
          }
	  for (i = 1; i <= dr.num_waves; i++)
             {printf(", Cref%d, Aref%d, Csig%d, Asig%d", i,i,i,i);
	      }

          putchar(NEWLN_CHAR);
     }

     printf(!csv ? "PType=%02x MType=%02x SerNum=%02x%02x%02x ExtTemp=%04x IntTEmp=%04x Time=%08x" :
            "%02x, %02x, %02x%02x%02x, %04x, %04x, %08x",
            dr.pkttype, dr.metertype, dr.serNum[0], dr.serNum[1], dr.serNum[2],
            dr.ext_temp, dr.int_temp, dr.time);

     if (eng_units)
     {
          if (epoch)
               print_time(dr.time);
          else
               printf(", ");
          
          dt = (double) A_EXT * pow((double) dr.ext_temp, 3) +
                ((double) B_EXT * pow((double) dr.ext_temp, 2)) +
                ((double) C_EXT * (double) dr.ext_temp) + (double) D_EXT;
          printf(!csv ? "ExtTemp=%f " : "%f, ", dt);
          dt = 5.0 * (double) dr.int_temp / 65535;
          dt = 10000.0 * dt / (4.516 - dt);
          dt = 1 / (A_INT + B_INT * log(dt) + C_INT * pow(log(dt), 3)) - 273.15;
          printf(!csv ? "IntTemp=%f" : "%f, ", dt);
	  
          printf(!csv ? "NumWaves=%d" : "%d, ", dr.num_waves);
	  for (i = 1; i <= dr.num_waves; i++)
             {if (!csv) printf("Cref%d=%d Aref%d=%d Csig%d=%d Asig%d=%d", 
	                       i,dr.wave_len[i].cref, i,dr.wave_len[i].aref, i,dr.wave_len[i].csig, i,dr.wave_len[i].asig);
             else printf("%d, %d, %d, %d, ", dr.wave_len[i].cref, dr.wave_len[i].aref, dr.wave_len[i].csig, dr.wave_len[i].asig);
             }
          printf(!csv ? "Checksum=%02x" : "%02x", dr.checksum);

	  
	  
     }
    
     putchar(NEWLN_CHAR);
}

/*********************************************************************************************
 **
 **   Function: main
 **
 **   
 **   
 **   
 *********************************************************************************************/
int main(int argc, char *argv[])
{
     int          c                     = 0;
     int          len                   = 0;
     int          bindx                 = 0;
     byte         xbuf[BUFFER_SIZE + 1] = {0};

     
     opterr = 0;
     
     //Get args and check usage
	while((c = getopt(argc, argv, "cDehit:")) != FAIL)
     {
          switch (c)
          {
          case 'c':
               csv = TRUE;
               break;
          case 'D':
               debug = TRUE;
               break;
          case 'e':
               eng_units = TRUE;
               break;
          case 'h':
               print_usage(argv[0]);
               exit(EXIT_FAILURE);
          case 'i':
               stop_on_error = FALSE;
               break;
          case 't':
               if (!set_start_time())
                    exit(EXIT_FAILURE);               
               break;
          case '?':
               fprintf(stderr, "[%s]: Unknown argument [%c]\n", argv[0], optopt);
               exit(EXIT_FAILURE);
          default:
               fprintf(stderr, "[%s]: Unknown return from getopt [%d][%c]\n", argv[0], c, c);
               exit(EXIT_FAILURE);
          }
     }

     while ((c = getchar()) != EOF)
     {
          if (bindx < 4)
          {
               if (c != (bindx % 2 ? 0xFF : 0x00))
                    bindx = 0;
               else
                    xbuf[bindx++] = c;
               continue;
          }
          if (debug)
               fprintf(stderr, "Found four byte packet header flag\n");
          
          if (fread(xbuf + bindx, 1, 2, stdin) != 2)
               corrupted_buffer("Short read of data buffer lenght", TRUE);
          len = get_short(xbuf + bindx);
          bindx += 2;
          if (fread(xbuf + bindx, 1, len - 4, stdin) != len - 4)
               corrupted_buffer("Short read of data buffer", TRUE);
          process_buffer(xbuf, len);
          bindx = 0;
     }
     

          
	exit(EXIT_SUCCESS);
}
