/* =======================================================
** WHOI OOI/CGSN 3DMGX Dump Program
** 
** Description:
**
** 
** Usage: 3dmgx_dump -o offset -a
**
**        
**
** History:
**     Date      Who    Description
**    -------    ---    ----------------------------------
**   03/25/2011  ME     New
**   09/24/2011  ME     Fixed sign extension problem
**   10/11/2011  ME     Added CSV output
**   10/12/2011  ME     Added time option
**   10/20/2011  ME/SL  Solved timezone problem with timegm and changed csv to ssv
**   11/09/2011  ME/SL  Moved cg_util.h to top, removed net_comms.h, cg_systypes.h
** =======================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>

#include "cg_util.h"
#include "cg_systypes.h"

/**
 **
 ** Defines
 **
 **/

#define VERSION               "v2.0-20111011"

#define CB_CMD                0xCB                               // Acceleration, Angular Rate & Magnetometer Command Byte
#define CB_LEN                43                                 // Bytes including header of data unit
#define CF_CMD                0xCF                               // Euler Angles and Angular Rates Command Byte
#define CF_LEN                31                                 // Bytes including header of data unit

#define RAD2DEG               57.295779513
#define TIMERSCALEA           62500.0                            // Convert 3dmgx3 time to seconds
#define TIMERSCALEB           19660800.0                         // Convert 3dmgx2 time to seconds
#define DATESTR               14                                 // yyyymmddhhmmss string size    

//define command results structure


typedef struct  // CB Command = AARM - Acceleration, Angular Rate & Magnetometer
{
     float ax, ay, az;          // Accel x,y,z      - g (1g=9.80665m/sec^2)
     float rx, ry, rz;          // AngRate x,y,z    - rads/sec
     float mx, my, mz;          // Mag x,y,z        - gauss
     uint timer;                // converted to secs - raw/19,660,800 (rolls over 218sec)
} cb_struct;

typedef struct  // CF Command = EAAR - Euler Angles and Angular Rates
{
     float r, p, y;             // Roll, Pitch, Yaw - radians	  
     float rx, ry, rz;          // AngRate x,y,z    - rads/sec
     uint timer;                // converted to secs - raw/19,660,800 (rolls over 218sec)
} cf_struct;
      
static byte x_buf[1025] = { 0 };   // Global variable

/*********************************************************************************************
**
**   Function: swap4
**   
**   Swap 4 bytes
**   
**
**
**   Does not alter x_buf
**   
*********************************************************************************************/
static void swap4(byte *c, byte *b)
{
  c[0] = b[3];
  c[1] = b[2];
  c[2] = b[1];
  c[3] = b[0];
}  // End of swap4

/*********************************************************************************************
**
**   Function: copy_bytes_to_struct
**   
**   Copy buffer to structure swaping byte order
**   
**   
*********************************************************************************************/
static void copy_bytes_to_struct(byte *c, byte *b, int s) 
{
     int i;
     
     for (i = 0; i < s; i += 4)
          swap4(c + i, b + i);
} // End of copy_bytes_to_struct

          
/*********************************************************************************************
**
**   Function: check_checksum
**   
**   Return TRUE if match
**   FALSE if not
**
**   
*********************************************************************************************/
static int check_checksum(int len)
{
     int             i;
     int calc_checksum = 0;
     int      checksum;
     
     for (i = 0; i < len - 2; i++)
          calc_checksum += x_buf[i];

     checksum = x_buf[len - 2] << 8;
     checksum += x_buf[len - 1];
     if (calc_checksum == checksum)
          return TRUE;
     return FALSE;
}

/********************************************************************************************
** 
** Function: print_usage
** 
**
*********************************************************************************************/
static void print_usage(char *argv0)
{
     fprintf(stderr, "[%s]: %s  [%s compiled on %s at %s]\n", argv0, VERSION, __FILE__, __DATE__, __TIME__);
     fprintf(stderr, "Usage: %s [-a] [-c] [-D] [-h] [-o offset]\n", argv0);
     fprintf(stderr, "\t-a - use 3dmgx2 time format (default is 3dmgx3 format)\n");
     fprintf(stderr, "\t-c - put out just data in SSV format\n");
     fprintf(stderr, "\t-h - Help prints this message (overrides all others)\n");
     fprintf(stderr, "\t-o offset - skip offset number of hearder bytes\n");
     fprintf(stderr, "\t-t datetime - Date and time as start reference for data [yyyymmddhhmmss]\n");
}  // End of printf_usage


/*********************************************************************************************
**
**   Function: corrupted_buffer
**
**   
**   
**   
*********************************************************************************************/
static void corrupted_buffer(char *msg)
{
     fprintf(stderr, "Buffer No Good - %s\n", msg);
     exit(EXIT_FAILURE);
}

/*********************************************************************************************
**
**   Function: set_start_time
**
**   Returns:
**
**   TRUE or FALSE
**   
**   
*********************************************************************************************/
static int set_start_time(time_t *pT)
{
     int       i               = 0;
     char      t_buf[5]        = {0};
     char timestr[DATESTR + 1] = {0};
     struct tm *ptm            = NULL;

     strncpy(timestr, optarg, DATESTR);
     for (i = 0; i < DATESTR; i++)
          if (!isdigit(timestr[i]))
          {
               fprintf(stderr, "Time must be of the format yyyymmddhhmmss - not [%s]\n", timestr);
               return FALSE;
          }

     if ((*pT = time(NULL)) == FAIL)
     {
          fprintf(stderr, "Function time returned error [errno=%d]\n", errno);
          return FALSE;
     }
     if ((ptm = gmtime(pT)) == NULL)
     {
          fprintf(stderr, "Function gmtime returned error [errno=%d]\n", errno);
          return FALSE;
     }

     strncpy(t_buf, timestr, 4);
     t_buf[4] = NUL_CHAR;
     ptm->tm_year = atoi(t_buf) - 1900;
     i = 4;
     
     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_mon = atoi(t_buf) - 1;
     i += 2;
     
     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_mday = atoi(t_buf);
     i += 2;

     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_hour = atoi(t_buf);
     i += 2;

     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_min = atoi(t_buf);
     i += 2;

     strncpy(t_buf, timestr + i, 2);
     t_buf[2] = NUL_CHAR;
     ptm->tm_sec = atoi(t_buf);

     if ((*pT = timegm(ptm)) == FAIL)
     {
          fprintf(stderr, "Could not gennerate calendar time from [%s], errno [%d]\n", timestr, errno);
          return FALSE;
     }

     return TRUE;
}

/*********************************************************************************************
**
**   Function: print_time
**
**   
**   
**   
*********************************************************************************************/
static void print_time(float ftime, time_t epoch_start_time)
{
     struct tm *pTm      = NULL;
     int        subsec;
     
     epoch_start_time += (int) ftime;

     ftime -= (int) ftime;
     subsec = ftime * 100;
     
     if ((pTm = gmtime(&epoch_start_time)) == NULL)
     {
          printf("TIME ERROR\n");
          return;
     }
     printf("%04d/%02d/%02d %02d:%02d:%02d.%02d ",
            (int) pTm->tm_year + 1900,
            (int) pTm->tm_mon + 1,
            (int) pTm->tm_mday,
            (int) pTm->tm_hour,
            (int) pTm->tm_min,
            (int) pTm->tm_sec,
            subsec);
}

/*********************************************************************************************
**
**   Function: main
**
**   
**   
**   
*********************************************************************************************/
int main(int argc, char *argv[])
{
	int     c                 = 0;
	int     offset            = 0;
	int     alt_inst          = FALSE;
	int     ssv               = FALSE;  // space separated values, in this context
	int     firstcb           = TRUE;
	int     firstcf           = TRUE;
	time_t  epoch_start_time  = 0;
	
	opterr = 0;
	
	//Get args and check usage
	while((c = getopt(argc, argv, "acho:t:")) != FAIL)
     {
          switch (c)
          {
          case 'a':
               alt_inst = TRUE;
               break;
          case 'c':
               ssv = TRUE;
               break;
          case 'h':
               print_usage(argv[0]);
               exit(EXIT_FAILURE);
          case 'o':
               offset = atoi(optarg);
               break;
          case 't':
               if (!set_start_time(&epoch_start_time))
                    exit(EXIT_FAILURE);               
               break;
          case '?':
               fprintf(stderr, "[%s]: Unknown argument [%c]\n", argv[0], optopt);
               exit(EXIT_FAILURE);
          default:
               fprintf(stderr, "[%s]: Unknown return from getopt [%d][%c]\n", argv[0], c, c);
               exit(EXIT_FAILURE);
          }
     }

     if (offset)
          while ((c = getchar()) != EOF)
          {
               if (--offset == 0)
                    break;
          }

     while ((c = getchar()) != EOF)
     {
          x_buf[0] = c;
          
          switch (c)
          {
          	case CB_CMD:  // Acceleration, Angular Rate & Magnetometer Command Byte
          	{
               cb_struct s;

               if (ssv && firstcb)
               {
                    firstcb = FALSE;
                    firstcf = TRUE;
                    if (epoch_start_time)
                    {
                         printf("date_utc time_utc ax_g ay_g az_g rx_rad/s ry_rad/s rz_rad/s mx_gauss my_gauss ");
                         printf("mz_gauss t_sec\n");
                    }
                    else
                         printf("ax_g ay_g az_g rx_rad/s ry_rad/s rz_rad/s mx_gauss my_gauss mz_gauss t_sec\n");
               }
               
               if ((c = fread(x_buf + 1, 1, sizeof(s) + 2, stdin)) != sizeof(s) + 2)
                    corrupted_buffer("Short Buffer Read");

               if (!check_checksum(sizeof(s) + 3))
                    corrupted_buffer("Bad Checksum");
               copy_bytes_to_struct((byte *) &s, x_buf + 1, sizeof(s));

               if (epoch_start_time)
                    print_time(alt_inst ? (float) (s.timer / TIMERSCALEB) : (float) (s.timer / TIMERSCALEA),
                               epoch_start_time);
               if (ssv)
                    printf("%f %f %f %f %f %f %f %f %f %6.2f\n",
                           s.ax, s.ay, s.az, s.rx, s.ry, s.rz, s.mx, s.my, s.mz, (float) s.timer / TIMERSCALEA);
               else
                    printf("%s CB_AARM ax: %f ay: %f az: %f rx: %f ry: %f rz: %f mx: %f my: %f mz: %f t: %6.2f\n",
                           alt_inst ? "3DM-GX2" : "3DM-GX3",
                           s.ax, s.ay, s.az, s.rx, s.ry, s.rz, s.mx, s.my, s.mz, (float) s.timer / TIMERSCALEA);
          
          	}
          	break;
          	case CF_CMD:  //Euler Angles and Angular Rates Command Byte
          	{
               cf_struct s;

               if (ssv && firstcf)
               {
                    firstcf = FALSE;
                    firstcb = TRUE;
                    if (epoch_start_time)
                         printf("date_utc time_utc roll_deg pitch_deg yaw_deg rx_rad/s ry_rad/sec rz_rad/s t_sec\n");
                    else
                         printf("roll_deg pitch_deg yaw_deg rx_rad/s ry_rad/sec rz_rad/s t_sec\n");
               }
               
               if (fread(x_buf + 1, 1, sizeof(s) + 2, stdin) != sizeof(s) + 2)
                    corrupted_buffer("Short Buffer Read");
               if (!check_checksum(sizeof(s) + 3))
                    corrupted_buffer("Bad Checksum");
               copy_bytes_to_struct((byte *) &s, x_buf + 1, sizeof(s));

               if (epoch_start_time)
                    print_time(alt_inst ? (float) (s.timer / TIMERSCALEB) : (float) (s.timer / TIMERSCALEA),
                               epoch_start_time);
               if (ssv)
                    printf("%f %f %f %f %f %f %6.2f\n",
                           s.r * RAD2DEG, s.p * RAD2DEG, s.y  * RAD2DEG, s.rx, s.ry, s.rz, (float) s.timer / TIMERSCALEA);
               else
                    printf("%s CF_EAAR r: %f p: %f y: %f rx: %f ry: %f rz: %f t: %6.2f\n",
                           alt_inst ? "3DM-GX2" : "3DM-GX3",
                           s.r * RAD2DEG, s.p * RAD2DEG, s.y  * RAD2DEG, s.rx, s.ry, s.rz, (float) s.timer / TIMERSCALEA);

          	}
          	break;
          	default:
               corrupted_buffer("Header Byte Not Found");
          	break;
          }
     }
	exit(EXIT_SUCCESS);
}
