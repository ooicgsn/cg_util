/*
 * *****IMPORTANT*****
 * if not working on server
 * use this compile line: gcc -m32 -lm -Wall wh_adcp_dump.c -o wh_adcp_dump
 * *****IMPORTANT*****
 *
 * File:   wh_adcp_dump.c
 * Author: Jeff O'Brien
 * email jkobrien@whoi.edu
 * 
 * Usage: adcp_dump <output dir> <input file>
 *        Where: outdir is output directory or 'stdout'
 * 
 * History:
 *     Date      Who    Description
 *    -------    ---    ----------------------------------
 * 03/29/2013   JKOB    Create
 * 09/30/2013    SL     Support output dir of 'stdout'
 * 11/07/2013   JKOB    Changed max bins from 40 to 100
 * =======================================================
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <dirent.h>

#define PROFILE_COMPLETE -1L
#define PROFILE_RESTART -2L
#define RESTART_WITH_BACKTRACK -3L
#define PROFILE_NOT_COMPLETED -4L
#define DEPLOYMENT_STOPPED -5L
#define TELEMETRY_RESTART -6L

#define VERSION "v1.0-20131107"

#define SUCCESS         1
#define FAILURE         0
#define YES             1
#define NO              0

#define MAX_BINS       100
#define MAX_BAD_CHARS   40

#pragma pack(push,1)
typedef struct {
    unsigned short id, ensemble_size; //id is always 0x7F6E
    unsigned long ensemble_number;
    unsigned char unit_id, fw_vers, fw_rev;
    unsigned short year;
    unsigned char month, day, hour, minute, second, hsec;
    short heading, pitch, roll, temp; //units of .01 degrees or .01 degrees Celsius
    long pressure; //units of .01 kPa
    unsigned char components; //bits 7654 = subsampling number from 3rd param of PB command PB001,020,1    bits 3210 = flags for PO1111 command
    unsigned char start_bin, num_bins;
} WH_ADCP_Config_t;
#pragma pack(pop)

//function declarations
static void usage_err(char *str);
/*
static short ReverseBytesShort(const short inShort);
static long ReverseBytesLong(const long inLong);
static float ReverseBytesFloat(const float inFloat);
*/
void GetGMTDateTime(char *szDate, char *szTime, time_t *ptt);
int check_dir(char *dir);
int GetBaseName(char *filePath, char *baseName);
int Unpack_WH_ADCP(char *szInputFilename, char *szOutputDir);

int main(int argc, char** argv) {

    int i = 0;
    char szFilename[256];

    //If no args, give usage
    if (argc < 3) {
        usage_err(argv[0]);
        return (EXIT_FAILURE);
    }

    // No need to check/create dir if specified as stdout
    if (strcmp(argv[1],"stdout") != 0) check_dir(argv[1]);

    for (i = 2; i < argc; i++) {
        GetBaseName(argv[i], szFilename);
        Unpack_WH_ADCP(argv[i], argv[1]);
    }

    return (EXIT_SUCCESS);
}

int Unpack_WH_ADCP(char *szInputFilename, char *szOutputDir) {

    char szOutputFilenameText[256];
    char szFilename[256];
    char szTmpFilename[256];
    char szLine[256];
    char *p;

    FILE *pInputFile, *pOutputFileText;

    int c;
    int nResult, i;
    int get_comp1,get_comp2,get_comp3,get_comp4;

    unsigned short in_chksum = 0;
    unsigned short out_chksum = 0;
    unsigned short adcp_id = 0;
    
    short comp1[MAX_BINS];
    short comp2[MAX_BINS];
    short comp3[MAX_BINS];
    short comp4[MAX_BINS];

    unsigned short comp_count = 0;

    WH_ADCP_Config_t wh_adcp_config;
    unsigned char *pConfig;
    unsigned char *pBin;

    pInputFile = fopen(szInputFilename, "rb");
    if (pInputFile == NULL) {
        perror("Error opening input file");
        return FAILURE;
    }

    GetBaseName(szInputFilename, szFilename);

    strcpy(szTmpFilename, szFilename);
    p = strchr(szTmpFilename,'.');
    *p = 0;

    // If Outputdir is specified as stdout, just write to stdout
    if (strcmp(szOutputDir,"stdout") == 0) {pOutputFileText = stdout;}
    else {sprintf(szOutputFilenameText, "%s/%s.txt", szOutputDir, szTmpFilename); //setup output file name
          pOutputFileText = fopen(szOutputFilenameText, "wt");
	  }

    if (pOutputFileText == NULL) {
        perror("Error opening output file");
        return FAILURE;
    }

    printf("#Processing file: %s\n", szFilename);

    while ((c = fgetc(pInputFile)) != EOF) {
        if (c == ']') {
            c = fgetc(pInputFile);
            if (c == ':') {
                i = 0;
                while (i++ < MAX_BAD_CHARS) {
                    nResult = fread(&adcp_id, 1, sizeof (adcp_id), pInputFile);
                    if (adcp_id == 0x7F6E) break;
                    fseek(pInputFile, -1, SEEK_CUR); //go back one byte and try again up to 10x
                }
                if (i >= MAX_BAD_CHARS) {
                    printf("Failed to find begin of record after %d bad chars\n",MAX_BAD_CHARS);
                    continue;
                }

                fseek(pInputFile, -2, SEEK_CUR); //we found begin so go back 2 so they can go into struct
                in_chksum = 0;
                out_chksum = 0;
                nResult = fread(&wh_adcp_config, 1, sizeof (wh_adcp_config), pInputFile);
                
                comp_count = 0;
                get_comp1 = 0;
                get_comp2 = 0;
                get_comp3 = 0;
                get_comp4 = 0;

                if (wh_adcp_config.components & 0x1) {comp_count++; get_comp1=1;}
                if (wh_adcp_config.components & 0x2) {comp_count++; get_comp2=1;}
                if (wh_adcp_config.components & 0x4) {comp_count++; get_comp3=1;}
                if (wh_adcp_config.components & 0x8) {comp_count++; get_comp4=1;}
                
                //printf("debug: comp_count=%hu, num_bins=%hu\n",(unsigned short)comp_count, (unsigned short)wh_adcp_config.num_bins);

                pConfig = (unsigned char*) &wh_adcp_config;
                for (i = 0; i < (sizeof (wh_adcp_config)); i++) out_chksum += *pConfig++;
                
                if (get_comp1) {
                    for (i = 0; i < wh_adcp_config.num_bins; i++) {
                        nResult = fread(&comp1[i], 1, 2, pInputFile);
                        pBin = (unsigned char*) &comp1[i];
                        out_chksum += *pBin++;
                        out_chksum += *pBin++;
                    }
                }
                
                if (get_comp2) {
                    for (i = 0; i < wh_adcp_config.num_bins; i++) {
                        nResult = fread(&comp2[i], 1, 2, pInputFile);
                        pBin = (unsigned char*) &comp2[i];
                        out_chksum += *pBin++;
                        out_chksum += *pBin++;
                    }
                }
                
                if (get_comp3) {
                    for (i = 0; i < wh_adcp_config.num_bins; i++) {
                        nResult = fread(&comp3[i], 1, 2, pInputFile);
                        pBin = (unsigned char*) &comp3[i];
                        out_chksum += *pBin++;
                        out_chksum += *pBin++;
                    }
                }
                
                if (get_comp4) {
                    for (i = 0; i < wh_adcp_config.num_bins; i++) {
                        nResult = fread(&comp4[i], 1, 2, pInputFile);
                        pBin = (unsigned char*) &comp4[i];
                        out_chksum += *pBin++;
                        out_chksum += *pBin++;
                    }
                }

                nResult = fread(&in_chksum, 1, sizeof (in_chksum), pInputFile);
                
                out_chksum = out_chksum % 65535;
                        
                if (in_chksum == out_chksum) {
                    //printf("Success! Checksum matches\n");
                    sprintf(szLine,"#Date_Time,Ensemble,Heading,Pitch,Roll,Temp(C),Pressure(kPa)\n");
                    fprintf(pOutputFileText, "%s", szLine);
                    sprintf(szLine,"%d/%02d/%02d %02d:%02d:%02d.%02d,%ld,%5.2f,%5.2f,%5.2f,%4.2f,%6.2f\n",
                            wh_adcp_config.year, wh_adcp_config.month, wh_adcp_config.day,
                            wh_adcp_config.hour, wh_adcp_config.minute, wh_adcp_config.second, wh_adcp_config.hsec, wh_adcp_config.ensemble_number,
                            ((float)wh_adcp_config.heading / (float)100.0), ((float)wh_adcp_config.pitch / (float)100.0), ((float)wh_adcp_config.roll / (float)100.0),
                            ((float)wh_adcp_config.temp / (float)100.0), (wh_adcp_config.pressure / (float)100.0));
                    fprintf(pOutputFileText, "%s", szLine);
/*
                    sprintf(szLine,"Ensemble %ld\n", wh_adcp_config.ensemble_number);
                    fprintf(pOutputFileText, "%s", szLine);
                    sprintf(szLine,"%d/%02d/%02d %02d:%02d:%02d.%02d\n", wh_adcp_config.year, wh_adcp_config.month, wh_adcp_config.day, wh_adcp_config.hour, wh_adcp_config.minute, wh_adcp_config.second, wh_adcp_config.hsec);
                    fprintf(pOutputFileText, "%s", szLine);
                    sprintf(szLine,"Heading %5.2f, Pitch %5.2f, Roll %5.2f\n", ((float)wh_adcp_config.heading / (float)100.0), ((float)wh_adcp_config.pitch / (float)100.0), ((float)wh_adcp_config.roll / (float)100.0));
                    fprintf(pOutputFileText, "%s", szLine);
                    sprintf(szLine,"Temp %4.2fC, Pressure %6.2f kPa\n", ((float)wh_adcp_config.temp / (float)100.0), (wh_adcp_config.pressure / (float)100.0));
                    fprintf(pOutputFileText, "%s", szLine);
*/
                    sprintf(szLine,"#Bin,East,North,Up,Err\n");
                    fprintf(pOutputFileText, "%s", szLine);
                    for (i = 0; i < wh_adcp_config.num_bins; i++) {
                        sprintf(szLine,"%02d,%hd,%hd,%hd,%hd\n", i + 1, comp1[i], comp2[i], comp3[i], comp4[i]);
                        fprintf(pOutputFileText, "%s", szLine);
                    }
                    sprintf(szLine,"\n");
                    fprintf(pOutputFileText, "%s", szLine);
                } else printf("Failure! Checksum does not match\n");

            }
        }
    }


    fclose(pInputFile);
    fclose(pOutputFileText);

    return SUCCESS;

}

static void usage_err(char *str) {
    printf("\n%s %s\nUsage: %s <output dir> <input file>\n", str, VERSION, str);
    printf("\tWhere:\tmust specify adcp output dir and adcp*.DAT wildcard list for input files\n");
    printf("\t\tNote: outdir can be specified as 'stdout'\n");
}

/*
static short ReverseBytesShort(const short inShort) {
    short retVal;
    char *shortToConvert = (char*) & inShort;
    char *returnShort = (char*) & retVal;

    // swap the bytes into a temporary buffer
    returnShort[0] = shortToConvert[1];
    returnShort[1] = shortToConvert[0];

    return retVal;
}

static long ReverseBytesLong(const long inLong) {
    long retVal;
    char *longToConvert = (char*) & inLong;
    char *returnLong = (char*) & retVal;

    // swap the bytes into a temporary buffer
    returnLong[0] = longToConvert[3];
    returnLong[1] = longToConvert[2];
    returnLong[2] = longToConvert[1];
    returnLong[3] = longToConvert[0];

    return retVal;
}

static float ReverseBytesFloat(const float inFloat) {
    float retVal;
    char *floatToConvert = (char*) & inFloat;
    char *returnFloat = (char*) & retVal;

    // swap the bytes into a temporary buffer
    returnFloat[0] = floatToConvert[3];
    returnFloat[1] = floatToConvert[2];
    returnFloat[2] = floatToConvert[1];
    returnFloat[3] = floatToConvert[0];

    return retVal;
}
*/

/*
static short byteswap_short( const short x){

    short y = ((x & 0xff) << 8) | ((x & 0xff00) >> 8);
    return y;
}

static long byteswap_long( const long x){

    long y = ((x & 0xffff) << 16) | ((x & 0xffff0000) >> 16);
    return y;
}

static float byteswap_float( const float x){

    float y = ((x & 0xffff) << 16) | ((x & 0xffff0000) >> 16);
    return y;
}
*/

void GetGMTDateTime(char *szDate, char *szTime, time_t *ptt) {
    struct tm *tmDT;

    tzset();
    if (ptt)
        tmDT = gmtime(ptt);
    else {
        time_t tt;
        time(&tt);

        tmDT = gmtime(&tt);
    }

    sprintf(szDate, "%02d/%02d/%04d", tmDT->tm_mon + 1, tmDT->tm_mday, tmDT->tm_year + 1900);
    sprintf(szTime, "%02d:%02d:%02d", tmDT->tm_hour, tmDT->tm_min, tmDT->tm_sec);
}

int check_dir(char *dir) {
    DIR *pdir;
    if ((pdir = opendir(dir)) != NULL) {
        closedir(pdir);
        return 1;
    }
    return (mkdir(dir, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH));
}

int GetBaseName(char *filePath, char *baseName) {
    int i;
    char *p;

    i = strlen(filePath);
    p = &filePath[i];
    // extract the bare filename
    if (i == 0) return FAILURE;

    while (*--p != '/') {
        if (--i == 0) break;
    }
    if (i != 0) {
        p++;
        strcpy(baseName, p);
    } else strcpy(baseName, filePath); //this means all we have is a filname that must be in the same directory as the executable

    return SUCCESS;
}

