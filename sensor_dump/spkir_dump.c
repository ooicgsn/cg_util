/* =======================================================
** WHOI OOI/CGSN SPKIR Dump Program
** 
** Description:
**
** 
** Usage: spkir_dump [-c] [-e] [-h] 
**
**        
**
** History:
**     Date      Who     Description
**    -------    ---     ----------------------------------
**   01/25/2013  ME      Based on pco2w dump
**   01/28/2013  ME      Added eng_units (not actual just best effort)
** =======================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>

#include "cg_util.h"
#include "cg_systypes.h"

/**
 **
 ** Defines
 **
 **/

#define VERSION               "v1.0-20130125"
#define DATA_LEN                60              // Time is four (4) bytes long
#define TIME_STAMP_LEN          24              // Lenght of time stamp
#define BUFFER_SIZE            512              // For buffer

static char x_buf[BUFFER_SIZE + 1]     = {0};
static int  csv                        = FALSE; // Comma Seperated Values (DEFAULT)
static int  eng_units                  = FALSE; //
static int  debug                      = FALSE; //
static int  dump_all                   = FALSE; //




/********************************************************************************************
** 
** Function: print_usage
** 
**
*********************************************************************************************/
static void print_usage(char *argv0)
{
     fprintf(stderr, "[%s]: %s  [%s compiled on %s at %s]\n", argv0, VERSION, __FILE__, __DATE__, __TIME__);
     fprintf(stderr, "Usage: %s [-c] [-e] [-D] [-a] [-h] < stdin > stdout\n", argv0);
     fprintf(stderr, "\t-c - Put out data in CSV format\n");
     fprintf(stderr, "\t-D - Put out debug information\n");
     fprintf(stderr, "\t-e - Put out engineering units\n");
     fprintf(stderr, "\t-a - Put out all records in file\n");
     fprintf(stderr, "\t-h - Help prints this message (overrides all others)\n");
}  // End of printf_usage


/*********************************************************************************************
**
**   Function: out_channel
**
**   
**   
**   
*********************************************************************************************/
static void out_7channel(byte *p)
{
     int              i;
     unsigned int    ui;

          
     for (i = 0; i < 7; i++)
     {
          ui  =  *p << 24;
          ui += *(p + 1) << 16;
          ui += *(p + 2) << 8;
          ui += *(p + 3);
          if (csv)
               eng_units ? printf("%f,", 1.363 * 2.05985193860 * 0.00000001 * (ui - 2147419836.7)) :
                    printf("%u,", (int) ui);     // Channel 1
          else
               eng_units ? printf("Channel %d: %f, ", i + 1,
                                  1.363 * 2.05985193860 * 0.00000001 * (ui - 2147419836.7)) :
                    printf("Channel %d: %u, ", i + 1, ui);     // Channel 1
          p += 4;
     }
}

/*********************************************************************************************
**
**   Function: out_short
**
**   
**   
**   
*********************************************************************************************/
static short out_short(byte *p)
{
     short val = 0;

     val = *p << 8;
     val += *(p + 1);
     return val;
}


/*********************************************************************************************
**
**   Function: out_ushort
**
**   
**   
**   
*********************************************************************************************/
static unsigned short out_ushort(byte *p)
{
     unsigned short val = 0;

     val = *p << 8;
     val += *(p + 1);
     return val;
}

     
/*********************************************************************************************
**
**   Function: data_proc
**
**   
**   
**   
*********************************************************************************************/
static void proc_data(byte *p)
{
     char           b[BUFFER_SIZE + 1];
     unsigned short us;
     short           s;
     
     
     strncpy(b, (char *) p, 6);                                  // Instrument
     b[6] = NUL_CHAR;
     printf(csv ? "%s," : "Instrument: %s ", b);
     p += 6;
     strncpy(b, (char *) p, 4);                                  // Serial Number
     b[4] = NUL_CHAR;
     printf(csv ? "%s," : "Serial Number: %s ", b);
     p += 4;
     strncpy(b, (char *) p, 10);                                 // Timer
     b[10] = NUL_CHAR;
     printf(csv ? "%s," : "Timer: %s ", b);
     p += 10;
     s = out_short(p);
     printf(csv ? "%d," : "Sample Delay: %d ", s);               // Sample Delay
     p += 2;
     out_7channel(p);                                            // Output the 7 channels
     p += 28;
     us = out_ushort(p);                                         // Vin Sense
     eng_units ? printf(csv ? "%2.2f," : "Vin Sense: %2.2f ", us * 0.03 ): printf(csv ? "%u," : "Vin Sense: %u ", us);
     p += 2;
     us = out_ushort(p);                                         // Va Sense
     eng_units ? printf(csv ? "%2.2f," : "Va Sense: %2.2f ", us * 0.03) : printf(csv ? "%u," : "Va Sense: %u ", us);
     p += 2;
     us = out_ushort(p);                                         // Int. Temp
     eng_units ? printf(csv ? "%2.2f," : "Int Temp: %2.2f ", (us * 0.5) - 50) : printf(csv ? "%u," : "Int Temp: %u ", us);
     p += 2;
     printf(csv ? "%u\n" : "Frame Counter: %u\n", *p);           // Frame Counter
}


/*********************************************************************************************
**
**   Function: main
**
**   
**   
**   
*********************************************************************************************/
int main(int argc, char *argv[])
{
     int     len               = 0;
     int       i               = 0;
     int       c               = 0;
     int   first               = TRUE;
     
     
     opterr = 0;
     //Get args and check usage
	while((c = getopt(argc, argv, "acDeh")) != FAIL)
     {
          switch (c)
          {
          case 'a':
               dump_all = TRUE;
               break;
          case 'c':
               csv = TRUE;
               break;
          case 'D':
               debug = TRUE;
               break;
          case 'e':
               eng_units = TRUE;
               break;
          case 'h':
               print_usage(argv[0]);
               exit(EXIT_FAILURE);
          case '?':
               fprintf(stderr, "[%s]: Unknown argument [%c]\n", argv[0], optopt);
               exit(EXIT_FAILURE);
          default:
               fprintf(stderr, "[%s]: Unknown return from getopt [%d][%c]\n", argv[0], c, c);
               exit(EXIT_FAILURE);
          }
     }

     for (;;)
     {
          i = 0;
          
          while (((c = getchar()) != EOF) && (c != '[') && (c != 'S'))
          {
               x_buf[i++] = c;
               continue;
          }
          x_buf[i - 1] = NUL_CHAR;                     // Null out the space after timestamp
          if ((i != 0) && (i != TIME_STAMP_LEN))
              fprintf(stderr, "Bad read of data, read %d expected %d\n", i, TIME_STAMP_LEN);
               
          switch (c)
          {
          case '[':
               if (i && dump_all)
                    printf("%s ", x_buf);
               x_buf[0] = c;
               fgets(x_buf + 1, BUFFER_SIZE, stdin);
               if (dump_all)
                    printf("%s", x_buf);
               break;
          case 'S':
               if (first && csv)
               {
                    first = FALSE;
                    !eng_units ? printf("Date Time,Inst,SN,Timer,Delay,C1,C2,C3,C4,C5,C6,C7,Vin,Va,Temp,Frame\n") : 
                         printf("Date Time,Inst,SN,Timer(s),Delay(ms),C1(uW/cm^2/nm),C2(uW/cm^2/nm),C3(uW/cm^2/nm),C4(uW/cm^2/nm),C5(uW/cm^2/nm),C6(uW/cm^2/nm),C7(uW/cm^2/nm),Vin(V),Va(V),Temp(C),Frame\n");
                    
                         
               }
               if (i)
                    printf(csv ? "%s," : "%s ", x_buf);
               x_buf[0] = c;
               if ((len = fread(x_buf + 1, 1, DATA_LEN - 1, stdin)) != DATA_LEN - 1)
               {
                    fprintf(stderr, "Short read of data, read %d expected %d\n", len, DATA_LEN - 1);
                    exit(EXIT_FAILURE);
               }
               x_buf[DATA_LEN] = NUL_CHAR;

               if (debug)
               {
                    fprintf(stderr, "Processing data buffer\n");
                    for (len = 0; len < DATA_LEN + i - 3; len++)
                         fprintf(stderr, "%02X ", (byte) x_buf[len]);
                    fprintf(stderr, "\n");
               }
               
               proc_data((byte *) x_buf);
               break;
          case EOF:
               exit(EXIT_SUCCESS);
          default:
               fprintf(stderr, "Line starts with unexpected character [%c]\n", CHAR(c));
               exit(EXIT_FAILURE);
          }
     }
     exit(EXIT_SUCCESS);
}
