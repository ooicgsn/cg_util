/* 
 * File:   mcat_dump.c
 * Author: Jeff O'Brien
 * email: jkobrien@whoi.edu
 *
 * Usage: mcat_dump <output dir> <input file>
 *        Where: outdir is output directory or 'stdout'
 *
 * Created on July 28, 2011, 11:36 AM
 *
 * History:
 *     Date      Who    Description
 *    -------    ---    ----------------------------------
 * 07/28/2011   JKOB    Create
 * 09/30/2011   JKOB    Fixed GMT timestamp issue
 * 09/30/2013    SL     Support output dir of 'stdout'
 * 06/24/2014    SL     Eliminated // from Status|Begin Data|End Data
 * =======================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <dirent.h>
//#include <getopt.h>

#define VERSION "v1.0-20130930"

#define SUCCESS                         1
#define FAILURE                         0
#define YES                             1
#define NO                              0

void GetGMTDateTime(char *szDate, char *szTime, time_t *ptt);
long GetSecondsSince2K();
int ReadMCATFile(char *szInputFilename, char *szOutputDir);
int check_dir(char *dir);
int GetBaseName(char *filePath, char *baseName);

static void usage_err(char *str) {
    printf("\n%s %s\nUsage: %s <output dir> <input file>\n", str, VERSION, str);
    printf("\t\tNote: outdir can be specified as 'stdout'\n");
}

int main(int argc, char** argv) {

    int i = 0;

    //If no args, give usage
    if (argc < 3) {
        usage_err(argv[0]);
        return (EXIT_FAILURE);
    }

    // No need to check/create dir if specified as stdout
    if (strcmp(argv[1],"stdout") != 0) check_dir(argv[1]);

    for(i=2;i<argc;i++){
        ReadMCATFile(argv[i], argv[1]);
    }

    return (EXIT_SUCCESS);
}

/*
temperature (°C, ITS-90) = (temperature number / 10000) – 10
conductivity (S/m) = (conductivity number / 100000) – 0.5
pressure (db) = [pressure number * pressure range / (0.85 * 65536)] – (0.05 * pressure range)
where
Pressure range is in decibars. The file header shows the pressure range in psia in the GetCC or DC (calibration coefficients) command response. Convert the pressure range from psia to decibars using the following equation:
Pressure (db) = 0.6894757 * [Pressure (psia) – 14.7]
Date and time = date and time number (seconds since January 1, 2000).
 */
int ReadMCATFile(char *szInputFilename, char *szOutputDir) {

    int nResult = 0;
    int nLen = 0;
    int nPressureRange = 0;
    int bWriteFile = YES;

    char szLine[100];
    char strTemp[10];
    char strCond[10];
    char strPressure[10];
    char strInvertPressure[10];
    char strTS[10];
    char strInvertTS[12];
    char tmpStr[32];
    char szDate[16];
    char szTime[16];
    char szOutputFilename[256];
    char szFilename[256];

    long lTemp = 0;
    long lCond = 0;
    long lPressure = 0;
    long lTS = 0;
    //static const long l2KSecs = 946699200L;
    static const long l2KSecs = 946684800;
    float fTemp = 0.0;
    float fCond = 0.0;
    float fPressure = 0.0;
    float fPressRange = 0.0;

    char *p;

    GetBaseName(szInputFilename, szFilename);
    sprintf(szOutputFilename, "%s/%s.txt", szOutputDir, szFilename);

    FILE *pInputFile, *pOutputFile;

    pInputFile = fopen(szInputFilename, "r");
    if (pInputFile == NULL) {
        perror("Error opening input file");
        return FAILURE;
    }

    // need to peek at file to see if valid before opening output file
    if (fgets(szLine, 100, pInputFile)) {
        if (strstr((char *) szLine, "Status")) {
            
            if (bWriteFile) {
                if (strcmp(szOutputDir,"stdout") == 0) 
		     {pOutputFile = stdout;}
                else {pOutputFile = fopen(szOutputFilename, "wt");}
                if (pOutputFile == NULL) {
                    perror("Error opening output file");
                    return FAILURE;
                }
                if (bWriteFile) fprintf(pOutputFile, "#%s", szLine);
            } else printf("#%s", szLine);
        } else {
            fclose(pInputFile);
            printf("Error: Invalid file\n");
            return FAILURE;
        }
    }

    

    while (fgets(szLine, 100, pInputFile)) {
        
        if (bWriteFile) fprintf(pOutputFile, "#%s", szLine);
        else printf("#%s", szLine);

        strcpy(tmpStr, "PressureRange =");
        nLen = strlen(tmpStr);

        if ((p = strstr((char *) szLine, tmpStr))) {
            //fPressRange = (float)(atoi(p + nLen));
            nPressureRange = atoi(p + nLen); //this is the pressure range in psia
            fPressRange = 0.6894757 * ((float) nPressureRange - 14.7); //this is it converted to decibars
        }

        if (strstr(szLine, "Begin Data")) {
            if (bWriteFile) fprintf(pOutputFile, "#yyyy/mm/dd hh:mm:ss, Temp(C), Cd(S/m), P(db)\n");
            else printf("#yyyy/mm/dd hh:mm:ss, Temp(C), Cd(S/m), P(db)\n");
            break;
        }
    }

    while (fgets(szLine, 100, pInputFile)) {

        if (strstr(szLine, "End Data")) {
            if (bWriteFile) fprintf(pOutputFile, "#%s", szLine);
            else printf("#%s\n", szLine);
            break;
        } else {

            //result = sscanf(mystring, "%s",buf);
            nLen = strlen(szLine);
            if (nLen == 24) { //with pressure
                nResult = sscanf(szLine, "%5s%5s%4s%8s", strTemp, strCond, strPressure, strTS);
                if (nResult == 4) {
                    sprintf(tmpStr, "0x%s", strTemp);
                    strcpy(strTemp, tmpStr);
                    lTemp = strtol(strTemp, NULL, 0);
                    fTemp = ((float) lTemp / (float) 10000.0) - (float) 10.0;

                    sprintf(tmpStr, "0x%s", strCond);
                    strcpy(strCond, tmpStr);
                    lCond = strtol(strCond, NULL, 0);
                    fCond = ((float) lCond / (float) 100000.0) - (float) 0.5;

                    strInvertPressure[0] = '0';
                    strInvertPressure[1] = 'x';
                    strInvertPressure[2] = strPressure[2];
                    strInvertPressure[3] = strPressure[3];
                    strInvertPressure[4] = strPressure[0];
                    strInvertPressure[5] = strPressure[1];
                    strInvertPressure[6] = 0;
                    lPressure = strtol(strInvertPressure, NULL, 0);
                    fPressure = (((float) lPressure * fPressRange) / (0.85 * (float) 65536.0)) - (0.05 * fPressRange);

                    strInvertTS[0] = '0';
                    strInvertTS[1] = 'x';
                    strInvertTS[2] = strTS[6];
                    strInvertTS[3] = strTS[7];
                    strInvertTS[4] = strTS[4];
                    strInvertTS[5] = strTS[5];
                    strInvertTS[6] = strTS[2];
                    strInvertTS[7] = strTS[3];
                    strInvertTS[8] = strTS[0];
                    strInvertTS[9] = strTS[1];
                    strInvertTS[10] = 0;
                    lTS = strtol(strInvertTS, NULL, 0);
                    //l2KSecs = GetSecondsSince2K();
                    lTS = l2KSecs + lTS;
                    GetGMTDateTime(szDate, szTime, &lTS);
                    
                    if (bWriteFile) fprintf(pOutputFile, "%s %s, %0.4f, %0.5f, % 0.3f\n", szDate, szTime, fTemp, fCond, fPressure);
                    else printf("%s %s, %0.4f, %0.5f, % 0.3f\n", szDate, szTime, fTemp, fCond, fPressure);

                }

            } else if (nLen == 20) { //no pressure
                nResult = sscanf(szLine, "%5s%5s%8s", strTemp, strCond, strTS);
                if (nResult == 3) {
                    sprintf(tmpStr, "0x%s", strTemp);
                    strcpy(strTemp, tmpStr);
                    lTemp = strtol(strTemp, NULL, 0);
                    fTemp = ((float) lTemp / (float) 10000) - (float) 10;

                    sprintf(tmpStr, "0x%s", strCond);
                    strcpy(strCond, tmpStr);
                    lCond = strtol(strCond, NULL, 0);
                    fCond = ((float) lCond / (float) 100000) - (float) 0.5;

                    strInvertTS[0] = '0';
                    strInvertTS[1] = 'x';
                    strInvertTS[2] = strTS[6];
                    strInvertTS[3] = strTS[7];
                    strInvertTS[4] = strTS[4];
                    strInvertTS[5] = strTS[5];
                    strInvertTS[6] = strTS[2];
                    strInvertTS[7] = strTS[3];
                    strInvertTS[8] = strTS[0];
                    strInvertTS[9] = strTS[1];
                    strInvertTS[10] = 0;
                    lTS = strtol(strInvertTS, NULL, 0);
                    //l2KSecs = GetSecondsSince2K();
                    lTS = l2KSecs + lTS;
                    GetGMTDateTime(szDate, szTime, &lTS);

                    
                    if (bWriteFile) fprintf(pOutputFile, "%s %s, %0.4f, %0.5f\n", szDate, szTime, fTemp, fCond);
                    else printf("%s %s, %0.4f, %0.5f\n", szDate, szTime, fTemp, fCond);

                }
            } else printf("Error: Invalid line length");


        }
    }
    fclose(pInputFile);
    if (bWriteFile) {
        fclose(pOutputFile);
        printf("Converted uCat file written to %s\n", szOutputFilename);
    }
    return SUCCESS;
}

void GetGMTDateTime(char *szDate, char *szTime, time_t *ptt) {
    struct tm *tmDT;

    tzset();
    if (ptt)
        tmDT = gmtime(ptt);
    else {
        time_t tt;
        time(&tt);

        tmDT = gmtime(&tt);
    }

    sprintf(szDate, "%04d/%02d/%02d", tmDT->tm_year + 1900, tmDT->tm_mon + 1, tmDT->tm_mday);
    sprintf(szTime, "%02d:%02d:%02d", tmDT->tm_hour, tmDT->tm_min, tmDT->tm_sec);
}

long GetSecondsSince2K() {

    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = gmtime(&rawtime);

    timeinfo->tm_mon = 1 - 1;
    timeinfo->tm_mday = 1;
    timeinfo->tm_year = 2000 - 1900;
    timeinfo->tm_hour = 0;
    timeinfo->tm_min = 0;
    timeinfo->tm_sec = 0;

    return mktime(timeinfo);

}

int check_dir(char *dir) {
    DIR *pdir;
    if ((pdir = opendir(dir)) != NULL) {
        closedir(pdir);
        return 1;
    }
    return (mkdir(dir, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH));
}

int GetBaseName(char *filePath, char *baseName) {
    int i;
    char *p;

    i = strlen(filePath);
    p = &filePath[i];
    // extract the bare filename
    if (i == 0) return FAILURE;

    while (*--p != '/') {
        if (--i == 0) break;
    }
    if (i != 0) {
        p++;
        strcpy(baseName, p);
    } else strcpy(baseName, filePath);//this means all we have is a filname that must be in the same directory as the executable

    return SUCCESS;
}

