/* =======================================================
** WHOI OOI/CGSN Aquadopp and vel3d Dump Program
** 
** Description: Reads binary Vector file from stdin and prints
**              ascii output to stdout
**
** Usage: nortek_dump < stdin > stdout
**         (see usage below)
**
** History:
**     Date      Who    Description
**    -------    ---    ----------------------------------
**   10/31/2012  ME     New based on aqua_dump
**   03/14/2013  ME     Added Head and Instrument configuration
**   03/15/2013  ME     Merged Aqua and Vel3d dump programs
**   03/28/2013  ME     Added using Vector System Data for time
**   03/28/2013  ME     Added Vector System Timing and just VVD with System Data Engineering
**   03/29/2013  ME     Added Vector Velocity Data and Header
**   
** =======================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <ctype.h>
#include <string.h>

#include "cg_util.h"
#include "cg_systypes.h"

/**
 **
 ** Defines
 **
 **/

#define VERSION               "v2.0-20111020"

#define CHECKSUM_BASE           0xB58C               // Bases Added to checksum

#define NORTEK_DATA_HDR         0xA5                 // Header byte of Data

#define NORTEK_USER_CONF_ID     0x00                 // User configuration data
#define NORTEK_USER_CONF_LEN    512                  // User configuration length

#define AQU_VEL_DATA_ID         0x01                 // Aquadopp Velocity Data Header
#define AQU_VEL_DATA_LEN        42                   // Bytes including header of data unit

#define NORTEK_HEAD_CONF_ID     0x04                 // Head Configuration
#define NORTEK_HEAD_CONF_LEN    224                  // Head Configuration length

#define NORTEK_HRDWR_CONF_ID    0x05                 // Hardware Configuration
#define NORTEK_HRDWR_CONF_LEN   48                   // Hardware Configuration length

#define AQU_DIG_DATA_HDR_ID     0x06                 // Aquadopp Diagnostics Data Header
#define AQU_DIG_DATA_HDR_LEN    36                   // Bytes including header of data unit

#define VEC_PROB_CHK_DATA_ID    0x07                 // Vector Velocity Data
#define VEC_PROB_CHK_DATA_LEN   910                  // Bytes including header of data unit

#define VEC_VEL_DATA_ID         0x10                 // Vector Velocity Data
#define VEC_VEL_DATA_LEN        24                   // Bytes including header of data unit

#define VEC_SYS_DATA_ID         0x11                 // Vector Velocity Data
#define VEC_SYS_DATA_LEN        28                   // Bytes including header of data unit

#define VEC_VEL_DATA_HDR_ID     0x12                 // Vector Velocity Data Header
#define VEC_VEL_DATA_HDR_LEN    42                   // Bytes including header of data unit

#define AQU_DIG_DATA_ID         0x80                 // Aquadopp Velocity Data Header
#define AQU_DIG_DATA_LEN        42                   // Bytes including header of data unit

#define NO_FL                   0.0                  // Used when there is no float conversion needed
#define NO_PR                  -1.0                  // Used to indicate do not print number
#define FIRST                   1                    // First value of array
#define BUF_SIZE                1025                 // Size of three buffers
#define TIME_SIZE               100                  // Size of time buffer

// Program varaiables
static byte x_buf[BUF_SIZE]             = { 0 };     // Hold the buffer being read
static byte s_buf[BUF_SIZE]             = { 0 };     // Save the buffer
static byte vsd_buf[BUF_SIZE]           = { 0 };     // Last Vector System Buffer read
static char vhs_time[TIME_SIZE]         = { 0 };      // Holds last Vecotor Velocity Header or System time string
static char t_time[TIME_SIZE]           = { 0 };     // Holds last time string
static int  debug                       = FALSE;     // Debug
static int  csv                         = FALSE;     // Output CSV format
static int  eng_units                   = FALSE;     // Output converted to engineering units
static int  ignore                      = FALSE;     // ignore errors
static int  hexopt                      = FALSE;     // Output CSV in hex
static uint last_id                     = 0xFF;      // Last data type printed (set to unknown id)
static int  vsd_time                    = FALSE;     // Vector System Data Time
static int  vvdh_recs                   = 0;         // Number of records in VVDH
static int  vvdh_numrec                 = FAIL;      // number of vector records processed
static int  add_recs                    = 0;         // Number of records in ADD
static int  add_numrec                  = 0;         // Number of records in ADD processed
static int  offset                      = 0;         // offset or skipped characters in buffer
static int  byte_cnt                    = 0;         // Bytes in file read


// Different types of Nortek records
static int  usr_conf                    = FALSE;     // Configuration data
static int  aqu_vel                     = FALSE;     // Aquadopp velocity data
static int  head_conf                   = FALSE;     // Head configuration
static int  hrdwr_conf                  = FALSE;     // Hardware configuration
static int  aqu_diag                    = FALSE;     // Diagnostics
static int  vec_prob_chk                = FALSE;     // Probe check data
static int  vec_vel                     = FALSE;     // Full Vector Data
static int  vec_sys                     = FALSE;     // Vector System Data
static int  vec_vel_hdr                 = FALSE;     // Vector Velocity Data Header
static int  vvd_vvh                     = FALSE;     // Just the vector velocity data with eng_units
static int  vvd_vsd                     = FALSE;     // Just the vector velocity data with eng_units from sys data




/*********************************************************************************************
 * Clock data (6 bytes) NOTE! BCD format                                                     *
 *********************************************************************************************
     byte       cMinute;          // minute
     byte       cSecond;          // second
     byte       cDay;             // day
     byte       cHour;            // hour
     byte       cYear;            // year
     byte       cMonth;           // month

 *********************************************************************************************
 * Hardware Configuration                                                                    *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification = 0x05
     ushort     hSize;            // size of structure (words)
     byte       SerialNo[14]      // instrument type and serial number
     ushort     Config            // board configuration
                           // bit 0: Recorder installed (0=no, 1=yes)
                           // bit 1: Compass installed (0=no, 1=yes)
     ushort     hFrequency        // board frequency [kHz]
     ushort     hPICversion       // PIC code version number
     ushort     hHWrevision       // Hardware revision
     ushort     hRecSize          // Recorder size (*65536 bytes)
     ushort     hStatus           // status: bit 0: Velocity range
                                     (0=normal, 1=high)
     byte       cspare[12]        // spare
     word       FWversion         // firmware version
     ushort     hChecksum;        // checksum

 *********************************************************************************************
 * Head Configuration                                                                        *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification = 0x04
     ushort     hSize;            // size of structure (words)
     ushort     Config            // head configuration:
                           // bit 0: Pressure sensor (0=no, 1=yes)
                           // bit 1: Magnetometer sensor (0=no, 1=yes)
                           // bit 2: Tilt sensor (0=no, 1=yes)
                           // bit 3: Tilt sensor mounting (0=up, 1=down)
     ushort     hFrequency        // head frequency (kHz)
     ushort     hType             // head type
     byte       cSerialNo[12]     // head serial number
     byte       cSystem[176]      // system data
     byte       cSpare[22]        // spare
     ushort     hNBeams           // number of beams
     ushort     hChecksum;        // checksum
     
 *********************************************************************************************
 * User Configuration                                                                        *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification (0x01=normal, 0x80=diag)
     ushort     hSize;            // size of structure (words)
     ushort     hTranPulLen;      // transmit pulse length (counts)
     ushort     hBlankDist;       // blanking distance (counts)
     ushort     hRecLen;          // receive length (counts)
     ushort     hTimeBP;          // time between pings (counts)
     ushort     hTimeBB;          // time between burst sequences (counts)
     ushort     hNPings;          // number of beam sequences per burst
     ushort     hAvgInterval;     // average interval in seconds
     ushort     hNBeams;          // number of beam
     ushort     hTimCtrlReg;      // timing controller mode
                           // bit 1: profile (0=single, 1=continuous)
                           // bit 2: mode (0=burst, 1=continuous)
                           // bit 5: power level (0=1, 1=2, 0=3, 1=4)
                           // bit 6: power level (0 0 1 1 )
                           // bit 7: synchout position (0=middle of sample, 1=end of sample (Vector))
                           // bit 8: sample on synch (0=disabled,1=enabled, rising edge)
                           // bit 9: start on synch (0=disabled,1=enabled,rising edge)
     ushort     hPwrCtrlReg;      // power control register
                           // bit 5: power level (0=1, 1=2, 0=3, 1=4)
                           // bit 6: power level (0 0 1 1 )
     ushort     hA1;              // not used
     ushort     hB0;              // not used
     ushort     hB1;              // not used
     ushort     hCompUpdRt;       // compass update rate
     ushort     hCoordSystem;     // coordinate system (0=ENU, 1=XYZ, 2=BEAM)
     ushort     hNBins;           // number of cells
     ushort     hBinLength;       // cell size
     ushort     hMeasInterval;    // measurement interval
     byte       cDepName[6];      // recorder deployment name
     ushort     hWrapMode;        // recorder wrap mode (0=NO WRAP, 1=WRAP WHEN FULL)
     byte       Depclock[6];      // deployment start time
     ushort     hDiagInterval[2]; // number of seconds between diagnostics
     ushort     hMode;            // mode:
                           // bit 0: use user specified sound speed (0=no,1=yes)
                           // bit 1: diagnostics/wave mode 0=disable,1=enable)
                           // bit 2: analog output mode (0=disable,1=enable)
                           // bit 3: output format (0=Vector, 1=ADV)
                           // bit 4: scaling (0=1 mm, 1=0.1 mm)
                           // bit 5: serial output (0=disable, 1=enable)
                           // bit 6: reserved EasyQ
                           // bit 7: stage (0=disable, 1=enable)
                           // bit 8: output power for analog input (0=disable, 1=enable)
     ushort     hAdjSoundSpeed;   // user input sound speed adjustment factor
     ushort     hNSampDiag;       // samples (AI if EasyQ) in diagnostics mode
     ushort     hNBeamsCellDiag;  // beams / cell number to measure in diagnostics mode
     ushort     hNPingsDiag;      // pings in diagnostics/wave mode
     ushort     hModeTest;        // mode test:
                           // bit 0: correct using DSP filter (0=no filter,1=filter)
                           // bit 1: filter data output (0=total corrected velocity,1=only correction part)
     ushort     hAnaInAddr;       // analog input address
     ushort     hSWVersion;       // software version
     ushort     hSpare;           // Spare
     byte       cVelAdjTble[180]; // velocity adjustment table
     byte       cComments[180];   // file comments
     ushort     hWaveMode;        // wave measurement mode
                           // bit 0: data rate (0=1 Hz, 1=2 Hz)
                           // bit 1: wave cell position (0=fixed,1=dynamic)
                           // bit 2: type of dynamic position (0=pct of mean pressure, 1=pct of min re)
     ushort     hDynPercPos;      // percentage for wave cell positioning(=32767x#%/100) (# means number of)
     ushort     hT1;              // wave transmit pulse
     ushort     hT2;              // fixed wave blanking distance (counts)
     ushort     hT3;              // wave measurement cell size
     ushort     hNSamp;           // number of diagnostics/wave samples
     ushort     hA1b;             // not used
     ushort     hB0b;             // not used
     ushort     hB1b;             // not used
     ushort     hSpareB;          // Spare
     ushort     hAnaOutScale;     // analog output scale factor (16384=1.0, max = 4.0)
     ushort     hCorrThresh;      // correlation threshold for resolving
     ushort     hSpareC;          // Spare
     ushort     hTiLag2;          // transmit pulse length (counts) second lag     
     byte       cSpare[30];       // Spare
     byte       cQualConst[16];   // stage match filter constants (EZQ)
     ushort     hChecksum;        // =b58c(hex) + sum of all bytes in structure
 
 *********************************************************************************************
 * Aquadopp velocity data 3 beams                                                            *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification (0x01=normal, 0x80=diag)
     ushort     hSize;            // size of structure (words)
     byte       clock[6];         // date and time
     short      hError;           // error code:
                           // bit 0: compass (0=ok, 1=error)
                           // bit 1: measurement data (0=ok, 1=error)
                           // bit 2: sensor data (0=ok, 1=error)
                           // bit 3: tag bit (0=ok, 1=error)
                           // bit 4: flash (0=ok, 1=error)
                           // bit 5:
                           // bit 6: serial CT sensor read (0=ok, 1=error)
     ushort     hAnaIn1;          // analog input 1
     ushort     hBattery;         // battery voltage (0.1 V)
     union {
       ushort   hSoundSpeed;      // speed of sound (0.1 m/s)
       ushort   hAnaIn2;          // analog input 2
     } u;
     short      hHeading;         // compass heading (0.1 deg)
     short      hPitch;           // compass pitch (0.1 deg)
     short      hRoll;            // compass roll (0.1 deg)
     byte       cPressureMSB;     // pressure MSB
     char       cStatus;          // status:
                           // bit 0: orientation (0=up, 1=down)
                           // bit 1: scaling (0=mm/s, 1=0.1mm/s)
                           // bit 2: pitch (0=ok, 1=out of range)
                           // bit 3: roll (0=ok, 1=out of range)
                           // bit 4: wakeup state:
                           // bit 5:(00=bad power,01=break,10=power applied,11=RTC alarm)
                           // bit 6: power level:
                           // bit 7: (00=0(high), 01=1, 10=2, 11=3(low))
     ushort     hPressureLSW;     // pressure LSW
     short      hTemperature;     // temperature (0.01 deg C)
     short      hVel[3];          // velocity
     byte       cAmp[3];          // amplitude
     char       cFill;
     ushort     hChecksum;        // checksum

 *********************************************************************************************
 * Aquadopp Diagnostics Data Header                                                          *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification = 0x06
     ushort     hSize;            // total size of structure (words)
     ushort     hRecords;         // number of diagnostics samples to follow
     ushort     hCell;            // cell number of stored diagnostics data
     byte       cNoise[4];        // noise amplitude (counts)
     byte       clock[6];
     ushort     hSpare1;
     byte       hDistance[8];     // distance
     byte       hSpare[6];
     ushort     hChecksum;        // checksum

 *********************************************************************************************
 * Vector Velocity Data Header (3 beams)                                                     *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification (0x12)
     ushort     hSize;            // size of structure (words)
     byte       clock[6];         // date and time
     ushort     hRecords;         // number of velocity samples to follow
     byte       cNoise[3];        // noise amplitude (counts)
     byte       cspare;           // Not used
     byte       cCorr[3];         // noise correlation
     byte       cspare;           // Not used
     byte       cspare[2]         // Not used
     ushort     hChecksum;        // checksum

 *********************************************************************************************
 * Vector Velocity Data (3 beams)                                                            *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification = 0x10
     byte       cAnaIn2LSB;       // analog input 2 LSB
     byte       cCount;           // ensemble counter
     byte       cPressureMSB;     // pressure MSB
     byte       cAnaIn2MSB;       // analog input 2 MSB
     ushort     hPressureLSW;     // pressure LSW
     ushort     hAnan1;          // analog input 1 (fast)
     short      hVel[3];          // velocity
     byte       cAmp[3];          // amplitude
     byte       cCorr[3];         // correlation (0-100)
     short      hChecksum;        // checksum

 *********************************************************************************************
 * Vector System Data                                                                        *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification = 0x10
     ushort     hSize;            // size of structure (words)
     byte       clock[6];         // date and time
     ushort     hBattery;         // battery voltage (0.1 V)
     ushort     hSoundSpeed;      // speed of sound (0.1 m/s)
     short      hHeading;         // compass heading (0.1 deg)
     short      hPitch;           // compass pitch (0.1 deg)
     short      hRoll;            // compass roll (0.1 deg)
     short      hTemperature;     // temperature (0.01 deg C)
     char       cError;           // error code
     char       cStatus;          // status
     ushort     hAnaIn;           // analog input (slow)
     short      hChecksum;        // checksum

 *********************************************************************************************
 * Vector Probe Check Data                                                                   *
 *********************************************************************************************
     byte       cSync;            // sync = 0xa5
     byte       cId;              // identification = 0x10
     ushort     hSize;            // size of structure (words)
     ushort     hSamples          // Number of samples per beam
     ushort     hFirstSamp        // First Sample number
     byte       hDoNotKnow[900]   // What goes here?
     short      hChecksum;        // checksum
*********************************************************************************************/
     
/********************************************************************************************
** 
** Function: print_usage
** 
**
*********************************************************************************************/
static void print_usage(char *argv0)
{
     fprintf(stderr,"[%s]: %s  [%s compiled on %s at %s]\n", argv0, VERSION, __FILE__, __DATE__, __TIME__);
     fprintf(stderr,"Usage: %s [-o offset] [-a] [-c] [-e] [-i] [-o] [-h] -t 1-9 [-t 1-9]\n", argv0);
     fprintf(stderr,"\t-a - All data block types\n");
     fprintf(stderr,"\t-c - CSV format\n");
     fprintf(stderr,"\t-D - Debug\n");
     fprintf(stderr,"\t-e - Engineering units\n");
     fprintf(stderr,"\t-i - Ignore errors (skip to next valid protocol unit)\n");
     fprintf(stderr,"\t-o offset - skip offset number of hearder bytes\n");
     fprintf(stderr,"\t-h - Help prints this message (overrides all others)\n");
     fprintf(stderr,"\t-s - Use Vector System Time\n");
     fprintf(stderr,"\t-t - type [1-11]\n");
     fprintf(stderr,"\t\t1 - User configuration\n");
     fprintf(stderr,"\t\t2 - Aquadopp Data\n");
     fprintf(stderr,"\t\t3 - Head configuration\n");
     fprintf(stderr,"\t\t4 - Hardware configuration\n");
     fprintf(stderr,"\t\t5 - Aquadopp diagnostics\n");
     fprintf(stderr,"\t\t6 - Vector Probe Check Data\n");
     fprintf(stderr,"\t\t7 - Vector Velocity Data\n");
     fprintf(stderr,"\t\t8 - Vector System Data\n");
     fprintf(stderr,"\t\t9 - Vector Velocity Header Data\n");
     fprintf(stderr,"\t\t10 - Combined Vector Velocity Header and Velocity Data\n");
     fprintf(stderr,"\t\t11 - Combined Vector System and Velocity Data\n");
     fprintf(stderr,"\n\tAll option [-a] overrides all type designations [1-9]\n");
     fprintf(stderr,"\tDefault is Aquadopp Data\n");
}  // End of print_usage

/*********************************************************************************************
**
**   Function: get_args
**
**   Get command-line arguments and update dlog context
**
**   x_buf can be altered
**   
*********************************************************************************************/
static void get_args(int argc, char *argv[])
{
     int        c    = 0;
     
     opterr = 0;
     
     // Get args and check usage
	while((c = getopt(argc, argv, "acDeio:hst:x")) != FAIL)
     {
          switch (c)
          {
          case 'a':
               usr_conf = aqu_vel = head_conf = hrdwr_conf = aqu_diag = vec_prob_chk = vec_vel = vec_sys = TRUE;
               break;
          case 'c':
               csv = TRUE;
               break;
          case 'e':
               eng_units = TRUE;
               break;
          case 'D':
               debug = TRUE;
               break;
          case 'h':
               print_usage(argv[0]);
               exit(EXIT_FAILURE);
          case 'i':
               ignore = TRUE;
               break;
          case 'o':
               offset = atoi(optarg);
               break;
          case 'x':
               hexopt = TRUE;
               break;
          case 's':
               vsd_time = TRUE;
               break;
          case 't':
               c = atoi(optarg);
               switch (c)
               {
               case 1:
                    usr_conf = TRUE;        // Configuration data
                    break;
               case 2:
                    aqu_vel = TRUE;         // Aquadopp velocity data
                    break;
               case 3:
                    head_conf = TRUE;       // Head configuration
                    break;
               case 4:
                    hrdwr_conf = TRUE;      // Hardware configuration
                    break;
               case 5:
                    aqu_diag = TRUE;        // Diagnostics
                    break;
               case 6:
                    vec_prob_chk = TRUE;    // Probe check data
                    break;
               case 7:
                    vec_vel = TRUE;         // Full Vector Data
                    break;
               case 8:
                    vec_sys = TRUE;         // Vector System Data
                    break;
               case 9:
                    vec_vel_hdr = TRUE;     // Vector Velocity Data Header
                    break;
               case 10:
                    vec_vel = TRUE;         // Full Vector Data
                    vec_vel_hdr = TRUE;     // Vector Velocity Data Header
                    vvd_vvh = TRUE;         // Merge Vector Data and Vector Velocity Data Header
                    break;
               case 11:
                    vec_vel = TRUE;         // Full Vector Data
                    vec_sys = TRUE;         // Vector System Data
                    vvd_vsd = TRUE;         // Merge Vector Data and Vector Velocity Data Header
                    vsd_time = TRUE;        
                    break;
               default:
                    print_usage(argv[0]);
                    exit(EXIT_FAILURE);
               }
               break;
          case '?':
               fprintf(stderr, "[%s]: Unknown argument [%c]\n", argv[0], optopt);
               exit(EXIT_FAILURE);
          default:
               fprintf(stderr, "[%s]: Unknown return from getopt [%d][%c]\n", argv[0], c, c);
               exit(EXIT_FAILURE);
          }
     }
     if (!usr_conf && !aqu_vel && !head_conf && !hrdwr_conf && !aqu_diag && !vec_prob_chk &&
         !vec_vel && !vec_sys && !vec_vel_hdr)
          aqu_vel = TRUE;
          
}  // End of get_args

/*********************************************************************************************
**
**   Function: corrupted_buffer
**
**   
*********************************************************************************************/
static void corrupted_buffer(char *msg)
{
     fprintf(stderr, "Buffer No Good - %s\n", msg);
     fflush(stderr);
     fflush(stdout);
     if (ignore == FALSE)
          exit(EXIT_FAILURE);
}

/*********************************************************************************************
**
**   Function: check_checksum
**   
**   Return
**
**   TRUE if match
**   FALSE if not
**
**
**   Does not alter x_buf
**   
*********************************************************************************************/
static int check_checksum(int len)
{
     int                   i = 0;
     ushort         checksum = 0;
     ushort    calc_checksum = CHECKSUM_BASE;
     
     for (i = 0; i < len - 2; i += 2)
     {
          calc_checksum +=  (BYTE(x_buf[i + 1]) << 8);
          calc_checksum +=   BYTE(x_buf[i]);
     }

     checksum  = BYTE(x_buf[len - 1]) << 8;
     checksum += BYTE(x_buf[len - 2]);

     return (calc_checksum == checksum);
}

/*********************************************************************************************
**
**   Function: bcd_to_decimal
**
**   Converts BCD to decimap
**   
**   
*********************************************************************************************/
static int bcd_to_decimal(byte b)
{
     int ones, tens;

     ones = b & 0x0F;
     tens = b & 0XF0;
     tens = tens >> 4;
     return ((tens * 10) + ones);
}

/*********************************************************************************************
**
**   Function: out_byte
**
**   Prints the element of structure
**   
**   
*********************************************************************************************/
static byte out_byte(int *i, int count, char *s)
{
     byte val;
     int array = FALSE;


     if (count > 1)
          array = FIRST;
     
     while (count--)
     {
          
          val = BYTE(x_buf[*i]);

          if (array == FALSE)
          {
               if (csv)
                    printf(hexopt ? "%x, " : "%u, ", val);
               else
                    printf("%23s: %10u    [x%02x]%6c\n", s, val, val, isprint(val) ? val : '.');
          }
          else if ((array == FIRST) || val)
          {
               if (csv)
                    printf(hexopt ? "%x, " : "%u, ", val);
               else
               {
                    if (array < 10)
                         printf("%20s[%d]: %10u    [x%02x]%6c\n", s, array, val, val, isprint(val) ? val : '.');
                    else if (array < 100)
                         printf("%19s[%d]: %10u    [x%02x]%6c\n", s, array, val, val, isprint(val) ? val : '.');
                    else
                         printf("%18s[%d]: %10u    [x%02x]%6c\n", s, array, val, val, isprint(val) ? val : '.');
               }
               array++;
          }
          *i += 1;
     }
     return val;
}

/*********************************************************************************************
**
**   Function: out_ushort
**
**   Prints the element of structure using engineering units (if specified)
**   
**   
*********************************************************************************************/
static uint out_ushort(int *i, int count, char *s, float sf)
{
     ushort val;
     float val_f;
     int array = FALSE;


     if (count > 1)
          array = FIRST;
     

     while (count--)
     {
          val = 0;
          
          val  = BYTE(x_buf[*i + 1]) << 8;
          val  |= BYTE(x_buf[*i]);

          if (sf != NO_FL)
               val_f = val * sf;

          if (array == FALSE)
          {
               if (sf != NO_FL)
               {
                    if (csv)
                         printf("%.2f, ", val_f);
                    else
                         printf("%23s: %10.2f    [x%04x]\n", s, val_f, val);
               }
               else
               {
                    if (csv)
                         printf(hexopt ? "%x, " : "%u, ", val);
                    else
                         printf("%23s: %10u    [x%04x]\n", s, val, val);
               }
          }
          else if ((array == FIRST) || val)
          {
               if (sf != NO_FL)
               {
                    if (csv)
                         printf("%.2f, ", val_f);
                    else 
                    {
                         if (array < 10)
                              printf("%20s[%d]: %10.2f    [x%04x]\n", s, array, val_f, val);
                         else if (array < 100)
                              printf("%19s[%d]: %10.2f    [x%04x]\n", s, array, val_f, val);
                         else
                              printf("%18s[%d]: %10.2f    [x%04x]\n", s, array, val_f, val);
                    }
               }
               else
               {
                    if (csv)
                         printf(hexopt ? "%x, " : "%u, ", val);
                    else 
                    {
                         if (array < 10)
                              printf("%20s[%d]: %10u    [x%04x]\n", s, array, val, val);
                         else if (array < 100)
                              printf("%19s[%d]: %10u    [x%04x]\n", s, array, val, val);
                         else
                              printf("%18s[%d]: %10u    [x%04x]\n", s, array, val, val);
                    }
               }
               array++; 
          }
          *i += 2;
     } // end-while
     return val;
}

/*********************************************************************************************
**
**   Function: out_short
**
**   Prints the element of structure using engineering units (if specified)
**   
**   
*********************************************************************************************/
static int out_short(int *i, int count, char *s, float sf)
{
     short val;
     float val_f;
     int array = FALSE;


     if (count > 1)
          array = FIRST;
     

     while (count--)
     {
          val = 0;
          
          val  = BYTE(x_buf[*i + 1]) << 8;
          val += BYTE(x_buf[*i]);

          if (sf != NO_FL)
               val_f = val * sf;

          if (array == FALSE)
          {
               if (sf != NO_FL)
               {
                    if (csv)
                         printf("%.2f, ", val_f);
                    else
                         printf("%23s: %10.2f    [x%04x]\n", s, val_f, val);
               }
               else
               {
                    if (csv)
                         printf(hexopt ? "%x, " : "%d, ", val);
                    else
                         printf("%23s: %10d    [x%04x]\n", s, val, SHORT(val));
               }
          }
          else if ((array == FIRST) || val)
          {
               if (sf != NO_FL)
               {
                    if (csv)
                         printf("%d, ", val);
                    else 
                    {
                         if (array < 10)
                              printf("%20s[%d]: %10.2f    [x%04x]\n", s, array, val_f, val);
                         else if (array < 100)
                              printf("%19s[%d]: %10.2f    [x%04x]\n", s, array, val_f, val);
                         else
                              printf("%18s[%d]: %10.2f    [x%04x]\n", s, array, val_f, val);
                    }
               }
               else
               {
                    if (csv)
                         printf(hexopt ? "%x, " : "%d, ", val);
                    else 
                    {
                         if (array < 10)
                              printf("%20s[%d]: %10d    [x%04x]\n", s, array, val, SHORT(val));
                         else if (array < 100)
                              printf("%19s[%d]: %10d    [x%04x]\n", s, array, val, SHORT(val));
                         else
                              printf("%18s[%d]: %10d    [x%04x]\n", s, array, val, SHORT(val));
                    }
               }
               array++;
          }
          *i += 2;
     } // end-while
     return val;
}

/*********************************************************************************************
**
**   Function: out_word
**
**   Prints the element of structure
**   
**   
*********************************************************************************************/
static int out_word(int *i, int count, char *s)
{
     int val, t;
     int array = FALSE;


     if (count > 1)
          array = FIRST;
     
     while (count--)
     {
          val = 0;

          val  = BYTE(x_buf[*i + 1]) << 8;
          val += BYTE(x_buf[*i]);

          
          t  = BYTE(x_buf[*i + 3]) << 8;
          t += BYTE(x_buf[*i + 2]);

          val += t;
          
          if (array == FALSE)
          {
               if (csv)
                    printf(hexopt ? "%x, " : "%u, ", val);              
               else
                    printf("%23s: %10d    [x%04x]\n", s, val, val);
          }
          else if ((array == FIRST) || val)
          {
               if (csv)
                    printf(hexopt ? "%x, " : "%u, ", val);
               else
               {
                    if (array < 10)
                         printf("%20s[%d]: %10d    [x%04x]\n", s, array, val, val);
                    else if (array < 100)
                         printf("%19s[%d]: %10d    [x%04x]\n", s, array, val, val);
                    else
                         printf("%18s[%d]: %10d    [x%04x]\n", s, array, val, val);
               }
               array++;
          }
          
          *i += 4;
     }
     return val;
}

/*********************************************************************************************
**
**   Function: get_clock
**
**   Get the clock element of structure
**   
**   
*********************************************************************************************/
static void get_clock(int *i, int vvdh)
{
     if (csv)
          sprintf(vvdh ? vhs_time : t_time, "%04d/%02d/%02d %02d:%02d:%02d, ",
                  bcd_to_decimal(BYTE(x_buf[*i + 4])) + 2000,  // Year
                  bcd_to_decimal(BYTE(x_buf[*i + 5])),         // Month
                  bcd_to_decimal(BYTE(x_buf[*i + 2])),         // Day
                  bcd_to_decimal(BYTE(x_buf[*i + 3])),         // Hour
                  bcd_to_decimal(BYTE(x_buf[*i + 0])),         // Minute
                  bcd_to_decimal(BYTE(x_buf[*i + 1])));        // Second
     else
          sprintf(vvdh ? vhs_time : t_time, "%23s: %02d/%02d/%02d %02d:%02d:%02d\n",
                 "Date",
                 bcd_to_decimal(BYTE(x_buf[*i + 4])) + 2000,  // Year
                 bcd_to_decimal(BYTE(x_buf[*i + 5])),         // Month
                 bcd_to_decimal(BYTE(x_buf[*i + 2])),         // Day
                 bcd_to_decimal(BYTE(x_buf[*i + 3])),         // Hour
                 bcd_to_decimal(BYTE(x_buf[*i + 0])),         // Minute
                 bcd_to_decimal(BYTE(x_buf[*i + 1])));        // Second
     *i += 6;
}

/*********************************************************************************************
**
**   Function: print_nortek_user_conf
**
**   Prints the structure  (512)
**   
**   
*********************************************************************************************/
static void print_nortek_user_conf(int chk)
{
     int i = 0;

     if (debug) printf("%s\n", __FUNCTION__);

     if (csv)
     {
          if (last_id != NORTEK_USER_CONF_ID)
          {
               last_id = NORTEK_USER_CONF_ID;
               printf("USRCNF: T1, T2, T3, T4, T5, NPings, AvgInterval, NBeams, TimCtrlReg, PwrCrtlReg, A1, B0, B1, ");
               printf("CompassUpdRate, CoordSystem, NBins, BinLength, MeasInterval, DeployName, WrapMode, ");
               printf("clockDeply, DiagInterval, Mode, AdjSoundSpeed, NSampDiag, NBeamsCellDiag, NpingsDiag, ");
               printf("ModeTest, AnalnAddr, SWVersion, Spare, VelAdjTable, Comments, Mode, DynPercPos, T1, ");
               printf("T2, T3, NSamp, A1, B0, B1, Spare, AnaOutScale, CorrThresh, Spare, TiLag2, Spare, QualConst\n");
          }
          if (!chk)
               putchar(STAR_CHAR);
     }
     else
          printf("Start of Nortek User Conf:(Checksum %s)\n", chk ? "Good" : "Bad");
     out_byte(&i, 1, "cSync");                         // sync = 0xa5
     out_byte(&i, 1, "cId");                           // identification = 0x00
     out_ushort(&i, 1, "hSize", NO_FL);                // total size of structure (&i, 1, words)
     out_ushort(&i, 1, "hTranPulLen", NO_FL);          // transmit pulse length (counts)
     out_ushort(&i, 1, "hBlankDist", NO_FL);           // blanking distance (counts)
     out_ushort(&i, 1, "hRecLen", NO_FL);              // receive length (counts)
     out_ushort(&i, 1, "hTimeBP", NO_FL);              // time between pings (counts)
     out_ushort(&i, 1, "hTimeBB", NO_FL);              // time between burst sequences (counts)
     out_ushort(&i, 1, "hNPings", NO_FL);              // number of beam sequences per burst
     out_ushort(&i, 1, "hAvgInterval", NO_FL);         // average interval in seconds
     out_ushort(&i, 1, "hNBeams", NO_FL);              // number of beam
     out_ushort(&i, 1, "hTimCtrlReg", NO_FL);          // timing controller mode
                                                // bit 1: profile (0=single, 1=continuous)
                                                // bit 2: mode (0=burst, 1=continuous)
                                                // bit 5: power level (0=1, 1=2, 0=3, 1=4)
                                                // bit 6: power level (0 0 1 1 )
                                                // bit 7: synchout position (0=middle of sample, 1=end of sample (Vector))
                                                // bit 8: sample on synch (0=disabled,1=enabled, rising edge)
                                                // bit 9: start on synch (0=disabled,1=enabled,rising edge)
     out_ushort(&i, 1, "hPwrCtrlReg", NO_FL);          // power control register
                                                       // bit 5: power level (0=1, 1=2, 0=3, 1=4)
                                                       // bit 6: power level (0 0 1 1 )
     out_ushort(&i, 1, "hA1", NO_FL);                  // not used
     out_ushort(&i, 1, "hB0", NO_FL);                  // not used
     out_ushort(&i, 1, "hB1", NO_FL);                  // not used
     out_ushort(&i, 1, "hCompUpdRt", NO_FL);           // compass update rate
     out_ushort(&i, 1, "hCoordSystem", NO_FL);         // coordinate system (0=ENU, 1=XYZ, 2=BEAM)
     out_ushort(&i, 1, "hNBins", NO_FL);               // number of cells
     out_ushort(&i, 1, "hBinLength", NO_FL);           // cell size
     out_ushort(&i, 1, "hMeasInterval", NO_FL);        // measurement interval
     out_byte(&i, 6, "cDepName");                      // recorder deployment name
     out_ushort(&i, 1, "hWrapMode", NO_FL);            // recorder wrap mode (0=NO WRAP, 1=WRAP WHEN FULL)
     get_clock(&i, FALSE);
     fputs(t_time, stdout);
     out_word(&i, 1, "DiagInterval");                  // number of seconds between diagnostics
     out_ushort(&i, 1, "hMode", NO_FL);                // mode:
                                                // bit 0: use user specified sound speed (0=no,1=yes)
                                                // bit 1: diagnostics/wave mode 0=disable,1=enable)
                                                // bit 2: analog output mode (0=disable,1=enable)
                                                // bit 3: output format (0=Vector, 1=ADV)
                                                // bit 4: scaling (0=1 mm, 1=0.1 mm)
                                                // bit 5: serial output (0=disable, 1=enable)
                                                // bit 6: reserved EasyQ
                                                // bit 7: stage (0=disable, 1=enable)
                                                // bit 8: output power for analog input (0=disable, 1=enable)
     out_ushort(&i, 1, "hAdjSoundSpeed", NO_FL);       // user input sound speed adjustment factor
     out_ushort(&i, 1, "hNSampDiag", NO_FL);           // samples (AI if EasyQ) in diagnostics mode
     out_ushort(&i, 1, "hNBeamsCellDiag", NO_FL);      // beams / cell number to measure in diagnostics mode
     out_ushort(&i, 1, "hNPingsDiag", NO_FL);          // pings in diagnostics/wave mode
     out_ushort(&i, 1, "hModeTest", NO_FL);            // mode test:
                                                // bit 0: correct using DSP filter (0=no filter,1=filter)
                                                // bit 1: filter data output (0=total corrected velocity,1=correction part)
     out_ushort(&i, 1, "hAnaInAddr", NO_FL);           // analog input address
     out_ushort(&i, 1, "hSWVersion", NO_FL);           // software version
     out_ushort(&i, 1, "hSpare", NO_FL);               // Spare
     out_byte(&i, 180, "cVelAdjTble");                 // velocity adjustment table
     out_byte(&i, 180, "cComments");                   // file comments
     out_ushort(&i, 1, "hWaveMode", NO_FL);            // wave measurement mode
                                                // bit 0: data rate (0=1 Hz, 1=2 Hz)
                                                // bit 1: wave cell position (0=fixed,1=dynamic)
                                                // bit 2: type of dynamic position (0=pct of mean pressure, 1=pct)
     out_ushort(&i, 1, "hDynPercPos", NO_FL);          // percentage for wave cell positioning(=32767x#%/100)
     out_ushort(&i, 1, "hT1", NO_FL);                  // wave transmit pulse
     out_ushort(&i, 1, "hT2", NO_FL);                  // fixed wave blanking distance (counts)
     out_ushort(&i, 1, "hT3", NO_FL);                  // wave measurement cell size
     out_ushort(&i, 1, "hNSamp", NO_FL);               // number of diagnostics/wave samples
     out_ushort(&i, 1, "hA1b", NO_FL);                 // not used
     out_ushort(&i, 1, "hB0b", NO_FL);                 // not used
     out_ushort(&i, 1, "hB1b", NO_FL);                 // not used
     out_ushort(&i, 1, "hSpareB", NO_FL);              // Spare
     out_ushort(&i, 1, "hAnaOutScale", NO_FL);         // analog output scale factor (16384=1.0, max = 4.0)
     out_ushort(&i, 1, "hCorrThresh", NO_FL);          // correlation threshold for resolving
     out_ushort(&i, 1, "hSpareC", NO_FL);              // Spare
     out_ushort(&i, 1, "hTiLag2", NO_FL);              // transmit pulse length (counts) second lag     
     out_byte(&i, 30, "cSpare");                       // Spare
     out_byte(&i, 16, "cQualConst");                   // stage match filter constants (EZQ)
     if (csv)
     {
          putchar(NEWLN_CHAR);
          return;
     }
     out_ushort(&i, 1, "hChecksum", NO_FL);            // =b58c(hex) + sum of all bytes in structure
     printf("End of Nortek User Conf (%d bytes)\n\n", i);
     
}  // End of print_nortek_user_conf

/*********************************************************************************************
**
**   Function: print_aqu_vel_data (and aqu_diag_data)
**
**   Prints the structure (42 bytes)
**   
**   
*********************************************************************************************/
static void print_aqu_vel_data(int chk, int diag)
{
     int press = 0;
     int     i = 0;

     if (debug) printf("%s\n", __FUNCTION__);

     if (csv && diag)                          // Do not output csv for diagnostics data
          return;
            
     if (csv)
     {
          if (!diag && (last_id != AQU_VEL_DATA_ID))
          {
               last_id = AQU_VEL_DATA_ID;
               printf("AQVEL: date_utc time_utc, ErrFlag, sndspd_m/s, hdg_deg, pitch_deg, roll_deg, pressMSB, StatusFlag,");
               printf(" pressLSW, temp_degC, velE_mm/s, velN_mm/s, velU_mm/s, ampB1, ampB2, ampB3,");
               if (eng_units)
                    printf(" press_dbar,");
               putchar(NEWLN_CHAR);
          }
          else if (diag && (last_id != AQU_DIG_DATA_ID))
          {
               last_id = AQU_DIG_DATA_ID;
               printf("AQDIG: date_utc time_utc, ErrFlag, sndspd_m/s, hdg_deg, pitch_deg, roll_deg, pressMSB, StatusFlag,");
               printf(" pressLSW, temp_degC, velE_mm/s, velN_mm/s, velU_mm/s, ampB1, ampB2, ampB3,");
               if (eng_units)
                    printf(" press_dbar,");
               putchar(NEWLN_CHAR);
          }
          if (!chk)
               putchar(STAR_CHAR);
          i += 4;
     }
     else
     {
          if (diag)
               printf("Start of Aquadopp Diagnostics Data:(Checksum %s) %d of %d\n", chk ? "Good" : "Bad",
                      ++add_numrec, add_recs);
          else
               printf("Start of Aquadopp Velocity Data:(Checksum %s)\n", chk ? "Good" : "Bad");
          out_byte(&i, 1, "cSync");                        // sync = 0xa5
          out_byte(&i, 1, "cId");                          // identification (0x01=normal, 0x80=diag)
          out_ushort(&i, 1, "hSize", NO_FL);               // size of structure (words)
     }

     get_clock(&i, FALSE);
     fputs(t_time, stdout);

     out_short(&i, 1, "hError", NO_FL);                    // error code:
                                                    // bit 0: compass (0=ok, 1=error)
                                                    // bit 1: measurement data (0=ok, 1=error)
                                                    // bit 2: sensor data (0=ok, 1=error)
                                                    // bit 3: tag bit (0=ok, 1=error)
                                                    // bit 4: flash (0=ok, 1=error)
                                                    // bit 5:
                                                    // bit 6: serial CT sensor read (0=ok, 1=error)
     if (csv)
          i += 4;
     else
     {
          out_ushort(&i, 1, "hAnaIn1", NO_FL);             // analog input 1
          out_ushort(&i, 1, "hBattery", .1);               // battery voltage (0.1 V)
     }
     
     out_ushort(&i, 1, "hSoundSpeed/hAnaIn2", .1);         // speed of sound (0.1 m/s)
     out_short(&i, 1, "hHeading", .1);                     // compass heading (0.1 deg)
     out_short(&i, 1, "hPitch", .1);                       // compass pitch (0.1 deg)
     out_short(&i, 1, "hRoll", .1);                        // compass roll (0.1 deg)
     press = out_byte(&i, 1, "cPressureMSB");              // pressure MSB (0.001 dbar)
     press *= 65536;

     out_byte(&i, 1, "cStatus");                           // status:
                                                    // bit 0: orientation (0=up, 1=down)
                                                    // bit 1: scaling (0=mm/s, 1=0.1mm/s)
                                                    // bit 2: pitch (0=ok, 1=out of range)
                                                    // bit 3: roll (0=ok, 1=out of range)
                                                    // bit 4: wakeup state:
                                                    // bit 5: (00=bad power,01=break,10=power applied,11=RTC alarm)
                                                    // bit 6: power level:
                                                    // bit 7: (00=0(high), 01=1, 10=2, 11=3(low))

     press += out_ushort(&i, 1, "hPressureLSW", NO_FL);    // pressure LSW
     out_short(&i, 1, "hTemperature",.01);                 // temperature (0.01 deg C)
     out_short(&i, 3, "hVel", NO_FL);                      // velocity
     out_byte(&i, 3, "cAmp");                              // amplitude

     if (csv)
     {
          printf("%.3f,\n", (float) press / 1000.0);
          return;
     }
     
     if (eng_units)
          printf("%23s: %10.3f    [x%04x]\n", "Derived Pressure", press/ 1000.0, press);         

     out_byte(&i, 1, "cFill");                        // Fill
     out_ushort(&i, 1, "hChecksum", NO_FL);           // checksum
     printf("End of Aquadopp Velocity Data (%d bytes)\n\n", i);
}  // End of print_aqu_vel_data

/*********************************************************************************************
**
**   Function: print_nortek_head_conf
**
**   
**   
**   
*********************************************************************************************/
static void print_nortek_head_conf(int chk)
{
     int i = 0;

     if (debug) printf("%s\n", __FUNCTION__);

     if (csv)
     {
          if (last_id != NORTEK_HEAD_CONF_ID)
          {
               last_id = NORTEK_HEAD_CONF_ID;
               printf("NHC: Confg, Freq, type, SerialNo[12], System[176], NBeams\n");
          }
          if (!chk)
               putchar(STAR_CHAR);
          i += 4;
     }
     else
     {
          printf("Start of Nortek Head Configuration:(Checksum %s)\n", chk ? "Good" : "Bad");
          out_byte(&i, 1, "cSync");            // sync = 0xa5
          out_byte(&i, 1, "cId");              // identification = 0x04
          out_ushort(&i, 1, "hSize", NO_FL);     // size of structure (words)
     }
     out_ushort(&i, 1, "hConfig", NO_FL);       // head configuration:
                           // bit 0: Pressure sensor (0=no, 1=yes)
                           // bit 1: Magnetometer sensor (0=no, 1=yes)
                           // bit 2: Tilt sensor (0=no, 1=yes)
                           // bit 3: Tilt sensor mounting (0=up, 1=down)
     out_ushort(&i, 1, "hFrequency", NO_FL);    // head frequency (kHz)
     out_ushort(&i, 1, "hType", NO_FL);         // head type
     out_byte(&i, 12, "cSerialNo");             // head serial number
     out_byte(&i, 176, "cSystem");              // system data
     if (csv)
          i += 22;
     else
          out_byte(&i, 22,  "cSpare");          // spare
     out_ushort(&i, 1, "hNBeams", NO_FL);       // number of beams
     if (csv)
     {
          putchar(NEWLN_CHAR);
          return;
     }
     out_ushort(&i, 1, "hChecksum", NO_FL);    // checksum
     printf("End of Nortek Head Configuration(%d bytes)\n\n", i);
}  // End of print_nortek_head_conf

/*********************************************************************************************
**
**   Function: print_nortek_hardware_conf
**
**   
**   
**   
*********************************************************************************************/
static void print_nortek_hardware_conf(int chk)
{
     int i = 0;
     
     if (debug) printf("%s\n", __FUNCTION__);

     if (csv)
     {
          if (last_id != NORTEK_HRDWR_CONF_ID)
          {
               last_id = NORTEK_HRDWR_CONF_ID ;
               printf("NHWC: SerlNo[14], Config, Req, PICVer, HWVer, RecSz, Status, FWVer\n");
          }
          if (!chk)
               putchar(STAR_CHAR);
          i += 4;
     }
     else
     {
          printf("Start of Nortek Hardware Configuration:(Checksum %s)\n", chk ? "Good" : "Bad");
          out_byte(&i, 1, "cSync");               // sync = 0xa5
          out_byte(&i, 1, "cId");                 // identification = 0x05
          out_ushort(&i, 1, "hSize", NO_FL);      // size of structure (words)
     }
     
          out_byte(&i, 14,  "cSerialNo[14]");      // instrument type and serial number
          out_ushort(&i, 1,  "hConfig", NO_FL);    // board configuration
                                                   // bit 0: Recorder installed (0=no, 1=yes)
                                                   // bit 1: Compass installed (0=no, 1=yes)
          out_ushort(&i, 1, "hFrequency", NO_FL);  // board frequency [kHz]
          out_ushort(&i, 1, "hPICversion", NO_FL); // PIC code version number
          out_ushort(&i, 1, "hHWrevision", NO_FL); // Hardware revision
          out_ushort(&i, 1, "hRecSize", NO_FL);    // Recorder size (*65536 bytes)
          out_ushort(&i, 1, "hStatus", NO_FL);     // status: bit 0: Velocity range
          if (csv)
               i +=12;                             // (0=normal, 1=high)
          else
               out_byte(&i, 12, "cSpare[12]");     // spare
          out_word(&i, 1, "FWversion");            // firmware version
          if (csv)
          {
               putchar(NEWLN_CHAR);
               return;
          }
          out_ushort(&i, 1, "hChecksum", NO_FL);  // checksum
          printf("End of Nortek Hardware Configuration (%d bytes)\n\n", i);
     
}  // End of print_nortek_hardware_conf

/*********************************************************************************************
**
**   Function: print_aqu_diag_data_header
**
**   Prints the structure (36 bytes)
**   
**   
*********************************************************************************************/
static void print_aqu_diag_data_header(int chk)
{
     int i = 0;

     if (debug) printf("%s\n", __FUNCTION__);

     if (csv)
     {
          if (last_id != AQU_DIG_DATA_HDR_ID)
          {
               last_id = AQU_DIG_DATA_HDR_ID;
               printf("AQDDH: Recs, Cell, Noise[4], Clock, Spare, Dist, Spare\n");
          }
          if (!chk)
               putchar(STAR_CHAR);
          i += 4;
     }
     else
     {
          printf("Start of Aquadopp Diagnostics Data Header:(Checksum %s)\n", chk ? "Good" : "Bad");
          out_byte(&i, 1, "cSync");                    // sync = 0xa5
          out_byte(&i, 1, "cId");                      // identification = 0x06
          out_ushort(&i, 1, "hSize", NO_FL);           // total size of structure (&i, 1, words)
     }
     add_numrec = 0;
     add_recs = out_ushort(&i, 1, "hRecords", NO_FL);  // number of diagnostics samples to follow
     out_ushort(&i, 1, "hCell", NO_FL);                // cell number of stored diagnostics data
     out_byte(&i, 4, "cNoise");                        // noise amplitude (&i, 1, counts)
     get_clock(&i, FALSE);
     fputs(t_time, stdout);
     out_ushort(&i, 1, "hSpare1", NO_FL);              // Not used filed
     out_ushort(&i, 4, "hDistance", NO_FL);            // distance
     out_ushort(&i, 3, "hSpare", NO_FL);               // Not used field
     if (csv)
     {
          putchar(NEWLN_CHAR);
          return;
     }
     out_ushort(&i, 1, "hChecksum", NO_FL);            // checksum
     printf("End of Aquadopp Diagnostics Data Header (%d bytes)\n\n", i);
     
}  // End of print_aqu_diag_data_header

/*********************************************************************************************
**
**   Function: print_vec_prob_chk_data
**
**   
**   
*********************************************************************************************/
static void print_vec_prob_chk_data(int chk)
{
     int         i = 0;
     int num_sampl = 0;
     

     if (debug) printf("%s\n", __FUNCTION__);

     if (csv)
     {
          if (last_id != VEC_PROB_CHK_DATA_ID)
          {
               last_id =  VEC_PROB_CHK_DATA_ID;
               printf("VPCD: samples, 1st_sample, B1[1..n], B2[1..n], B3[1..n]\n");
          }
          if (!chk)
               putchar(STAR_CHAR);
          i += 4;
     }
     else
     {
          printf("Start of Vector Probe Check Data:(Checksum %s)\n", chk ? "Good" : "Bad");
          out_byte(&i, 1, "cSync");                        // sync = 0xa5
          out_byte(&i, 1, "cId");                          // identification (0x01=normal, 0x80=diag)
          out_ushort(&i, 1, "hSize", NO_FL);               // size of structure (words)
     }
     num_sampl = out_ushort(&i, 1, "hSamples", NO_FL);     // Number of Samples
     out_ushort(&i, 1, "hFirstSamp", NO_FL);               // First Sample

     out_byte(&i, num_sampl, "AmpB1");
     out_byte(&i, num_sampl, "AmpB2");
     out_byte(&i, num_sampl, "AmpB3");
     
     if (csv)
     {
          putchar(NEWLN_CHAR);
          return;
     }
     out_ushort(&i, 1, "hChecksum", NO_FL);                // checksum
     printf("End of Vector Probe Check Data (%d bytes)\n\n", i);
}  // End of print_vec_prob_chk_data
     
/*********************************************************************************************
**
**   Function: print_vec_vel_data_hdr
**
**   
**   
*********************************************************************************************/
static void print_vec_vel_data_hdr(int chk)
{
     int        i = 0;
     ushort   val = 0;
     
     if (debug) printf("%s\n", __FUNCTION__);

     if (!vvd_vvh)
     {
          if (csv)
          {
               if (last_id != VEC_VEL_DATA_HDR_ID)
               {
                    last_id = VEC_VEL_DATA_HDR_ID;
                    printf("VVDH: date_utc time_utc, num_rec, noise1, noise2, noise3, corr1, corr2, corr3\n");
               }
               if (!chk)
                    putchar(STAR_CHAR);
                    i += 4;
          }
          else
          {
               printf("Start of Vector Velocity Data Header:(Checksum %s)\n", chk ? "Good" : "Bad");
               out_byte(&i, 1, "cSync");                   // sync = 0xa5
               out_byte(&i, 1, "cId");                     // identification (0x01=normal, 0x80=diag)
               out_ushort(&i, 1, "hSize", NO_FL);          // size of structure (words)
          }
     }
     else
          i += 4;
     
     get_clock(&i, TRUE);                                  // date and time
     vvdh_numrec = 0;
     if (vvd_vvh)                                          // still get the record number from header
     {
          val  = BYTE(x_buf[i + 1]) << 8;
          val += BYTE(x_buf[i]);
          vvdh_recs = val;
          return;                                          // Print nothing and return
     }
     fputs(vhs_time, stdout);                              // Output the time
     vvdh_recs = out_ushort(&i, 1, "hRecords", NO_FL);     // Number of records
     out_byte(&i, 1, "cNoise1");                           // Noise beam 1
     out_byte(&i, 1, "cNoise2");                           // Noise beam 2
     out_byte(&i, 1, "cNoise3");                           // Noise beam 3
     i++;                                                  // Skip spare
     out_byte(&i, 1, "cCorrel1");                          // Correl beam 1
     out_byte(&i, 1, "cCorrel2");                          // Correl beam 2
     out_byte(&i, 1, "cCorrel3");                          // Correl beam 3
     if (csv)
     {
          putchar(NEWLN_CHAR);
          return;
     }
     i += 21;                                              // Skip 1 + 20 spares
     out_ushort(&i, 1, "hChecksum", NO_FL);                // checksum
     printf("End of Vector Velocity Data Header (%d bytes)\n\n", i);
}  // End of print_vec_vel_data_hdr

/*********************************************************************************************
**
**   Function: print_vec_sys_data
**
**   
**   
*********************************************************************************************/
static void print_vec_sys_data(int chk)
{
     int        i = 0;

     if (debug) printf("%s\n", __FUNCTION__);

     if (!vvd_vsd)
     {
          if (csv)
          {
               if (last_id != VEC_SYS_DATA_ID)
               {
                    last_id = VEC_SYS_DATA_ID;
                    printf("VSD: date_utc time_utc, bat_V, sndspd_m/s, hdg_deg, pitch_deg, roll_deg, temp_degC, ");
                    printf("error, status, analn\n");
               }
               if (!chk)
                    putchar(STAR_CHAR);
               i += 4;
          }
          else
          {
               printf("Start of Vector System Data:(Checksum %s)\n", chk ? "Good" : "Bad");
               out_byte(&i, 1, "cSync");                        // sync = 0xa5
               out_byte(&i, 1, "cId");                          // identification (0x01=normal, 0x80=diag)
               out_ushort(&i, 1, "hSize", NO_FL);               // size of structure (words)
          }
     }
     else
          i += 4;
     
     get_clock(&i, vsd_time);                              // date and time
     if (!chk && vvd_vsd)
          putchar(STAR_CHAR);
     fputs(vsd_time ? vhs_time : t_time, stdout);
     out_ushort(&i, 1, "hBattery", .1);                    // battery voltage (0.1 V)
     out_ushort(&i, 1, "hSoundSpeed", .1);                 // speed of sound (0.1 m/s)
     out_short(&i, 1, "hHeading", .1);                     // compass heading (0.1 deg)
     out_short(&i, 1, "hPitch", .1);                       // compass pitch (0.1 deg)
     out_short(&i, 1, "hRoll", .1);                        // compass roll (0.1 deg)
     out_short(&i, 1, "hTemperature",.01);                 // temperature (0.01 deg C)
     out_byte(&i, 1, "cError");                            // error
     out_byte(&i, 1, "cStatus");                           // status:
                                                    // bit 0: orientation (0=up, 1=down)
                                                    // bit 1: scaling (0=mm/s, 1=0.1mm/s)
                                                    // bit 2: pitch (0=ok, 1=out of range)
                                                    // bit 3: roll (0=ok, 1=out of range)
                                                    // bit 4: wakeup state:
                                                    // bit 5: (00=bad power,01=break,10=power applied,11=RTC alarm)
                                                    // bit 6: power level:
                                                    // bit 7: (00=0(high), 01=1, 10=2, 11=3(low))
     out_ushort(&i, 1, "hAnaIn", NO_FL);                   // Analog In
     if (csv || vvd_vsd)
     {
          if (!vvd_vsd)
               putchar(NEWLN_CHAR);
          return;
     }
     out_ushort(&i, 1, "hChecksum", NO_FL);                // checksum
     printf("End of Vector System Data (%d bytes)\n\n", i);
}  // End of print_vec_sys_data

/*********************************************************************************************
**
**   Function: print_vec_vel_data
**
**   
**   
*********************************************************************************************/
static void print_vec_vel_data(int chk)
{
     int i = 0;
     int    press = 0;
     int    analn = 0;
     byte   val;
     

     if (debug) printf("%s\n", __FUNCTION__);

     if (csv)
     {
          if (last_id != VEC_VEL_DATA_ID)
          {
               last_id =  VEC_VEL_DATA_ID;
               if (vvd_vsd)
               {
                    printf("VVD: date_utc time_utc, bat_V, sndspd_m/s, hdg_deg, pitch_deg, roll_deg, temp_degC, ");
                    printf("error, status, analn, ");
                    printf("analn2LSB, count, pressMSB, analn2MSB, pressLSW, analn1, ");
               }
               else
                    printf("VVD: date_utc time_utc, analn2LSB, count, pressMSB, analn2MSB, pressLSW, analn1, ");
               printf("velB1_mms/s, VelB2_mm/s, VelB3_mm/s, ampB1, ampB2, ampB3, corrB1, corrB2, corrB3,");
               if (eng_units)
                    printf(" press_dbar, Analog,");
               putchar(NEWLN_CHAR);
          }
          if (!chk)
               putchar(STAR_CHAR);
          i += 2;
     }
     else
     {
          printf("Start of Vector Velocity Data:(Checksum %s) %d of %d\n",
                 chk ? "Good" : "Bad", vvdh_numrec + 1, vvdh_recs);
          out_byte(&i, 1, "cSync");                         // sync = 0xa5
          out_byte(&i, 1, "cId");                           // identification = 0x06
     }
     if (vvd_vsd && vsd_buf[0])
     {
          memcpy(s_buf, x_buf, BUF_SIZE);
          memcpy(x_buf, vsd_buf, BUF_SIZE);
          print_vec_sys_data(check_checksum(VEC_SYS_DATA_LEN));
          memcpy(x_buf, s_buf, BUF_SIZE);
     }
     else if (eng_units)
     {
          if (vvdh_numrec != FAIL)                          // Had header time data
          {
               if (vvdh_numrec >= vvdh_recs - 1)            // No current header time data
               {
                    vvdh_numrec = FAIL;
                    strcpy(vhs_time, csv ? "0000/00/00 00:00:00, " : "                   Date: 0000/00/00 00:00:00\n");
               }
               else
                    vvdh_numrec++;
          }
          fputs(vhs_time, stdout);
     }
     
     analn = out_byte(&i, 1, "cAnaln2LSB");                 // Analog 2 LSB
     if ((vvdh_numrec != (val = out_byte(&i, 1, "cCount"))) && (vvdh_numrec != FAIL))  // Ensemble counter
         fprintf(stderr, "Warning record number [%d] not what was expected [%d]\n", val, vvdh_numrec);
     press = out_byte(&i, 1, "cPressureMSB");               // Pressure MSB
     press *= 65536;                                        // 16 bit word shift
     analn += (out_byte(&i, 1, "cAnaln2MSB") * 255);        // Analog 2 MSB
     press += out_ushort(&i, 1, "hPressureLSW", NO_FL);     // Pressure LSW
     out_ushort(&i, 1, "hAnaln1", NO_FL);                   // Analog 1
     out_short(&i, 3, "hVel", NO_FL);                       // Velocity
     out_byte(&i, 3, "cAmp");                               // Amplitude
     out_byte(&i, 3, "cCorr");                              // Correlation

     if (eng_units)
     {
          if (csv)
          {
               printf("%.3f, %d,\n", (float) press / 1000.0, analn);
               return;
          }
          printf("%23s: %10.3f    [x%04x]\n", "Derived Pressure", press / 1000.0, press);
          printf("%23s: %10d\n", "Derived Analog", analn);         
     }
     else if (csv)
     {
          putchar(NEWLN_CHAR);
          return;
     }
     
     out_ushort(&i, 1, "hChecksum", NO_FL);            // checksum
     printf("End of Vector Velocity Data (%d bytes)\n\n", i);
     
}  // End of print_vec_vel_data

/*********************************************************************************************
**
**   Function: main
**
*********************************************************************************************/
int main(int argc, char *argv[])
{
     int c;
     uint current_id;
    
     
     get_args(argc, argv);
     
     strcpy(vhs_time, csv ? "0000/00/00 00:00:00, " : "                   Date: 0000/00/00 00:00:00\n");
     if (offset)
          while ((c = getchar()) != EOF)
          {
               byte_cnt++;
               if (--offset <= 0)
                    break;
          }
     

     while ((c = getchar()) != EOF)
     {
          byte_cnt++;
          memset(x_buf, 0, sizeof(x_buf));
          x_buf[0] = c;
          if (debug) printf("%s: 1st Character Read %3d [0x%0x]\n", __FUNCTION__, c, c);
          
          
          if (x_buf[0] != NORTEK_DATA_HDR)
          {
               sprintf((char *) x_buf, "Sync Byte (0xa5) Not Found, read %3d [0x%02x] %d bytes into file",
                       x_buf[0], x_buf[0], byte_cnt);
               corrupted_buffer((char *) x_buf);
               offset = 0;
               while ((c = getchar()) != EOF)
               {
                    byte_cnt++;
                    offset++;
                    if ((x_buf[0] = c) == NORTEK_DATA_HDR)
                         break;
               }
               fprintf(stderr,"Skipping %d characters\n", offset);
          }

          if ((c = getchar()) == EOF)
          {
               corrupted_buffer("Short buffer - could not read header id byte");
               continue;
          }
          byte_cnt++;
          x_buf[1] = current_id = c;
          if (debug) printf("%s: 2nd Character Read %3d [0x%02x]\n", __FUNCTION__, c, c);
          switch (x_buf[1])
          {
          case NORTEK_USER_CONF_ID:
               if ((c = fread(x_buf + 2, 1, NORTEK_USER_CONF_LEN - 2, stdin)) != NORTEK_USER_CONF_LEN - 2)
                    corrupted_buffer("Short read of User Configuration");
               else if (usr_conf)
                    print_nortek_user_conf(check_checksum(NORTEK_USER_CONF_LEN));
               byte_cnt += c;
               break;
          case AQU_VEL_DATA_ID:
               if ((c = fread(x_buf + 2, 1, AQU_VEL_DATA_LEN - 2, stdin)) != AQU_VEL_DATA_LEN - 2)
                    corrupted_buffer("Short read Aquadopp Velocity Data");
               else if (aqu_vel)
                    print_aqu_vel_data(check_checksum(AQU_VEL_DATA_LEN), FALSE);
               byte_cnt += c;
               break;
         case NORTEK_HEAD_CONF_ID:
               if ((c = fread(x_buf + 2, 1, NORTEK_HEAD_CONF_LEN - 2, stdin)) != NORTEK_HEAD_CONF_LEN - 2)
                    corrupted_buffer("Short read of Head Configuration");
               else if (head_conf)
                    print_nortek_head_conf(check_checksum(NORTEK_HEAD_CONF_LEN));
               break;
          case NORTEK_HRDWR_CONF_ID:
               if ((c = fread(x_buf + 2, 1, NORTEK_HRDWR_CONF_LEN - 2, stdin)) != NORTEK_HRDWR_CONF_LEN - 2)
                    corrupted_buffer("Short read of Hardware Configuration");
               else if (hrdwr_conf)
                    print_nortek_hardware_conf(check_checksum(NORTEK_HRDWR_CONF_LEN));
               byte_cnt += c;
               break;
           case AQU_DIG_DATA_HDR_ID:
               if (fread(x_buf + 2, 1, AQU_DIG_DATA_HDR_LEN - 2, stdin) != AQU_DIG_DATA_HDR_LEN - 2)
                    corrupted_buffer("Short Buffer Read");
               else if (aqu_diag)
                    print_aqu_diag_data_header(check_checksum(AQU_DIG_DATA_HDR_LEN));
               byte_cnt += c;
               break;
          case VEC_PROB_CHK_DATA_ID:
               if (fread(x_buf + 2, 1, VEC_PROB_CHK_DATA_LEN - 2, stdin) != VEC_PROB_CHK_DATA_LEN - 2)
                    corrupted_buffer("Short read Vectore Prob Cheack Data");
               else if (vec_prob_chk)
                    print_vec_prob_chk_data(check_checksum(VEC_PROB_CHK_DATA_LEN));
               break;
          case VEC_VEL_DATA_HDR_ID:
               if ((c = fread(x_buf + 2, 1, VEC_VEL_DATA_HDR_LEN - 2, stdin)) != VEC_VEL_DATA_HDR_LEN - 2)
                    corrupted_buffer("Short read of Vectore Velocity Header Data");
               else if (vec_vel_hdr)
                    print_vec_vel_data_hdr(check_checksum(VEC_VEL_DATA_HDR_LEN));
               byte_cnt += c;
               break;
          case VEC_SYS_DATA_ID:
               if (fread(x_buf + 2, 1, VEC_SYS_DATA_LEN - 2, stdin) != VEC_SYS_DATA_LEN - 2)
                    corrupted_buffer("Short read Vector System Data");
               else
               {
                    memcpy(vsd_buf, x_buf, BUF_SIZE);
                    if (vec_sys && !vvd_vsd)
                         print_vec_sys_data(check_checksum(VEC_SYS_DATA_LEN));
               }
               byte_cnt += c;
               break;
          case VEC_VEL_DATA_ID:
               if ((c = fread(x_buf + 2, 1, VEC_VEL_DATA_LEN - 2, stdin)) != VEC_VEL_DATA_LEN - 2)
                    corrupted_buffer("Short read of Vectore Velocity Data");
               else if (vec_vel)
                    print_vec_vel_data(check_checksum(VEC_VEL_DATA_LEN));
               byte_cnt += c;
               break;
          case AQU_DIG_DATA_ID:
               if ((c = fread(x_buf + 2, 1, AQU_DIG_DATA_LEN - 2, stdin)) != AQU_DIG_DATA_LEN - 2)
                    corrupted_buffer("Short read Aquadopp Velocity Data");
               else if (aqu_diag)
                    print_aqu_vel_data(check_checksum(AQU_VEL_DATA_LEN), TRUE);
               byte_cnt += c;
               break;
          default:
               corrupted_buffer("ID Byte Not Found");
               break;
          }
     }
     
	exit(EXIT_SUCCESS);
}
