/*
 * *****IMPORTANT*****
 * use this compile line: gcc -m32 -lm -Wall mmp_unpack.c -o mmp_unpack
 *
 * File:   mmp_unpack.c
 * Author: Jeff O'Brien
 * email jkobrien@whoi.edu
 *
 * Usage: mmp_unpack <output dir> <input file> [-t]
 *        Where: -t option is used to create TIMETAGS.TXT file
 *               outdir is output directory or 'stdout'
 *
 * Created on September 27, 2011, 7:40 PM
 *
 * History:
 *     Date      Who    Description
 *    -------    ---    ----------------------------------
 * 09/27/2011   JKOB    Create
 * 09/30/2011   JKOB    added timetags.txt functionality
 * 10/05/2011   JKOB    added ability get calibration files from CONFIG.BIN
 *                      output parameters.txt to display these values
 * 09/30/2013    SL     Support output dir of 'stdout'
 * 10/30/2013 JKOB Added support for nortek .DEC file
 * 11/13/2013 JKOB/SL   Fixed aquadop velocityScale casting problem
 * 11/24/2013 JKOB      Add first non-zero pressure from E file for TIMETAGS2.TXT
 * 10/27/2015    SL     Fixed infinite loop bug in unpackCTD
 * 03/13/2017    CC     Applied infinite loop bug fix made to unpackCTD to unpackENG and unpackAquaDop
 * =======================================================
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <dirent.h>

#define PROFILE_COMPLETE -1L
#define PROFILE_RESTART -2L
#define RESTART_WITH_BACKTRACK -3L
#define PROFILE_NOT_COMPLETED -4L
#define DEPLOYMENT_STOPPED -5L
#define TELEMETRY_RESTART -6L

#define VERSION "v1.0-20151027"

#define SUCCESS                         1
#define FAILURE                         0
#define YES                             1
#define NO                              0

#define MAX_BINS                        20

enum {
    SMOOTH_RUNNING = 0,   /*  0 */
    MISSION_COMPLETE,     /*  1 */
    OPERATOR_CTRL_C,      /*  2 */
    TT8_COMM_FAILURE,     /*  3 */
    CTD_COMM_FAILURE,     /*  4 */
    ACM_COMM_FAILURE,     /*  5 */
    TIMER_EXPIRED,        /*  6 */
    MIN_BATTERY,          /*  7 */
    AVG_MOTOR_CURRENT,    /*  8 */
    MAX_MOTOR_CURRENT,    /*  9 */
    SINGLE_PRESSURE,      /* 10 */
    AVG_PRESSURE,         /* 11 */
    AVG_TEMPERATURE,      /* 12 */
    TOP_PRESSURE,         /* 13 */
    BOTTOM_PRESSURE,      /* 14 */
    PRESSURE_RATE_ZERO,   /* 15 */
    STOP_NULL,            /* 16 */
    FLASH_CARD_FULL,      /* 17 */
    FILE_SYSTEM_FULL,     /* 18 */
    TOO_MANY_OPEN_FILES,  /* 19 */
    AANDERAA_COMM_FAILURE /* 20 */
};

typedef struct {
    time_t sensor_start_time;
    time_t profile_start_time;
} StartTimes_t;

typedef struct {
    ushort CoreFlag; //current, voltage, pressure
    ushort FluorometerFlag; //fluorometer value & gain
    ushort TurbidityFlag; //turbidity value & gain
    ushort OptodeFlag; //oxygen & temperature
    ushort ParFlag; //PAR value
    ushort BBFL2Flag; //Puck Scatter, Chl & CDOM value
    ushort BioSuiteFlag; //Triplet & PAR
    ushort FLBBFlag; //Wetlabs FLBB
} ComponentFlags_t;

typedef struct {
    short ramp_status;
    short profile_status;
    time_t profile_end_time;
    time_t sensor_end_time;
} OffloadProfileMessage_t;

#pragma pack(push,1)
typedef struct {
    float current;
    float voltage;
    float pressure;

    float Par_value;

    short Puck_scatter;
    short Puck_chl;
    short Puck_CDOM;

} OffloadEngineeringData_t;
#pragma pack(pop)

#pragma pack(push,1)
typedef struct {
    unsigned char C1, C2, C3;
    unsigned char T1, T2, T3;
    unsigned char D1, D2, D3;
    unsigned char X1, X2;
} OffloadCTDOXData_t;
#pragma pack(pop)

typedef struct {
    time_t sensor_on_time;
    time_t sensor_off_time;
} SensorTimes_t;

/*
#pragma pack(push,1)
typedef struct {
    unsigned short velocityA, velocityB, velocityC, velocityD;
    unsigned short MX, MY, MZ;
    unsigned short pitch, roll;
} OffloadACMData_t;
#pragma pack(pop)
*/

#pragma pack(push,1)
typedef struct {
    short velocityA, velocityB, velocityC, velocityD;
    short MX, MY, MZ;
    short pitch, roll;
} OffloadACMData_t;
#pragma pack(pop)

/* MAVS4 Configuration Array */
#pragma pack(push,1)
typedef struct {
  long     SYS_CLOCK ;
  long     BaudRate ;
  short    VersionNumber ;
  short    Initialized ;          /* 42 => been configured at least once */
  short    /* Velocity */  V_offset[4] ;      /* velocity always enabled */
  float                    V_scale ;          /* cm/s */
  short    AnalogOut ;
  short    Compass,        M0_offset,  M1_offset,  M2_offset ;
  float                    M0_scale,   M1_scale,   M2_scale ;
  short    Tilt,           TY_offset,  TX_offset ;   /* y -> pitch, x -> roll */
  float                    TY_scale,   TX_scale ;    /* degrees */
  float                    TY_tempco,  TX_tempco ;   /* mV / degC */
  short    FastSensor ;
  short    Thermistor ;
  float                    Th_offset ;
  short    Pressure,       P_offset ;
  float                    P_scale ;
  float                    P_mA ;
  short    Auxiliary1,     A1_offset ;
  float                    A1_scale ;
  float                    A1_mA ;
  short    Auxiliary2,     A2_offset ;
  float                    A2_scale ;
  float                    A2_mA ;
  short    Auxiliary3,     A3_offset ;
  float                    A3_scale ;
  float                    A3_mA ;
  short    SensorOrientation ;
  long     SerialNumber ;
  char     QueryCharacter ;
  short    PowerUpTimeOut ;
} MAVS4_Config_t;
#pragma pack(pop)

int bWriteHeader = YES;

//function declarations
static void usage_err(char *str);
static short ReverseBytesShort(const short inShort);
//static int ReverseBytesInt( const int inInt );
static long ReverseBytesLong( const long inLong );
static float ReverseBytesFloat( const float inFloat );
void GetGMTDateTime(char *szDate, char *szTime, time_t *ptt);
int check_dir(char *dir);
int GetBaseName(char *filePath, char *baseName);
void ProfileTerminationMessage(short profile_termination_status, char *szStatusStr);
int UnpackENG (char *szInputFilename, char *szOutputDir);
int UnpackCTD (char *szInputFilename, char *szOutputDir);
int UnpackACM (char *szInputFilename, char *szOutputDir);
int UnpackAquaDop(char *szInputFilename, char *szOutputDir);
int ProcessRecordCTDOX(OffloadCTDOXData_t *data, float *c, float *t, float *d, short *ox);
int WriteTimeTags (char *szInputFilename, char *szOutputDir);
int WriteTimeTagsHeader(char *szOutputDir);

int main(int argc, char** argv) {

    int i = 0;
    char szFilename[256];

    //If no args, give usage
    if (argc < 3) {
        usage_err(argv[0]);
        return (EXIT_FAILURE);
    }

    // No need to check/create dir if specified as stdout
    if (strcmp(argv[1],"stdout") != 0) check_dir(argv[1]);


    if (strcmp(argv[argc - 1], "-t") == 0) {

        WriteTimeTagsHeader(argv[1]);
        for (i = 2; i < argc-1; i++) {
            GetBaseName(argv[i], szFilename);
            if (strlen(szFilename) == 12) {
                if (((int) szFilename[8] == '.') && ((int) szFilename[9] == 'D') && ((int) szFilename[10] == 'A') && ((int) szFilename[11] == 'T')) {
                    if ((int) szFilename[0] == 'E') WriteTimeTags(argv[i], argv[1]);
                    else printf("Error: Unknown file type, skipping\n");
                } else printf("Error: Unknown file type (bad file extension), skipping\n");
            } else printf("Error: Unknown file type (bad filename length), skipping\n");
        }

        return (EXIT_SUCCESS);

    }

    for (i = 2; i < argc; i++) {
        GetBaseName(argv[i], szFilename);
        if (strlen(szFilename) == 12) {
            if (((int) szFilename[8] == '.') && ((int) szFilename[9] == 'D') && ((int) szFilename[10] == 'A') && ((int) szFilename[11] == 'T')) {
                if ((int) szFilename[0] == 'E') UnpackENG(argv[i], argv[1]);
                else if ((int) szFilename[0] == 'C') UnpackCTD(argv[i], argv[1]);
                //else if ((int) szFilename[0] == 'A') UnpackACM(argv[i], argv[1]);
                else printf("Error: Unknown file type, skipping\n");
            } else if (((int) szFilename[8] == '.') && ((int) szFilename[9] == 'D') && ((int) szFilename[10] == 'E') && ((int) szFilename[11] == 'C')) {
                if ((int) szFilename[0] == 'A') UnpackAquaDop(argv[i], argv[1]);
            } else printf("Error: Unknown file type (bad file extension), skipping\n");
        } else printf("Error: Unknown file type (bad filename length), skipping\n");
    }

    return (EXIT_SUCCESS);
}

int UnpackENG (char *szInputFilename, char *szOutputDir){

    int nResult, done;
    int nProfileNumber = -1;
    long lRecordCount = 0;
    long message_type;
    char szDate[32];
    char szTime[32];
    char szStatus[128];
    char szOutputFilename[256];
    char szFilename[256];
    char szTmpFilename[256];
    char szLine[256];
    char *p;
    FILE *pInputFile, *pOutputFile;

    StartTimes_t start_times;
    ComponentFlags_t component_flags;
    OffloadProfileMessage_t offload_message;
    OffloadEngineeringData_t eng_data;

    pInputFile = fopen(szInputFilename, "rb");
    if (pInputFile == NULL) {
        perror("Error opening input file");
        return FAILURE;
    }

    GetBaseName(szInputFilename, szFilename);

    strcpy(szTmpFilename, szFilename);
    szTmpFilename[8] = 0; //terminate filename at decimal point so we can do atoi next
    

    // If Outputdir is specified as stdout, just write to stdout
    if (strcmp(szOutputDir,"stdout") == 0) {pOutputFile = stdout;}
    else {sprintf(szOutputFilename, "%s/%s.TXT", szOutputDir, szTmpFilename); //setup output file name
          pOutputFile = fopen(szOutputFilename, "wt");
	  }
    
    if (pOutputFile == NULL) {
        perror("Error opening output file");
        return FAILURE;
    }
    printf("Processing file: %s\n", szFilename);
    
    p = szTmpFilename;
    nProfileNumber = atoi(p+1); //skip first char (E,A,C)

    sprintf(szLine, "\n\n Profile %d\n\n", nProfileNumber);
    //printf("%s", szLine);
    fprintf(pOutputFile, "%s", szLine);

    nResult = fread (&component_flags,1,sizeof(component_flags),pInputFile);
    nResult = fread (&start_times,1,sizeof(start_times),pInputFile);

    start_times.sensor_start_time = ReverseBytesLong(start_times.sensor_start_time);
    start_times.profile_start_time = ReverseBytesLong(start_times.profile_start_time);

    GetGMTDateTime(szDate, szTime, &start_times.sensor_start_time);
    sprintf(szLine, " Sensors were turned on at  %s %s\n", szDate, szTime);
    //printf("%s", szLine);
    fprintf(pOutputFile, "%s", szLine);

    GetGMTDateTime(szDate, szTime, &start_times.profile_start_time);
    sprintf(szLine, " Vehicle began profiling at %s %s\n", szDate, szTime);
    //printf("%s", szLine);
    fprintf(pOutputFile, "%s", szLine);
    
    sprintf(szLine, "    Date      Time     [mA]    [V]     [dbar]      Par[mV]   scatSig   chlSig  CDOMSig\n");
    //printf("%s", szLine);
    fprintf(pOutputFile, "%s", szLine);

    done = NO;
    while (!done) {
        nResult = fread(&message_type, 1, sizeof (message_type), pInputFile);
        if (nResult != sizeof(message_type)) {done = YES; continue;} // read eof or error - set done yes to prevent infinite loop

        message_type = ReverseBytesLong(message_type);

        if (message_type == PROFILE_COMPLETE) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
            fclose(pInputFile);

            offload_message.ramp_status = ReverseBytesShort(offload_message.ramp_status);
            offload_message.profile_status = ReverseBytesShort(offload_message.profile_status);

            offload_message.profile_end_time = ReverseBytesLong(offload_message.profile_end_time);
            offload_message.sensor_end_time = ReverseBytesLong(offload_message.sensor_end_time);

            ProfileTerminationMessage(offload_message.ramp_status, szStatus);
            sprintf(szLine, "\n Ramp exit:    %s\n", szStatus);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            ProfileTerminationMessage(offload_message.profile_status, szStatus);
            sprintf(szLine, " Profile exit: %s\n", szStatus);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            GetGMTDateTime(szDate, szTime, &offload_message.profile_end_time);
            sprintf(szLine, "\n Vehicle motion stopped at %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            GetGMTDateTime(szDate, szTime, &offload_message.sensor_end_time);
            sprintf(szLine, " Sensor logging stopped at %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            fclose(pOutputFile);

            printf("Records processed: %ld\nENG file %s complete\n\n", lRecordCount, szFilename);

            done = YES;
            continue;
        } else if (message_type == PROFILE_RESTART) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
        } else if (message_type == RESTART_WITH_BACKTRACK) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
        } else if (message_type == PROFILE_NOT_COMPLETED) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
        } else if (message_type == DEPLOYMENT_STOPPED) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
            fclose(pInputFile);

            offload_message.ramp_status = ReverseBytesShort(offload_message.ramp_status);
            offload_message.profile_status = ReverseBytesShort(offload_message.profile_status);

            offload_message.profile_end_time = ReverseBytesLong(offload_message.profile_end_time);
            offload_message.sensor_end_time = ReverseBytesLong(offload_message.sensor_end_time);

            ProfileTerminationMessage(offload_message.ramp_status, szStatus);
            sprintf(szLine, "\n Ramp exit:    %s\n", szStatus);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            ProfileTerminationMessage(offload_message.profile_status, szStatus);
            sprintf(szLine, " Profile exit: %s\n", szStatus);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            GetGMTDateTime(szDate, szTime, &offload_message.profile_end_time);
            sprintf(szLine, "\n Vehicle motion stopped at %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            GetGMTDateTime(szDate, szTime, &offload_message.sensor_end_time);
            sprintf(szLine, " Sensor logging stopped at %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            fclose(pOutputFile);

            printf("Records processed: %ld\nENG file %s complete\n\n", lRecordCount, szFilename);
            
            done = YES;
            continue;
        } else if (message_type == TELEMETRY_RESTART)nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
        else {
            nResult = fread(&eng_data, 1, sizeof (eng_data), pInputFile);
            GetGMTDateTime(szDate, szTime, &message_type);
            eng_data.current = ReverseBytesFloat(eng_data.current);
            eng_data.voltage = ReverseBytesFloat(eng_data.voltage);
            eng_data.pressure = ReverseBytesFloat(eng_data.pressure);
            eng_data.Par_value = ReverseBytesFloat(eng_data.Par_value);
            eng_data.Puck_scatter = ReverseBytesShort(eng_data.Puck_scatter);
            eng_data.Puck_chl = ReverseBytesShort(eng_data.Puck_chl);
            eng_data.Puck_CDOM = ReverseBytesShort(eng_data.Puck_CDOM);
            sprintf(szLine, " %s %s %6.f %6.1f %10.3f %12.2f %8d %8d %8d\n", szDate, szTime, eng_data.current, eng_data.voltage, eng_data.pressure, eng_data.Par_value, eng_data.Puck_scatter, eng_data.Puck_chl, eng_data.Puck_CDOM);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);
            lRecordCount++;
        }
    }
return SUCCESS;
}

int UnpackCTD (char *szInputFilename, char *szOutputDir){

    int nResult, done;
    long lRecordCount = 0;
    int nProfileNumber = -1;
    char szDate[32];
    char szTime[32];
    char szOutputFilename[256];
    char szFilename[256];
    char szTmpFilename[256];
    char szLine[256];
    char *p;
    FILE *pInputFile, *pOutputFile;

    SensorTimes_t sensor_times;
    OffloadCTDOXData_t ctd_data;

    float cond, temp, depth;
    short oxy;

    pInputFile = fopen(szInputFilename, "rb");
    if (pInputFile == NULL) {
        perror("Error opening input file");
        return FAILURE;
    }

    GetBaseName(szInputFilename, szFilename);

    strcpy(szTmpFilename, szFilename);
    szTmpFilename[8] = 0; //terminate filename at decimal point so we can do atoi next

    // If Outputdir is specified as stdout, just write to stdout
    if (strcmp(szOutputDir,"stdout") == 0) {pOutputFile = stdout;}
    else {sprintf(szOutputFilename, "%s/%s.TXT", szOutputDir, szTmpFilename); //setup output file name
          pOutputFile = fopen(szOutputFilename, "wt");
	  }

    if (pOutputFile == NULL) {
        perror("Error opening output file");
        return FAILURE;
    }

    printf("Processing file: %s\n", szFilename);

    p = szTmpFilename;
    nProfileNumber = atoi(p+1); //skip first char (E,A,C)

    sprintf(szLine, "\n\nProfile %d\n\n", nProfileNumber);
    //printf("%s", szLine);
    fprintf(pOutputFile, "%s", szLine);

    sprintf(szLine, " mmho/cm   Celsius    dbars     hz\n\n");
    //printf("%s", szLine);
    fprintf(pOutputFile, "%s", szLine);

    done = NO;
    while (!done) {
        nResult = fread(&ctd_data, 1, sizeof (ctd_data), pInputFile);
        if (nResult != sizeof(ctd_data)) {done = YES; continue;} // read eof or error - set done yes to prevent infinite loop
        if ((ctd_data.C1 == 0xFF) && (ctd_data.C2 == 0xFF) && (ctd_data.C3 == 0xFF) && (ctd_data.T1 == 0xFF) && (ctd_data.T2 == 0xFF) && (ctd_data.T3 == 0xFF) && (ctd_data.D1 == 0xFF) && (ctd_data.D2 == 0xFF) && (ctd_data.D3 == 0xFF) && (ctd_data.X1 == 0xFF) && (ctd_data.X2 == 0xFF)) {
            nResult = fread(&sensor_times, 1, sizeof (sensor_times), pInputFile);

            sprintf(szLine, "\n\nProfile %d\n", nProfileNumber);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            sensor_times.sensor_on_time = ReverseBytesLong(sensor_times.sensor_on_time);
            GetGMTDateTime(szDate, szTime, &sensor_times.sensor_on_time);
            sprintf(szLine, "\nCTD turned on at  %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            sensor_times.sensor_off_time = ReverseBytesLong(sensor_times.sensor_off_time);
            GetGMTDateTime(szDate, szTime, &sensor_times.sensor_off_time);
            sprintf(szLine, "CTD turned off at %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFile, "%s", szLine);

            fclose(pOutputFile);

            printf("Records processed: %ld\nCTD file %s complete\n\n", lRecordCount, szFilename);

            done = YES;
            continue;
        }
        ProcessRecordCTDOX(&ctd_data, &cond, &temp, &depth, &oxy);
        sprintf(szLine, "%+8.4f  %+08.4f  %+09.3f  %d\n", cond, temp, depth, oxy);
        //printf("%s", szLine);
        fprintf(pOutputFile, "%s", szLine);
        lRecordCount++;
    }

    return SUCCESS;
}

int ProcessRecordCTDOX(OffloadCTDOXData_t *data, float *c, float *t, float *d, short *ox){

    long tmpLong;
    short tmpShort;

    tmpLong = 0L;
    tmpLong = tmpLong + (data->C1 << 16);
    tmpLong = tmpLong + (data->C2 << 8);
    tmpLong = tmpLong + (data->C3);

    *c = ((float)tmpLong/10000.0) - 0.5;

    tmpLong = 0L;
    tmpLong = tmpLong + (data->T1 << 16);
    tmpLong = tmpLong + (data->T2 << 8);
    tmpLong = tmpLong + (data->T3);

    *t = ((float)tmpLong/10000.0) - 5.0;

    tmpLong = 0L;
    tmpLong = tmpLong + (data->D1 << 16);
    tmpLong = tmpLong + (data->D2 << 8);
    tmpLong = tmpLong + (data->D3);

    *d = ((float)tmpLong/100.0) - 10.0;

    tmpShort = 0;
    tmpShort = tmpShort + (data->X1 << 8);
    tmpShort = tmpShort + (data->X2);

    *ox = tmpShort;

    return SUCCESS;

}

int UnpackAquaDop(char *szInputFilename, char *szOutputDir) {

    int bNullRecordFound = NO;
    int i = 0;
    int nResult, done;
    int nProfileNumber = -1;
    long lRecordCount = 0;
    char szDate[32];
    char szTime[32];
    char szOutputFilenameText[256];
    char szFilename[256];
    char szTmpFilename[256];
    char szLine[256];
    char tmpStr[256];
    char *p;

    FILE *pInputFile, *pOutputFileText;

    unsigned short year;
    unsigned char month;
    unsigned char day;
    unsigned char hour;
    unsigned char minute;
    unsigned char seconds;

    unsigned short soundspeed;
    short temperature;
    unsigned short heading;
    short pitch;
    short roll;
    short magX;
    short magY;
    short magZ;
    unsigned char beams; //number of beams for this record
    unsigned char cells; //number of cells for this record
    unsigned char beam1; //which beam is in Velocity0
    unsigned char beam2; //which beam is in Velocity1
    unsigned char beam3; //which beam is in Velocity2
    unsigned char beam4;
    unsigned char beam5;
    float VelocityScale;
    short Velocity0;
    short Velocity1;
    short Velocity2;
    unsigned char Amplitude0;
    unsigned char Amplitude1;
    unsigned char Amplitude2;
    unsigned char Correlation0;
    unsigned char Correlation1;
    unsigned char Correlation2;

    SensorTimes_t sensor_times;
    unsigned char Selection[26];
    
    float fVScale = 0.0;

    pInputFile = fopen(szInputFilename, "rb");
    if (pInputFile == NULL) {
        perror("Error opening input file");
        return FAILURE;
    }

    GetBaseName(szInputFilename, szFilename);

    strcpy(szTmpFilename, szFilename);
    szTmpFilename[8] = 0; //terminate filename at decimal point so we can do atoi next


    // If Outputdir is specified as stdout, just write to stdout
    if (strcmp(szOutputDir,"stdout") == 0) {pOutputFileText = stdout;}
    else {sprintf(szOutputFilenameText, "%s/%s.TXT", szOutputDir, szTmpFilename); //setup output file name
          pOutputFileText = fopen(szOutputFilenameText, "wt");
	  }

    if (pOutputFileText == NULL) {
        perror("Error opening output file");
        return FAILURE;
    }

    printf("Processing file: %s\n", szFilename);

    p = szTmpFilename;
    nProfileNumber = atoi(p + 1); //skip first char (E,A,C)

    sprintf(szLine, "\n\nProfile %d\n\n", nProfileNumber);
    //printf("%s", szLine);
    fprintf(pOutputFileText, "%s", szLine);

    nResult = fread(Selection, 1, 26, pInputFile);

    int BytesInOneRecord = 0;
    if (Selection[0]) {
        BytesInOneRecord += 6;
        strcpy(szLine,"MM-DD-YYYY HH:MM:SS");
    }
    if (Selection[1]) {
        BytesInOneRecord += 2;
        strcat(szLine,",Sndm/s");
    }
    if (Selection[2]) {
        BytesInOneRecord += 2;
        strcat(szLine,",TmpC");
    }
    if (Selection[3]) {
        BytesInOneRecord += 2;
        strcat(szLine,",Heading");
    }
    if (Selection[4]) {
        BytesInOneRecord += 2;
        strcat(szLine,",Pitch");
    }
    if (Selection[5]) {
        BytesInOneRecord += 2;
        strcat(szLine,",Roll");
    }
    if (Selection[6]) {
        BytesInOneRecord += 2;
        strcat(szLine,",magnHx");
    }
    if (Selection[7]) {
        BytesInOneRecord += 2;
        strcat(szLine,",magnHy");
    }
    if (Selection[8]) {
        BytesInOneRecord += 2;
        strcat(szLine,",magnHz");
    }
    if (Selection[9]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Beams");
    }
    if (Selection[10]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Cells");
    }
    if (Selection[11]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Beam1");
    }
    if (Selection[12]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Beam2");
    }
    if (Selection[13]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Beam3");
    }
    if (Selection[14]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Beam4");
    }
    if (Selection[15]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Beam5");
    }
    if (Selection[16]) {
        BytesInOneRecord += 1;
        //strcat(szLine,",Vscale");
    }
    if (Selection[17]) {
        BytesInOneRecord += 2;
        strcat(szLine,",Vel[0,0]");
    }
    if (Selection[18]) {
        BytesInOneRecord += 2;
        strcat(szLine,",Vel[1,0]");
    }
    if (Selection[19]) {
        BytesInOneRecord += 2;
        strcat(szLine,",Vel[2,0]");
    }
    if (Selection[20]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Amp[0,0]");
    }
    if (Selection[21]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Amp[1,0]");
    }
    if (Selection[22]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Amp[2,0]");
    }
    if (Selection[23]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Corr[0,0]");
    }
    if (Selection[24]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Corr[1,0]");
    }
    if (Selection[25]) {
        BytesInOneRecord += 1;
        strcat(szLine,",Corr[2,0]");
    }
    strcat(szLine,"\n\n");
    //printf("%s", szLine);
    fprintf(pOutputFileText, "%s", szLine);

    unsigned char Buffer[BytesInOneRecord];

    done = NO;
    while (!done) {

        nResult = fread(Buffer, 1, BytesInOneRecord, pInputFile);
        if (nResult != sizeof(Buffer)) {done = YES; continue;} // read eof or error - set done yes to prevent infinite loop
        bNullRecordFound = YES;
        for (i=0;i<BytesInOneRecord;i++){
            if ((int)Buffer[i] == 0) continue;
            else {
                bNullRecordFound = NO;
                break;
            }
        }
        
        if (bNullRecordFound) {
            nResult = fread(&sensor_times, 1, sizeof (sensor_times), pInputFile);

            sensor_times.sensor_on_time = ReverseBytesLong(sensor_times.sensor_on_time);
            GetGMTDateTime(szDate, szTime, &sensor_times.sensor_on_time);
            sprintf(szLine, "\nAquaDopp2 turned on at  %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFileText, "%s", szLine);

            sensor_times.sensor_off_time = ReverseBytesLong(sensor_times.sensor_off_time);
            GetGMTDateTime(szDate, szTime, &sensor_times.sensor_off_time);
            sprintf(szLine, "AquaDopp2 turned off at %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFileText, "%s", szLine);

            printf("Records processed: %ld\nAquaDopp2 file %s complete\n\n", lRecordCount, szFilename);

            fclose(pOutputFileText);

            done = YES;
            continue;
        }
//mclane output
//.DEC file
//MM-DD-YYYY HH:MM:SS,TmpC,Heading,Pitch,Roll,Vel[0,0],Vel[1,0],Vel[2,0],Amp[0,0],Amp[1,0],Amp[2,0]
//04-04-2013 09:15:18,19.1500,307.1000,0.8000,57.9000,-4.93100,-21.94200,-0.00100,071,057,063
//.DAT full file       
//MM-DD-YYYY HH:MM:SS,Sndm/s,TmpC,Heading,Pitch,Roll,magnHx,magnHy,magnHz,Beams,Cells,Beam1,Beam2,Beam3,Beam4,Beam5,Vel[0,0],Vel[1,0],Vel[2,0],Amp[0,0],Amp[1,0],Amp[2,0],Corr[0,0],Corr[1,0],Corr[2,0]
//04-04-2013 09:15:18.375,1500.0000,19.1500,307.1000,0.8000,57.9000,8398,-13790,211,3,1,2,3,4,0,0,-4.93100,-21.94200,-0.00100,071,057,063,062,027,065

        i = 0;
        if (Selection[0]) {
            year = Buffer[i++]+1900;
            month = Buffer[i++]+1;
            day = Buffer[i++];
            hour = Buffer[i++];
            minute = Buffer[i++];
            seconds = Buffer[i++];
            sprintf(tmpStr, "%02d-%02d-%04d %02d:%02d:%02d",month,day,year,hour,minute,seconds);
            strcpy(szLine,tmpStr);
        }
        if (Selection[1]) {
            soundspeed = (unsigned short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%2.4f",(float)soundspeed/10.0);
            strcat(szLine,tmpStr);
        }
        if (Selection[2]) {
            temperature = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%2.4f",(float)temperature/100.0);
            strcat(szLine,tmpStr);
        }
        if (Selection[3]) {
            heading = (unsigned short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%2.4f",(float)heading/10.0);
            strcat(szLine,tmpStr);
        }
        if (Selection[4]) {
            pitch = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%2.4f",(float)pitch/10.0);
            strcat(szLine,tmpStr);
        }
        if (Selection[5]) {
            roll = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%2.4f",(float)roll/10.0);
            strcat(szLine,tmpStr);
        }
        if (Selection[6]) {
            magX = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]);  
            i+=2;
            sprintf(tmpStr, ",%d",magX);
            strcat(szLine,tmpStr);
        }
        if (Selection[7]) {
            magY = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%d",magY);
            strcat(szLine,tmpStr);
        }
        if (Selection[8]) {
            magZ = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%d",magZ);
            strcat(szLine,tmpStr);
        }
        if (Selection[9]) {
            beams = Buffer[i++];
            sprintf(tmpStr, ",%d",beams);
            strcat(szLine,tmpStr);
        }
        if (Selection[10]) {
            cells = Buffer[i++];
            sprintf(tmpStr, ",%d",cells);
            strcat(szLine,tmpStr);
        }
        if (Selection[11]) {
            beam1 = Buffer[i++];
            sprintf(tmpStr, ",%d",beam1);
            strcat(szLine,tmpStr);
        }
        if (Selection[12]) {
            beam2 = Buffer[i++];
            sprintf(tmpStr, ",%d",beam2);
            strcat(szLine,tmpStr);
        }
        if (Selection[13]) {
            beam3 = Buffer[i++];
            sprintf(tmpStr, ",%d",beam3);
            strcat(szLine,tmpStr);
        }
        if (Selection[14]) {
            beam4 = Buffer[i++];
            sprintf(tmpStr, ",%d",beam4);
            strcat(szLine,tmpStr);
        }
        if (Selection[15]) {
            beam5 = Buffer[i++];
            sprintf(tmpStr, ",%d",beam5);
            strcat(szLine,tmpStr);
        }
        if (Selection[16]) {
            VelocityScale = (signed char)Buffer[i++];
            //sprintf(tmpStr, ",%f",VelocityScale);
            //strcat(szLine,tmpStr);
            fVScale = powf(10.0, VelocityScale);
        }
        if (Selection[17]) {
            Velocity0 = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]);  
            i+=2;
            sprintf(tmpStr, ",%2.5f",(float)Velocity0 * fVScale);
            strcat(szLine,tmpStr);
        }
        if (Selection[18]) {
            Velocity1 = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%2.5f",(float)Velocity1 * fVScale);
            strcat(szLine,tmpStr);
        }
        if (Selection[19]) {
            Velocity2 = (short)((((short)Buffer[i+1])<<8)+(short)Buffer[i]); 
            i+=2;
            sprintf(tmpStr, ",%2.5f",(float)Velocity2 * fVScale);
            strcat(szLine,tmpStr);
        }
        if (Selection[20]) {
            Amplitude0 = Buffer[i++];
            sprintf(tmpStr, ",%03d",Amplitude0);
            strcat(szLine,tmpStr);
        }
        if (Selection[21]) {
            Amplitude1 = Buffer[i++];
            sprintf(tmpStr, ",%03d",Amplitude1);
            strcat(szLine,tmpStr);
        }
        if (Selection[22]) {
            Amplitude2 = Buffer[i++];
            sprintf(tmpStr, ",%03d",Amplitude2);
            strcat(szLine,tmpStr);
        }
        if (Selection[23]) {
            Correlation0 = Buffer[i++];
            sprintf(tmpStr, ",%03d",Correlation0);
            strcat(szLine,tmpStr);
        }
        if (Selection[24]) {
            Correlation1 = Buffer[i++];
            sprintf(tmpStr, ",%03d",Correlation1);
            strcat(szLine,tmpStr);
        }
        if (Selection[25]) {
            Correlation2 = Buffer[i++];
            sprintf(tmpStr, ",%03d",Correlation2);
            strcat(szLine,tmpStr);
        }
        strcat(szLine,"\n");
        
        //printf("%s", szLine);
        fprintf(pOutputFileText, "%s", szLine);
        lRecordCount++;
    }

    return SUCCESS;
}

int UnpackACM (char *szInputFilename, char *szOutputDir){

    int nResult, done;
    int nProfileNumber = -1;
    long lRecordCount = 0;
    char szDate[32];
    char szTime[32];
    char szOutputFilenameBin[256];
    char szOutputFilenameText[256];
    char szOutputFilenameParms[256];
    char szConfigFilename[256];
    char szFilename[256];
    char szTmpFilename[256];
    char szLine[256];
    char *p, *pp;
    FILE *pInputFile, *pOutputFileBin, *pOutputFileText, *pConfigFile, *pParmsFile;

    float fVA, fVB, fVC, fVD;
    float fNorm, fMX, fMY, fMZ;
    float fPitch, fRoll;

/*
    const float fVelocityScale = 0.0056066;
    const float fPitchScale = 0.01217;
    const float fRollScale = 0.01223;
*/
    float fVelocityScale = 0.0;
    float fPitchScale = 0.0;
    float fRollScale = 0.0;
    const float pi = 3.14159265;

    SensorTimes_t sensor_times;
    OffloadACMData_t acm_data;
    MAVS4_Config_t mavs_config;

    unsigned short BytesPerRec; //number of bytes per record
    unsigned short RecPerProf; //number of records per profile

    GetBaseName(szInputFilename, szFilename);
    strcpy(szConfigFilename, szInputFilename);

    pp = strstr(szConfigFilename, szFilename);
    *pp = 0;
    strcat(szConfigFilename, "CONFIG.BIN");

    pConfigFile = fopen(szConfigFilename, "rb");
    if (pConfigFile == NULL) {
        perror("Error opening mavs config file");
        return FAILURE;
    }

    nResult = fread(&mavs_config, 1, sizeof (mavs_config), pConfigFile);
    fclose(pConfigFile);

    mavs_config.V_scale = ReverseBytesFloat(mavs_config.V_scale);
    mavs_config.TX_scale = ReverseBytesFloat(mavs_config.TX_scale);
    mavs_config.TY_scale = ReverseBytesFloat(mavs_config.TY_scale);
    mavs_config.SerialNumber = ReverseBytesLong(mavs_config.SerialNumber);

    fVelocityScale = mavs_config.V_scale * 2.0;

    fPitchScale = mavs_config.TY_scale;
    fRollScale = mavs_config.TX_scale;

    sprintf(szOutputFilenameParms, "%s/parameters.txt", szOutputDir);
    pParmsFile = fopen(szOutputFilenameParms, "wt");
    if (pParmsFile == NULL) {
        perror("Error opening mavs parms file");
        return FAILURE;
    }
    sprintf(szLine, "MAVS Unpacking Parameters:\nSerial Number: %ld\n", mavs_config.SerialNumber);
    fprintf(pParmsFile, "%s", szLine);
    sprintf(szLine, "Velocity Scale: %09.7f\nPitch Scale: %07.5f\nRoll Scale: %07.5f\n", fVelocityScale, fPitchScale, fRollScale);
    fprintf(pParmsFile, "%s", szLine);
    fclose(pParmsFile);

    pInputFile = fopen(szInputFilename, "rb");
    if (pInputFile == NULL) {
        perror("Error opening input file");
        return FAILURE;
    }

    GetBaseName(szInputFilename, szFilename);

    strcpy(szTmpFilename, szFilename);
    szTmpFilename[8] = 0; //terminate filename at decimal point so we can do atoi next

    sprintf(szOutputFilenameBin, "%s/%s.BIN", szOutputDir, szTmpFilename); //setup output file name
    pOutputFileBin = fopen(szOutputFilenameBin, "wb");
    if (pOutputFileBin == NULL) {
        perror("Error opening output file");
        return FAILURE;
    }

    sprintf(szOutputFilenameText, "%s/%s.TXT", szOutputDir, szTmpFilename); //setup output file name
    pOutputFileText = fopen(szOutputFilenameText, "wt");
    if (pOutputFileText == NULL) {
        perror("Error opening output file");
        return FAILURE;
    }

    printf("Processing file: %s\n", szFilename);

    p = szTmpFilename;
    nProfileNumber = atoi(p+1); //skip first char (E,A,C)

    sprintf(szLine, "\n\nProfile %d\n\n", nProfileNumber);
    //printf("%s", szLine);
    fprintf(pOutputFileText, "%s", szLine);

    sprintf(szLine, "VA(cm/s)  VB(cm/s)  VC(cm/s)  VD(cm/s)  MX(norm)  MY(norm)  MZ(norm)  PT(deg)  RL(deg)\n");
    //printf("%s", szLine);
    fprintf(pOutputFileText, "%s", szLine);

    nResult = fread(&BytesPerRec, 1, 2, pInputFile);
    nResult = fread(&RecPerProf, 1, 2, pInputFile);
    BytesPerRec = ReverseBytesShort(BytesPerRec);
    RecPerProf = ReverseBytesShort(RecPerProf);
    //printf("BytesPerRec=%d, RecPerProf=%d\n", BytesPerRec, RecPerProf);

    done = NO;
    while (!done) {

        nResult = fread(&acm_data, 1, sizeof (acm_data), pInputFile);
        if ((acm_data.velocityA == (short)0xFFFF) && (acm_data.velocityB == (short)0xFFFF) && (acm_data.velocityC == (short)0xFFFF) && (acm_data.velocityD == (short)0xFFFF)
                && (acm_data.MX == (short)0xFFFF) && (acm_data.MY == (short)0xFFFF) && (acm_data.MZ == (short)0xFFFF) && (acm_data.pitch == (short)0xFFFF) && (acm_data.roll == (short)0xFFFF)) {
            nResult = fread(&sensor_times, 1, sizeof (sensor_times), pInputFile);

            sensor_times.sensor_on_time = ReverseBytesLong(sensor_times.sensor_on_time);
            GetGMTDateTime(szDate, szTime, &sensor_times.sensor_on_time);
            sprintf(szLine, "\nACM turned on at  %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFileText, "%s", szLine);

            sensor_times.sensor_off_time = ReverseBytesLong(sensor_times.sensor_off_time);
            GetGMTDateTime(szDate, szTime, &sensor_times.sensor_off_time);
            sprintf(szLine, "ACM turned off at %s %s\n", szDate, szTime);
            //printf("%s", szLine);
            fprintf(pOutputFileText, "%s", szLine);

            printf("Records processed: %ld\nACM file %s complete\n\n", lRecordCount, szFilename);

            fclose(pOutputFileBin);
            fclose(pOutputFileText);

            done = YES;
            continue;
        }
        fwrite(&acm_data, 1, sizeof (acm_data), pOutputFileBin);
        acm_data.velocityA = ReverseBytesShort(acm_data.velocityA);
        acm_data.velocityB = ReverseBytesShort(acm_data.velocityB);
        acm_data.velocityC = ReverseBytesShort(acm_data.velocityC);
        acm_data.velocityD = ReverseBytesShort(acm_data.velocityD);
        acm_data.MX = ReverseBytesShort(acm_data.MX);
        acm_data.MY = ReverseBytesShort(acm_data.MY);
        acm_data.MZ = ReverseBytesShort(acm_data.MZ);
        acm_data.pitch = ReverseBytesShort(acm_data.pitch);
        acm_data.roll = ReverseBytesShort(acm_data.roll);

        fVA = ((float)acm_data.velocityA) * fVelocityScale;
        fVB = ((float)acm_data.velocityB) * fVelocityScale;
        fVC = ((float)acm_data.velocityC) * fVelocityScale;
        fVD = ((float)acm_data.velocityD) * fVelocityScale;

        fMX = (float)acm_data.MX;
        fMY = (float)acm_data.MY;
        fMZ = (float)acm_data.MZ;

        fNorm = (float)(sqrt((fMX * fMX) + (fMY * fMY) + (fMZ * fMZ))) ;
        fMX = fMX / fNorm ;
        fMY = fMY / fNorm ;
        fMZ = fMZ / fNorm ;

        //tilt next
        fPitch = -asin((((acm_data.pitch * fPitchScale) / 5.0) / 10.0));//tilt average = 5.0, tilt scale = 10.0 from upp.m
        fRoll  = -asin((((acm_data.roll  * fRollScale)  / 5.0) / 10.0));//tilt average = 5.0, tilt scale = 10.0 from upp.m

        fPitch = fPitch * (180 / pi) ;
        fRoll  = fRoll  * (180 / pi) ;

        //-17.23  -20.35   11.14   11.03  0.1540  0.4919  0.8569  -1.1   0.1
        sprintf(szLine, "%6.2f %6.2f %6.2f %6.2f %06.4f %06.4f %06.4f %03.1f %03.1f\n", fVA, fVB, fVC, fVD, fMX, fMY, fMZ, fPitch, fRoll);
        //printf("%s", szLine);
        fprintf(pOutputFileText, "%s", szLine);
        lRecordCount++;
    }

    return SUCCESS;
}

int WriteTimeTags (char *szInputFilename, char *szOutputDir){

    int nResult, done, bGotFirstPressure;
    int nProfileNumber = -1;
    long lRecordCount = 0;
    long message_type;
    char szDate[32];
    char szTime[32];
    char szStatus[128];
    char szOutputFilename[256];
    char szFilename[256];
    char szTmpFilename[256];
    char *p;
    float fFirstPressure = 0.0;
    float fLastPressure = 0.0;
    FILE *pInputFile, *pOutputFile;

    StartTimes_t start_times;
    ComponentFlags_t component_flags;
    OffloadProfileMessage_t offload_message;
    OffloadEngineeringData_t eng_data;
    
    bGotFirstPressure = NO;

    pInputFile = fopen(szInputFilename, "rb");
    if (pInputFile == NULL) {
        perror("Error opening input file");
        return FAILURE;
    }

    GetBaseName(szInputFilename, szFilename);

    strcpy(szTmpFilename, szFilename);
    szTmpFilename[8] = 0; //terminate filename at decimal point so we can do atoi next

    sprintf(szOutputFilename, "%s/TIMETAGS2.TXT", szOutputDir); //setup output file name

    pOutputFile = fopen(szOutputFilename, "at");
    if (pOutputFile == NULL) {
        perror("Error opening output file");
        return FAILURE;
    }
    printf("Processing file: %s\n", szFilename);

    p = szTmpFilename;
    nProfileNumber = atoi(p+1); //skip first char (E,A,C)
    fprintf(pOutputFile, " %07d  ", nProfileNumber);

    nResult = fread (&component_flags,1,sizeof(component_flags),pInputFile);
    nResult = fread (&start_times,1,sizeof(start_times),pInputFile);

    start_times.sensor_start_time = ReverseBytesLong(start_times.sensor_start_time);
    start_times.profile_start_time = ReverseBytesLong(start_times.profile_start_time);

    GetGMTDateTime(szDate, szTime, &start_times.sensor_start_time);
    //sprintf(szLine, "%s %s  ", szDate, szTime);
    fprintf(pOutputFile, "%s %s  ", szDate, szTime);

    GetGMTDateTime(szDate, szTime, &start_times.profile_start_time);
    //sprintf(szLine, "%s %s  ", szDate, szTime);
    fprintf(pOutputFile, "%s %s  ", szDate, szTime);

    done = NO;
    while (!done) {
        nResult = fread(&message_type, 1, sizeof (message_type), pInputFile);

        message_type = ReverseBytesLong(message_type);

        if (message_type == PROFILE_COMPLETE) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
            fclose(pInputFile);

            offload_message.ramp_status = ReverseBytesShort(offload_message.ramp_status);
            offload_message.profile_status = ReverseBytesShort(offload_message.profile_status);

            offload_message.profile_end_time = ReverseBytesLong(offload_message.profile_end_time);
            offload_message.sensor_end_time = ReverseBytesLong(offload_message.sensor_end_time);

            GetGMTDateTime(szDate, szTime, &offload_message.profile_end_time);
            //sprintf(szLine, "%s %s  ", szDate, szTime);
            fprintf(pOutputFile, "%s %s  ", szDate, szTime);

            GetGMTDateTime(szDate, szTime, &offload_message.sensor_end_time);
            //sprintf(szLine, "%s %s         ", szDate, szTime);
            fprintf(pOutputFile, "%s %s         ", szDate, szTime);

            ProfileTerminationMessage(offload_message.ramp_status, szStatus);
            //sprintf(szLine, "%d  %s        ", offload_message.ramp_status, szStatus);
            fprintf(pOutputFile, "%d  [%s]        ", offload_message.ramp_status, szStatus);

            ProfileTerminationMessage(offload_message.profile_status, szStatus);
            //sprintf(szLine, "%d  %s\n", offload_message.profile_status, szStatus);
            fprintf(pOutputFile, "%d  [%s]  %10.3f  %10.3f\n", offload_message.profile_status, szStatus, fFirstPressure, fLastPressure);

            fclose(pOutputFile);

            printf("Records processed: %ld\nENG file %s complete\n\n", lRecordCount, szFilename);

            done = YES;
            continue;
        } else if (message_type == PROFILE_RESTART) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
        } else if (message_type == RESTART_WITH_BACKTRACK) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
        } else if (message_type == PROFILE_NOT_COMPLETED) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
        } else if (message_type == DEPLOYMENT_STOPPED) {
            nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
            fclose(pInputFile);

            offload_message.ramp_status = ReverseBytesShort(offload_message.ramp_status);
            offload_message.profile_status = ReverseBytesShort(offload_message.profile_status);

            offload_message.profile_end_time = ReverseBytesLong(offload_message.profile_end_time);
            offload_message.sensor_end_time = ReverseBytesLong(offload_message.sensor_end_time);

            GetGMTDateTime(szDate, szTime, &offload_message.profile_end_time);
            //sprintf(szLine, "%s %s  ", szDate, szTime);
            fprintf(pOutputFile, "%s %s  ", szDate, szTime);

            GetGMTDateTime(szDate, szTime, &offload_message.sensor_end_time);
            //sprintf(szLine, "%s %s         ", szDate, szTime);
            fprintf(pOutputFile, "%s %s         ", szDate, szTime);

            ProfileTerminationMessage(offload_message.ramp_status, szStatus);
            //sprintf(szLine, "%d  %s        ", offload_message.ramp_status, szStatus);
            fprintf(pOutputFile, "%d  [%s]        ", offload_message.ramp_status, szStatus);

            ProfileTerminationMessage(offload_message.profile_status, szStatus);
            //sprintf(szLine, "%d  %s\n", offload_message.profile_status, szStatus);
            fprintf(pOutputFile, "%d  [%s]  %10.3f\n", offload_message.profile_status, szStatus, fLastPressure);

            fclose(pOutputFile);

            printf("Records processed: %ld\nENG file %s complete\n\n", lRecordCount, szFilename);

            done = YES;
            continue;
        } else if (message_type == TELEMETRY_RESTART)nResult = fread(&offload_message, 1, sizeof (offload_message), pInputFile);
        else {
            nResult = fread(&eng_data, 1, sizeof (eng_data), pInputFile);
            //GetGMTDateTime(szDate, szTime, &message_type);
            //eng_data.current = ReverseBytesFloat(eng_data.current);
            //eng_data.voltage = ReverseBytesFloat(eng_data.voltage);
            eng_data.pressure = ReverseBytesFloat(eng_data.pressure);
            fLastPressure = eng_data.pressure;

            if (!bGotFirstPressure) {
                if (fLastPressure > 0.0) {
                    fFirstPressure = fLastPressure;
                    bGotFirstPressure = YES;
                }
            }
            
            //eng_data.Par_value = ReverseBytesFloat(eng_data.Par_value);
            //eng_data.Puck_scatter = ReverseBytesShort(eng_data.Puck_scatter);
            //eng_data.Puck_chl = ReverseBytesShort(eng_data.Puck_chl);
            //eng_data.Puck_CDOM = ReverseBytesShort(eng_data.Puck_CDOM);
            //sprintf(szLine, " %s %s %6.f %6.1f %10.3f %12.2f %8d %8d %8d\n", szDate, szTime, eng_data.current, eng_data.voltage, eng_data.pressure, eng_data.Par_value, eng_data.Puck_scatter, eng_data.Puck_chl, eng_data.Puck_CDOM);
            //printf("%s", szLine);
            //fprintf(pOutputFile, "%s", szLine);
            lRecordCount++;
        }
    }
return SUCCESS;
}

int WriteTimeTagsHeader(char *szOutputDir) {

    char szOutputFilename[256];
    FILE *pOutputFile;

    sprintf(szOutputFilename, "%s/TIMETAGS2.TXT", szOutputDir); //setup output file name

    pOutputFile = fopen(szOutputFilename, "wt");
    if (pOutputFile == NULL) {
        perror("Error opening output file");
        return FAILURE;
    }
    printf("Writing TIMETAGS2.TXT Header\n");

    fprintf(pOutputFile, "Profile Termination Log\n\n");
    fprintf(pOutputFile, " Profile  Sensor On            Motion Start         Motion Stop          Sensor Off                  Ramp Status                Profile Status       StartDepth   EndDepth\n");
    fprintf(pOutputFile, " -------  -------------------  -------------------  -------------------  -------------------         -----------                --------------       ----------   --------\n");
    fclose(pOutputFile);
    return SUCCESS;

}

static void usage_err(char *str) {
    printf("\n%s %s\nUsage: %s <output dir> <input file> [-t]\n", str, VERSION, str);
    printf("\tWhere:\t[-t] option is used to create TIMETAGS2.TXT file\n");
    printf("\t\tmust specify eng file output dir and E*.DAT wildcard list for input files\n");
    printf("\t\tNote: outdir can be specified as 'stdout'\n");
}

static short ReverseBytesShort(const short inShort) {
    short retVal;
    char *shortToConvert = (char*) & inShort;
    char *returnShort = (char*) & retVal;

    // swap the bytes into a temporary buffer
    returnShort[0] = shortToConvert[1];
    returnShort[1] = shortToConvert[0];

    return retVal;
}

/*
static int ReverseBytesInt(const int inInt) {
    long retVal;
    char *intToConvert = (char*) & inInt;
    char *returnInt = (char*) & retVal;

    // swap the bytes into a temporary buffer
    returnInt[0] = intToConvert[3];
    returnInt[1] = intToConvert[2];
    returnInt[2] = intToConvert[1];
    returnInt[3] = intToConvert[0];

    return retVal;
}
*/

static long ReverseBytesLong(const long inLong) {
    long retVal;
    char *longToConvert = (char*) & inLong;
    char *returnLong = (char*) & retVal;

    // swap the bytes into a temporary buffer
    returnLong[0] = longToConvert[3];
    returnLong[1] = longToConvert[2];
    returnLong[2] = longToConvert[1];
    returnLong[3] = longToConvert[0];

    return retVal;
}

static float ReverseBytesFloat(const float inFloat) {
    float retVal;
    char *floatToConvert = (char*) & inFloat;
    char *returnFloat = (char*) & retVal;

    // swap the bytes into a temporary buffer
    returnFloat[0] = floatToConvert[3];
    returnFloat[1] = floatToConvert[2];
    returnFloat[2] = floatToConvert[1];
    returnFloat[3] = floatToConvert[0];

    return retVal;
}

void GetGMTDateTime(char *szDate, char *szTime, time_t *ptt) {
    struct tm *tmDT;

    tzset();
    if (ptt)
        tmDT = gmtime(ptt);
    else {
        time_t tt;
        time(&tt);

        tmDT = gmtime(&tt);
    }

    sprintf(szDate, "%02d/%02d/%04d", tmDT->tm_mon + 1, tmDT->tm_mday, tmDT->tm_year + 1900);
    sprintf(szTime, "%02d:%02d:%02d", tmDT->tm_hour, tmDT->tm_min, tmDT->tm_sec);
}

int check_dir(char *dir) {
    DIR *pdir;
    if ((pdir = opendir(dir)) != NULL) {
        closedir(pdir);
        return 1;
    }
    return (mkdir(dir, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH));
}

int GetBaseName(char *filePath, char *baseName) {
    int i;
    char *p;

    i = strlen(filePath);
    p = &filePath[i];
    // extract the bare filename
    if (i == 0) return FAILURE;

    while (*--p != '/') {
        if (--i == 0) break;
    }
    if (i != 0) {
        p++;
        strcpy(baseName, p);
    } else strcpy(baseName, filePath); //this means all we have is a filname that must be in the same directory as the executable

    return SUCCESS;
}

void ProfileTerminationMessage(short profile_termination_status, char *szStatusStr) {
    switch (profile_termination_status) {
        case SMOOTH_RUNNING: strcpy(szStatusStr, "SMOOTH RUNNING");
            break;
        case OPERATOR_CTRL_C: strcpy(szStatusStr, "OPERATOR CTRL C");
            break;
        case TT8_COMM_FAILURE: strcpy(szStatusStr, "TT8 COMM FAILURE");
            break;
        case CTD_COMM_FAILURE: strcpy(szStatusStr, "CTD COMM FAILURE");
            break;
        case TIMER_EXPIRED: strcpy(szStatusStr, "TIMER EXPIRED");
            break;
        case MIN_BATTERY: strcpy(szStatusStr, "MIN BATTERY");
            break;
        case MAX_MOTOR_CURRENT: strcpy(szStatusStr, "MAX MOTOR CURRENT");
            break;
        case TOP_PRESSURE: strcpy(szStatusStr, "TOP PRESSURE");
            break;
        case BOTTOM_PRESSURE: strcpy(szStatusStr, "BOTTOM PRESSURE");
            break;
        case PRESSURE_RATE_ZERO: strcpy(szStatusStr, "PRESSURE RATE ZERO");
            break;
        case STOP_NULL: strcpy(szStatusStr, "STOP NULL");
            break;
        case FLASH_CARD_FULL: strcpy(szStatusStr, "FLASH CARD FULL");
            break;
        case FILE_SYSTEM_FULL: strcpy(szStatusStr, "FILE SYSTEM FULL");
            break;
        case TOO_MANY_OPEN_FILES: strcpy(szStatusStr, "TOO MANY OPEN FILES");
            break;
        default: strcpy(szStatusStr, "UNKNOWN CONDITION");
            break;
    }
}

