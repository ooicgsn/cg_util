from the d201109 directory execute the following line

tools/mcat_dump mcat imm/MC02*.DAT

usage description below:
tools/mcat_dump v0.1-20110926
Usage: tools/mcat_dump <output dir> <input file>
note: <output dir> is a directory relative to where you run the program from
note: <input file> can be wildcarded

