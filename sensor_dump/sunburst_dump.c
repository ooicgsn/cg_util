/* =======================================================
** WHOI OOI/CGSN Sunburst 2PCO2W/PHSEN Dump Program
** 
** Description:
**
** 
** Usage: sunburst_dump [-a] [-c] [-x] [-h] 
**
**        
**
** History:
**     Date      Who     Description
**    -------    ---     ----------------------------------
**   10/17/2012  ME      New
**   12/19/2012  SL/FNS  Added pco2 calculation and removed column numbers
**   12/21/2012  ME      Corrected operations and field formating
**   03/07/2013  ME      Added PHSEN functionality
**   03/12/2013  ME      Added Point PH calculation
**   03/26/2013  ME      Added dump of other record types
**   04/02/2013  ME      Added MNM dump for records collected over modem
**   04/03/2013  ME      Fixed multi-record problem for inductive
**   04/04/2013  ME      Added Jeff's temp. fix
**   11/18/2013  ME      Removed Jeff's fix that broke PH_DATA
**   12/09/2013  ME      Added ability to supress raw data
** =======================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <math.h>

#include "cg_util.h"
#include "cg_systypes.h"

/**
 **
 ** Defines
 **
 **/

#define VERSION               "v2.1-20121019"
#define TIME_BYTES            4                 // Time is four (4) bytes long
#define TIME_STAMP_LEN        24                // Length in characters of time stamp
#define BUFFER_SIZE           1024              // For buffer
#define JAN_1904_TO_JAN_1970  2082844800        // Second from 1/1/1904 to 1/1/1970 to configure instrument
#define ea434                 17709             // Defines for PH calculation
#define ea578                 107
#define eb434                 2287
#define eb578                 38913
#define BLANK_MEAS            4                 // Number of BLANK measurments in PH data
#define PH_MEAS               23                // Number of PH measurments in PH data

// Record types and lengths
#define POWER_REC             0x11
#define POWER_LEN             0x07              // Data length in bytes not including 3 char header
#define GENERIC_REC           0x10              // Generic record type
#define GENERIC_LEN           0x09              // Data length in bytes not including 3 char header
#define SERIAL_REC            0x12              // Not supported
#define SERIAL2_REC           0x13              // Not supported
#define CONTROL_REC           0x80              // Controle record type
#define CONTROL_LEN           0x12              // Data length in bytes not including 3 char header
#define PCO2_REC              0x04              // PCO2 Record Type
#define PCO2_LEN              0x27              // Data length in bytes not including 3 char header
#define PH_REC                0x0A              // PH Record type
#define PHA_REC               0x08              // Alternate PH Record type
#define PH_LEN                0xE7              // Data length in bytes not including 3 char header
#define PH_LEN_1ST            12                // Charactera in the 1st part of the PHSEN

typedef struct 
{
     int ref434;
     int sig434;
     int ref578;
     int sig578;
} ph_data_struct;

static char xbuf[BUFFER_SIZE + 1]          = {0};
static int  csv                            = FALSE; // Comma Seperated Values (DEFAULT)
static int  eng_units                      = FALSE; // engineering units
static int  supress                        = FALSE; // Supress raw output
static int  debug                          = FALSE; // Debug llevel
static int  ignore                         = FALSE; // Ignore bad data file lines (recover as best as possible)
static int  inductive                      = FALSE; // Try to collect data as best a possible
static int  hexout                         = FALSE; // Output hex as opposed to engineering values
static int  last_rec                       = FALSE; // Last type displayed
static int  pco2_rec                       = FALSE; // Last type displayed
static int  phsen_rec                      = FALSE; // Last type displayed
static int  pwr_rec                        = FALSE; // Put out power record
static int  ctrl_rec                       = FALSE; // Put or control record
static int  gen_rec                        = FALSE; // Put of generic records
static int  blnk_rec                       = FALSE; // Put out PCO2 Blank records
static int  ser_rec                        = FALSE; // Put out PCO2 Blank records
static ph_data_struct ph_blank[BLANK_MEAS] = {{0}};
static ph_data_struct ph_meas[PH_MEAS]     = {{0}};


/********************************************************************************************
** 
** Function: print_usage
** 
**
*********************************************************************************************/
static void print_usage(char *argv0)
{
     fprintf(stderr,"[%s]: %s  [%s compiled on %s at %s]\n", argv0, VERSION, __FILE__, __DATE__, __TIME__);
     fprintf(stderr,"Usage: %s [-o offset] [-a] [-c] [-e] [-i] [-o] [-h] -t 1-8 [-t 1-8]\n", argv0);
     fprintf(stderr,"\t-a - All data block types\n");
     fprintf(stderr,"\t-c - CSV format\n");
     fprintf(stderr, "\t-D - Put out debug information\n");
     fprintf(stderr,"\t-e - Engineering units\n");
     fprintf(stderr,"\t-h - Help prints this message (overrides all others)\n");
     fprintf(stderr,"\t-i - Ignore errors (skip to next valid protocol unit)\n");
     fprintf(stderr,"\t-q - Inductive modem records\n");
     fprintf(stderr,"\t-s - Supress raw output\n");
     fprintf(stderr,"\t-x - Put out data in hex format\n");
     fprintf(stderr,"\t-t - type [1-7]\n");
     fprintf(stderr,"\t\t1 - pco2 data\n");
     fprintf(stderr,"\t\t2 - phsen data\n");
     fprintf(stderr,"\t\t3 - pco2 blank data\n");
     fprintf(stderr,"\t\t4 - general device data\n");
     fprintf(stderr,"\t\t5 - control and error data\n");
     fprintf(stderr,"\t\t6 - power data\n");
     fprintf(stderr,"\t\t7 - serial data (not supported)\n");
     fprintf(stderr,"\t All option [-a] overrides all type designations [1-8]\n");
}  // End of printf_usage

/********************************************************************************************
** 
** Function: get_args
** 
**
*********************************************************************************************/
static void get_args(int argc, char *argv[])
{
     int c;
     
     opterr = 0;
     //Get args and check usage
	while((c = getopt(argc, argv, "acDehiqsxt:")) != FAIL)
     {
          switch (c)
          {
          case 'a':
               pco2_rec = phsen_rec = blnk_rec = gen_rec = ctrl_rec = pwr_rec = ser_rec = TRUE;
               break;
          case 'c':
               csv = TRUE;
               break;
          case 'D':
               debug = TRUE;
               break;
          case 'e':
               eng_units = TRUE;
               break;
          case 'h':
               print_usage(argv[0]);
               exit(EXIT_FAILURE);
          case 'i':
               ignore = TRUE;
               break;
          case 'q':
               inductive = TRUE;
               break;
          case 'x':
               hexout = TRUE;
               break;
          case 's':
               supress = TRUE;
               break;
         case 't':
               c = atoi(optarg);
               switch (c)
               {
               case 1:
                    pco2_rec = TRUE;
                    break;
               case 2:
                    phsen_rec = TRUE;
                    break;
               case 3:
                    blnk_rec = TRUE;
                    break;
               case 4:
                    gen_rec = TRUE;
                    break;
               case 5:
                    ctrl_rec = TRUE;
                    break;
               case 6:
                    pwr_rec = TRUE;
                    break;
               case 7:
                    ser_rec = TRUE;
                    break;
               default:
                    print_usage(argv[0]);
                    exit(EXIT_FAILURE);
               }
               break;
           case '?':
               fprintf(stderr, "[%s]: Unknown argument [%c]\n", argv[0], optopt);
          default:
               fprintf(stderr, "[%s]: Unknown return from getopt [%d][%c]\n", argv[0], c, c);
          }
     }
     if (!pco2_rec && !phsen_rec && !pwr_rec && !ctrl_rec && !gen_rec && !blnk_rec)
          pco2_rec = phsen_rec = TRUE;
}  // End of get_args

/*********************************************************************************************
**
**   Function: get_time
**
**   
**   
**   
*********************************************************************************************/
static void get_time(time_t ct)
{
     struct tm *tm;
     
     ct = ct - JAN_1904_TO_JAN_1970;
     if ((tm = gmtime(&ct)) == NULL)
          return;

     fprintf(debug ? stderr : stdout, "%d/%02d/%02d %02d:%02d:%02d",
             tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
            tm->tm_hour, tm->tm_min, tm->tm_sec);
     fprintf(debug ? stderr : stdout, csv ? ", " : " ");
}  // End of get_time

/*********************************************************************************************
**
**   Function: get_val
**
**   
**   Gets a value nbytes long (2 characters per byte) and returns value
**   
*********************************************************************************************/
static uint get_val(char *ptr, int nbytes, int d)
{
     uint    val         =  0;
     char    tb[8 + 1]   = {0};                 // Max fieled is four bytes (8 chars) 

     if (nbytes > 4)
     {
          fprintf(stderr, "Byte value [%d] too large: %s\n", nbytes, xbuf);
          exit(EXIT_FAILURE);
     }
     
     strncpy(tb, ptr, nbytes * 2);            // Two characters per byte
     tb[nbytes * 2] = NUL_CHAR;
     sscanf(tb, "%x", &val);

     if ((debug) && d) fprintf(stderr, "\nValue = %04u [0x%03X], tb = %s ", val, val, tb);
          
     return val;
}  // End of get_val

/*********************************************************************************************
**
**   Function: check_sum
**
**   
**   
**   
*********************************************************************************************/
static int check_sum(char *ptr, int len)
{
     uint total  = 0;
     uint i      = 0;

     while (i++ < len - 1)                      // Count all but the last byte
     {
          total += get_val(ptr, 1, FALSE);
          ptr += 2;                             // Next byte (2 characters)
     }
     i = get_val(ptr, 1, FALSE);

     if ((total & 0xFF) != i)
     {
          fprintf(stderr, "Check_sum error: Total = %X, Value = %X for: %s\n", total & 0xFF, i, xbuf);
          return FALSE;
     }
     if (debug) fprintf(stderr, "Checksum matches\n");
     
     return TRUE;
}  // End of check_sum

/********************************************************************************************
** 
** Function: calculate_pco2
** 
**
*********************************************************************************************/
static float calculate_pco2(float ratio434, float ratio620, float temp_c)
{
     double a434, a620, ratio, rco2, tcor_rco2i, tcoeff, tcor_rco2;
     float pco2;
     double e1, e2, e3;
     float CalA, CalB, CalC, CalT;

     //Define calibration coefficents (from serial#; SAMI2-C0051)
     CalA = -0.0459;
     CalB =  1.0761;
     CalC = -2.1138;
     CalT = 14.2;

     //Define E-Values (from matlab script)
     float Ea434 = 19706 - 29.3 * CalT; 
     float Ea620 = 34;
     float Eb434 = 3073;
     float Eb620 = 44327 - 70.6 * CalT;
     e1 = Ea620/Ea434; e2 = Eb620/Ea434; e3 = Eb434/Ea434;


     a434 = -1.0 * log10((double) ratio434);
     a620 = -1.0 * log10((double) ratio620);

     ratio = a620/a434;
     rco2 = -1.0 * log10((double) (ratio - e1) / (e2 - e3 * ratio));

     tcor_rco2i = rco2+(0.007*(temp_c*CalT));
     tcoeff     = (0.0075778)*(0.0012389 * tcor_rco2i)*(0.00048757*tcor_rco2i*tcor_rco2i);
     tcor_rco2  = rco2+tcoeff*(temp_c*CalT);

     pco2 = pow(10, (((-1.0 * CalB) + pow((CalB*CalB) - (4*CalA*(CalC-tcor_rco2)),0.5)) / (2 * CalA)));
     return pco2;
}  // End of calculate_pco2

/*********************************************************************************************
**
**   Function: pco2_data
**
**   
**   
**   
*********************************************************************************************/
static void pco2_data(char *ptr, int len, int blank)
{
     int    i             = 0;
     time_t ct            = 0;
     float battery_volts  = 0.0;
     float temp_c         = 0.0;
     float pco2_ppm       = 0.0;
     float ratio434       = 0.0;
     float ratio620       = 0.0;
     double rt            = 0;
     double invt          = 0;
     double temp_k        = 0;
     char tbuf[3]         = {0};
     uint tval            = 0;

     if (len != (PCO2_LEN * 2))
     {
          fprintf(stderr, "Bad pco2 data string length [%d]: %s\n", len, xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }
     
     strncpy(tbuf, ptr, 2);           // Copy the record type
     tbuf[2] = NUL_CHAR;
     sscanf(tbuf, "%x", &tval);
     if (len != (tval * 2))
     {
          fprintf(stderr, "Bad PCO2 data [%s] Lenght in buffer [%d] does not equal read length [%d]\n",
                  xbuf, tval * 2, len);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if (check_sum(ptr, PCO2_LEN) == FALSE)
     {
          fprintf(stderr, "Bad PCO2 data string checksum: %s\n", xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if ((last_rec != PCO2_REC) && eng_units)
     {
          !supress ? fprintf(debug ? stderr : stdout, csv ?
                  "PCO2: Record, RawTime, DrkRf, DrkSg, 434Rf, 434Sg, 620Rf, 620Sg, 434Rt,\
 620Rt, DrkRf, DrkSg, 434Rf, 434Sg, 620Rf, 620Sg, Bat_r, tmp_r, InstDate  InstTime,  Bat_V, Tem_C, PCO2_ppm\n" :
                  "PCO2: Record RawTime DrkRf DrkSg 434Rf 434Sg 620Rf 620Sg 434Rt 620Rt\
 DrkRf DrkSg 434Rf 434Sg 620Rf 620Sg Bat_r tmp_r InstDate InstTime Bat_V Tem_C PCO2_ppm\n") :
               fprintf(debug ? stderr : stdout, csv ?
                  "PCO2: InstDate  InstTime,  Bat_V, Tem_C, PCO2_ppm\n" :
                       "PCO2: InstDate InstTime Bat_V Tem_C PCO2_ppm\n");
          last_rec = PCO2_REC;
     }
     
     if (!supress)
     {
          if (blank)
               fprintf(debug ? stderr : stdout,"Blank ");
          fprintf(debug ? stderr : stdout, csv ? "PC02: %s, " : "PC02: %s ", xbuf);
     }
     else 
     {
          if (blank)
               fprintf(debug ? stderr : stdout,"Blank ");
          fprintf(debug ? stderr : stdout, csv ? "PC02: " : "PC02: ");
     }

     ptr += 4;                       // Skip length field and type

     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%08X, " : "%08X ", get_val(ptr, TIME_BYTES, TRUE)) : 
               fprintf(debug ? stderr : stdout, csv ? "%u, "   : "%u "  , get_val(ptr, TIME_BYTES, TRUE));
     }
     
     ct = (time_t) get_val(ptr, TIME_BYTES, FALSE);
     ptr += (TIME_BYTES * 2);

     while (i < 14)                  // Print 14 sets of (2 bytes) light measurements
     {
          if (!supress)
          {
               hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ",  get_val(ptr, 2, TRUE)) : 
                    fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ",  get_val(ptr, 2, TRUE));
          }

          // Save some values for pc02 calculation
          if (i == 6) ratio434 = get_val(ptr,2, FALSE);
          if (i == 7) ratio620 = get_val(ptr,2, FALSE);

          ptr += 4;
          i++;
     }

     // Print out battery value and conversion to volts
          
     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ", get_val(ptr, 2, TRUE)) : 
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ", get_val(ptr, 2, TRUE));  // Battery
     }
     
     if (eng_units)
          battery_volts = get_val(ptr, 2, FALSE) / 4096.0 * 5.0 * 3.0;
     ptr += 4;

     // Print temperature value and conversion to degC
     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ", get_val(ptr, 2, TRUE)) :
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ", get_val(ptr, 2, TRUE));  // Thermistor
     }
     
     if (eng_units)
     {
          get_time(ct);
          fprintf(debug ? stderr : stdout, csv ? "%.2f, " : "%.2f ", battery_volts);
          rt = get_val(ptr, 2, FALSE) / (4096.0 - get_val(ptr, 2, FALSE)) * 17400.0;
          invt = 0.0010183+0.000241 * (log(rt) + 0.00000015*pow(log(rt),3));
          temp_k = 1.0/invt;
          temp_c = temp_k - 273.15;
          fprintf(debug ? stderr : stdout, csv ? "%.2f, " : "%.2f ",temp_c);
          // Print out calculated PCO2
          pco2_ppm = calculate_pco2(ratio434, ratio620, temp_c);
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%.2f" : "%.2f", pco2_ppm) :
                   fprintf(debug ? stderr : stdout, csv ? "%.2f" : "%.2f", pco2_ppm);
     }
     fprintf(debug ? stderr : stdout, "\n");
}  // End of pco2_data

/*********************************************************************************************
**
**   Function: get_phdata
**
**   
**   Gets n ph_data values
**
**   Note: ptr increments by 16 per 4 values
**   
*********************************************************************************************/
static void get_phdata(char *ptr, ph_data_struct *ph)
{
     ph->ref434 = get_val(ptr, 2, TRUE);
     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ",  ph->ref434) : 
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ",  ph->ref434);
     }
     
     ptr += 4;
     ph->sig434 = get_val(ptr, 2, TRUE);
     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ",  ph->sig434) : 
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ",  ph->sig434);
     }
     
     ptr += 4;
     ph->ref578 = get_val(ptr, 2, TRUE);
     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ",  ph->ref578) : 
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ",  ph->ref578);
     }
     
     ptr += 4;
     ph->sig578 = get_val(ptr, 2, TRUE);
     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ",  ph->sig578) : 
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ",  ph->sig578);
     }
     
}  // End of get_phdata

/********************************************************************************************
** 
** Function: calculate_ph
** 
**
*********************************************************************************************/
static float calculate_ph(float temp1, float temp2)
{
     int i;
     float blank434  = 0.0;
     float blank578  = 0.0;
     float meas434   = 0.0;
     float meas578   = 0.0;
     float A434Blank = 0.0;
     float A578Blank = 0.0;
     float A434i     = 0.0;
     float A578i     = 0.0;
     float A434      = 0.0;
     float A578      = 0.0;
     float R         = 0.0;
     float Ea434     = 0.0;
     float Ea578     = 0.0;
     float Eb434     = 0.0;
     float Eb578     = 0.0;
     float e1        = 0.0;
     float e2        = 0.0;
     float e3        = 0.0;
     float V1        = 0.0;
     float V2        = 0.0;
     float pKa       = 0.0;

     if (debug) fprintf(stderr, "\n");
     for (i = 0; i < BLANK_MEAS; i++)
     {
          if (debug) fprintf(stderr, "Blank [%2d] ref434 = %5d, sig434 = %5d, ref578 = %5d, sig578 = %5d\n",
                     i + 1, ph_blank[i].ref434, ph_blank[i].sig434, ph_blank[i].ref578, ph_blank[i].sig578);
          
          blank434 += ((float) ph_blank[i].sig434 / (float) ph_blank[i].ref434);
          blank578 += ((float) ph_blank[i].sig578 / (float) ph_blank[i].ref578);
     }
     blank434 = blank434 / 4.0;
     blank578 = blank578 / 4.0;
     if (debug) fprintf(stderr, "blank434=%.5f, blank578=%.5f\n", blank434, blank578);
     
     for (i = 0; i < PH_MEAS; i++)
     {
          if (debug) fprintf(stderr, "Meas  [%2d] ref434 = %5d, sig434 = %5d, ref578 = %5d, sig578 = %5d\n",
                            i + 1, ph_meas[i].ref434, ph_meas[i].sig434, ph_meas[i].ref578, ph_meas[i].sig578);
          
          meas434 += (float) ((float) ph_meas[i].sig434 / (float) ph_meas[i].ref434);
          meas578 += (float) ((float) ph_meas[i].sig578 / (float) ph_meas[i].ref578);
     }
     meas434 = meas434 / 23.0;
     meas578 = meas578 / 23.0;
     if (debug) fprintf(stderr, "meas434=%.5f, meas578=%.5f\n", meas434, meas578);

     A434Blank = -1.0 * log10f(blank434);
     A434i     = -1.0 * log10f(meas434);
     A434      = A434i - A434Blank;

     A578Blank = -1.0 * log10f(blank578);
     A578i     = -1.0 * log10f(meas578);
     A578      = A578i - A578Blank;

     pKa       = (1245.69 / (temp2 * 273.15)) + 3.8275;

     R         = A578 / A434;
     Ea434     = ea434 - (26 * (((temp1 + temp2) / 2) - 24.788));
     Ea578     = ea578 + (((temp1 + temp2) / 2) - 24.788);
     Eb434     = eb434 - (12 * (((temp1 + temp2) / 2) - 24.788));
     Eb578     = eb578 - (71 * (((temp1 + temp2) / 2) - 24.788));
     e1        = Ea578 / Ea434;
     e2        = Eb578 / Ea434;
     e3        = Eb434 / Ea434;
     V1        = R - e1;
     V2        = (e2 - R) * e3;
     
     return ((float) pKa + log10f(V1 / V2));
}  // End of calculate_ph

/*********************************************************************************************
**
**   Function: ph_data
**
**   
**   
**   
*********************************************************************************************/
static void ph_data(char *ptr, int len)
{
     char tmpbuf[BUFFER_SIZE + 1] = {0};
     char tbuf[3]                 = {0};
     uint tval                    = 0;
     char *p                      = NULL;
     int  tmpl                    = 0;
     int    i                     = 0;
     time_t ct                    = 0;
     float battery_volts          = 0.0;
     float temp_c1                = 0.0;
     float temp_c2                = 0.0;
     double srt                   = 0;
     double ert                   = 0;
     double invt                  = 0;
     double temp_k                = 0;
   
     if (!inductive && (len != PH_LEN_1ST))
     {
          fprintf(stderr, "Bad phsen data string length [%d]: %s\n", len, xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }
     while (len < PH_LEN * 2)
     {
          if ((fgets(tmpbuf, BUFFER_SIZE, stdin) == NULL) || ((tmpl = strlen(tmpbuf)) < TIME_STAMP_LEN))
          {
               fprintf(stderr, "Short read of PHSEN data, %d or %d read\n", len, PH_LEN * 2);
               if (ignore)
                    return;
               exit(EXIT_SUCCESS);
          }
          if ((p = strchr(tmpbuf, NEWLN_CHAR)) != NULL)    // Remove any terminating newline
               *p = NUL_CHAR;
          if (debug) fprintf(stderr, "Processing [%s]\n", tmpbuf);
          
          tmpl -= TIME_STAMP_LEN + 1;                      // Skip past the time stamp
          strcat(ptr, tmpbuf + TIME_STAMP_LEN);            // Build the buffer (ignoring other timestamps)
          len += tmpl;
          while (ptr[i])
               if (isdigit(ptr[i]) || isalpha(ptr[i]))
                    i++;
               else
               {
                    fprintf(stderr, "Bad PHSEN data, [%c] in string \"%s\" not allowed\n", ptr[i], ptr);
                    if (ignore)
                         return;
                    exit(EXIT_SUCCESS);
               }
          
     }
     strncpy(tbuf, ptr, 2);           // Copy the record type
     tbuf[2] = NUL_CHAR;
     sscanf(tbuf, "%x", &tval);
     if (len != (tval * 2))
     {
          fprintf(stderr, "Bad PH Data [%s] Lenght in buffer [%d] does not equal read length [%d]\n",
                  xbuf, tval * 2, len);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if (check_sum(ptr, PH_LEN) == FALSE)
     {
          fprintf(stderr, "Bad PHSEN data string checksum: %s\n", xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if ((last_rec != PH_REC) && eng_units)
     {
          !supress ? fprintf(debug ? stderr : stdout, csv ?
                             "PHSEN: Record, RawTime, RawTemp1, Blanks[4][4], Measurements[23][4],\
 RawBattery, RawTemp2, InstDate  InstTime, Tmp1_C, pH, Bat_V, Tmp2_C\n" :
                             "PHSEN: Record RawTime RawTemp1 Blanks[4][4] Measurements[23][4]\
 RawBattery RawTemp2 InstDate  InstTime Tmp1_C pH Bat_V Tmp2_C\n") :
               fprintf(debug ? stderr : stdout, csv ?
                       "PHSEN: InstDate  InstTime, Tmp1_C, pH, Bat_V, Tmp2_C\n" :
                       "PHSEN: InstDate  InstTime Tmp1_C pH Bat_V Tmp2_C\n");
          last_rec = PH_REC;
     }
     !supress ? fprintf(debug ? stderr : stdout, csv ? "PHSEN: %s, " : "PHSEN: %s ", xbuf) :
          fprintf(debug ? stderr : stdout, csv ? "PHSEN: " : "PHSEN: ");
     
     ptr += 4;                                                                   // Skip length field and type

     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%08X, " : "%08X ", get_val(ptr, TIME_BYTES, TRUE)) :
               fprintf(debug ? stderr : stdout, csv ? "%u, "   : "%u "  , get_val(ptr, TIME_BYTES, TRUE)); 
     }
     
     ct = (time_t) get_val(ptr, TIME_BYTES, FALSE);
     ptr += (TIME_BYTES * 2);

     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ", get_val(ptr, 2, TRUE)) : 
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ", get_val(ptr, 2, TRUE));
     }
     
     srt = get_val(ptr, 2, FALSE);
     ptr += 4;

     i = 0;
     while (i < 4)  // Print 16 sets of (2 bytes) reference light measurements
     {
          get_phdata(ptr, ph_blank + i);                                         // Blank sequence [i]
          if (debug) fprintf(stderr, "End of Blank sequence %d", i + 1);
          ptr += 16;
          i++;
     }
     i = 0;
     while (i < 23)  // Print 23 sets of (2 bytes) 4 light measurements
     {
          get_phdata(ptr, ph_meas + i);                                          // Measurement sequence [i]
          if (debug) fprintf(stderr, "End of Measurement sequence %d", i + 1);
          ptr += 16;
          i++;
     }
     ptr += 4;                                                                   // Skip unused entry
     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ", get_val(ptr, 2, TRUE)) :
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ", get_val(ptr, 2, TRUE)); 
     }
     
     battery_volts = get_val(ptr, 2, FALSE) / 4096.0 * 5.0 * 3.0;                // Calculate Battery
     ptr += 4;

     if (!supress)
     {
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ", get_val(ptr, 2, TRUE)) :
               fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ", get_val(ptr, 2, TRUE)); 
     }
     
     ert = get_val(ptr, 2, FALSE);

     if (eng_units)
      {
           get_time(ct);                                                         // Calculate and print time
           srt = (srt / (4096.0 - srt)) * 17400.0;                               // Calculate Start Temp
           invt = 0.0010183 + (0.000241 * logf(srt)) + (0.00000015 * pow(logf(srt),3));
           temp_k = 1.0/invt;
           temp_c1 = temp_k - 273.15;
           fprintf(debug ? stderr : stdout, csv ? "%.2f, " : "%.2f ",temp_c1);   // Printf Start Temp
           ert = (ert / (4096.0 - ert)) * 17400.0;                               // Calculate End Temp
           invt = 0.0010183 + (0.000241 * logf(ert)) + (0.00000015 * pow(logf(ert),3));
           temp_k = 1.0/invt;
           temp_c2 = temp_k - 273.15;
           fprintf(debug ? stderr : stdout, csv ? "%.2f, " : "%.2f ", calculate_ph(temp_c1, temp_c2));
           fprintf(debug ? stderr : stdout, csv ? "%.2f, " : "%.2f ", 
                   battery_volts);                                               // Print Battery
           fprintf(debug ? stderr : stdout, csv ? "%.2f, " : "%.2f ", temp_c2);  // Prinf End Temp
      }

     fprintf(debug ? stderr : stdout, "\n");
     return;
}  // End of ph_data

/*********************************************************************************************
**
**   Function: power_data
**
**   
*********************************************************************************************/
static void power_data(char *ptr, int len)
{
     time_t ct            = 0;
     char   tbuf[3]       = {0};
     uint   tval          = 0;


     if (len != (POWER_LEN * 2))
     {
          fprintf(stderr, "Bad power data string length [%d]: %s\n", len, xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }
     
     strncpy(tbuf, ptr, 2);           // Copy the record type
     tbuf[2] = NUL_CHAR;
     sscanf(tbuf, "%x", &tval);
     if (len != (tval * 2))
     {
          fprintf(stderr, "Bad power data [%s] Lenght in buffer [%d] does not equal read length [%d]\n",
                  xbuf, tval * 2, len);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if (check_sum(ptr, POWER_LEN) == FALSE)
     {
          fprintf(stderr, "Bad power data string checksum: %s\n", xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if ((last_rec != POWER_REC) && eng_units)
     {
          fprintf(debug ? stderr : stdout, csv ? "POUT: Record, RawTime, InstDate InstTime\n" :
                       "POUT: Record RawTime InstDate InstTime\n");
          last_rec = POWER_REC;
     }

     fprintf(debug ? stderr : stdout, csv ? "POUT: %s, " : "POUT: %s ", xbuf);
     ptr += 4;                       // Skip length field and type field
     
     csv ? fprintf(debug ? stderr : stdout, hexout ? "%08X, " : "%05u, " , get_val(ptr, TIME_BYTES, TRUE)) :
           fprintf(debug ? stderr : stdout, hexout ? "%08X "  : "%05u "  , get_val(ptr, TIME_BYTES, TRUE));
     if (eng_units)
     {
          ct = (time_t) get_val(ptr, TIME_BYTES, FALSE);
          get_time(ct);
     }
     fprintf(debug ? stderr : stdout, "\n");
}  // End of power_data

/*********************************************************************************************
**
**   Function: generic_data
**
**   
*********************************************************************************************/
static void generic_data(char *ptr, int len)
{
     time_t ct            = 0;
     char   tbuf[3]       = {0};
     uint   tval          = 0;

     if (len != (GENERIC_LEN * 2))
     {
          fprintf(stderr, "Bad power data string length [%d]: %s\n", len, xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     strncpy(tbuf, ptr, 2);           // Copy the record length
     tbuf[2] = NUL_CHAR;
     sscanf(tbuf, "%x", &tval);

     if (len != (tval * 2))
     {
          fprintf(stderr, "Bad generic data [%s] Lenght in buffer [%d] does not equal read length [%d]\n",
                  xbuf, tval * 2, len);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if (check_sum(ptr, GENERIC_LEN) == FALSE)
     {
          fprintf(stderr, "Bad power data string checksum: %s\n", xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if ((last_rec != GENERIC_REC) && eng_units)
     {
          fprintf(debug ? stderr : stdout, csv ? "GOUT: Record, RawTime, Voltage, InstDate InstTime\n" :
                       "GOUT: Record RawTime Voltage InstDate InstTime\n");
          last_rec = GENERIC_REC;
     }

     fprintf(debug ? stderr : stdout, csv ? "GOUT: %s, " : "GOUT: %s ", xbuf);
     ptr += 4;                       // Skip length field and type field
     
     csv ? fprintf(debug ? stderr : stdout, hexout ? "%08X, " : "%05u, " , get_val(ptr, TIME_BYTES, TRUE)) :
           fprintf(debug ? stderr : stdout, hexout ? "%08X "  : "%05u "  , get_val(ptr, TIME_BYTES, TRUE));
     ct = (time_t) get_val(ptr, TIME_BYTES, FALSE);
     ptr += (TIME_BYTES * 2);

     hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ", get_val(ptr, 2, TRUE)) :
              fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ", get_val(ptr, 2, TRUE)); 
     if (eng_units)
          get_time(ct);

     fprintf(debug ? stderr : stdout, "\n");
}  // End of generic_data

/*********************************************************************************************
**
**   Function: do_flags
**
**   
*********************************************************************************************/
static void do_flags(uint flag)
{
     if (flag)
          fprintf(debug ? stderr : stdout, "Flags: ");
     if (flag & 0x01)                              // bit 0
          fprintf(debug ? stderr : stdout, "Clock started, ");
     if (flag & 0x02)                              // bit 1
          fprintf(debug ? stderr : stdout, "Recording started, ");
     if (flag & 0x04)                              // bit 2
          fprintf(debug ? stderr : stdout, "Recording ended on time, ");
     if (flag & 0x08)                              // bit 3
          fprintf(debug ? stderr : stdout, "Recording ended memory full, ");
     if (flag & 0x10)                              // bit 4
          fprintf(debug ? stderr : stdout, "Recording ended due to error or failure or user stopped, ");
     if (flag & 0x20)                              // bit 5
          fprintf(debug ? stderr : stdout, "Data download, ");
     if (flag & 0x40)                              // bit 6
          fprintf(debug ? stderr : stdout, "Flash Open, ");
     if (flag & 0x80)                              // bit 7
          fprintf(debug ? stderr : stdout, "Low or no battery before start 256*t_pmi seconds - fatal, ");
     if (flag & 0x100)                             // bit 8
          fprintf(debug ? stderr : stdout, "Battery low on measure cycle - fatal, ");
     if (flag & 0x200)                             // bit 9
          fprintf(debug ? stderr : stdout, "Battery low on blank cycle - fatal, ");
     if (flag & 0x400)                             // bit 10
          fprintf(debug ? stderr : stdout, "Battery low on external device cycle - fatal, ");
     if (flag & 0x800)                             // bit 11
          fprintf(debug ? stderr : stdout, "External device 1 fault, ");
     if (flag & 0x1000)                            // bit 12
          fprintf(debug ? stderr : stdout, "External device 2 fault, ");
     if (flag & 0x2000)                            // bit 13
          fprintf(debug ? stderr : stdout, "External device 3 fault, ");
     if (flag & 0x4000)                            // bit 14
          fprintf(debug ? stderr : stdout, "Erased, ");
     if (flag & 0x8000)                            // bit 15
          fprintf(debug ? stderr : stdout, "Power on flags not valid, ");
}  // End of do_flags

/*********************************************************************************************
**
**   Function: do_type
**
**   
*********************************************************************************************/
static void do_type(uint type)
{
     fprintf(debug ? stderr : stdout, "Type: ");
     switch (type)
     {
     case 0x80:
          fprintf(debug ? stderr : stdout, csv ? "Launch, " : "Launch ");
          break;
     case 0x81:
          fprintf(debug ? stderr : stdout, csv ? "Start, " : "Start ");
          break;
     case 0x83:
          fprintf(debug ? stderr : stdout, csv ? "Shutdown, " : "Shutdown ");
          break;
     case 0x85:
          fprintf(debug ? stderr : stdout, csv ? "Handshake_on, " : "Handshake_on ");
          break;
     case 0x86:
          fprintf(debug ? stderr : stdout, csv ? "Batt_restored, " : "Batt_restored ");
          break;
     case 0x87:
          fprintf(debug ? stderr : stdout, csv ? "Stopped_by_User, " : "Stopped_by_User ");
          break;
     case 0xC0:
          fprintf(debug ? stderr : stdout, csv ? "Batt_Low_Pump, " : "Batt_Low_Pump ");
          break;
     case 0xC1:
          fprintf(debug ? stderr : stdout, csv ? "Batt_Low_Valve, " : "Batt_Low_Valve ");
          break;
     case 0xC2:
          fprintf(debug ? stderr : stdout, csv ? "Que_overflow, " : "Que_overflow ");
          break;
     case 0xC3:
          fprintf(debug ? stderr : stdout, csv ? "No_Battery, " : "No_Battery ");
          break;
     case 0xC4:
          fprintf(debug ? stderr : stdout, csv ? "Drvr_not_found, " : "Drvr_not_found ");
          break;
     case 0xC5:
          fprintf(debug ? stderr : stdout, csv ? "Drvr_is_Off, " : "Drvr_is_Off ");
          break;
     case 0xC6:
          fprintf(debug ? stderr : stdout, csv ? "Serial_Timeout, " : "Serial_Timeout ");
          break;
     }
}  // End of do_type

/*********************************************************************************************
**
**   Function: contrl_error_data
**
**   
*********************************************************************************************/
static void cntrl_error_data(char *ptr, int len, int c_type)
{
     time_t ct            = 0;
     int    rec_len       = CONTROL_LEN;
     int    batt_error    = FALSE;
     uint   flags         = 0;
     char   tbuf[3]       = {0};
     uint   tval          = 0;

     if ((c_type == 0xC0) || (c_type == 0xC1))
     {
          rec_len += 3;
          batt_error = TRUE;
     }
          
     if (len != (rec_len * 2))
     {
          fprintf(stderr, "Bad control record string length [%d], expected [%d] in %s\n",
                  len, (rec_len * 2), xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     strncpy(tbuf, ptr, 2);           // Copy the record type
     tbuf[2] = NUL_CHAR;
     sscanf(tbuf, "%x", &tval);
     if (len != (tval * 2))
     {
          fprintf(stderr, "Buffer [%s] Lenght in buffer [%d] does not equal read length [%d]\n",
                  xbuf, tval * 2, len);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }

     if (check_sum(ptr, rec_len) == FALSE)
     {
          fprintf(stderr, "Bad control record string checksum: %s\n", xbuf);
          if (ignore)
               return;
          exit(EXIT_FAILURE);
     }
     if ((last_rec != CONTROL_REC) && eng_units)
     {
          fprintf(debug ? stderr : stdout, csv ? "COUT: Record, RawTime, Flags, #Records, #Errors, #Bytes, InstDate  InstTime,\
 Flags, Type\n" :
                       "COUT: Record RawTime Flags #Records #Errors #Bytes InstDate InstTime Flags\
 Type\n");
          last_rec = CONTROL_REC;
     }
      
     fprintf(debug ? stderr : stdout, csv ? "COUT: %s, " : "COUT: %s ", xbuf);
     ptr += 4;                       // Skip length field and type field
     
     csv ? fprintf(debug ? stderr : stdout, hexout ? "%08X, " : "%05u, " , get_val(ptr, TIME_BYTES, TRUE)) :
           fprintf(debug ? stderr : stdout, hexout ? "%08X "  : "%05u "  , get_val(ptr, TIME_BYTES, TRUE));
     ct = (time_t) get_val(ptr, TIME_BYTES, FALSE);
     ptr += (TIME_BYTES * 2);

     // Print Flaggs
     hexout ? fprintf(debug ? stderr : stdout, csv ? "%04X, " : "%04X ", get_val(ptr, 2, TRUE)) :
              fprintf(debug ? stderr : stdout, csv ? "%05u, " : "%05u ", get_val(ptr, 2, TRUE));       // Flags
     flags = get_val(ptr, 2, FALSE);
     ptr += 4;

     // Print Records
     hexout ? fprintf(debug ? stderr : stdout, csv ? "%06X, " : "%06X ", get_val(ptr, 3, TRUE)) :
              fprintf(debug ? stderr : stdout, csv ? "%08u, " : "%08u ", get_val(ptr, 3, TRUE));       // Records
     ptr += 6;

     // Print Errors
     hexout ? fprintf(debug ? stderr : stdout, csv ? "%06X, " : "%06X ", get_val(ptr, 3, TRUE)) :
              fprintf(debug ? stderr : stdout, csv ? "%08u, " : "%08u ", get_val(ptr, 3, TRUE));       // Errors
     ptr += 6;
     
     // Print Bytes
     hexout ? fprintf(debug ? stderr : stdout, csv ? "%06X, " : "%06X ", get_val(ptr, 3, TRUE)) :
              fprintf(debug ? stderr : stdout, csv ? "%08u, " : "%08u ", get_val(ptr, 3, TRUE));       // Bytes
     ptr += 6;

     if (batt_error)
     {
          // Print Bat
          hexout ? fprintf(debug ? stderr : stdout, csv ? "%06X, " : "%06X ", get_val(ptr, 3, TRUE)) :
                   fprintf(debug ? stderr : stdout, csv ? "%08u, " : "%08u ", get_val(ptr, 3, TRUE));  // Bytes
          ptr += 6;
     }
     
     if (eng_units)
     {
          get_time(ct);
          do_flags(flags);
          do_type(c_type);
     }
     fprintf(debug ? stderr : stdout, "\n");
}  // End of cntrl_error_data

/*********************************************************************************************
**
**   Function: main
**
**   
**   
**   
*********************************************************************************************/
int main(int argc, char *argv[])
{
     char  *ptr                   = NULL;
     char  *ptbuf                 = NULL;                        // Points to 1st character in current record
     uint  tval                   = 0;
     int   len                    = 0;
     uint  clen                   = 0;
     char  tb[3]                  = {0};
     char  tbuf[BUFFER_SIZE + 1]  = {0};

     get_args(argc, argv);

     for (;;)
     {
          if ((inductive == FALSE) || (ptbuf == NULL))           // not inductive or tbuff null
          {
               if (fgets(xbuf, BUFFER_SIZE, stdin) == NULL)      // Get a line from the file
                    break;
               if (inductive)
                     ptbuf = strncpy(tbuf, xbuf, BUFFER_SIZE);   // save the line 
          }
          if (inductive && ptbuf)
          {
               if ((ptr = strchr(ptbuf, CR_CHAR)) != NULL)       // Find the 1st <cr>
                    *ptr = NEWLN_CHAR;                           // Convert <cr> to new line
               strncpy(xbuf, ptbuf, BUFFER_SIZE);                // Copy the now <nl> terminated line to xbuf
               if (ptr)
                    ptbuf = ptr + 1;                             // Point to the character after the <cr>
               else
                    ptbuf = NULL;
          }

          if ((ptr = strchr(xbuf, NEWLN_CHAR)) != NULL)          // Remove any terminating newline
               *ptr = NUL_CHAR;                                  // and replace it witt <nul>

          if (strlen(xbuf) == 0)                                 // 
              continue;

          if (debug) fprintf(stderr, "Processing \"%s\" ", xbuf);
          
          if (((ptr = strchr(xbuf, STAR_CHAR)) == NULL) || ((len = strlen(ptr)) < 7))
          {
               if (!((inductive && strchr(xbuf, HASH_CHAR) != NULL) ||
                     (!inductive && strchr(xbuf, '[') != NULL)))
                    fprintf(stderr, "Unknown data string: %s\n", xbuf);
               else
                    if (debug) fprintf(stderr, " Ignoring\n");
               ptbuf = NULL;
               continue;
          }

          ptr += 3;                            // Skip the pre-amble (go to length filed)
          len -= 3;                            // Length of the string from the length field

          strncpy(tb, ptr, 2);                 // Get to the length
          tb[2] = NUL_CHAR;
          sscanf(tb, "%x", &clen);

          strncpy(tb, ptr + 2, 2);             // Copy the record type
          tb[2] = NUL_CHAR;
          sscanf(tb, "%x", &tval);

          if (((clen * 2) != len) && ((tval != PH_REC) && (tval != PHA_REC)))
          {
               fprintf(stderr, "Record length [%d - 0x%02X * 2] does not equal byte length [%d]\n",
                       clen * 2, clen, len);
               if (ignore)
                    continue;
               exit(EXIT_FAILURE);
          }
          if (debug) fprintf(stderr, "type = 0x%02x reclen = 0x%02x bytes [%d char.]  [strlen = %d char.]\n",
                             tval, clen, clen * 2, len);

          switch(tval)
          {
          case PCO2_REC:                               // CO2Aver+
          case 0x05:                                   // CO2AvBlnk+
               tval == PCO2_REC ? pco2_rec ? pco2_data(ptr, len, FALSE) : NULL :
                                  blnk_rec ? pco2_data(ptr, len, TRUE) : NULL;
               break;
          case PH_REC:                                 // ph(vb+)
          case PHA_REC:                                // phStd(vb+)
               phsen_rec ? ph_data(ptr, len) : NULL;
               break;
          case SERIAL_REC:                             //  Serial 1
          case 0x22:                                   //  Serial 2
          case 0x32:                                   //  Serial 3
          case SERIAL2_REC:                            //  Serial 31
          case 0x23:                                   //  Serial 32
          case 0x33:                                   //  Serial 33
          case 0x40:                                   //  Pre-StrtDl
          case 0x41:                                   //  Pre-StrtRd
          case 0x47:                                   //  Pre-StrtB7
               if (ser_rec)
                    fprintf(stderr, "Record type %u [hex: %02x] not supported. Discarding [%s]\n",
                            tval, tval, xbuf);
               break;
          case GENERIC_REC:                            // Generic Device 1
          case 0x20:                                   // Generic Device 2
          case 0x30:                                   // Generic Device 3
               gen_rec ? generic_data(ptr, len) : NULL;
               break;
          case POWER_REC:                              // Power Device 1
          case 0x21:                                   // Power Device 2
          case 0x31:                                   // Power Device 3
               pwr_rec ? power_data(ptr, len) : NULL;
               break;
          case CONTROL_REC:                            // Launch
          case 0x81:                                   // Start
          case 0x83:                                   // Shutdown
          case 0x85:                                   // Handshake_on
          case 0x86:                                   // Batt_restored
          case 0x87:                                   // Stopped_by_User
          case 0xC0:                                   // Batt_Low_Pump
          case 0xC1:                                   // Batt_Low_Valve
          case 0xC2:                                   // Que_overflow
          case 0xC3:                                   // No_Battery
          case 0xC4:                                   // Drvr_not_found
          case 0xC5:                                   // Drvr_is_Off
          case 0xC6:                                   // Serial_Timeout
               ctrl_rec ? cntrl_error_data(ptr, len, tval) : NULL;
               break;
          default:
               fprintf(stderr, "Record type %u [hex: %02x] not known.  Discarding [%s]\n", tval, tval, xbuf);
               if (!ignore)
                    exit(EXIT_FAILURE);
          }
     }
          exit(EXIT_SUCCESS);
}
