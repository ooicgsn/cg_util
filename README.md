# README #

This captures an overview of the Coastal/Global Utilities (common) software as well as the steps needed to deploy this shared software to a deployed system.

## Utilities Overview ##
The CG_Util software contains software that is common between multiple servers (i.e. repositories).  This includes the cgi_bin data and other sensor dump tools that are used by the OMS, PSS, and Platcon software.  

The CG_Util files are not deployed standalone to any server, but are used by other deployed software.

![dataflow-cg_util.JPG](https://bitbucket.org/repo/LBBBzd/images/3547522365-dataflow-cg_util.JPG)
The Utilities software consists of the following:

* **cgi-bin/** --> Provides the common scripts for viewing and plotting data
    * **oms/**  --> Provides the scripts that are unique to OMS for viewing and plotting data
    * **oms_pwrmodel/** --> Provides the scripts that are unique to OMS Power Model for viewing and plotting data
    * **platcon/** --> Provides the scripts that are unique to Platcon_SW for viewing and plotting data
* **sensor_dump/** --> Provides scripts for unpacking and performing rough processing (dumping) of sensor data
* **cg_util.spec** --> Used to create the cg_util rpm install file 

This directory also contains a **wiki/** where supporting documentation about this software is captured. For more information on how to use the wiki, see the [wiki/wiki-README.md](./wiki/wiki-README.md) file.

## Set Up To Deploy To CG_Util ##

* cg_util has a .spec file that can be used to create an rpm package file installation for a centos server. This file can also be converted to a deb package for installation on debian/ubuntu. For detailed instructions see [create an rpm](https://bitbucket.org/ooicgsn/documentation/src/master/wiki/deb_and_rpm_creation/create_an_rpm.md) and [create a deb from an rpm](https://bitbucket.org/ooicgsn/documentation/src/master/wiki/deb_and_rpm_creation/create_a_deb_from_an_rpm.md) (in the [BitBucket documentation repository](https://bitbucket.org/ooicgsn/documentation/)). Be sure to add the dependencies from the cg_util.spec file to the deb package.

* cg_util can be deployed as part of another deployment (as a manual submodule).
* See OMS_SW, Platcon_SW, and/or PSS_SW deployment information to see how these common files are used.

## Contribution Guidelines ##
* Follow the flow shown in the [BitBucket User's Guide](https://bitbucket.org/snippets/ooicgsn/7K6dM/bitbucket-git-users-guide). If you are new to Git or version control, the [BitBucket Overview](https://bitbucket.org/snippets/ooicgsn/yjgzd/bitbucket-git-overview-pdf) provides a simple overview to help understand the flow that is described in more detail in the aforementioned [User's Guide](https://bitbucket.org/snippets/ooicgsn/7K6dM/bitbucket-git-users-guide).

    * Create a new branch for any new development
    * Ensure any updates are properly reviewed/tested prior to deployment by creating pull requests in BitBucket
    * Verify that all updates work with oms_sw AND platcon_sw AND pss_sw
* Writing tests - None
* Other guidelines - Please set your IDE or text editor tabs to be 4 spaces prior to making updates.

## Who Do I Talk To? ##
* Contact: Steph Petillo (spetillo@whoi.edu), Peter Brickley (pbrickley@whoi.edu)
* Since this is common/shared software, it may be necessary to get multiple teams involved when changes are made.

