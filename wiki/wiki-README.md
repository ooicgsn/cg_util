# Wiki README #

This document explains how to use this area as a wiki. Do not delete this document.

## **Files & Folders** ##
* Files using markdown syntax must end in .md in order to be displayed correctly (see *Markdown Syntax* section below). Syntax for Creole, reStructuredText, and Textile are also supported.
* File & folder names can not include spaces or special characters.   
    * We recommend the use of underscores instead of spaces.
* Any images should be placed in the [wiki/images](./images) folder. Note: you may need to create this folder.
* You can create folders (of any depth) to group similar files
* Relative links should always be used to reference files in the same wiki (e.g. use ./wiki-README.md, instead of the full BitBucket path name to a file)
  * If relative links are not used, the links between files will not work offline.
  * Relative links at the same level work in MarkView. Relative links at different levels do not work in MarkView.


## **Markdown Syntax** ##

* We are using Markdown files (*.md) in our wiki. Click [here](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html) for more information on [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
* Here are a few BitBucket markdown pointers. These are cases where MME handles these different than BitBucket, so you have to be careful!
    * File names must end in .md to be rendered correctly.
    * The first bulleted line must be all the way to the left (column 0).  If you try to indent a bullet by 4 spaces and it's the first line, you will get a code block instead
    * To create sub lists, bullets must be 4 spaces further right than the previous bullet. This will NOT turn into a code block if the previous rule was followed.
        * Note: MME will allow fewer than 4 spaces (as few as 2), but these will not display correctly in BitBucket.
    * BitBucket will often turn 7 digit numbers into links to a commit by that number (whether or not that commit exists). If you don't want that, add an empty link '\[\]\(\)' every 6 digits and it will remove the commit link.  
        * e.g. Change '123456789' to '1234\[\]\(\)56789'
    * All lines that precede bulleted lines must end with two spaces or else the bullets will not render correctly in BitBucket.
    * If a line is not bulleted and it precedes a bulleted line, you will need a blank line between the non-bulleted line and the bulleted line or else the bulleted line will not display correctly (generally, it will italicize or something else weird).




## **Tool Support** ##


### **Minimalist Markdown Editor (MME) -- Offline Wiki / Markdown Editor** ###
* Option #1: Use Online Editor (no file/tab support; requires internet access)
    * Use the [online editor](http://markdown.pioul.fr/) in any browser
* Option #2: Use the Chrome App (has file/tab support; can be used with no internet access)
    * Go to [Minimalist Markdown Editor link in Chrome](https://chrome.google.com/webstore/detail/minimalist-markdown-edito/pghodfjepegmciihfhdipmimghiakcjf?hl=en)  
    * Click "Add to Chrome" button at top right  
    * Go to: [chrome://apps/](chrome://apps/)
    * Click on the icon for the Minimalist Markdown Editor
* To Edit (using either option)  
    * When the editor is open, you will see a two-paned window.  
    * Clear the data on the left side of the window and either start typing (for a new file) or paste in the markdown from an existing file (to edit)
    * When complete, paste the updated Markdown into the appropriate file and save.


### **MarkView -- Offline Wiki / Markdown Viewer** ###
**MarkView Install**  

* Go to [MarkView link in Chrome](https://chrome.google.com/webstore/detail/markview/iaddkimmopgchbbnmfmdcophmlnghkim?utm_source=chrome-app-launcher-info-dialog)  
* Click "Add to Chrome" button at top right  
* Go to: [chrome://extensions/](chrome://extensions/)  
* Scroll to MarkView.  Make sure that both "Allow access to file URLs" and "Enabled" are both checked

**MarkView Use --  REMINDER: THIS IS ONLY FOR VIEWING. TO EDIT, USE MME AS DESCRIBED ABOVE**   

* To view a file in MarkView, drag the file into a Chrome window
* If the Markdown file is displaying in MarkView, you can edit it using MME or your local text editor.  When you click Save, the file will update in MarkView, so you can immediately assess your changes.
* There are some buttons on the lower right  
  * "S" allows you to view the source Markdown.
  * Up Arrow brings you to the top of the page
  * Down Arrow brings you to the bottom of the page
* Relative links at the same level work in MarkView. Relative links at different levels do not work in MarkView
