#!/usr/bin/perl
# plot_gflt.pl -  Quick script to generate rt ground fault plot
#
# Usage: plot_gflt.pl [dcl=dclN]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create from plot_cpm.html, but now use in-line data
#      08/07/2011  SL     Added dcl arg to plot from cg_data/dclN/syslog dir
#      07/10/2013  SL     Support STC
##################################################################################

#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'QUERY_STRING'} ne "")
 {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }
if ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = $value\n";
}

#Get current id and extract data from either mpic cpm or dcl string
%MyID  = &loadCfg("../Cfg/current_id");

if ($arg{'type'} eq "stc")
        {@gflt_enable =  `./view_syslog limit=1500 s=\" stc: \" extract=17    label=enable`; 
         @gflt_data   =  `./view_syslog limit=1500 s=\" stc: \" extract=18,19,20,21 label=gflt1,gflt2,gflt3,gflt4`; 
         }
elsif ($arg{'dcl'} eq "")
  {if ($MyID{'id'} =~ /^dcl/)
        {@gflt_enable =  `./view_syslog limit=1500 s=\" dcl: \" extract=20    label=enable`; 
         @gflt_data   =  `./view_syslog limit=1500 s=\" dcl: \" extract=21,22,23 label=gflt1,gflt2,gflt3`; 
         }
   else {@gflt_enable =  `./view_syslog limit=1500 s=\" cpm: \" extract=19    label=enable`; 
         @gflt_data   =  `./view_syslog limit=1500 s=\" cpm: \" extract=20,21,22,23 label=gflt1,gflt2,gflt3,gflt4`; 
         }
   }
else 
  {@gflt_enable =  `./view_dcl_syslog dcl=$arg{'dcl'} limit=1500 s=\" dcl: \" extract=20    label=enable`; 
   @gflt_data   =  `./view_dcl_syslog dcl=$arg{'dcl'} limit=1500 s=\" dcl: \" extract=21,22,23 label=gflt1,gflt2,gflt3`; 
   }

$html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<table>
   <td>$MyID{'id'} gflt Enable
       <div id="graphdiv1"
        style="width:500px; height:200px;"></div>
   </td>
   <td> gflt uA
       <div id="graphdiv2"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
EndHTML
    for ($i=0;$i<$#gflt_enable;$i++)
       {$line = $gflt_enable[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#gflt_enable-1){$html_out .= "\"$line\\n\" +\n";}
                            else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      valueRange: [1,4]
     }          // options
  );

  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
EndHTML
    for ($i=0;$i<$#gflt_data;$i++)
       {$line = $gflt_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#gflt_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: true,
      rollPeriod: 3
     }          // options
  );

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;


#
# Subroutines Follow
#

sub loadCfg {my($file) = $_[0];
   my(%Cfg);
   my($line,$att,$val);
   my($att_val,$comment);
   my($FD);

  if (!open($FD,"$file")) {print "***Unable to open Cfg File [$file]\n";
                           return 0;
                           }

  while ($line = <$FD>)
      {next if ($line =~ /^#/); #skip comments
       next if ($line !~ /=/);  #skip blank lines or don't contain '='
       ($att_val,$comment) = split("#",$line,2);
       ($att,$val) = split("=",$att_val,2);
       $att =~ s/^(\s+)//; #remove leading spaces
       $att =~ s/(\s+)$//; #remove trailing spaces
       $val =~ s/^(\s+)//; #remove leading spaces
       $val =~ s/(\s+)$//; #remove trailing spaces
       $Cfg{$att} = $val;
       }
   close($FD);

   return %Cfg;
}

