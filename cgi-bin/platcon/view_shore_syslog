#!/usr/bin/perl
# view_shore_syslog.pl -  Quick script to view/search latest syslog files
#
# Usaage: view_shore_syslog omc= platform_id= deploy_dir= [s=pattern] [extract=col1,... label=lab1,... sum=0|1]
#                           [lf=syslog file | date=yyyymmdd]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      07/25/2011  SL     Create from view_syslog
#      08/18/2016  JJP	  Use /cmn-bin ScriptAlias for common view_* and plot_*
#			  files. NOTE: there is a reference to
#			  html/plot_ports.html - this file does not exist.
##################################################################################

my($base_dir) = "/webdata/CGO/OMC";
my($ddir);          #base_dir/<omc>/<platform_id>/<deploy_dir>/cg_data
my($syslog_file);   #ddir/syslog or ddir/dclN/syslog/yyyymmdd_hhmm.syslog.log
my($results,@all);

#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'QUERY_STRING'} ne "")
 {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }
if ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = $value\n";
}

#TEMP - setup defaults
$arg{'omc'}         = "whoi"; #whoi, sio, osu
$arg{'platform_id'} = "ast2_sm";
$arg{'deploy_dir'}  = "d201109";

if ($arg{'omc'} eq "" || $arg{'platform_id'} eq "" || $arg{'deploy_dir'} eq "")
   {print "Content-type: text/html\n\n";
    print "***Error: omc, platform_id, and deploy_dir must be specified\n";
    exit(-1);
    }

if ($#pairs == -1) {$arg{'view'} = "platform_status";}


#
# Get current version by looking at 'current' symbolic link
$version = "version: " .  readlink("/root/current");

#%MyID = loadCfg("../Cfg/current_id");

$ddir = "$base_dir/$arg{'omc'}/$arg{'platform_id'}/$arg{'deploy_dir'}/cg_data";
if ($arg{'dcl'} eq "") {$ddir .= "/syslog";}
                  else {$ddir .= "/$arg{'dcl'}/syslog";
		        $MyID = $arg{'dcl'};
			}
                  

# Get latest syslog file (yyyymmdd_hhmm.syslog.log)
$syslog_file  = `cd $ddir && ls -1r *.syslog.log | head -1`;



#If either lf or date specified, use that syslog file instead
if ($arg{'lf'} ne "") {$syslog_file = $arg{'lf'};}
elsif ($arg{'date'} ne "") {$syslog_file = "$arg{'date'}.syslog.log";}

if ($arg{'s'} ne "")
  {$results = `grep "$arg{'s'}" $ddir/$syslog_file`;
   }
elsif ($arg{'rtf'} =~ /^cpm_status/)
     {open(IN,"$ddir/cpm_status.txt") || warn "***Unable to read cpm_status.txt\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'rtf'} =~ /^dcl_status/)
     {open(IN,"$ddir/dcl_status.txt") || warn "***Unable to read dcl_status.txt\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'rtf'} =~ /^sys_status/)
     {open(IN,"$ddir/sys_status.txt") || warn "***Unable to read sys_status.txt\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'rtf'} =~ /^cpm_mission/)
     {open(IN,"$ddir/../cfg_files/cpm_mission.cfg") || warn "***Unable to read cpm_mission.cfg\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'rtf'} =~ /^dcl_mission/)
     {open(IN,"$ddir/../cfg_files/dcl_mission.cfg") || warn "***Unable to read dcl_mission.cfg\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'rtf'} =~ /^cg_net/)
     {open(IN,"$ddir/../cfg_files/cg_net.cfg") || print "***Unable to read cg_net.cfg\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'rtf'} =~ /^fault_table/)
     {my(@cpm_fault_table, @dcl_fault_table, @psc_fault_table);
      open(IN,"../Cfg/mpic_cpm_faults.txt") || warn "***Unable to read cpm_fault_table\n";
      (@cpm_fault_table) = <IN>;
      open(IN,"../Cfg/mpic_dcl_faults.txt") || warn "***Unable to read dcl_fault_table\n";
      (@dcl_fault_table) = <IN>;
      open(IN,"../Cfg/psc_faults.txt") || warn "***Unable to read psc_fault_table\n";
      (@psc_fault_table) = <IN>;
      close(IN);
      push(@all, "\n======================   CPM Fault Table   ==========================\n");
      push(@all,@cpm_fault_table);
      push(@all, "\n\n\n======================   DCL Fault Table   ==========================\n");
      push(@all,@dcl_fault_table);
      push(@all, "\n\n\n======================   PSC Fault Table   ==========================\n");
      push(@all,@psc_fault_table);
      }
elsif ($arg{'view'} =~ /^platform_status/)
     {open(IN,"./view_status|") || warn "***Unable to read view_status\n";
      while ($line = <IN>)
        {next if ($line =~  /^Content-type/);
         push(@all, $line);
         }
      close(IN);
      }
elsif ($arg{'view'} =~ /^istatus/)
     {open(IN,"/cmn-bin/view_istatus|") || warn "***Unable to read istatus\n";
      while ($line = <IN>)
        {next if ($line =~ /^Content-type/);
         push(@all, $line);
         }
      close(IN);
      }
elsif ($arg{'rtf'} =~ /^serial_cfg/)
     {open(IN,"$ddir/../cfg_files/cpm_serial.cfg") || warn "***Unable to read cpm_serial.cfg\n";
      my(@cpm_scfg) = <IN>;
      close(IN);
      push(@all, "\n======================   CPM SERIAL Config ==========================\n");
      push(@all,@cpm_scfg);
      #
      #read cpm_mission.cfg file and loop through all configured DCL ports
      if  ($MyID{'id'} =~ /^stc/i || $MyID{'id'} =~ /^dcl/i)
        {#just show dcl_serial.cfg
         open(IN,"$ddir/../cfg_files/dcl_serial.cfg") || warn "***Unable to read dcl_serial.cfg\n";
         my(@dcl_scfg) = <IN>;
         close(IN);
         push(@all, "\n\n\n======================   DCL SERIAL Config ==========================\n");
         push(@all,@dcl_scfg);
         }
      else
       {#cpm - display all configured DCL ports
        my(%Cfg) = &loadCfg("../Cfg/cpm_mission.cfg");
        for ($i=1;$i<=$Cfg{'max_dcls'};$i++)
          {next if ($Cfg{"dcl$i.pwr"} == -1);
           open(IN,"$ddir/../dcl$i/cfg_files/dcl$i\_serial.cfg") || warn "***Unable to read dcl$i\_serial.cfg\n";
           my(@dcl_scfg) = <IN>;
           close(IN);
           push(@all, "\n\n\n======================   DCL$i SERIAL Config ==========================\n");
           push(@all,@dcl_scfg);
           }
        }
      }
elsif ($arg{'plot'} =~ /^cpu/)
     {#open(IN,"../html/plot_cpu.html") || warn "***Unable to read plot_cpu.html\n";
      open(IN,"/cmn-bin/plot_cpu.pl|") || warn "***Unable to read plot_cpu\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^ports/) # TODO plot_ports.html does not exist on the system
     {open(IN,"../html/plot_ports.html") || warn "***Unable to read plot_ports.html\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^port1/)
     {open(IN,"/cmn-bin/plot_portN.pl port=1|") || warn "***Unable to read plot_portN port=1\n";

      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^port2/)
     {open(IN,"/cmn-bin/plot_portN.pl port=2|") || warn "***Unable to read plot_portN port=2\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^port3/)
     {open(IN,"/cmn-bin/plot_portN.pl port=3|") || warn "***Unable to read plot_portN port=3\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^port4/)
     {open(IN,"/cmn-bin/plot_portN.pl port=4|") || warn "***Unable to read plot_portN port=4\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^port5/)
     {open(IN,"/cmn-bin/plot_portN.pl port=5|") || warn "***Unable to read plot_portN port=5\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^port6/)
     {open(IN,"/cmn-bin/plot_portN.pl port=6|") || warn "***Unable to read plot_portN port=6\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^port7/)
     {open(IN,"/cmn-bin/plot_portN.pl port=7|") || warn "***Unable to read plot_portN port=7\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^port8/)
     {open(IN,"/cmn-bin/plot_portN.pl port=8|") || warn "***Unable to read plot_portN port=8\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^dcl/)
     {open(IN,"/cmn-bin/plot_dcl.pl|") || warn "***Unable to read plot_dcl\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^cpm/)
     {open(IN,"/cmn-bin/plot_cpm.pl|") || warn "***Unable to read plot_cpm\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^ldet/)
     {open(IN,"/cmn-bin/plot_ldet.pl|") || warn "***Unable to read plot_ldet\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^psc/)
     {open(IN,"/cmn-bin/plot_psc.pl|") || warn "***Unable to read plot_psc\n";
      @all = <IN>;
      close(IN);
      }
elsif ($arg{'plot'} =~ /^ntp/)
     {open(IN,"/cmn-bin/plot_ntp.pl|") || warn "***Unable to read plot_ntp\n";
      @all = <IN>;
      close(IN);
      }
     
else {open(IN,"$ddir/$syslog_file") || warn "***Unable to read syslog file [$syslog_file]\n";
      @all = <IN>;
      close(IN);
      }

if ($arg{'extract'} eq "") 
     {print "Content-type: text/html\n\n";
      print <<"EndHTML";
      <HTML>
      <HEAD><TITLE>$syslog_file</TITLE>
            <STYLE TYPE="text/css">
            <!--
             TH{font-family: Arial; font-size: 10pt;}
             TD{font-family: Arial; font-size: 10pt;}
             -->
            </STYLE>
      </HEAD>
      <BODY vlink=blue>
EndHTML
      }
else {print "Content-type: text/plain\n\n";}
$timestamp = gmtimestamp();

$stc_menu_bar = <<"EndHTML";
    <b>Current Time:</b> $timestamp &nbsp &nbsp $version
    <table cellspacing=0 cellpadding=2 border=1>
       <td valign=top><b>View</b>&nbsp
       <td valign=top>  <a href=?view=platform_status>Status</a>&nbsp
       <td valign=top>  <a href=?view=sys_status>System</a>&nbsp
       <td valign=top colspan=2>  
                        <a href=?rtf=cpm_status.txt>CPM</a>&nbsp
                        <a href=?s=\"C_CTL">CTL</a>&nbsp
                        <a href=?rtf=cpm_mission.cfg>MF</a>&nbsp
       <td valign=top colspan=2>
                        <a href=?rtf=dcl_status.txt>DCL</a>&nbsp
                        <a href=?s=\"D_CTL">CTL</a>&nbsp
                        <a href=?rtf=dcl_mission.cfg>MF</a>&nbsp
       <td valign=top>  <a href=?s=\"ALM\">Alarms</a>&nbsp
       <td valign=top>  <a href=?s=\"ERR\">Errors</a>&nbsp
       <td valign=top>  <a href=?s=\"STATUS:\">Msgs</a>  &nbsp
       <td valign=top>  <a href=?rtf=serial_cfg>Serial Cfg</a>&nbsp
       <td valign=top>  <a href=?rtf=cg_net.cfg>Network Cfg</a>&nbsp
       <td valign=top>  <a href=?rtf=fault_tables>Fault Tables</a>&nbsp
       <td valign=top>  <a href=/cg/data target=ViewDDir>Data</a>&nbsp
       </tr><tr>
       <td valign=top><b>Syslog</b>&nbsp
       <td valign=top><a href=?s=>All</a>            &nbsp
       <td valign=top>  <a href=?s=\"cpm:\">MPIC</a> &nbsp
       <td valign=top>  <a href=?s=\"psc:\">PSC</a> &nbsp
       <td valign=top>  <a href=?s=\"C_GPS\">GPS</a> &nbsp
       <td valign=top>  <a href=?s=\"C_PPS\">PPS</a> &nbsp
       <td valign=top>  <a href=?s=\"NTP:\">NTP</a>  &nbsp
       <td valign=top>  <a href=?s=\"CPU\">CPU</a>  &nbsp
       <td valign=top>  <a href=?s=\"3DM\">3DM</a>  &nbsp
       <td valign=top>  <a href=?s=\"C_TELEM_SYS\">Telem</a>  &nbsp
       <td valign=top>  <a href=?s=\"dcl:\">DCL Ports</a>  &nbsp
       <td valign=top colspan=2>  <a href=?s=\"DLOG\">DLOG:</a>&nbsp
              <a href=?s=\"DLOGP1\">1</a>
              <a href=?s=\"DLOGP2\">2</a>
              <a href=?s=\"DLOGP3\">3</a>
              <a href=?s=\"DLOGP4\">4</a>
              <a href=?s=\"DLOGP5\">5</a>
              <a href=?s=\"DLOGP6\">6</a>
              <a href=?s=\"DLOGP7\">7</a>
              <a href=?s=\"DLOGP8\">8</a>&nbsp
       </td>
       </tr><tr>
      </table>
      <table>
       <td valign=top><b>Plot:</b>&nbsp
                        <a href=?plot=ntp>ntp</a>
                        <a href=?plot=cpm>cpm</a>
                        <a href=?plot=dcl>dcl</a>
                        <a href=?plot=psc>psc</a>
                        <a href=?plot=cpu>cpu</a>
                        <a href=?plot=ldet>ldet</a> &nbsp
                        <a href=?plot=ports>ports:</a>
                              <a href=?plot=port1>1</a>
                              <a href=?plot=port2>2</a>
                              <a href=?plot=port3>3</a>
                              <a href=?plot=port4>4</a>
                              <a href=?plot=port5>5</a>
                              <a href=?plot=port6>6</a>
                              <a href=?plot=port7>7</a>
                              <a href=?plot=port8>8</a> &nbsp
                              <a href=?view=istatus>istatus</a>
       </td>
      </table>
EndHTML


$cpm_menu_bar = <<"EndHTML";
    <b>Current Time:</b> $timestamp &nbsp &nbsp $version
    <table cellspacing=0 cellpadding=2 border=1>
       <td valign=top><b>View</b>&nbsp
       <td valign=top>  <a href=?view=platform_status>Status</a>&nbsp
       <td valign=top>  <a href=?view=sys_status>System</a>&nbsp
       <td valign=top colspan=2>  
                        <a href=?rtf=cpm_status.txt>CPM</a>&nbsp
                        <a href=?s=\"C_CTL">CTL</a>&nbsp
                        <a href=?rtf=cpm_mission.cfg>MF</a>&nbsp
       <td valign=top>  <a href=?s=\"ALM\">Alarms</a>&nbsp
       <td valign=top>  <a href=?s=\"ERR\">Errors</a>&nbsp
       <td valign=top>  <a href=?s=\"STATUS:\">Msgs</a>  &nbsp
       <td valign=top>  <a href=?rtf=serial_cfg>Serial Cfg</a>&nbsp
       <td valign=top>  <a href=?rtf=cg_net.cfg>Network Cfg</a>&nbsp
       <td valign=top>  <a href=?rtf=fault_tables>Fault Tables</a>&nbsp
       <td valign=top>  <a href=/cg/data target=ViewDDir>Data</a>&nbsp
       </tr><tr>
       <td valign=top><b>Syslog</b>&nbsp
       <td valign=top><a href=?s=>All</a>            &nbsp
       <td valign=top>  <a href=?s=\"cpm:\">MPIC</a> &nbsp
       <td valign=top>  <a href=?s=\"C_POWER_SYS%20\">PSC</a> &nbsp
       <td valign=top>  <a href=?s=\"C_GPS\">GPS</a> &nbsp
       <td valign=top>  <a href=?s=\"C_PPS\">PPS</a> &nbsp
       <td valign=top>  <a href=?s=\"NTP:\">NTP</a>  &nbsp
       <td valign=top>  <a href=?s=\"CPU\">CPU</a>  &nbsp
       <td valign=top>  <a href=?s=\"C_IRID\">Irid</a>  &nbsp
       </tr><tr>
      </table>
      <table>
       <td valign=top><b>Plot:</b>&nbsp
                        <a href=?plot=ntp>ntp</a>
                        <a href=?plot=cpm>cpm</a>
                        <a href=?plot=psc>psc</a>
                        <a href=?plot=cpu>cpu</a> 
                        <a href=?plot=ldet>ldet</a> &nbsp
       </td>
      </table>
EndHTML


$dcl_menu_bar = <<"EndHTML";
    <b>Current Time:</b> $timestamp &nbsp &nbsp $version
    <table cellspacing=0 cellpadding=2 border=1>
       <td valign=top><b>View</b>&nbsp
       <td valign=top>  <a href=?view=platform_status>Status</a>&nbsp
       <td valign=top>  <a href=?view=sys_status>System</a>&nbsp
       <td valign=top colspan=2>  
                        <a href=?rtf=cpm_status.txt>CPM</a>&nbsp
                        <a href=?s=\"C_CTL">CTL</a>&nbsp
                        <a href=?rtf=cpm_mission.cfg>MF</a>&nbsp
       <td valign=top colspan=2>
                        <a href=?rtf=dcl_status.txt>DCL</a>&nbsp
                        <a href=?s=\"D_CTL">CTL</a>&nbsp
                        <a href=?rtf=dcl_mission.cfg>MF</a>&nbsp
       <td valign=top>  <a href=?s=\"ALM\">Alarms</a>&nbsp
       <td valign=top>  <a href=?s=\"ERR\">Errors</a>&nbsp
       <td valign=top>  <a href=?s=\"STATUS:\">Msgs</a>  &nbsp
       <td valign=top>  <a href=?rtf=serial_cfg>Serial Cfg</a>&nbsp
       <td valign=top>  <a href=?rtf=cg_net.cfg>Network Cfg</a>&nbsp
       <td valign=top>  <a href=?rtf=fault_tables>Fault Tables</a>&nbsp
       <td valign=top>  <a href=/cg/data target=ViewDDir>Data</a>&nbsp
       </tr><tr>
       <td valign=top><b>Syslog</b>&nbsp
       <td valign=top><a href=?s=>All</a>            &nbsp
       <td valign=top>  <a href=?s=\"cpm:\">MPIC</a> &nbsp
       <td valign=top>  <a href=?s=\"C_POWER_SYS%20\">PSC</a> &nbsp
       <td valign=top>  <a href=?s=\"C_GPS\">GPS</a> &nbsp
       <td valign=top>  <a href=?s=\"C_PPS\">PPS</a> &nbsp
       <td valign=top>  <a href=?s=\"NTP:\">NTP</a>  &nbsp
       <td valign=top>  <a href=?s=\"CPU\">CPU</a>  &nbsp
       <td valign=top>  <a href=?s=\"3DM\">3DM</a>  &nbsp
       <td valign=top>  <a href=?s=\"C_TELEM_SYS\">Telem</a>  &nbsp
       <td valign=top>  <a href=?s=\"dcl:\">DCL Ports</a>  &nbsp
       <td valign=top colspan=2>  <a href=?s=\"DLOG\">DLOG:</a>&nbsp
              <a href=?s=\"DLOGP1\">1</a>
              <a href=?s=\"DLOGP2\">2</a>
              <a href=?s=\"DLOGP3\">3</a>
              <a href=?s=\"DLOGP4\">4</a>
              <a href=?s=\"DLOGP5\">5</a>
              <a href=?s=\"DLOGP6\">6</a>
              <a href=?s=\"DLOGP7\">7</a>
              <a href=?s=\"DLOGP8\">8</a>&nbsp
       </td>
       </tr><tr>
      </table>
      <table>
       <td valign=top><b>Plot:</b>&nbsp
                        <a href=?plot=ntp>ntp</a>
                        <a href=?plot=cpm>cpm</a>
                        <a href=?plot=dcl>dcl</a>
                        <a href=?plot=psc>psc</a>
                        <a href=?plot=cpu>cpu</a>
                        <a href=?plot=ldet>ldet</a> &nbsp
                        <a href=?plot=ports>ports:</a>
                              <a href=?plot=port1>1</a>
                              <a href=?plot=port2>2</a>
                              <a href=?plot=port3>3</a>
                              <a href=?plot=port4>4</a>
                              <a href=?plot=port5>5</a>
                              <a href=?plot=port6>6</a>
                              <a href=?plot=port7>7</a>
                              <a href=?plot=port8>8</a> &nbsp
                              <a href=?view=istatus>istatus</a>
       </td>
      </table>
EndHTML

if ($arg{'extract'} eq "")
  {if    ($MyID{'id'} =~ /cpm/i) {print "$cpm_menu_bar";}
   elsif ($MyID{'id'} =~ /dcl/i) {print "$dcl_menu_bar";}
   else {print "$stc_menu_bar";}

   if ($arg{'plot'} eq "" && $arg{'view'} ne "platform_status"  && $arg{'view'} ne "istatus") 
        {print "<pre>$results @all</pre>\n";}
   else {print "$results @all<br>\n";}

   if    ($MyID{'id'} =~ /cpm/i) {print "$cpm_menu_bar";}
   elsif ($MyID{'id'} =~ /dcl/i) {print "$dcl_menu_bar";}
   else {print "$stc_menu_bar";}
   }
else
  {#extract columns
   my(@col_list)   = split(',',$arg{'extract'});
   my(@label_list) = split(',',$arg{'label'});
   my($line,$col,$fields,$i);
   print "timestamp, ";
   $i = 0;
   foreach $label (@label_list)
     {if ($i++ == $#label_list) {print "$label ";}
                           else {print "$label, ";}
      }
   print "\n";

   my($line_cnt) = 0;
   my($MAX_EXTRACT_LIMIT) = $arg{'limit'};  #Max points to extract and plot
   foreach $line (split('\n',$results))
     {(@fields) = split(' ',$line);
      $fields[1] = substr($fields[1],0,length($fields[1])-4); #elim .nnn secs
      print "$fields[0] $fields[1], ";
      $i=0; 
      foreach $col (@col_list)
        {if ($arg{'sum'} eq "1")
           {$fields[$col-1] += $prev_sum[$col-1];
            $prev_sum[$col-1] = $fields[$col-1];
            }
         if ($i++ == $#col_list) {print "$fields[$col-1] ";}
                            else {print "$fields[$col-1], ";}
         }
      print "\n";
      last if ($MAX_EXTRACT_LIMIT ne "" && $line_cnt++ > $MAX_EXTRACT_LIMIT);
      }
   }

if ($arg{'extract'} eq "")
  {print "</BODY>\n";
   print "</HTML>\n";
   }

exit;

#
# Subroutines Follow
#
sub gmtimestamp {local($no_gmtlabel) = $_[0];
        local($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime;
        # zero fill hour,min,sec and month
        if ($sec < 10) {$sec = "0$sec";}
        if ($min < 10) {$min = "0$min";}
        if ($hour < 10) {$hour = "0$hour";}
        $mon += 1; #from gmtime defined 0-11, make it 1-12
        if ($mon < 10) {$mon = "0$mon";}
        if ($mday < 10) {$mday = "0$mday";}
        #make year 4-digits - Note: Year is years since 1900
        $year += 1900;
        if ($no_gmtlabel)
             {return "$year/$mon/$mday $hour:$min:$sec";}
        else {return "$year/$mon/$mday $hour:$min:$sec GMT";}
}



sub loadCfg {my($file) = $_[0];
   my(%Cfg);
   my($line,$att,$val);
   my($att_val,$comment);
   my($FD);

  if (!open($FD,"$file")) {print "***Unable to open Cfg File [$file]\n";
                           return 0;
                           }

  while ($line = <$FD>)
      {next if ($line =~ /^#/); #skip comments
       next if ($line !~ /=/);  #skip blank lines or don't contain '='
       ($att_val,$comment) = split("#",$line,2);
       ($att,$val) = split("=",$att_val,2);
       $att =~ s/^(\s+)//; #remove leading spaces
       $att =~ s/(\s+)$//; #remove trailing spaces
       $val =~ s/^(\s+)//; #remove leading spaces
       $val =~ s/(\s+)$//; #remove trailing spaces
       $Cfg{$att} = $val;
       }
   close($FD);

   return %Cfg;
}

