#!/usr/bin/perl
# view_status.pl -  Quick script to view platform real-time status
#
# Usaage: view_status
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create
#      05/10/2011  SL     Updated to support platform.ctype ie; ffc, stc, mfn, bep
#      05/25/2011  SL     Added telem (irid)
#      06/20/2011  SL     Added if CPM, display status from associated DCLs
#      03/28/2013  SL     Updated to support new cpm/dcl numbering scheme and stc1
#      06/20/2013  SL     Updated to display status from all cpms/dcls configured
#                         in cpm_mission.cfg file if run on a CPM.
#      10/09/2013  SL     Add option to view full platform
#      08/18/2016  JJP	  Use /cmn-bin ScriptAlias for common view_* files
##################################################################################

my($ddir) = "/data/cg_data";
my($results,@all);
my($DEBUG) = 0;

my($RED)    = "#ff0000";
my($YELLOW) = "#ffff00";
my($GREEN)  = "#00dd00";
my($GRAY)   = "#cccccc";

#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'QUERY_STRING'} ne "")
 {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }
if ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = $value\n";
}

if ($arg{'d'} ne "") {$DEBUG = 1;}

# Load platform and mission cfg files
%MyID  = &loadCfg("../Cfg/current_id");
%Cfg   = &loadCfg("../Cfg/cpm_mission.cfg");

#Load net cfg file to get IP Addresses for each dcl
%NCfg = &load_net_cfg("../Cfg/cg_net.cfg");


print "Content-type: text/html\n\n";
      print <<"EndHTML";
      <HTML>
      <HEAD><TITLE>$MyID{'platform'} Status</TITLE>
            <STYLE TYPE="text/css">
            <!--
             TH{font-family: Arial; font-size: 10pt;}
             TD{font-family: Arial; font-size: 10pt;}
             -->
            </STYLE>
      </HEAD>
      <BODY vlink=blue><br>
EndHTML

if    ($arg{'view_platform'} eq "1")
         {$platform_status = "<table bgcolor=#004488 border=1 cellspacing=5 cellpadding=3 valign=top>\n";
          foreach $cpm (split(' ',$Cfg{'platform.cpms'}))
	     {$cpm_caps = $cpm; $cpm_caps =~ tr/a-z/A-Z/;
              if ($cpm eq $MyID{'id'}) {$platform_status .= "<td valign=top>\n";
	                                $platform_status .= `./view_cpm_status`;
	                                $platform_status .= "</td>\n";
					}
	      else {$cpm_status = `wget -q -t1 -T 4 -O - http://$NCfg{"$cpm_caps.ip_addr"}/cg-bin/view_cpm_status`;
                    if ($cpm_status eq "") {$cpm_status = "<h2>$cpm_caps Status Not Available [$NCfg{\"$cpm_caps.ip_addr\"}]</h2>\n";} 
                    $platform_status .= "<td valign=top>$cpm_status</td>";
	            }
              # Add-in any DCL status
              my $cpm_num = $cpm; $cpm_num =~ s/cpm//;
              foreach $dcl (split(' ',$Cfg{'platform.dcls'}))
   	         {next if (substr($dcl,3,1) ne "$cpm_num"); #only want dcls controlled by $cpm
		  $dcl_caps = $dcl; $dcl_caps =~ tr/a-z/A-Z/;
                  $dcl_status = `wget -q -t1 -T 4 -O - http://$NCfg{"$dcl_caps.ip_addr"}/cmn-bin/view_dcl_status`;
                  if ($dcl_status eq "") {$dcl_status = "<h2>$dcl_caps Status Not Available [$NCfg{\"$dcl_caps.ip_addr\"}]</h2>\n";} 
                  $platform_status .= "<td valign=top>$dcl_status</td>";
	          }
	      $platform_status .= "</tr>\n";
	      }
          $platform_status .= "</table>\n";
          }
elsif ($MyID{'id'} =~ /^cpm/)
         {$platform_status = `/cmn-bin/view_cpm_status`;}
elsif ($MyID{'id'} =~ /^dcl/)
         {$platform_status = `/cmn-bin/view_dcl_status`;}
elsif ($MyID{'id'} =~ /^stc/)
         {$platform_status = `/cmn-bin/view_stc_status`;}
else {$platform_status = "Unknown platform id [$MyID{'id'}]\n"};

$platform_status =~ s#Content-type: text/html##;
#print "<pre> $platform_status </pre>";
print "$platform_status";

print "</BODY>\n";
print "</HTML>\n";

exit;

#
# Subroutines Follow
#


sub loadCfg {my($file) = $_[0];
   my(%Cfg);
   my($line,$att,$val);
   my($att_val,$comment);
   my($FD);

  if (!open($FD,"$file")) {print "***Unable to open Cfg File [$file]\n";
                           return 0;
                           }

  while ($line = <$FD>)
      {next if ($line =~ /^#/); #skip comments
       next if ($line !~ /=/);  #skip blank lines or don't contain '='
       ($att_val,$comment) = split("#",$line,2);
       ($att,$val) = split("=",$att_val,2);
       $att =~ s/^(\s+)//; #remove leading spaces
       $att =~ s/(\s+)$//; #remove trailing spaces
       $val =~ s/^(\s+)//; #remove leading spaces
       $val =~ s/(\s+)$//; #remove trailing spaces
       $Cfg{$att} = $val;
       }
   close($FD);

   return %Cfg;
}


sub load_net_cfg {my($file) = $_[0];
   my(%Cfg,$line,$CFG_FD);
   if (!open($CFG_FD,"$file")) {print "***Unable to open config file [$file]";
                                exit(-1);
                                }
  while ($line = <$CFG_FD>)
      {next if ($line =~ /^#/ || length($line) < 2); #skip comments & blank lines
       chomp($line);
       $line =~ s/(\s+)/ /gs; #eliminate multiple spaces
       my($id,$name,$udp_port,$ip_addr,$block,$timeout,$debug_level) = split(' ',$line);
       $Cfg{"$name.id"} = "$id";
       $Cfg{"$name.udp_port"} = "$udp_port";
       $Cfg{"$name.ip_addr"}  = "$ip_addr";
       #print "Cfg{\"$name.ip_addr\"}  = $ip_addr\n";
       }
   close($CFG_FD);
   return %Cfg;
}

