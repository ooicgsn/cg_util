#!/usr/bin/perl
# plot_psc.pl -  Quick script to generate rt psc data plot
#
# Usage: plot_psc.pl
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create from plot_psc.html, but now use in-line data
#      08/03/2011  SL     Updated to plot currents for bt, pv, wt. Added plot
#                         for power generation sources
#      10/18/2011  SL     Added battery charge and re-arranged plots
#      11/16/2013  SL     Added 300V Cvtr plots
##################################################################################
@vmain_data =  `./view_syslog limit=1500 s=\" psc: \" extract=6   label=Vmain`; 
@imain_data =  `./view_syslog limit=1500 s=\" psc: \" extract=7   label=Imain`; 
@bat_data  =  `./view_syslog limit=1500 s=\" psc: \" extract=47,51,55,59  label=bt1,bt2,bt3,bt4`; 
@pv_data   =  `./view_syslog limit=1500 s=\" psc: \" extract=15,19,23,27 label=pv1,pv2,pv3,pv4`; 
@wt_data   =  `./view_syslog limit=1500 s=\" psc: \" extract=31,35 label=wt1,wt2`; 
@src_data  =  `./view_syslog limit=1500 s=\" psc: \" extract=13,17,21,25,29,33,37,41 label=pv1,pv2,pv3,pv4,wt1,wt2,fc1,fc2`; 
@bcharge   =  `./view_syslog limit=1500 s=\" psc: \" extract=8 label=Charge`; 
@cvtr_v    =  `./view_syslog limit=1500 s=\" psc: \" extract=75 label=cvt_v`;
@cvtr_i    =  `./view_syslog limit=1500 s=\" psc: \" extract=76 label=cvt_i`;


$html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<table>
   <td>PSC Vmain
       <div id="graphdiv1"
        style="width:500px; height:200px;"></div>
   </td>
   <td>PSC Imain (ma)  
       <div id="graphdiv6"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td>Battery Charge (%)  
       <div id="graphdiv7"
        style="width:500px; height:200px;"></div>
   </td>
   <td>Sources
       <div id="graphdiv5"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td>PV 1-4 ma
       <div id="graphdiv3"
        style="width:500px; height:200px;"></div>
   </td>
   <td>WT 1-2 ma
       <div id="graphdiv4"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td>BT 1-4 ma
       <div id="graphdiv2"
        style="width:500px; height:px;"></div>
   </td>
   <td>300V Cvtr - volts
       <div id="graphdiv8" 
        style="width:500px; height:240px;"></div>
   </td>
   <td>300V Cvtr - ma
       <div id="graphdiv9" 
        style="width:500px; height:240px;"></div>
   </td>
   </tr>

</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
EndHTML
    for ($i=0;$i<$#vmain_data;$i++)
       {$line = $vmain_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#vmain_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      valueRange: [20,32]
     }          // options
  );

  g6 = new Dygraph(
    document.getElementById("graphdiv6"),
EndHTML
    for ($i=0;$i<$#imain_data;$i++)
       {$line = $imain_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#imain_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g7 = new Dygraph(
    document.getElementById("graphdiv7"),
EndHTML
    for ($i=0;$i<$#bcharge;$i++)
       {$line = $bcharge[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#bcharge-1){$html_out .= "\"$line\\n\" +\n";}
                        else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
EndHTML
    for ($i=0;$i<$#bat_data;$i++)
       {$line = $bat_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#bat_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g3 = new Dygraph(
    document.getElementById("graphdiv3"),
EndHTML
    for ($i=0;$i<$#pv_data;$i++)
       {$line = $pv_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#pv_data-1){$html_out .= "\"$line\\n\" +\n";}
                        else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g4 = new Dygraph(
    document.getElementById("graphdiv4"),
EndHTML
    for ($i=0;$i<$#wt_data;$i++)
       {$line = $wt_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#wt_data-1){$html_out .= "\"$line\\n\" +\n";}
                        else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g5 = new Dygraph(
    document.getElementById("graphdiv5"),
EndHTML
    for ($i=0;$i<$#src_data;$i++)
       {$line = $src_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#src_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      stackedGraph: true,
      writeStroke: 2.0,
      valueRange: [0,8]
     }          // options
  );

  g8 = new Dygraph(
    document.getElementById("graphdiv8"),
EndHTML
    for ($i=0;$i<$#cvtr_v;$i++)
       {$line = $cvtr_v[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#cvtr_v-1){$html_out .= "\"$line\\n\" +\n";}
                       else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
     }          // options
  );

  g9 = new Dygraph(
    document.getElementById("graphdiv9"),
EndHTML
    for ($i=0;$i<$#cvtr_i;$i++)
       {$line = $cvtr_i[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#cvtr_i-1){$html_out .= "\"$line\\n\" +\n";}
                       else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
     }          // options
  );

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

