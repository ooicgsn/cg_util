#!/usr/bin/perl
# plot_ntp.pl -  Quick script to generate rt ntp plot
#
# Usage: plot_ntp.pl pid= deploy= [dcl=dclN]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create from plot_ntp.html, but now use in-line data
#      08/07/2011  SL     Added dcl arg to plot from cg_data/dclN/syslog dir
#      12/01/2013  SL     Added date arg
#      12/23/2014  SL     Updated to use cg_util.pl
#      02/02/2015  SL     Support Deployment dir
##################################################################################
use strict;
require "cg_util.pl";

my %arg    = &get_args(0);
my $pid    = $arg{'pid'};
my $deploy = $arg{'deploy'};

if ($pid eq "" || $deploy eq "") {print "pid and deploy must be specified\n"; exit(-1);}

my (@latency_data, @jitter_data);
if ($arg{'dcl'} eq "")
  {@latency_data = `./view_syslog pid=$pid deploy=$deploy s=\" NTP: \" extract=10 label=Latency date=$arg{'date'}`; 
   @jitter_data  = `./view_syslog pid=$pid deploy=$deploy s=\" NTP: \" extract=11 label=Jitter date=$arg{'date'}`; 
   }
else
  {@latency_data = `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} s=\" NTP: \" extract=10 label=Latency date=$arg{'date'}`; 
   @jitter_data  = `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} s=\" NTP: \" extract=11 label=Jitter date=$arg{'date'}`; 
   }

my ($i, $line);
my $tag = "$arg{'dcl'}";
my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<table boarer=1>
<td><table>
   <td>$tag NTP Latency (ms)
       <div id="graphdiv1_$tag"
        style="width:500px; height:300px;"></div>
   </td>
   <td>Jitter (ms)
       <div id="graphdiv2_$tag"
        style="width:500px; height:300px;"></div>
   </td>
</td>
</table>
</table>

<script type="text/javascript">

  g1_$tag = new Dygraph(
    document.getElementById("graphdiv1_$tag"),
EndHTML
    for ($i=0;$i<$#latency_data;$i++)
       {$line = $latency_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
	my($ts,$latency) = split(',',$line);
	next if ($latency !~ /\d/);
        if ($i < $#latency_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 1.0,
      showRoller: false,
      rollPeriod: 1
      //valueRange: [-1.0,1.0]
     }          // options
  );

  g2_$tag = new Dygraph(
    document.getElementById("graphdiv2_$tag"),
EndHTML
    for ($i=0;$i<$#jitter_data;$i++)
       {$line = $jitter_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
	my($ts,$jitter) = split(',',$line);
	next if ($jitter !~ /\d/);
        if ($i < $#jitter_data-1){$html_out .= "\"$line\\n\" +\n";}
                            else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;
