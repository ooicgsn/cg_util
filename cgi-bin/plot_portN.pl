#!/usr/bin/perl
# plot_portN.pl -  Quick script to generate rt DCL port status
#
# Usage: plot_portN.pl port=n [dcl=dclN]
#             where n=1-8
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create
#      08/07/2011  SL     Added dcl arg to plot from cg_data/dclN/syslog
#      07/10/2013  SL     Support STC
#      09/18/2013  SL     Updated to support new STC firmware - v2.07
##################################################################################

my($ddir) = "/data/cg_data";
my($syslog_file);
my($results,@all);
my($DEBUG) = 0;

my($RED)    = "#ff0000";
my($YELLOW) = "#ffff00";
my($GREEN)  = "#00dd00";
my($GRAY)   = "#cccccc";

my $plot_col_tx_rx_log   = "11,13,15";
my $plot_col_gd_bd       = "17,19,21";
my %plot_col;
$plot_col{'p1.state'} = "29";
$plot_col{'p1.ma'}    = "31";
$plot_col{'p2.state'} = "34";
$plot_col{'p2.ma'}    = "36";
$plot_col{'p3.state'} = "39";
$plot_col{'p3.ma'}    = "41";
$plot_col{'p4.state'} = "44";
$plot_col{'p4.ma'}    = "46";
$plot_col{'p5.state'} = "49";
$plot_col{'p5.ma'}    = "51";
$plot_col{'p6.state'} = "54";
$plot_col{'p6.ma'}    = "56";
$plot_col{'p7.state'} = "59";
$plot_col{'p7.ma'}    = "61";
$plot_col{'p8.state'} = "64";
$plot_col{'p8.ma'}    = "66";

my $stc_plot_col_tx_rx_log   = "11,13,15";
my $stc_plot_col_gd_bd       = "17,19,21";
my %stc_plot_col;
$stc_plot_col{'p1.state'} = "61";
$stc_plot_col{'p1.ma'}    = "62";
$stc_plot_col{'p2.state'} = "66";
$stc_plot_col{'p2.ma'}    = "67";
$stc_plot_col{'p3.state'} = "71";
$stc_plot_col{'p3.ma'}    = "72";
$stc_plot_col{'p5.state'} = "76";
$stc_plot_col{'p5.ma'}    = "77";
$stc_plot_col{'p7.state'} = "81";
$stc_plot_col{'p7.ma'}    = "82";

#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'QUERY_STRING'} ne "")
 {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }
if ($#ARGV >= 0) {@pairs = @ARGV;}


#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = $value\n";
}

#print "Content-type: text/html\n\n";

if ($arg{'d'} ne "") {$DEBUG = 1;}

if ($arg{'port'} eq "")
   {print "Usage $0 port=<1-8>\n";
    exit(-1);
    }

if ($arg{'type'} eq "stc")
  {@state_data =  `./view_syslog limit=25 s=\"stc: \" extract=$stc_plot_col{"p$arg{'port'}.state"} label=state`; 
   @ma_data    =  `./view_syslog limit=25 s=\"stc: \" extract=$stc_plot_col{"p$arg{'port'}.ma"} label=ma`; 
   @txrx_data  =  `./view_syslog limit=25 s=\"DLOGP$arg{'port'} istatus:\" extract=$stc_plot_col_tx_rx_log label=Tx,Rx,Log`; 
   @gdbd_data  =  `./view_syslog limit=25 s=\"DLOGP$arg{'port'} istatus:\" extract=$stc_plot_col_gd_bd label=Good,Bad,BB`; 
   }
elsif ($arg{'dcl'} eq "")
  {@state_data =  `./view_syslog s=\"dcl: \" extract=$plot_col{"p$arg{'port'}.state"} label=state`; 
   @ma_data    =  `./view_syslog s=\"dcl: \" extract=$plot_col{"p$arg{'port'}.ma"} label=ma`; 
   @txrx_data  =  `./view_syslog s=\"DLOGP$arg{'port'} istatus:\" extract=$plot_col_tx_rx_log label=Tx,Rx,Log`; 
   @gdbd_data  =  `./view_syslog s=\"DLOGP$arg{'port'} istatus:\" extract=$plot_col_gd_bd label=Good,Bad,BB`; 
   }
else
  {@state_data =  `./view_dcl_syslog dcl=$arg{'dcl'} s=\"dcl: \" extract=$plot_col{"p$arg{'port'}.state"} label=state`; 
   @ma_data    =  `./view_dcl_syslog dcl=$arg{'dcl'} s=\"dcl: \" extract=$plot_col{"p$arg{'port'}.ma"} label=ma`; 
   @txrx_data  =  `./view_dcl_syslog dcl=$arg{'dcl'} s=\"DLOGP$arg{'port'} istatus:\" extract=$plot_col_tx_rx_log label=Tx,Rx,Log`; 
   @gdbd_data  =  `./view_dcl_syslog dcl=$arg{'dcl'} s=\"DLOGP$arg{'port'} istatus:\" extract=$plot_col_gd_bd label=Good,Bad,BB`; 
   }


#
#Read through dcl_serial.cfg file to get Instrument Name for DLOGPn
#metbk         /dev/ttts10    9600 N 8 1 0   MSG   DLOGP8 bin/dl_metbk
my($InstName) = "";
if ($arg{'dcl'} eq "") {open($Cfg_FD,"<../Cfg/dcl_serial.cfg");}
                  else {open($Cfg_FD,"<$ddir/$arg{'dcl'}/cfg_files/dcl_serial.cfg");}
while ($line = <$Cfg_FD>)
   {next if ($line =~ /^#/); #skip comments
    chomp($line);
    my($inst_name,$port,$baud,$par,$nbit,$sbit,$flow,$dlevel,$DLOGP) = split(' ',$line);
    if ($DLOGP =~ /^DLOGP/i)
          {my($dlogp_num);
           ($dlogp_num  = $DLOGP) =~ s/DLOGP//;
           if ($dlogp_num eq $arg{'port'}) {$InstName = "$inst_name";
                                            last;
                                            }
           }
    }
close($Cfg_FD);
#print "INST = $InstName\n";

my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<table>
   <td><b><font size=+1>$InstName</font></b> &nbsp DCL$arg{'dcl'} Port$arg{'port'} State
       <div id="graphdiv1"
        style="width:500px; height:200px;"></div>
   </td>
   <td>Current (ma)
       <div id="graphdiv2"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td>Tx/Rx/Logged
       <div id="graphdiv3"
        style="width:500px; height:200px;"></div>
   </td>
   <td>Good/Bad
       <div id="graphdiv4"
        style="width:500px; height:200px;"></div>
</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
EndHTML
    for ($i=0;$i<$#state_data;$i++)
       {$line = $state_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#state_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }

$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      fillGraph: true,
      stepPlot: true,
      rollPeriod: 1,
      writeStroke: 2.0,
      valueRange: [0,1.5]
     }          // options
  );



  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
EndHTML
    for ($i=0;$i<$#ma_data;$i++)
       {$line = $ma_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#ma_data-1){$html_out .= "\"$line\\n\" +\n";}
                        else {$html_out .= "\"$line\\n\" ,\n";}
        }

$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );



  g3 = new Dygraph(
    document.getElementById("graphdiv3"),
EndHTML
    for ($i=0;$i<$#txrx_data;$i++)
       {$line = $txrx_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }

$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsKMB: true
     }          // options
  );



  g4 = new Dygraph(
    document.getElementById("graphdiv4"),
EndHTML
    for ($i=0;$i<$#gdbd_data;$i++)
       {$line = $gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }

$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 1.0,
      drawPoints: true,
      showRoller: false,
      rollPeriod: 1,
      labelsKMB: true
     }          // options
  );


</script>
</body>
</html>
EndHTML


print "$html_out";
exit;

