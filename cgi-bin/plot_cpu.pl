#!/usr/bin/perl
# plot_cpu.pl -  Quick script to generate rt cpu plot
#
# Usage: plot_cpu.pl pid= deploy= [dcl=dclN]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create from plot_cpu.html, but now use in-line data
#      08/07/2011  SL     Added dcl arg to plot from cg_data/dclN/syslog dir
#      10/28/2012  SL     Added pid as arg
#      12/01/2013  SL     Added date as arg
#      12/23/2014  SL     Updated to use cg_util.pl
#      02/02/2015  SL     Support Deployment dir
##################################################################################
use strict;
require "cg_util.pl";

my %arg    = &get_args(0);
my $pid    = $arg{'pid'};
my $deploy = $arg{'deploy'};
if ($pid eq "" || $deploy eq "") {print "pid and deploy must be specified\n"; exit(-1);}

my (@cpu_data, @mem_data);
if ($arg{'type'} =~ /stc/i)
  {@cpu_data =  `./view_syslog pid=$pid deploy=$deploy limit=1500 s=\"stc:\" extract=11,12,13 label=1min,5min,15min date=$arg{'date'}`;
   @mem_data =  `./view_syslog pid=$pid deploy=$deploy limit=1500 s=\"stc:\" extract=15 label=Free date=$arg{'date'}`;
   }
else
  {if ($arg{'dcl'} eq "")
     {@cpu_data =  `./view_syslog pid=$pid deploy=$deploy s=\" C_STATUS CPU \" extract=11,12,13 label=1min,5min,15min date=$arg{'date'}`; 
      @mem_data =  `./view_syslog pid=$pid deploy=$deploy s=\" C_STATUS CPU \" extract=15 label=Free date=$arg{'date'}`; 
      }
   else
     {@cpu_data =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} s=\" D_STATUS CPU \" extract=11,12,13 label=1min,5min,15min date=$arg{'date'}`;
      @mem_data =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} s=\" D_STATUS CPU \" extract=15 label=Free date=$arg{'date'}`; 
      }
   }

my ($i, $line);
my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<!-- Platform ID: $arg{'pid'} <br>
-->

<table>
   <td>$arg{'type'} $arg{'dcl'} CPU Load
       <div id="graphdiv1_$arg{'type'}_$arg{'dcl'}"
        style="width:500px; height:300px;"></div>
   </td>
   <td>Free Memory
       <div id="graphdiv2_$arg{'type'}_$arg{'dcl'}"
        style="width:500px; height:300px;"></div>
   </td>
</table>

<script type="text/javascript">

  g1_$arg{'type'}_$arg{'dcl'} = new Dygraph(
    document.getElementById("graphdiv1_$arg{'type'}_$arg{'dcl'}"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?s=%22C_STATUS CPU%22&extract=11,12,13&label=1min,5min,15min", // URL or path to CSV file
EndHTML
    for ($i=0;$i<$#cpu_data;$i++)
       {$line = $cpu_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#cpu_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g2_$arg{'type'}_$arg{'dcl'} = new Dygraph(
    document.getElementById("graphdiv2_$arg{'type'}_$arg{'dcl'}"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?s=%22C_STATUS CPU%22&extract=15&label=Free", // URL or path to CSV file
EndHTML
    for ($i=0;$i<$#mem_data;$i++)
       {$line = $mem_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#mem_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      labelsKMB: true
     }          // options
  );

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

