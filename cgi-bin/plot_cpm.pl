#!/usr/bin/perl
# plot_cpm.pl -  Quick script to generate rt cpm plot
#
# Usage: plot_cpm.pl pid= deploy=
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create from plot_cpm.html, but now use in-line data
#      09/02/2011  SL     Eliminated BBat as no longer on CPM board and added 
#                         CPM main current
#      10/20/2011  SL     Added leak enable and leak detect
#      10/28/2012  SL     Added pid and cpmN as arg
#      12/01/2013  SL     Added date arg
#      12/23/2014  SL     Updated to use cg_util.pl
#      02/02/2015  SL     Support Deployment dir
##################################################################################
use strict;
require "cg_util.pl";

my %arg    = &get_args(0);
my $pid    = $arg{'pid'};
my $deploy = $arg{'deploy'};
my $limit  = 3000;

if ($pid eq "" || $deploy eq "") {print "pid and deploy must be specified\n"; exit(-1);}


my @bat_data   =  `./view_syslog pid=$pid deploy=$deploy cpm=$arg{'cpm'} limit=$limit s=\" cpm: \" extract=6   label=MainV date=$arg{'date'}`; 
my @curr_data  =  `./view_syslog pid=$pid deploy=$deploy cpm=$arg{'cpm'} limit=$limit s=\" cpm: \" extract=7   label=MainMa date=$arg{'date'}`; 
my @temp_data  =  `./view_syslog pid=$pid deploy=$deploy cpm=$arg{'cpm'} limit=$limit s=\" cpm: \" extract=12,13 label=t1,t2 date=$arg{'date'}`; 
my @humid_data =  `./view_syslog pid=$pid deploy=$deploy cpm=$arg{'cpm'} limit=$limit s=\" cpm: \" extract=15 label=Humidity date=$arg{'date'}`; 
my @press_data =  `./view_syslog pid=$pid deploy=$deploy cpm=$arg{'cpm'} limit=$limit s=\" cpm: \" extract=17 label=Pressure date=$arg{'date'}`; 
my @ldet_enable=  `./view_syslog pid=$pid deploy=$deploy cpm=$arg{'cpm'} limit=$limit s=\" cpm: \" extract=25 label=enable date=$arg{'date'}`; 
my @ldet_data  =  `./view_syslog pid=$pid deploy=$deploy cpm=$arg{'cpm'} limit=$limit s=\" cpm: \" extract=26,27 label=ldet1,ldet2 date=$arg{'date'}`; 
my @gflt_data  =  `./view_syslog pid=$pid deploy=$deploy cpm=$arg{'cpm'} limit=$limit s=\" cpm: \" extract=20,21,22,23 label=sbd,gps,vmain,telem date=$arg{'date'}`; 

my $cpm_caps = $arg{'cpm'}; $cpm_caps =~ tr/a-z/A-Z/;
my ($i, $line);

my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<!-- Platform ID: $pid<br>
-->
<table>
   <td>$cpm_caps MPIC Main Voltage
       <div id="graphdiv1"
        style="width:500px; height:200px;"></div>
   </td>
   <td>$cpm_caps MPIC Main Current (Ma) 
       <div id="graphdiv2"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td colspan=2>
   <table>
     <td>$cpm_caps Temperature (degC)
       <div id="graphdiv3" style="width:325px; height:200px;"></div>
     
     <td>$cpm_caps Humidity (%)
       <div id="graphdiv4" style="width:325px; height:200px;"></div>
     
     <td>$cpm_caps Pressure (psi)
       <div id="graphdiv5" style="width:325px; height:200px;"></div>
   </table>
   </td>
   </tr><tr>
   <td>$cpm_caps CPM Ldet Enable
       <div id="graphdiv6"
        style="width:500px; height:200px;"></div>
   </td>
   <td>$cpm_caps Ldet mV
       <div id="graphdiv7"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td>$cpm_caps Gflt Detect (uA)
       <div id="graphdiv8"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=6,8&label=Main,BBat", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#bat_data;$i++)
       {$line = $bat_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#bat_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      valueRange: [10,30]
     }          // options
  );

  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
EndHTML
    for ($i=0;$i<$#curr_data;$i++)
       {$line = $curr_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#curr_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g3 = new Dygraph(
    document.getElementById("graphdiv3"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=12,13&label=t1,t2", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#temp_data;$i++)
       {$line = $temp_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#temp_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: true,
      rollPeriod: 3
     }          // options
  );

  g4 = new Dygraph(
    document.getElementById("graphdiv4"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=15&label=Humidity", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#humid_data;$i++)
       {$line = $humid_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#humid_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: true,
      rollPeriod: 3,
     }          // options
  );

  g5 = new Dygraph(
    document.getElementById("graphdiv5"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=17&label=Pressure", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#press_data;$i++)
       {$line = $press_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#press_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 1.0,
      drawPoints: true,
      showRoller: true,
      rollPeriod: 3,
      valueRange: [14,15]
     }          // options
  );

  g6 = new Dygraph(
    document.getElementById("graphdiv6"),
EndHTML
    for ($i=0;$i<$#ldet_enable;$i++)
       {$line = $ldet_enable[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#ldet_enable-1){$html_out .= "\"$line\\n\" +\n";}
                            else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      valueRange: [1,4]
     }          // options
  );

  g7 = new Dygraph(
    document.getElementById("graphdiv7"),
EndHTML
    for ($i=0;$i<$#ldet_data;$i++)
       {$line = $ldet_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#ldet_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: true,
      rollPeriod: 3
     }          // options
  );


  g8 = new Dygraph(
    document.getElementById("graphdiv8"),
EndHTML
    for ($i=0;$i<$#gflt_data;$i++)
       {$line = $gflt_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#gflt_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );
</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

