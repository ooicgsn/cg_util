#!/bin/perl -w
###############################################################################
# WHOI OOI/CGSN network/UDP Comm routines
# 
# Description: Misc common perl routines 
#
# History:
#     Date      Who    Description
#    -------    ---    ----------------------------------
#   07/10/2010  SL     Create from existing code
#   12/23/2014  SL     Added get_arg and load_cfg
#   05/29/2015  SL     Updated sc in sec2hhmmss to integer
#   06/16/2015  SL     Updated dsltime2sec to strip-off ms
#######################################################################################
require "ctime.pl";
require "timelocal.pl";
use strict;

1;

sub time2dsltime_str {#takes total seconds and returns yyyy/mm/dd hh:mm:ss.ss
   my($time) = $_[0];
   my($outstr);
   my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdat) = gmtime($time);
   $year += 1900; $mon++;
   $outstr = sprintf("%04d/%02d/%02d %02d:%02d:%02d",$year,$mon,$mday,$hour,$min,$sec);
   return "$outstr";

}

sub dsltime2sec {#takes yyyy/mm/dd hh:mm:ss and returns total seconds
   my($dsltime) = $_[0];
   my($date_str,$time_str);
   my($sec,$min,$hour,$mday,$mon,$year);
   ($dsltime) = split('\.',$dsltime); #strip-off ms if provided
   return 0 if (length($dsltime) != 19);
   ($date_str,$time_str) = split(' ',$dsltime);
   ($year,$mon,$mday) = split('/',$date_str); $mon--;
   ($hour,$min,$sec) = split(':',$time_str);
   return(timegm($sec,$min,$hour,$mday,$mon,$year));
}

sub gmtimestamp {my($no_gmtlabel) = $_[0];
        my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime;
        # zero fill hour,min,sec and month
        if ($sec < 10)  {$sec = "0$sec";}
        if ($min < 10)  {$min = "0$min";}
        if ($hour < 10) {$hour = "0$hour";}
        $mon += 1; #from gmtime defined 0-11, make it 1-12
        if ($mon < 10)  {$mon = "0$mon";}
        if ($mday < 10) {$mday = "0$mday";}
        #make year 4-digits - Note: Year is years since 1900
        $year += 1900;
        if ($no_gmtlabel)
             {return "$year/$mon/$mday $hour:$min:$sec";}
        else {return "$year/$mon/$mday $hour:$min:$sec GMT";}
}


sub gmtimestamp_filename {
        my($opt) = $_[0]; #default:yymmdd 1-yymmdd_hh00 2-yymmdd_hhmm
        my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime;
        my($outstr);
        if (!defined $opt) {$opt = 0;}
        # zero fill hour,min,sec and month
        $mon += 1; #from gmtime defined 0-11, make it 1-12
        if ($mon < 10)  {$mon = "0$mon";}
        if ($mday < 10) {$mday = "0$mday";}
        if ($sec < 10)  {$sec = "0$sec";}
        #make year 4-digits - Note: Year is years since 1900
        $year += 1900;
        if ($opt == 1)
           {$outstr = sprintf("%04d%02d%02d_%02d00",$year,$mon,$mday,$hour);
            }
        elsif ($opt == 2)
           {$outstr = sprintf("%04d%02d%02d_%02d%02d",$year,$mon,$mday,$hour,$min);
            }
        elsif ($opt == 3)
           {$outstr = sprintf("%04d%02d%02d_%02d%02d%02d",$year,$mon,$mday,$hour,$min,$sec);
            }
        else
           {$outstr = sprintf("%04d%02d%02d",$year,$mon,$mday);
            }
        return "$outstr";
}


sub sec2hhmmss {#takes total seconds and returns d hh:mm:ss
        my($sec) = $_[0];
        my($dy,$hr,$mn,$sc);
        $dy = int($sec/86400);
        $hr = int(($sec - ($dy*86400))/3600);
        $mn = int(($sec - ($dy*86400) - ($hr*3600))/60);
        $sc = int($sec  - ($dy*86400) - ($hr*3600) - ($mn*60));
        if ($hr < 10) {$hr = "0$hr";}
        if ($mn < 10) {$mn = "0$mn";}
        if ($sc < 10) {$sc = "0$sc";}

        if ($dy == 0) {return "$hr:$mn:$sc";}
                 else {return "$dy $hr:$mn:$sc";}
}



#
# Returns arg hash array whether called from CGI or via cmd-line
#
sub get_args {my $debug = $_[0];
   my(%arg, $buffer, @pairs, $pair);
   #
   # Create key/value parameter pairs based on the request method
   #
   if ($ENV{'REQUEST_METHOD'} eq "POST")
      {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
       @pairs = split(/&/, $buffer);
       } 
   elsif ($ENV{'REQUEST_METHOD'} eq "GET")
      {@pairs = split(/&/, $ENV{'QUERY_STRING'});
       } 

   if ($#ARGV >= 0) {@pairs = @ARGV;}

   #
   # Put the keys and values in an associative array.
   # The value of a form variable can then be referenced
   # as $form{variable_name}
   #
   foreach $pair (@pairs) {
    my ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{"$key"} = "$value";
    print "arg[$key] = $value<br>\n" if $debug;
   }
   return %arg;
}


sub load_cfg {my($file) = $_[0];
   my(%Cfg,$line);
   my $CFG_FD;
   if (!open($CFG_FD,"$file")) {print "***Unable to open config file [$file]\n";
                                return;
                                }
  while ($line = <$CFG_FD>)
      {next if ($line =~ /^#/ || length($line) < 2); #skip comments & blank lines
       next if ($line !~ /=/);  #skip lines that don't contain '='
       chomp($line);
       ($line) = split('#',$line,2); #support end-line comments
       my($att,$val) = split("=",$line,2);
       $att =~ s/^(\s+)//; #remove leading spaces
       $att =~ s/(\s+)$//; #remove trailing spaces
       $val =~ s/^(\s+)//; #remove leading spaces
       $val =~ s/(\s+)$//; #remove trailing spaces
       $Cfg{"$att"} = "$val";
       #print "$att=[$val]\n" if $DEBUG;
       }
   close($CFG_FD);
   return %Cfg;
}

