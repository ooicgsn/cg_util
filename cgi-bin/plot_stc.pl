#!/usr/bin/perl
# plot_stc.pl -  Quick script to generate rt cpm plot
#
# Usage: plot_stc.pl pid= deploy=
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create from plot_cpm.html, but now use in-line data
#      09/27/2011  SL     Eliminate bbat as no longer on stc, added battery current,
#                         leak detect. Updated columns for humidity and pressure
#      10/18/2012  SL     Added support for platform ID and reference view_CPPM 
#                         for data (may want to pass-in PID type, ie; view_<type>)
#      12/01/2013  SL     Added date arg
#      12/23/2014  SL     Updated to use cg_util.pl
#      02/02/2015  SL     Support Deployment dir
##################################################################################
use strict;
require "cg_util.pl";

my %arg    = &get_args(0);
my $pid    = $arg{'pid'};
my $deploy = $arg{'deploy'};
my $limit  = 3000;

if ($pid eq "" || $deploy eq "") {print "pid and deploy must be specified\n"; exit(-1);}

my @volt_data  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" stc: \" extract=6   label=Volt date=$arg{'date'}`; 
my @curr_data  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" stc: \" extract=7   label=mA date=$arg{'date'}`; 
my @temp_data  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" stc: \" extract=10,11 label=t1,t2 date=$arg{'date'}`; 
my @humid_data =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" stc: \" extract=13 label=Humidity date=$arg{'date'}`; 
my @press_data =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" stc: \" extract=15 label=Pressure date=$arg{'date'}`; 
my @ldet_data  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" stc: \" extract=24,25 label=ld1,ld2 date=$arg{'date'}`; 
my @gflt_data  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" stc: \" extract=20,21,22,23 label=gflt_gps,gflt_sbd,gflt_vmain,gflt_telem date=$arg{'date'}`; 

my ($i, $line);
my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<table>
   <td>STC MPIC Battery
       <div id="graphdiv1"
        style="width:500px; height:200px;"></div>
   </td>
   <td>Battery Current
       <div id="graphdiv2"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td>Leak Detect
       <div id="graphdiv3"
        style="width:500px; height:200px;"></div>
   </td>
   <td>Gflt Detect
       <div id="graphdiv7"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td>Temperature
       <div id="graphdiv4"
        style="width:500px; height:200px;"></div>
   </td>
   <td>Humidity
       <div id="graphdiv5"
        style="width:500px; height:200px;"></div>
   </td>
   <td>Pressure
       <div id="graphdiv6"
        style="width:500px; height:200px;"></div>
</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=6,8&label=Main,BBat", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#volt_data;$i++)
       {$line = $volt_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#volt_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      drawPoints: true,
      valueRange: [10,30]
     }          // options
  );


  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=6,8&label=Main,BBat", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#curr_data;$i++)
       {$line = $curr_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#curr_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      drawPoints: true,
      // valueRange: [10,30]
     }          // options
  );


  g3 = new Dygraph(
    document.getElementById("graphdiv3"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=6,8&label=Main,BBat", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#ldet_data;$i++)
       {$line = $ldet_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#ldet_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      drawPoints: true,
      // valueRange: [10,30]
     }          // options
  );

  g4 = new Dygraph(
    document.getElementById("graphdiv4"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=12,13&label=t1,t2", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#temp_data;$i++)
       {$line = $temp_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#temp_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: true,
      drawPoints: true,
      rollPeriod: 1
     }          // options
  );

  g5 = new Dygraph(
    document.getElementById("graphdiv5"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=15&label=Humidity", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#humid_data;$i++)
       {$line = $humid_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#humid_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: true,
      drawPoints: true,
      rollPeriod: 1,
     }          // options
  );

  g6 = new Dygraph(
    document.getElementById("graphdiv6"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=17&label=Pressure", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#press_data;$i++)
       {$line = $press_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#press_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      fillGraph: true,
      strokeWidth: 1.0,
      drawPoints: true,
      showRoller: true,
      rollPeriod: 3,
      valueRange: [14,15]
     }          // options
  );


  g7 = new Dygraph(
    document.getElementById("graphdiv7"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?limit=1500&s=%22cpm:%22&extract=6,8&label=Main,BBat", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#gflt_data;$i++)
       {$line = $gflt_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#gflt_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      drawPoints: true,
      // valueRange: [10,30]
     }          // options
  );

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

