#!/usr/bin/perl
#!c:\Perl64\bin\perl -w
#
# powermodel.pl - Steven Lerner 12/2009
#
# Description: Simple power model simulation for OOI/CGSN platform. Takes
#              config file that describes options for power_generation 
#              (solar, wind, fuelcell), power_consumption (loads for hotel and 
#              sensors w/ specified duty-cycle), environmental conditions
#              (sun, wind, temperature), and duration (number of days).
#
#              Sample config file is including at bottom of this file
#
#              Outputs ASCII file containing day/hr, solar_pwr, wind_pwr, 
#                      fuelcell pwr, total power_gen, battery state, total
#                      power_con, sun, wind, temp or outputs associate
#                      plot (png file) via gnuplot
#
# Usage: powermodel.pl <cf=cfg_file> [-h] [duration=] [smf=sun_mf] [wmf=wind_mf] [tmf=temp_mf] [d=0|1]
#                      [otype=ascii, plot, d,h,gvc,e...]
#
# History:
#    Date      WHO   Description
#  --------    ---   ----------------------------------------------
#  12/12/2009  SL    Create
#  12/28/2009  SL    Updated for GUI - use PowerCfg|Hotel|Sensor.enable
#  01/17/2010  SL    Added solar radiation model, added PV tilt angle to model
#  01/18/2010  SL    Added flatline for wind turbine (some flatline, others 
#                    go to 0 at max spd), added auto cycle for FuelCell
#                    (kicks in if battery drops below some percentage and
#                    charges up to specified max battery charge).
#  01/22/2010  SL    Added Fuelcell init/off cycle loads
#  01/27/2010  SL    Added AUV Dock (v1.0)
#  05/22/2010  SL    Added data model for hotel and sensor loads. To enable,
#                    specify hotel|sensor.name.sample_size and sample_rate in 
#                    Cfg file. Telem throughput also added via name.telem_thruput
#                    (telem_cost - via per minute or per byte TBD)
#  07/01/2010  SL/JL/SM  Increased max duration from 365 to 720. Note solar model only
#                    goes to 365 days and external models would need to match 
#                    max duration if more than 365.
#  08/02/2012  SL    Added Battery Charge algorithm. Charge up to max_charge,
#                    then disconnect power-generation inputs until min_charge.
#                    Added optional start day cmd-line arg
#  11/27/2013  SL    Increase plot size
#  08/26/2016  JJP   Updated path to model_data for new structure
#  08/29/2016  JJP   Updated path to gnuplot for Ubuntu. Note: without some
# 		     additional code checking this will no longer work on CentOS
#		     CentOS path: /usr/local/bin/gnuplot
#  09/26/2016  JJP   Removed the hard coded path to gnuplot for Ubuntu. It is 
#		     not necessary. The path that it gets installed to is in 
#		     $PATH for both CentOS and Ubuntu.
#  04/10/2017  CC    Removed AUV Dock artifacts
################################################################################
#
#
#=====================================================================
#  Simulation Architecture Overview
#  ---------
#  | Solar |---|
#  ---------   |
#              |                      
#  ---------   |------> -----------    |--->Hotel (cpm/dcl/eth/telem)
#  | Wind  |----------> | Battery |--->|
#  ---------    |-----> -----------    |--->Sensors
#               |                      |
#  ------------ |                      |--->AUV Dock
#  |Fuel Cell |-|
#  ------------
#  Environmental Parameters: Sun, Wind, Temperature
#  Mission Parameters: Duration, load & duty cycles for power input
#                      and hotel/sensors loads 
#=====================================================================
#
require "flush.pl";

my($VERSION) = "v1-20100522";
my($DEBUG)   = 0;

#$gnuplot = "c:/gnuplot/bin/gnuplot.exe";
$gnuplot = "gnuplot";

#
# Create key/value parameter pairs based on the request method
#
if (defined $ENV{'REQUEST_METHOD'} && $ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      } 
elsif (defined $ENV{'REQUEST_METHOD'} && $ENV{'REQUEST_METHOD'} eq "GET")
     {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }

elsif ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
#print "Content-type: text/plain\n\n";
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair, 2);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = [$value]\n";
}


flush(STDOUT);
print "Content-type: text/plain\n\n";


#
#Check Usage
#
if ($arg{'h'})
   {print "powermodel - $VERSION\n";
    print "Usage: powermodel.pl <cf=cfg_file> [h=1] [sday=] [duration=] [smf=sun_mf] [wmf=wind_mf] [tmf=temp_mf] [d=1|0]\n";
    exit;
    }

#Parse args
if (!defined $arg{'otype'}) {$arg{'otype'} = "ascii";}
if (defined $arg{'d'} && $arg{'d'} == 1) {$DEBUG = 1;}
if (defined $arg{'cf'} && $arg{'cf'} ne "") 
             {$cfg_file = "$arg{'cf'}";}
        else {$cfg_file = "default_pwrmodel.cfg";}

#
#Announce version
#
print STDERR "CGSN powermodel - $VERSION\n";

my(%Cfg);
my($duration,$day,$hr,$sday);
my($sun,$wind,$temp);
my(@battery_state, @power_gen, @power_con);


#
#If cfg specified load it, otherwise give error and exit
#
if (-f "$cfg_file") {%Cfg = &load_cfg("$cfg_file");}
else {die "Error: Config file [$cfg_file] does not exist - exiting\n";}

#print out Cfg items if debug
if ($DEBUG)
  {foreach $item (sort keys %Cfg) {print "$item = $Cfg{\"$item\"}\n";}
   }

#
#Override any Cfg items with cmd-line args if specified
#
if (defined $arg{'smf'}) {$Cfg{'Env.sun.datafile'}  = "$arg{'smf'}";}
if (defined $arg{'wmf'}) {$Cfg{'Env.wind.datafile'} = "$arg{'wmf'}";}
if (defined $arg{'tmf'}) {$Cfg{'Env.temp.datafile'} = "$arg{'tmf'}";}

if (defined $arg{'duration'}) {$Cfg{'Duration.days'} = $arg{'duration'};}
if (defined $arg{'sday'}) {$Cfg{'Simulation.sday'} = $arg{'sday'};}


$duration = $Cfg{'Duration.days'};
if    ($duration < 1)   {$duration = 1;}
elsif ($duration > 720) {$duration = 720;}

if (defined $Cfg{'Simulation.sday'}) {$sday = $Cfg{'Simulation.sday'};}
                                else {$sday = 1;}
if    ($sday < 1)   {$sday = 1;}
elsif ($sday > 365) {$sday = 365;}

#Load environmental params - based on either input file or high/low cycles 
if ($Cfg{'Env.sun.datafile'}  ne "") 
     {@sun = load_env_datafile($Cfg{'Env.sun.datafile'});}
else {@sun = solarmodel($Cfg{'Platform.lat'},$Cfg{'Platform.lon'},$Cfg{'SolarPwr.tilt'},0.,$Cfg{'Env.sun.sf'});}
if ($Cfg{'Env.wind.datafile'} ne "")
     {@wind = load_env_datafile($Cfg{'Env.wind.datafile'});}
if ($Cfg{'Env.temp.datafile'} ne "")
     {@temp = load_env_datafile($Cfg{'Env.temp.datafile'});}

$ind = ($sday-1)*24;
foreach $day ($sday..$duration+$sday)
  {foreach $hr (0..23)
    {#use sun model instead
     #if ($Cfg{'Env.sun.datafile'} eq "")
     #  {my($high_start,$high_end) = split(',',$Cfg{'Env.sun.high_cycle'});
     #   my($high_val,$low_val)    = split(',',$Cfg{'Env.sun.high_low_val'});
     #   if ($hr >= $high_start && $hr <= $high_end)
     #        {$sun[$ind] = $high_val;}
     #   else {$sun[$ind] = $low_val;}
     #   }

     if ($Cfg{'Env.wind.datafile'} eq "")
       {my($high_start,$high_end) = split(',',$Cfg{'Env.wind.high_cycle'});
        my($high_val,$low_val)    = split(',',$Cfg{'Env.wind.high_low_val'});
        if ($hr >= $high_start && $hr <= $high_end)
             {$wind[$ind] = $high_val;}
        else {$wind[$ind] = $low_val;}
        }

     if ($Cfg{'Env.temp.datafile'} eq "")
       {my($high_start,$high_end) = split(',',$Cfg{'Env.temp.high_cycle'});
        my($high_val,$low_val)    = split(',',$Cfg{'Env.temp.high_low_val'});
        if ($hr >= $high_start && $hr <= $high_end)
             {$temp[$ind] = $high_val;}
        else {$temp[$ind] = $low_val;}
        }
     $ind++;
     } #end foreach hour
  } #end foreach day



#Put into lists - strip-off any spaces first
$Cfg{'PowerCfg'} =~ s/ //g; $Cfg{'HotelCfg'} =~ s/ //g; $Cfg{'SensorCfg'} =~ s/ //g;
my(@PowerCfg)  = split(',',$Cfg{'PowerCfg'});
my(@HotelCfg)  = split(',',$Cfg{'HotelCfg'});
my(@SensorCfg) = split(',',$Cfg{'SensorCfg'});

if ($DEBUG)
  {print "\n----------------------------\n";
   print "PowerCfg:  @PowerCfg\n";
   print "HotelCfg:  @HotelCfg\n";
   print "SensorCfg: @SensorCfg\n";
   print "----------------------------\n";
   }

#Setup initial conditions
$battery_state[0] = $Cfg{'Battery.capacity_wh'};
$fuelcell_gauge = $Cfg{'FuelCellPwr.capacity_l'};
$FuelCell_RunState = "Off";

$Battery_RunState = "Off";
$Battery_ChargeState = "Discharging";
$Enable_pwr_generation = 0;


#For each hour, calculate watt-hour for power generation, power consumption, and battery state
$ind = ($sday-1)*24; 
if ($ind > 0) {$battery_state[$ind-1] = $Cfg{'Battery.capacity_wh'};}

foreach $day ($sday..$duration+$sday)
  {foreach $hr (0..23)
    {#Initial generation and consumption
     $power_gen[$ind] = 0; #assume none
     $power_con[$ind] = 0; #assume none
     $power_con_hotel[$ind] = 0;
     $power_con_sensors[$ind] = 0;

     #Calculate power generation
     if ($Enable_pwr_generation && defined $Cfg{'PowerCfg.enable.solar'} && $Cfg{'PowerCfg.enable.solar'}=~ /on/i)
       {#TBD - degradation due to Temperature: 25degC w/ -.5% per degC
        if ($sun[$ind] > 0)
             {$solar_pwr_gen[$ind] = $Cfg{'SolarPwr.watts'} * $Cfg{'SolarPwr.efficiency'}/100.0 * $sun[$ind]/1367.0;
              }
        else {$solar_pwr_gen[$ind] = 0;
              }
        $power_gen[$ind] += $solar_pwr_gen[$ind];
        }
     else {$solar_pwr_gen[$ind] = 0;}
 
     if ($Enable_pwr_generation && defined $Cfg{'PowerCfg.enable.wind'} && $Cfg{'PowerCfg.enable.wind'}  =~ /on/i)
       {if ($wind[$ind] >= $Cfg{'WindPwr.min_wind_spd'})
             {if ($wind[$ind] <= $Cfg{'WindPwr.max_wind_spd'})
               {$wind_pwr_gen[$ind] = $Cfg{'WindPwr.watts'} * $Cfg{'WindPwr.efficiency'}/100.0 * $wind[$ind]/$Cfg{'WindPwr.max_wind_spd'};
                }
              else {#greater than max wnd speed, either flatline or go to 0
                    if ($Cfg{'WindPwr.enable.flatline'} =~ /on/i)
                         {$wind_pwr_gen[$ind] = $Cfg{'WindPwr.watts'} * $Cfg{'WindPwr.efficiency'}/100.0;
                          }
                    else {$wind_pwr_gen[$ind] = 0;
                          }
                    }
              }
        else {$wind_pwr_gen[$ind] = 0;
              }
        $power_gen[$ind] += $wind_pwr_gen[$ind];
        }
     else {$wind_pwr_gen[$ind] = 0;}

     #Fuel Cell - keep track of power_generation and fuel remaining
     $fuelcell_fuel_rem[$ind] = $fuelcell_gauge;
     $fuelcell_pwr_gen[$ind] = 0;
     if ($Enable_pwr_generation && defined $Cfg{'PowerCfg.enable.fuelcell'} && $Cfg{'PowerCfg.enable.fuelcell'} =~ /on/i)
       {if (defined $Cfg{'FuelCellPwr.enable.acycle'} && $Cfg{'FuelCellPwr.enable.acycle'} =~ /on/i)
          {#auto-cycle based on min/max battery charge
           ($acycle_bmin,$acycle_bmax) = split(',',$Cfg{'FuelCellPwr.acycle'});
           my($bat_percent);
	   if ($ind == 0)
	        {$bat_percent = $battery_state[0]/$Cfg{'Battery.capacity_wh'} * 100.0;}
           else {$bat_percent = $battery_state[$ind-1]/$Cfg{'Battery.capacity_wh'} * 100.0;}
           if ($bat_percent < $acycle_bmin || $FuelCell_AStart == 1)
             {$FuelCell_AStart = 1;
	      if ($FuelCell_RunState eq "Off") {$FuelCell_RunState = "Start";}
              if ($bat_percent < $acycle_bmax) 
                {if (($fuelcell_gauge -= $Cfg{'FuelCellPwr.burnrate'}) < 0) {$fuelcell_gauge = 0;}
                 $fuelcell_fuel_rem[$ind] = $fuelcell_gauge;
                 if ($fuelcell_gauge > 0)
                     {$fuelcell_pwr_gen[$ind] = $Cfg{'FuelCellPwr.watts'};}
                 else {$fuelcell_pwr_gen[$ind] = 0;}
                 $power_gen[$ind] += $fuelcell_pwr_gen[$ind];
                 }
              }
           if ($bat_percent > $acycle_bmax)
                {$FuelCell_AStart = 0;
                 $fuelcell_pwr_gen[$ind] = 0;
	         if ($FuelCell_RunState ne "Off") {$FuelCell_RunState = "Stop";}
                 }
          }
        else 
          {#manual cycle
           ($num_min,$n_hours) = split(',',$Cfg{'FuelCellPwr.cycle'});
           if (!($hr % $n_hours))
             {if (($fuelcell_gauge -= $Cfg{'FuelCellPwr.burnrate'}) < 0) {$fuelcell_gauge = 0;}
              $fuelcell_fuel_rem[$ind] = $fuelcell_gauge;
              if ($fuelcell_gauge > 0)
                   {$fuelcell_pwr_gen[$ind] = $Cfg{'FuelCellPwr.watts'};}
              else {$fuelcell_pwr_gen[$ind] = 0;}
              $power_gen[$ind] += $fuelcell_pwr_gen[$ind];
 	      $FuelCell_RunState = "Manual";
              }
           }

        #Model consumption during power on/off cycle
	if ($FuelCell_RunState eq "Start")
	      {$FuelCell_RunState = "Running";
	       #Subtract from battery power-on initial load
	       $fuelcell_load = $Cfg{'FuelCellPwr.init_load'} * $Cfg{'FuelCellPwr.init_dur'}/60.0;
               $power_con[$ind] += $fuelcell_load;
               $power_con_hotel[$ind] += $fuelcell_load;
               }
	elsif ($FuelCell_RunState eq "Stop")
	      {$FuelCell_RunState = "Off";
	       #Subtract from battery power-off initial load
	       $fuelcell_load = $Cfg{'FuelCellPwr.off_load'} * $Cfg{'FuelCellPwr.off_dur'}/60.0;
               $power_con[$ind] += $fuelcell_load;
               $power_con_hotel[$ind] += $fuelcell_load;
               }
	elsif ($FuelCell_RunState eq "Manual")
	      {$FuelCell_RunState = "Off";
	       #Subtract from battery power-on initial load
	       $fuelcell_load = $Cfg{'FuelCellPwr.init_load'} * $Cfg{'FuelCellPwr.init_dur'}/60.0;
               $power_con[$ind] += $fuelcell_load;
               $power_con_hotel[$ind] += $fuelcell_load;
	       #Subtract from battery power-off initial load
	       $fuelcell_load = $Cfg{'FuelCellPwr.off_load'} * $Cfg{'FuelCellPwr.off_dur'}/60.0;
               $power_con[$ind] += $fuelcell_load;
               $power_con_hotel[$ind] += $fuelcell_load;
               }

        }
   
   #calculate total loads for hotels and sensors based on duty-cycle 
   foreach $name (@HotelCfg)
      {next if (!defined $Cfg{"HotelCfg.enable.$name"} || $Cfg{"HotelCfg.enable.$name"} !~ /on/i);
       ($num_min,$n_hours) = split(',',$Cfg{"Hotel.$name.cycle"});
       if ($n_hours <= 0) {print "***Error with Hotel $name, n_hours=$n_hours\n";}
       next if (($hr % $n_hours));
       $hotel_load = $Cfg{"Hotel.$name.load"} * $num_min/60.0;
       $data_gen   = ($Cfg{"Hotel.$name.sample_size"} * $Cfg{"Hotel.$name.sample_rate"}) * $num_min;
       $telem_thruput = $Cfg{"Hotel.$name.telem_thruput"}*60.0 * $num_min;
       if (defined $Cfg{"Hotel.$name.telem_cost_per_min"})
           {$telem_cost = $Cfg{"Hotel.$name.telem_cost_per_min"} * $num_min;
            }
       elsif (defined $Cfg{"Hotel.$name.telem_cost_per_mbyte"})
           {$telem_cost = ($Cfg{"Hotel.$name.telem_cost_per_mbyte"}/$telem_thruput) * $num_min;
            }
       else {$telem_cost = 0;}
       if ($DEBUG)
          {print "$day $hr - $name: \t NumMin=$num_min, n-hours=$n_hours \t load $hotel_load ($Cfg{\"Hotel.$name.load\"})\n";
           }
       $power_con[$ind] += $hotel_load;
       $power_con_hotel[$ind] += $hotel_load;
       $data_gen[$ind] += $data_gen;
       $data_gen_hotel[$ind] += $data_gen;
       $telem_thruput_hotel[$ind] += $telem_thruput;
       $telem_cost_hotel[$ind] += $telem_cost;
       if ($name =~ /fb250_xmt|fbb_xmt/i) {$telem_thruput_fb250[$ind] += $telem_thruput;
                                   $telem_cost_fb250[$ind] += $telem_cost;
                                   }
       if ($name =~ /irid_xmt/i)  {$telem_thruput_irid[$ind]  += $telem_thruput;
                                   $telem_cost_irid[$ind] += $telem_cost;
                                   }
       }

   foreach $name (@SensorCfg)
      {next if (!defined $Cfg{"SensorCfg.enable.$name"} || $Cfg{"SensorCfg.enable.$name"}!~ /on/i);
       ($num_min,$n_hours) = split(',',$Cfg{"Sensor.$name.cycle"});
       if ($n_hours <= 0) {print "***Error with Sensor $name, n_hours=$n_hours\n";}
       next if (($hr % $n_hours));
       $sensor_load = $Cfg{"Sensor.$name.load"} * $num_min/60.0;
       $data_gen    = ($Cfg{"Sensor.$name.sample_size"} * $Cfg{"Sensor.$name.sample_rate"}) * $num_min;
       if ($DEBUG)
          {print "$day $hr - $name: \t NumMin=$num_min, n-hours=$n_hours \t load $sensor_load ($Cfg{\"Sensor.$name.load\"})\n";
           }
       $power_con[$ind] += $sensor_load;
       $power_con_sensors[$ind] += $sensor_load;
       $data_gen[$ind] += $data_gen;
       $data_gen_sensors[$ind] += $data_gen;
       }

   if ($ind == 0)
        {$battery_state[$ind] = $battery_state[0] + ($power_gen[$ind] - $power_con[$ind])*$Cfg{'Battery.charge_efficiency'}/100.0;}
   else 
        {$battery_state[$ind] = $battery_state[$ind-1] + ($power_gen[$ind] - $power_con[$ind])*$Cfg{'Battery.charge_efficiency'}/100.0;}
   if ($battery_state[$ind] > $Cfg{'Battery.capacity_wh'}) 
         {#If auto charge enable, allow it to go past 100%
          if (defined $Cfg{'Battery.enable.acycle'} && $Cfg{'Battery.enable.acycle'} =~ /on/i)
                    {#limit to either 100% or acycle_bmax if greater
                     ($acycle_bmin,$acycle_bmax) = split(',',$Cfg{'Battery.acycle'});
                     if ($acycle_bmax > 100) 
                       {my $limit =  $Cfg{'Battery.capacity_wh'} + $acycle_bmax/100.*$Cfg{'Battery.capacity_wh'};
                        if ($battery_state[$ind] > $limit) {$battery_state[$ind] = $limit;}
                        }
                     else {$battery_state[$ind] = $Cfg{'Battery.capacity_wh'};}
                     }
          else {$battery_state[$ind] = $Cfg{'Battery.capacity_wh'};
                }
          }
   elsif ($battery_state[$ind] < 0) {$battery_state[$ind] = 0;}

   #Do battery charging algorithm is enabled
   if (defined $Cfg{'Battery.enable.acycle'} && $Cfg{'Battery.enable.acycle'} =~ /on/i)
          {#auto-cycle based on min/max battery charge
           ($acycle_bmin,$acycle_bmax) = split(',',$Cfg{'Battery.acycle'});
           my($bat_percent);
	   if ($ind == 0)
	        {$bat_percent = $battery_state[0]/$Cfg{'Battery.capacity_wh'} * 100.0;}
           else {$bat_percent = $battery_state[$ind-1]/$Cfg{'Battery.capacity_wh'} * 100.0;}
           if ($bat_percent < $acycle_bmin || $Battery_AStart == 1)
             {$Battery_AStart = 1;
	      if ($Battery_RunState eq "Off") {$Battery_RunState = "Start";}
              if ($bat_percent < $acycle_bmax) 
                {#Need anything here?? - within charge cycle
                 }
              }
           if ($bat_percent >= $acycle_bmax)
                {$Battery_AStart = 0;
	         if ($Battery_RunState ne "Off") {$Battery_RunState = "Stop";}
                 }
           #Set ChargeState
	   if ($Battery_RunState eq "Start")
	      {$Battery_ChargeState = "Charging";
               $Enable_pwr_generation = 1;
               }
	   elsif ($Battery_RunState eq "Stop")
	      {$Battery_RunState = "Off";
               $Battery_ChargeState = "Discharging";
               $Enable_pwr_generation = 0;
               }
          }
    else {#Enable pwr-generation
          $Enable_pwr_generation = 1;
          }


   #keep some more stats
   $total_power_gen += $power_gen[$ind];
   $total_power_con += $power_con[$ind];
   $total_power_con_hotel += $power_con_hotel[$ind];
   $total_power_con_sensors += $power_con_sensors[$ind];
   $total_data_gen += $data_gen[$ind];
   $total_data_gen_hotel   += $data_gen_hotel[$ind];
   $total_data_gen_sensors += $data_gen_sensors[$ind];
   $total_telem_thruput += $telem_thruput_hotel[$ind];
   $total_telem_thruput_fb250 += $telem_thruput_fb250[$ind];
   $total_telem_thruput_irid += $telem_thruput_irid[$ind];
   $total_telem_cost += $telem_cost_hotel[$ind];
   $total_telem_cost_fb250 += $telem_cost_fb250[$ind];
   $total_telem_cost_irid += $telem_cost_irid[$ind];
   $ind++;

   } #end foreach hour
} #end foreach day


#
# Output Results
#
if ($arg{'otype'} =~ /ascii/i)
  {#print "\n";
   print "Platform: $Cfg{'Platform.name'}   ID: $Cfg{'Platform.id'}\n";
   print "          Location: $Cfg{'Platform.lat'}, $Cfg{'Platform.lon'}\n";
   #print "Power Generation: $Cfg{'PowerCfg'}\n";
   print "Power Generation: ";
      if (defined $Cfg{'PowerCfg.enable.solar'} && $Cfg{'PowerCfg.enable.solar'} =~ /on/i) {print "Solar ";}
      if (defined $Cfg{'PowerCfg.enable.wind'}  && $Cfg{'PowerCfg.enable.wind'}  =~ /on/i) {print "Wind ";}
      if (defined $Cfg{'PowerCfg.enable.fuelcell'}  && $Cfg{'PowerCfg.enable.fuelcell'} =~ /on/i) {print "Fuelcell ";}
   print "\n";
   print "Battery Capacity: $Cfg{'Battery.capacity_wh'} watt/hr\n";
   print "Hotel  Loads: ";
   foreach $name (@HotelCfg)
      {if (defined $Cfg{"HotelCfg.enable.$name"} && $Cfg{"HotelCfg.enable.$name"} =~ /on/i) {print "$name ";}
       }
   print "\n";
   print "Sensor Loads: ";
   foreach $name (@SensorCfg)
      {if (defined $Cfg{"SensorCfg.enable.$name"} && $Cfg{"SensorCfg.enable.$name"} =~ /on/i) {print "$name ";}
       }
   print "\n";
   print "Enviro Files: $Cfg{'Env.sun.datafile'} $Cfg{'Env.wind.datafile'} $Cfg{'Env.temp.datafile'}\n";
   print "\n";
   print "Start Day: $sday\n";
   print "Duration : $duration days\n";
   print "\n";
   print "day, hr, solar_p, wind_p, fcell_p, fuel_l, p_gen, p_con, p_con_h, p_con_s, batt_wh, batt_p, sun, wind, temp, data_gen, data_h, data_s, tel_tput, tel_tf, tel_ti, tel_c, tel_cf, tel_ci\n";
   }
if ($arg{'otype'} =~ /csv/i)
  {print "day, hr, solar_p, wind_p, fcell_p, fuel_l, p_gen, p_con, p_con_h, p_con_s, batt_wh, batt_p, sun, wind, temp, data_gen, data_h, data_s, tel_tput, tel_tf, tel_ti, tel_c, tel_cf, tel_ci\n";
   }

$ind = ($sday-1)*24; $plot_data = "";
foreach $day ($sday..$duration+$sday)
  {foreach $hr (0..23)
     {$plot_data .= sprintf("%03d %02d  %6.2f  %6.2f   %6.2f %6.1f    %7.2f  %5.2f  %5.2f %5.2f     %7.2f  %6.2f    %5.1f %5.1f %5.1f   %.2f %.2f %.2f   %.2f %.2f %.2f   %.2f %.2f %.2f\n", $day, $hr, 
             $solar_pwr_gen[$ind], $wind_pwr_gen[$ind], $fuelcell_pwr_gen[$ind], $fuelcell_fuel_rem[$ind], $power_gen[$ind], 
             $power_con[$ind], $power_con_hotel[$ind], $power_con_sensors[$ind], $battery_state[$ind],  $battery_state[$ind]/$Cfg{'Battery.capacity_wh'}*100.0, $sun[$ind], $wind[$ind], $temp[$ind], 
             $data_gen[$ind]/1000.0, $data_gen_hotel[$ind]/1000.0, $data_gen_sensors[$ind]/1000.0,
             $telem_thruput_hotel[$ind], $telem_thruput_fb250[$ind], $telem_thruput_irid[$ind],
             $telem_cost_hotel[$ind], $telem_cost_fb250[$ind], $telem_cost_irid[$ind]);
      $ind++;
      } #end foreach hr

    $daily_ave_pwrgen = $total_power_gen/($duration);
    $daily_ave_pwrcon = $total_power_con/($duration);
    $daily_ave_pwrcon_hotel   = $total_power_con_hotel/($duration);
    $daily_ave_pwrcon_sensors = $total_power_con_sensors/($duration);
    $daily_ave_data_gen = $total_data_gen/($duration);
    $daily_ave_data_gen_hotel   = $total_data_gen_hotel/($duration);
    $daily_ave_data_gen_sensors = $total_data_gen_sensors/($duration);
    $daily_ave_telem_thruput = $total_telem_thruput/($duration);
    $daily_ave_telem_thruput_fb250 = $total_telem_thruput_fb250/($duration);
    $daily_ave_telem_thruput_irid = $total_telem_thruput_irid/($duration);
    $daily_ave_telem_cost = $total_telem_cost/($duration);
    $daily_ave_telem_cost_fb250 = $total_telem_cost_fb250/($duration);
    $daily_ave_telem_cost_irid = $total_telem_cost_irid/($duration);
   } #end foreach day

if ($arg{'otype'} =~ /ascii/i) 
     {print "$plot_data";
      printf("\nDaily Average PowerGen=%.2f whr/d  PowerCon=%.2f whr/d (hotel: %.2f, sensors: %.2f)",$daily_ave_pwrgen,$daily_ave_pwrcon,$daily_ave_pwrcon_hotel,$daily_ave_pwrcon_sensors);
      if ($daily_ave_pwrcon > $daily_ave_pwrgen)
        {printf("  Est_#Days=%.2f\n",($Cfg{'Battery.capacity_wh'}-($Cfg{'Battery.capacity_wh'}*$Cfg{'Battery.min_charge_percent'}/100.0))/($daily_ave_pwrcon-$daily_ave_pwrgen));
         }
      printf("\nDaily Average DataGen=%.2f kBytes  (hotel: %.2f kB, sensors: %.2f kB)",$daily_ave_data_gen/1000.0,$daily_ave_data_gen_hotel/1000.0,$daily_ave_data_gen_sensors/1000.0);
      printf("\nDaily Average TelemThruput=%.2f kBytes  (fb250: %.2f kB, irid: %.2f kB)",$daily_ave_telem_thruput,$daily_ave_telem_thruput_fb250,$daily_ave_telem_thruput_irid);
      printf("\nDaily Average TelemCost=\$%.2f  (fb250: \$%.2f, irid: \$%.2f)",$daily_ave_telem_cost,$daily_ave_telem_cost_fb250,$daily_ave_telem_cost_irid);
      print "\n";
      }

elsif ($arg{'otype'} =~ /csv/i) 
     {#For some reason, s/(\s+)/, /g is stripping off \n.
      $plot_data =~ s/\n/zzz/g;
      $plot_data =~ s/(\s+)/, /g;
      $plot_data =~ s/zzz/\n/g;
      print "$plot_data";
      print "\n";
      }


if ($arg{'otype'} =~ /plot_pg/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800,500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"Watts\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Power Generation\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   print $GPLOT "plot '-' using 1:7 with lines title 'Total Generation', ";
   print $GPLOT "'-' using 1:3 with lines title 'Solar', ";
   print $GPLOT "'-' using 1:4 with lines title 'Wind', ";
   print $GPLOT "'-' using 1:5 with lines title 'FuelCell' \n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_pc/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"Watts\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Power Consumption\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   #print $GPLOT "plot '-' using 1:7 with points ps .5 title 'Power Con' \n";
   print $GPLOT "plot '-' using 1:8 with lines title 'Total Consumption', ";
   print $GPLOT "'-' using 1:9 with lines title 'Hotel Loads', ";
   print $GPLOT "'-' using 1:10 with lines title 'Sensors Loads' \n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_gc/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"Watts\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Power Generation vs Consumption\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   print $GPLOT "plot '-' using 1:7 with lines title 'Total Generation', ";
   print $GPLOT "'-' using 1:8 with lines title 'Total Consumption'\n ";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_fuel/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"Liters\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Fuel Remaining\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   print $GPLOT "plot '-' using 1:6 with lines title 'Fuel Remaining'\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_bp/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"Percent\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Battery Capacity\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   print $GPLOT "plot '-' using 1:12 with lines title 'Battery'\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_bwh/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"Watt/Hr\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Battery Capacity\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   print $GPLOT "plot '-' using 1:11 with lines title 'Battery'\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_solar/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"W/m^2\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   if ($Cfg{'SolarPwr.tilt'} > 0)
        {print $GPLOT "set title \"Solar Radiation Model for PV with $Cfg{'SolarPwr.tilt'} tilt angle\"\n";}
   else {print $GPLOT "set title \"Solar Radiation Model\"\n";}
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   print $GPLOT "plot '-' using 1:13 with lines title 'Solar Radiation'\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_wind/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"Knots\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Wind Model\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   print $GPLOT "plot '-' using 1:14 with lines title 'Wind Model'\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_temp/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"DegC\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Temperature Model\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp\n";
   print $GPLOT "plot '-' using 1:15 with lines title 'Sun Intensity'\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_dg/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"kBytes\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Data Generation\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp, data_gen, data_h, data_s\n";
   print $GPLOT "plot '-' using 1:16 with points title 'Total Generation', ";
   print $GPLOT "'-' using 1:17 with points title 'Hotel', ";
   print $GPLOT "'-' using 1:18 with points title 'Sensors' \n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
elsif ($arg{'otype'} =~ /plot_tt/i) 
  {#generate plot
   #$GPLOT = STDOUT;
   open($GPLOT,"|$gnuplot");
   #open($GPLOT,">STDOUT");
   print $GPLOT "set terminal png small x000000 xbbbbbb x006600 size 800, 500\n";
   print $GPLOT "set key below samp 1\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set ylabel \"kBytes\"\n";
   print $GPLOT "set xdata time\n";
   print $GPLOT "set timefmt \"%j %H\"\n";
   print $GPLOT "set grid\n";
   print $GPLOT "set title \"Telemetry Thruput\"\n";
   print $GPLOT "set format x \"%j\\n%H\"\n";
   print $GPLOT "#day, hr, solar_pwr, wind_pwr, fuelcell_pwr, fuelguage, pwr_gen, pwr_con, pwr_con_hotel, pwr_con_sens, batt_state, batt_p, sun, wind, temp, data_gen, data_h, data_s, telem_tput, telem_tf, telem_ti  telem_c, telem_cf, telem_ci\n";
   print $GPLOT "plot '-' using 1:19 with points title 'Total Thruput', ";
   print $GPLOT "'-' using 1:20 with points title 'FB250', ";
   print $GPLOT "'-' using 1:21 with points title 'Iridium' \n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   print $GPLOT "$plot_data";
   print $GPLOT "e\n";
   close($GPLOT);
   }
exit;




#
# Subroutines Follow
#
sub load_cfg {my($file) = $_[0];
   my(%Cfg,$line);
   if (!open(CFG,"$file")) {print "***Unable to open config file [$file]";
                             return;
                             }
  while ($line = <CFG>)
      {next if ($line =~ /^#/ || length($line) < 2); #skip comments & blank lines
       chomp($line);
       my($att,$val) = split("=",$line,2);
       $att =~ s/^(\s+)//; #remove leading spaces
       $att =~ s/(\s+)$//; #remove trailing spaces
       $val =~ s/^(\s+)//; #remove leading spaces
       $val =~ s/(\s+)$//; #remove trailing spaces
       $Cfg{"$att"} = "$val";
       #print "$att=[$val]\n" if $DEBUG;
       }
   close(CFG);
   return %Cfg;
}


sub load_env_datafile {my($file) = $_[0];
   my($line,$fd,$yday,$hr,$ind,@env_model);
   my($yyyy);
   if (!open($fd,"../../../pwrmodel/model_data/$file")) {print "***Unable to open enviro model file [$file]";
                             return;
                             }
   $ind = 0;
   while ($line = <$fd>)
      {next if ($line =~ /^#/); #skip comments 
       chomp($line); $line =~ s/\r//;
       #($yday,$hr,$env_model[$ind]) = split(' ',$line);
       ($yyyy,$yday,$hr,$env_model[$ind]) = split(',',$line);
       $ind++;
       }
   close($fd);
   return @env_model;
}


#
# Model solar radiation at the Earth's surface
# Source: http://solardat.uoregon.edu/SolarRadiationBasics.html
# Returns sun array containing solar radiation in Wm^2
#
sub solarmodel{my($lat)  = $_[0];
               my($lon)  = $_[1];
               my($tilt) = $_[2];
               my($az)   = $_[3];
               my($sf)   = $_[4]; #scale factor for atmosphere etc.

  my($pi) = 3.1415926535897;
  my($deg2rad) = $pi/180.0;
  my($solar_constant) = 1367; #kWm^2
  my($yday, $hr);
  my($b,$sun_rvector,$sr,$d,$Eqt,$tsolar,$w,$cos_z);
  my($T_rad,$cos_q);
  my(@sun);
  my($ind) = 0;
  if (!defined $tilt || $tilt eq "") {$tilt = 0;}
  if (!defined $az || $az eq "") {$az = 0;}
  if (!defined $sf || $sf eq "") {$sf = 1.0;}
  #For efficiency, calculate these once
  my($sin_lat)  = sin($lat  * $deg2rad);
  my($cos_lat)  = cos($lat  * $deg2rad);
  my($sin_tilt) = sin($tilt * $deg2rad);
  my($cos_tilt) = cos($tilt * $deg2rad);
  my($sin_d,$cos_d);
  foreach $yday (1..365)
  {foreach $hr (0..23)
    {#Source: http://solardat.uoregon.edu/SolarRadiationBasics.html
     # 	Io= 1367 * (Rav / R)2 W/m2
     #  Rav is mean sun-earth dist, R is actual sun-earth distance depending on day of year
     #  (Rav/R)2 =  1.00011 + 0.034221 * cos(b) + 0.001280 * sin(b) + 0.000719 * cos(2b) + 0.000077 * sin(2b)
     #	  	where b = 2pn / 365 radians
     $b = 2. * $pi * $yday /365.;
     $sun_rvector = 1.00011 + 0.034221 * cos($b) + 0.001280 * sin($b) + 
                    0.000719 * cos(2. * $b) + 0.000077 * sin(2. * $b);
     $d = 23.45 * $deg2rad * sin(2.*$pi * (284. + $yday) / 365.);
     $sin_d = sin($d);
     $cos_d = cos($d);
     $Eqt = &calc_Eqt($yday);
     #$tsolar = $hr + $Eqt/60.0 +  ($lon_sm - $lon)/15;
     $tsolar = $hr + $Eqt/60.0 +  (abs($lon) % 15)/15.0;
     #print "tsolar=[$tsolar]   eqt=$Eqt\n";
     $w = $pi * (12.0 - $tsolar)/12.0;
     if ($tilt == 0)
       {$cos_z = $sin_lat * $sin_d + $cos_lat * $cos_d * cos($w);
        $sr = $solar_constant/($sun_rvector) * $cos_z;
        }
     else 
       {#TBD - take into account the tilt angle
        # The amount of direct radiation on a horizontal surface can be calculated
        # by multiplying the direct normal irradiance times the cosine of the 
        # zenith angle. On a surface tilted T degrees from the horizontal and 
        # rotated g degrees from the north-south axis, the direct component on 
        # the tilted surface is determined by multiplying the direct normal 
        # irradiance by
        # From PVWatts - 
        # Increasing the tilt angle favors energy production in the winter
        #       while decreasing the tilt angle favors energy production
        #       in the summer.
        # The default value is an azimuth angle of 180° (south-facing) for 
        # locations in the northern hemisphere, and 0° (north-facing) for 
        # locations in the southern hemisphere. This normally maximizes energy 
        # production. For the northern hemisphere, increasing the azimuth angle 
        # favors afternoon energy production, while decreasing the azimuth 
        # angle favors morning energy production. The opposite is true for the 
        # southern hemisphere. 
        #
        my($g) = $az * $deg2rad;
        $cos_q = $sin_d * $sin_lat * $cos_tilt - 
                 $sin_d * $cos_lat * $sin_tilt * cos($g) + 
                 $cos_d * $cos_lat * $cos_tilt * cos($w) +
                 $cos_d * $sin_lat * $sin_tilt * cos($g) * cos($w) +
                 $cos_d * $sin_tilt * sin($g) * sin($w);

        $sr = $solar_constant/($sun_rvector) * $cos_q;
        } #end-if tilt != 0
     #printf("%03d %02d %.2f %.2f  %f  %f\n",$yday,$hr,$sr,$d,$sun_rvector,$cos_z);
     if ($sr < 0) {$sun[$ind++] = 0;}
     else         {$sun[$ind++] = $sr * $sf;}
     } #end-foreach hr
  } #end-foreach yday
  return @sun;
}

#Equation of time for sunmodel
sub calc_Eqt {my($yday) = $_[0];
  my($pi) = 3.1415926535897;
  if ($yday >= 1 && $yday <= 106)
    {return (-14.2 * sin($pi * ($yday + 7.0) / 111.0));}
  elsif ($yday >= 107 && $yday <= 166)
    {return (4.0 * sin($pi * ($yday - 106) / 59));}
  elsif ($yday >= 167 && $yday <= 246)
    {return (-6.5 * sin($pi * ($yday - 166.0) / 80.0));}
  elsif ($yday >= 247 && $yday <= 365)
    {return (16.4 * sin($pi * ($yday - 247.0) / 113.0));}
}

######################################################
#Sample Config File Follows
my($dummy_var) = "";
$dummy_var = <<"EndSampleConfigFile";
######################################################

######################################################
# Platform configuration file for OOI/CGSN Power Model
# Format: Name = value
#
# Comments begin with #
#
# Loads for Hotel and Sensors are in watts
# Cycle values (for Hotel and Sensors) are in the form number 
#       of m,n where m=number of minutes, every n-hours
#
# Last Updated: 2009/12/12 10:15:10
######################################################

Platform.name = Pioneer Array
Platform.id   = A1242C23
Platform.lat  = 41.0
Platform.lon  = -70.0

Duration.days = 1

#-------------- Power Generation Section -------------
#Avalible power cfg options: solar, wind fuelcell
PowerCfg = solar, wind, fuelcell
SolarPwr.watts = 250
SolarPwr.efficiency = 10
SolarPwr.tilt = 10

WindPwr.watts        = 200
WindPwr.efficiency   = 80
WindPwr.min_wind_spd = 50
WindPwr.max_wind_spd = 50
WindPwr.enable.flatline = 50

FuelCellPwr.watts  = 250
FuelCellPwr.cycle  = 30,5 
FuelCellPwr.capacity_l = 500
FuelCellPwr.burnrate = .5

Battery.voltage     = 24
Battery.max_current = 10
Battery.capacity_wh = 1000
Battery.charge_efficiency = 80
Battery.min_charge_percent = 50
#-----------------------------------------------------


#------------------ Hotel Config  Section ---------------
#Avalible hotel cfg options: cpm, dcl, gps, fb250, iridium, eth
HotelCfg  = cpm,dcl,gps,fb250,iridium,eth  
Hotel.cpm.load      = 2
Hotel.cpm.cycle     = 1,3
Hotel.eth.load      = 2
Hotel.eth.cycle     = 1,3
Hotel.dcl.load      = 2
Hotel.dcl.cycle     = 60,1
Hotel.gps.load      = .1
Hotel.gps.cycle     = 60,1
Hotel.fb250.load    = 20
Hotel.fb250.cycle   = 10,3
Hotel.iridium.load  = 2
Hotel.iridium.cycle = 5,3
#--------------------------------------------------------


#----------------- Sensor Config  Section ---------------
#Avalible sensor cfg options: ctd
SensorCfg = ctd
Sensor.ctd.load   = 2
Sensor.ctd.cycle  = 1,3
Sensor.ctd.sample_size = 50;
Sensor.ctd.sample_rate = 60;
#--------------------------------------------------------


#----------- Environmental Config  Section --------------
#high_cycle: start hour,end hour
#sun  high_low_val: Intensity - 0 no sun, 1 full intensity
#wind high_low_val: speed (knots)
#temp high_low_val: temperature (degC)
#
Env.sun.datafile      =
Env.sun.high_cycle    = 10,14
Env.sun.high_low_val  = 1,0
Env.wind.datafile     =
Env.wind.high_cycle   = 10,14
Env.wind.high_low_val = 10,0
Env.temp.datafile     =
Env.temp.high_cycle   = 07,16
Env.temp.high_low_val = 20,10
#--------------------------------------------------------


EndSampleConfigFile

 
