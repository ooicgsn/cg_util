Date:   	Wed, 12 May 2010 15:00:46 -0400
From:  	Robert Petitt <rpetitt@whoi.edu>
To:  	Steve Lerner <slerner@whoi.edu>
Cc:  	Micheil Boesel <mboesel@whoi.edu>
Subject:  	Wind turbine data

Steve,
The ip address of the serial port server on Iselin roof is:
128.128.88.239 and the telnet port is 7001.
The data streams at about 10 Hz and the format is:

Airbreeze Volts, Airbreeze Amps, Windsmile Volts, Windsmile Amps, Forgen
Volts, Forgen Amps, wind direction, wind speed

Conversion to true units:
True Airbreeze Amps = (Airbreeze Amps -2.505) * 10
True Windsmile Amps = (Windsmile Amps -2.505) * 25
True Forgen Amps = Forgen Amps -2.471
True wind direction = (wind direction * 128) + 140 (degrees)
True wind speed = wind speed / 4.56 (mph)

Let me know what you think.

Bob
