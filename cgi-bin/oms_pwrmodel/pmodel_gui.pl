#!/usr/bin/perl 
#!c:/Perl64/bin/perl 
###############################################################################
#
# pmodel_gui.pl - Steven Lerner 12/2009
#                 Woods Hole Oceanographic Institution
#
# Description: Web-based user interface for OOI/CGO power model/simulator
#
# Web Usage: pmodel_gui.pl?cfg=cfg_file[&duration=ndays][&debug=0|1]
#
# History:
#       Date       Who    Description
#       ----       ---    ----------------------------------------------------
#     12/26/2009   SL     Create
#     01/27/2010   SL     Added Solar Model SF, AUV Dock (v1.0)
#     05/22/2010   SL     Added data model interface for hotel & sensor loads
#                         link to admin page (TBD). Added telem thruput
#     08/04/2010   SL     Updated user interface to fully support all browsers
#                         elimiminated scrolling divs w/in tables and added 
#                         show/hide parameter options
#     08/02/2012   SL     Added battery charging parameters and start day (v1.3)
#     10/17/2012   SL     Relocated to /webdata/cgsn/omc
#     11/27/2013   SL     Increased results iframe size
#     08/16/2016   JJP	  Updated paths to cfg and gif files
#     08/26/2016   JJP	  ../Cfg is now ../../../platcon_configs/platform_cfgs
#     04/10/2017   CC	  Removed AUV Dock code
###############################################################################
require "getopts.pl";
require "flush.pl";
require "timelocal.pl";

my($VERSION) = "v1.3-20121017";
my($DEBUG)   = 0;



#@month=('null','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');


#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      } 
elsif ($ENV{'REQUEST_METHOD'} eq "GET")
     {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }

elsif ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair,2);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
}

#
#Check args and give usage
#
if (0 &&! $arg{'cfg'})
   {print "Content-type: text/html\n\n";
    print "$0 - $VERSION\n";
    print "Usage: $0 <cfg=cfg_file>[duration=][debug=0|1]\n";
    exit;
    }
if ($arg{'debug'} eq "1") {$DEBUG = 1;}
if (!defined $arg{'Duration.days'}) {$arg{'Duration.days'} = 1;}
if (!defined $arg{'Simulation.sday'}) {$arg{'Simulation.sday'} = 1;}

if ($arg{'cfg'} eq "") {$arg{'cfg'} = "pioneer.cfg";}


if ($arg{'expand'} ne "") 
  {$arg{'expand'} =~ s/,,/,/g; $arg{'expand'} =~ s/^,//g;
   $value = $arg{'expand'};
   (@expand_list) = split(',',$value);
   foreach $i (@expand_list)
         {$expand{$i} = 1;}
   }



print "Content-type: text/html\n\n";

#Give list of availabe CFGs
#Note - the power cfg files are now stored with platform cfgs
opendir(DIR,"../../../platcon_configs/platform_cfgs");
(@flist) = sort grep(/\.cfg$/,readdir(DIR));
closedir(DIR);
$cfg_list = "";
foreach $f (@flist)
      {chomp($f);
       my($name) = $f; $name =~ s/\.cfg//;
       if ($f eq $arg{'cfg'})
            {$cfg_list .= "<OPTION value=\"$f\" SELECTED>$name</OPTION>\n";}
       else {$cfg_list .= "<OPTION value=\"$f\">$name</OPTION>\n";}
       }

#print "Content-type: text/html\n\n";
print "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n";
print <<"EndComment";
<!-- 
        #Interactive OOI/CGO Buoy Power & Data Modeling Simulator
        #(c)2010 Deep Submergence Laboratory - SL
        #Woods Hole Oceanographic Institution
-->
EndComment
flush(STDOUT);

#
#Handle buttons here
#
if ($arg{'UseDefaultButton'} ne "") 
  {#print "Defaults Loaded\n";
   if (-f "/tmp/pwrmodel/$arg{'cfg'}.$ENV{'REMOTE_ADDR'}")
     {unlink("/tmp/pwrmodel/$arg{'cfg'}.$ENV{'REMOTE_ADDR'}") || print "Unable to load defaults";
      }
   }

if ($arg{'NoButton'} || $arg{'RunSimButton'} ne "" || $arg{'RunCSVButton'} ne "" || $arg{'PlotButton'} ne "") 
  {#save file to tmp dir with ip_addr extension
   if (!-d "/tmp/pwrmodel") {mkdir "/tmp/pwrmodel";}
   if (open($FD, ">/tmp/pwrmodel/$arg{'cfg'}.$ENV{'REMOTE_ADDR'}"))
     {foreach $key (sort keys %arg)
        {print $FD "$key = $arg{$key}\n";
         }
      close($FD);
      #print "Configuration saved<br>\n";
      }
   else {print "***Error, unable to save configuration<br>\n";}

   if ($arg{'PlotButton'}   ne "") {$arg{'otype'} = "plot_pc"};
   if ($arg{'RunSimButton'} ne "") {$arg{'otype'} = "ascii"};
   if ($arg{'RunCSVButton'} ne "") {$arg{'otype'} = "csv"};
   if ($arg{'otype'} eq "") {$arg{'otype'} = "ascii";}
   }


#
# Read configuration file - use last customized one if available
#
if (-f "/tmp/pwrmodel/$arg{'cfg'}.$ENV{'REMOTE_ADDR'}")
     {$CfgFile = "/tmp/pwrmodel/$arg{'cfg'}.$ENV{'REMOTE_ADDR'}";
      #print "Loaded customized cfg...<br>\n";
      }
else {$CfgFile = "../../../platcon_configs/platform_cfgs/$arg{'cfg'}";
      }
%Cfg = load_cfg("$CfgFile");
if (!defined %Cfg) {exit(-1);}

#if ($arg{'RunButton'} ne "") 
#  {$cmd = "powermodel.pl d=1 cf=$CfgFile";
#   $ret = `$cmd`;
#   print "<xmp>$ret</xmp>\n";
#   }


#Figure which power generation items should be checked on the form
$Cfg{'PowerCfg'} =~ s/ //g; #remove spaces
(@power_config_list) = split(',',$Cfg{'PowerCfg'});
foreach $item (@power_config_list)
   {if ($Cfg{"PowerCfg.enable.$item"} ne "")    {$CHECKED{"$item"} = "CHECKED";}
    }

#Figure which hotel items should be checked on the form
$Cfg{'HotelCfg'} =~ s/ //g; #remove spaces
(@hotel_load_list) = split(',',$Cfg{'HotelCfg'});
foreach $item (@hotel_load_list)
   {if ($Cfg{"HotelCfg.enable.$item"} ne "")    {$CHECKED{"Hotel.$item"} = "CHECKED";}
    }

if ($Cfg{'FuelCellPwr.enable.acycle'} ne "") 
      {$CHECKED{'FuelCellPwr.enable.acycle'} = "CHECKED";}
if ($Cfg{'Battery.enable.acycle'} ne "") 
      {$CHECKED{'Battery.enable.acycle'} = "CHECKED";}
if ($Cfg{'WindPwr.enable.flatline'} ne "") 
      {$CHECKED{'WindPwr.enable.flatline'} = "CHECKED";}

#Figure which sensor items should be checked on the form
$Cfg{'SensorCfg'} =~ s/ //g; #remove spaces
(@sensor_load_list) = split(',',$Cfg{'SensorCfg'});
foreach $item (@sensor_load_list)
   {if ($Cfg{"SensorCfg.enable.$item"} ne "")   {$CHECKED{"Sensor.$item"} = "CHECKED";}
    }

$fnt1 = "<font color=#eeeeee face=sans-serif size=2><b>";
$fnt2 = "<font color=#0088aa face=sans-serif size=2><b>";
$fnt3 = "<font color=#00aadd face=sans-serif size=2><b>";

print <<"EndHTML";
<html>
<head>
<title>CGO Buoy Power Model</title>
<script language="JavaScript">
    function updateCfgForm(form)
       {cfgFile = form.ConfigFile.options[form.ConfigFile.selectedIndex].value;
        window.location.href = "/cgo/pwrmodel-bin/pmodel_gui.pl?cfg=" + cfgFile;
        }
        
</script>

</head>
<body bgcolor=#0088aa xxxOnLoad=window.focus()>

<form method="post">

<div id="hiddenBlockDiv" style="position:absolute; height:0px; width:0px; top:-50px; left:1px;">
<input type="Submit" name=NoButton value="_" size=0> 
</div>


<!-- title & button Block area -->
<table bgcolor=#004466 cellspacing=0 cellpadding=0 width=100% border=0>
<td>
  <table bgcolor=#004466 border=0 cellspacing=2 cellpadding=0 width=175 height=85>
    <td width=175 valign=top>
       <table cellspacing=0 cellpadding=0 border=0>
        <td xxcolspan=2 width=100%>$fnt1 Select Cfg:</font>
  <SELECT NAME="ConfigFile" onChange="updateCfgForm(this.form)">
     $cfg_list
  </SELECT>

        </tr><tr>
        <td>$fnt1 ID: </font> $fnt3 $Cfg{'Platform.id'}</font>
        </tr><tr>
        <td xxcolspan=2>
            $fnt1 Lat: </font> $fnt3 $Cfg{'Platform.lat'}</font> &nbsp 
            $fnt1 Lon: </font> $fnt3 $Cfg{'Platform.lon'}</font>
        <br>$fnt1 View: </font> &nbsp 
            <a href=?cfg=$arg{'cfg'}&otype=drawing><font color=#00aadd>Drawing</font></a> </font> &nbsp 
            <a href=?cfg=$arg{'cfg'}&show=gmap><font color=#00aadd>Map</font></a> </font> 
      </table>
      
    </td>
  </table>

<td>
  <table bgcolor=#004466 border=0 cellspacing=2 cellpadding=0 width=750 height=85>
      <tr><td align=center><font face=sans-serif size=3 color=#00aaff><b>CGSN Buoy Power & Data Modeling Simulator</b></font><font size=1 color=#00aaff> - $VERSION</td>
  <td align=right rowspan=2 valign=top>
    <font size=2 color=#00aaff>Admin</font>&nbsp <br>
    <a href=/cgo/pwrmodel-data target=_AdminWin><font size=2 color=#00aaff>Models</font><br>
    <a href=/cgo/pwrmodel-cfg/$arg{'cfg'} target=_AdminWin><font size=2 color=#00aaff>Cfg File</font>
  </td></tr>
      <tr><td colspan=2 align=center>
            <input type="hidden" name="cfg" value="$arg{'cfg'}">
            <input type="hidden" name="Platform.name" value="$Cfg{'Platform.name'}">
            <input type="hidden" name="Platform.id" value="$Cfg{'Platform.id'}">
            <input type="hidden" name="Platform.lat" value="$Cfg{'Platform.lat'}">
            <input type="hidden" name="Platform.lon" value="$Cfg{'Platform.lon'}">
            <input type="hidden" name="PowerCfg" value="$Cfg{'PowerCfg'}">
            <input type="hidden" name="HotelCfg" value="$Cfg{'HotelCfg'}">
            <input type="hidden" name="SensorCfg" value="$Cfg{'SensorCfg'}">
            <input type=hidden name=expand value="$arg{'expand'}">
	<table cellspacing=0 cellpadding=1>
        <td>
	    $fnt1 SDay <input type="text" name="Simulation.sday" value="$arg{'Simulation.sday'}" size="2"> &nbsp
	    &nbsp
            $fnt1 Duration <input type="text" name="Duration.days" value="$arg{'Duration.days'}" size="2"> &nbsp
	<td><font size=1>
            <input type="Submit" name=UseDefaultButton value="Use Defaults"> 
<!--
            <input type="Submit" name=SaveButton value="Save"> 
-->
            <input type="Submit" name=RunSimButton value="Run SIM"> 
            <input type="Submit" name=RunCSVButton value="CSV"> 
            <input type="Submit" name=PlotButton value="Plot_pg">
            <input type="Submit" name=PlotButton value="Plot_pc">
            <input type="Submit" name=PlotButton value="Plot_gc">
</td></tr>
<tr><td colspan=10><font size=1>
            <input type="Submit" name=PlotButton value="Plot_fuel">
            <input type="Submit" name=PlotButton value="Plot_bwh">
            <input type="Submit" name=PlotButton value="Plot_bp">
            <input type="Submit" name=PlotButton value="Plot_solar">
            <input type="Submit" name=PlotButton value="Plot_wind">
            <input type="Submit" name=PlotButton value="Plot_temp">
            <input type="Submit" name=PlotButton value="Plot_dg">
            <input type="Submit" name=PlotButton value="Plot_tt">
    </table>
</table>

</table>


<!-- Input Block Area -->
<table bgcolor=#004466 border=0 cellspacing=3 cellpadding=0 width=100%>
  <td>
      <table cellspacing=0 cellpadding=0 border=1 width=100%>
EndHTML

if ($expand{"power_generation"})
  {($expand_list = $arg{'expand'}) =~ s/power_generation//;
   print <<"EndHTML";
             <td><table border=0>
	          <td valign=top><a href=?cfg=$arg{'cfg'}&expand=$expand_list>
	                <img src=/oms-html/images/down_white.gif border=0 valign=middle></a>
	          <td><a href=?cfg=$arg{'cfg'}&expand=$expand_list>$fnt1 Power Generation</a>
		  </table>
             <td><input type="CHECKBOX" name="PowerCfg.enable.solar"    $CHECKED{'solar'}>$fnt2 Solar PV &nbsp 
                 <input type="CHECKBOX" name="PowerCfg.enable.wind"     $CHECKED{'wind'}>$fnt2 Wind Turbine &nbsp
                 <input type="CHECKBOX" name="PowerCfg.enable.fuelcell" $CHECKED{'fuelcell'}>$fnt2 FuelCell
             </tr><tr>
             <td>&nbsp</td>
             <td>
                <table>
                   <td>$fnt2 SolarPwr <td>
                     <input type="text" name="SolarPwr.watts" value="$Cfg{'SolarPwr.watts'}" size="2">$fnt2 Watts &nbsp
                     <input type="text" name="SolarPwr.efficiency" value="$Cfg{'SolarPwr.efficiency'}" size="2"> % Efficiency &nbsp  
                     <input type="text" name="SolarPwr.tilt" value="$Cfg{'SolarPwr.tilt'}" size="2"> Tilt Angle
                     </tr><tr>
                   <td>$fnt2 WindPwr <td>
                    <input type="text" name="WindPwr.watts" value="$Cfg{'WindPwr.watts'}" size="2">$fnt2 Watts &nbsp
                    <input type="text" name="WindPwr.efficiency" value="$Cfg{'WindPwr.efficiency'}" size="2"> % Efficiency &nbsp
                    <input type="text" name="WindPwr.min_wind_spd" value="$Cfg{'WindPwr.min_wind_spd'}" size="2"> Min WSpd &nbsp 
                    <input type="text" name="WindPwr.max_wind_spd" value="$Cfg{'WindPwr.max_wind_spd'}" size="2"> Max WSpd (knots)
                    <input type="checkbox" name="WindPwr.enable.flatline" $CHECKED{"WindPwr.enable.flatline"}>$fnt2 Flat-Line
                     </tr><tr>
                   <td valign=top>$fnt2 FuelCell <td>
                    <input type="text" name="FuelCellPwr.watts" value="$Cfg{'FuelCellPwr.watts'}" size="2">$fnt2 Watts &nbsp
                    <input type="text" name="FuelCellPwr.capacity_l" value="$Cfg{'FuelCellPwr.capacity_l'}" size="2">$fnt2 Capacity (l) &nbsp
                    <input type="text" name="FuelCellPwr.burnrate" value="$Cfg{'FuelCellPwr.burnrate'}" size="2">$fnt2 Burnrate (l/h) &nbsp
</tr><tr><td>&nbsp</td><td>
                    <input type="checkbox" name="FuelCellPwr.enable.acycle" $CHECKED{"FuelCellPwr.enable.acycle"}>$fnt2 Auto Cycle 
                    <input type="text" name="FuelCellPwr.acycle" value="$Cfg{'FuelCellPwr.acycle'}" size="4">$fnt2 (B_Min,B_Max) &nbsp
                    or &nbsp $fnt2 Manual Cycle 
                    <input type="text" name="FuelCellPwr.cycle" value="$Cfg{'FuelCellPwr.cycle'}" size="4">$fnt2 (n min, every n hrs)
</tr><tr><td>&nbsp</td><td>
                    <input type="text" name="FuelCellPwr.init_load" value="$Cfg{'FuelCellPwr.init_load'}" size="3">$fnt2 InitLoad (watts) &nbsp
                    <input type="text" name="FuelCellPwr.init_dur" value="$Cfg{'FuelCellPwr.init_dur'}" size="2">$fnt2 InitDur (min) &nbsp
                    <input type="text" name="FuelCellPwr.off_load" value="$Cfg{'FuelCellPwr.off_load'}" size="3">$fnt2 OffLoad (watts) &nbsp
                    <input type="text" name="FuelCellPwr.off_dur" value="$Cfg{'FuelCellPwr.off_dur'}" size="2">$fnt2 OffDur (min) &nbsp
                 </table>
             </td></tr>
EndHTML
}
else {print <<"EndHTML";
             <td><table border=0>
	          <td valign=top><a href=?cfg=$arg{'cfg'}&expand=$arg{'expand'},power_generation>
	                <img src=/oms-html/images/right_white.gif border=0 valign=middle></a>
	          <td><a href=?cfg=$arg{'cfg'}&expand=$arg{'expand'},power_generation>$fnt1 Power Generation</a>
		  </table>
             <td><input type="CHECKBOX" name="PowerCfg.enable.solar"    $CHECKED{'solar'}>$fnt2 Solar PV &nbsp 
                 <input type="CHECKBOX" name="PowerCfg.enable.wind"     $CHECKED{'wind'}>$fnt2 Wind Turbine &nbsp
                 <input type="CHECKBOX" name="PowerCfg.enable.fuelcell" $CHECKED{'fuelcell'}>$fnt2 FuelCell
                    <input type="hidden" name="SolarPwr.watts" value="$Cfg{'SolarPwr.watts'}">
                    <input type="hidden" name="SolarPwr.efficiency" value="$Cfg{'SolarPwr.efficiency'}">
                    <input type="hidden" name="SolarPwr.tilt" value="$Cfg{'SolarPwr.tilt'}"> 
                    <input type="hidden" name="WindPwr.watts" value="$Cfg{'WindPwr.watts'}">
                    <input type="hidden" name="WindPwr.efficiency" value="$Cfg{'WindPwr.efficiency'}">
                    <input type="hidden" name="WindPwr.min_wind_spd" value="$Cfg{'WindPwr.min_wind_spd'}"> 
                    <input type="hidden" name="WindPwr.max_wind_spd" value="$Cfg{'WindPwr.max_wind_spd'}">
                    <input type="hidden" name="WindPwr.enable.flatline" $CHECKED{"WindPwr.enable.flatline"}>
                    <input type="hidden" name="FuelCellPwr.watts" value="$Cfg{'FuelCellPwr.watts'}">
                    <input type="hidden" name="FuelCellPwr.capacity_l" value="$Cfg{'FuelCellPwr.capacity_l'}">
                    <input type="hidden" name="FuelCellPwr.burnrate" value="$Cfg{'FuelCellPwr.burnrate'}">
                    <input type="hidden" name="FuelCellPwr.enable.acycle" $CHECKED{"FuelCellPwr.enable.acycle"}>
                    <input type="hidden" name="FuelCellPwr.acycle" value="$Cfg{'FuelCellPwr.acycle'}">
                    <input type="hidden" name="FuelCellPwr.cycle" value="$Cfg{'FuelCellPwr.cycle'}">
                    <input type="hidden" name="FuelCellPwr.init_load" value="$Cfg{'FuelCellPwr.init_load'}">
                    <input type="hidden" name="FuelCellPwr.init_dur" value="$Cfg{'FuelCellPwr.init_dur'}">
                    <input type="hidden" name="FuelCellPwr.off_load" value="$Cfg{'FuelCellPwr.off_load'}">
                    <input type="hidden" name="FuelCellPwr.off_dur" value="$Cfg{'FuelCellPwr.off_dur'}">
             </tr>


EndHTML
     }


print <<"EndHTML";

        <tr><td>$fnt1 Battery Config
             <td>$fnt2 Voltage <input type="text" name="Battery.voltage" value="$Cfg{'Battery.voltage'}" size=2> &nbsp
                 Max Curr <input type="text" name="Battery.max_current" value="$Cfg{'Battery.max_current'}" size=2> &nbsp 
                 Capacity_wh <input type="text" name="Battery.capacity_wh" value="$Cfg{'Battery.capacity_wh'}" size=6> &nbsp
                 Min Chrg <input type="text" name="Battery.min_charge_percent" value="$Cfg{'Battery.min_charge_percent'}" size=2> &nbsp
                 Charge Effic <input type="text" name="Battery.charge_efficiency" value="$Cfg{'Battery.charge_efficiency'}" size=2>

<br> $fnt2 Charge Algorithm: &nbsp 
                    <input type="checkbox" name="Battery.enable.acycle" $CHECKED{"Battery.enable.acycle"}>$fnt2 Auto Cycle 
                    <input type="text" name="Battery.acycle" value="$Cfg{'Battery.acycle'}" size="5">$fnt2 (B_Min,B_Max) &nbsp
</td></tr>

        <tr><td>$fnt1 Environmental
            <td><table>
                <td>$fnt2 Sun: 
                <td>  $fnt2 Datafile  <input type="text" name="Env.sun.datafile" value="$Cfg{'Env.sun.datafile'}" size=20> &nbsp
                      &nbsp or &nbsp Use internal Solar Radiation Model &nbsp 
                                     SF: <input type="text" name="Env.sun.sf" value="$Cfg{'Env.sun.sf'}" size=2>
<!--
HighCycle:<input type="text" name="Env.sun.high_cycle" value="$Cfg{'Env.sun.high_cycle'}" size=4> &nbsp
                                     HighLowVal:<input type="text" name="Env.sun.high_low_val" value="$Cfg{'Env.sun.high_low_val'}" size=4> &nbsp
-->
                </tr><tr>
                <td>$fnt2 Wind: 
                <td>  $fnt2 Datafile  <input type="text" name="Env.wind.datafile" value="$Cfg{'Env.wind.datafile'}" size=20> &nbsp
                      &nbsp or &nbsp HighCycle:<input type="text" name="Env.wind.high_cycle" value="$Cfg{'Env.wind.high_cycle'}" size=4> &nbsp
                                     HighLowVal:<input type="text" name="Env.wind.high_low_val" value="$Cfg{'Env.wind.high_low_val'}" size=4> &nbsp

                </tr><tr>
                <td>$fnt2 Temp: 
                <td>  $fnt2 Datafile  <input type="text" name="Env.temp.datafile" value="$Cfg{'Env.temp.datafile'}" size=20> &nbsp
                      &nbsp or &nbsp HighCycle:<input type="text" name="Env.temp.high_cycle" value="$Cfg{'Env.temp.high_cycle'}" size=4> &nbsp
                                     HighLowVal:<input type="text" name="Env.temp.high_low_val" value="$Cfg{'Env.temp.high_low_val'}" size=4> &nbsp
                 </table>
        </td></tr>

EndHTML


        if ($expand{"hotel_loads"})
          {($expand_list = $arg{'expand'}) =~ s/hotel_loads//;
	   print "<tr><td><table><td><a href=?cfg=$arg{'cfg'}&expand=$expand_list><img src=/oms-html/images/down_white.gif border=0>\n";
	   print "               <td><a href=?cfg=$arg{'cfg'}&expand=$expand_list>$fnt1 Hotel Loads</a></table>\n";
	   }
        else
	  {print "<tr><td><table><td><a href=?cfg=$arg{'cfg'}&expand=$arg{'expand'},hotel_loads><img src=/oms-html/images/right_white.gif border=0>\n";
	   print "               <td><a href=?cfg=$arg{'cfg'}&expand=$arg{'expand'},hotel_loads>$fnt1 Hotel Loads</a></table>\n";
	   }
        print "<td>\n";
              #Loop through each HotelLoad specified in the config file
              foreach $key (sort keys %Cfg)
               {next if ($key !~ /^Hotel/);
                ($tag,$name,$load_cycle) = split('\.',$key);  #Hotel.name.{load|cycle}
                next if ($load_cycle ne "load");
                print "<input type=\"CHECKBOX\" name=\"HotelCfg.enable.$name\" $CHECKED{\"Hotel.$name\"}>$fnt2 $name &nbsp \n";
                }
        print "</tr>\n";
        if ($expand{"hotel_loads"})
             {print "<tr><td>&nbsp</td><td><table>\n";
             foreach $key (sort keys %Cfg)
               {next if ($key !~ /^Hotel/);
                ($tag,$name,$load_cycle) = split('\.',$key);  #Hotel.name.{load|cycle}
                next if ($load_cycle ne "load");
                print "<tr><td>$fnt2 $name &nbsp \n";
                print "<td><input type=\"text\" name=\"Hotel.$name.load\" value=\"$Cfg{\"Hotel.$name.load\"}\" size=3>$fnt2 Load (watts) &nbsp &nbsp \n";
                print "    <input type=\"text\" name=\"Hotel.$name.cycle\" value=\"$Cfg{\"Hotel.$name.cycle\"}\" size=5>$fnt2 cycle (n min, every n hrs) &nbsp \n";
                if (defined $Cfg{"Hotel.$name.sample_size"})
                  {print "    &nbsp <font color=#dddddd>Data:</font> <input type=\"text\" name=\"Hotel.$name.sample_size\" value=\"$Cfg{\"Hotel.$name.sample_size\"}\" size=3>$fnt2 bytes &nbsp \n";
                   print "    <input type=\"text\" name=\"Hotel.$name.sample_rate\" value=\"$Cfg{\"Hotel.$name.sample_rate\"}\" size=3>$fnt2 samples/min \n";
                   }
                if (defined $Cfg{"Hotel.$name.telem_thruput"})
                  {print "    &nbsp <font color=#dddddd>Thruput:</font> <input type=\"text\" name=\"Hotel.$name.telem_thruput\" value=\"$Cfg{\"Hotel.$name.telem_thruput\"}\" size=3>$fnt2 kBps &nbsp \n";
                   }
                if (defined $Cfg{"Hotel.$name.telem_cost_per_min"})
                  {print "    <input type=\"text\" name=\"Hotel.$name.telem_cost_per_min\" value=\"$Cfg{\"Hotel.$name.telem_cost_per_min\"}\" size=3>$fnt2 \$/min \n";
                   }
                if (defined $Cfg{"Hotel.$name.telem_cost_per_mbyte"})
                  {print "    <input type=\"text\" name=\"Hotel.$name.telem_cost_per_mbyte\" value=\"$Cfg{\"Hotel.$name.telem_cost_per_mbyte\"}\" size=3>$fnt2 \$/mbyte \n";
                   }
                #print "    <input type=\"text\" name=\"Hotel.$name.quiescent\" value=\"$Cfg{\"Hotel.$name.quiescent\"}\" size=3>$fnt2 Quiescent (watts) \n";
                }
               print "</table>\n";
	      }
        else
             {#not expanded - setup as hidden fields
	      foreach $key (sort keys %Cfg)
               {next if ($key !~ /^Hotel/);
                ($tag,$name,$load_cycle) = split('\.',$key);  #Hotel.name.{load|cycle}
                next if ($load_cycle ne "load");
                print "    <input type=\"hidden\" name=\"Hotel.$name.load\" value=\"$Cfg{\"Hotel.$name.load\"}\">\n";
                print "    <input type=\"hidden\" name=\"Hotel.$name.cycle\" value=\"$Cfg{\"Hotel.$name.cycle\"}\">\n";
                if (defined $Cfg{"Hotel.$name.sample_size"})
                  {print " <input type=\"hidden\" name=\"Hotel.$name.sample_size\" value=\"$Cfg{\"Hotel.$name.sample_size\"}\">\n";
                   print " <input type=\"hidden\" name=\"Hotel.$name.sample_rate\" value=\"$Cfg{\"Hotel.$name.sample_rate\"}\">\n";
                   }
                if (defined $Cfg{"Hotel.$name.telem_thruput"})
                  {print "   <input type=\"hidden\" name=\"Hotel.$name.telem_thruput\" value=\"$Cfg{\"Hotel.$name.telem_thruput\"}\">\n";
                   }
                if (defined $Cfg{"Hotel.$name.telem_cost_per_min"})
                  {print "   <input type=\"hidden\" name=\"Hotel.$name.telem_cost_per_min\" value=\"$Cfg{\"Hotel.$name.telem_cost_per_min\"}\">\n";
                   }
                if (defined $Cfg{"Hotel.$name.telem_cost_per_mbyte"})
                  {print "   <input type=\"hidden\" name=\"Hotel.$name.telem_cost_per_mbyte\" value=\"$Cfg{\"Hotel.$name.telem_cost_per_mbyte\"}\">\n";
                   }
                #print "    <input type=\"text\" name=\"Hotel.$name.quiescent\" value=\"$Cfg{\"Hotel.$name.quiescent\"}\" size=3>$fnt2 Quiescent (watts) \n";
	       }
	      }
        print "</td></tr>\n";


        if ($expand{"sensor_loads"})
          {($expand_list = $arg{'expand'}) =~ s/sensor_loads//;
	   print "<tr><td><table><td><a href=?cfg=$arg{'cfg'}&expand=$expand_list><img src=/oms-html/images/down_white.gif border=0>\n";
	   print "               <td><a href=?cfg=$arg{'cfg'}&expand=$expand_list>$fnt1 Sensor Loads</a></table>\n";
	   }
        else
	  {print "<tr><td><table><td><a href=?cfg=$arg{'cfg'}&expand=$arg{'expand'},sensor_loads><img src=/oms-html/images/right_white.gif border=0>\n";
	   print "               <td><a href=?cfg=$arg{'cfg'}&expand=$arg{'expand'},sensor_loads>$fnt1 Sensor Loads</a></table>\n";
	   }
        print "<td>\n";
              #Loop through each SensorLoad specified in the config file
              foreach $key (sort keys %Cfg)
               {next if ($key !~ /^Sensor/);
                ($tag,$name,$load_cycle) = split('\.',$key);  #Hotel.name.{load|cycle}
                next if ($load_cycle ne "load");
                print "<input type=\"CHECKBOX\" name=\"SensorCfg.enable.$name\" $CHECKED{\"Sensor.$name\"}>$fnt2 $name &nbsp \n";
                }
        print "</tr>\n";
        if ($expand{"sensor_loads"})
             {print "<tr><td>&nbsp</td><td><table>\n";
              foreach $key (sort keys %Cfg)
               {next if ($key !~ /^Sensor/);
                ($tag,$name,$load_cycle) = split('\.',$key);  #Sensor.name.{load|cycle}
                next if ($load_cycle ne "load");
                print "<tr><td>$fnt2 $name &nbsp \n";
                print "<td><input type=\"text\" name=\"Sensor.$name.load\" value=\"$Cfg{\"Sensor.$name.load\"}\" size=3>$fnt2 Load (watts) &nbsp &nbsp \n";
                print "    <input type=\"text\" name=\"Sensor.$name.cycle\" value=\"$Cfg{\"Sensor.$name.cycle\"}\" size=5>$fnt2 cycle (n min, every n hrs) &nbsp \n";
                if (defined $Cfg{"Sensor.$name.sample_size"})
                  {print "    &nbsp <font color=#dddddd>Data:</font> <input type=\"text\" name=\"Sensor.$name.sample_size\" value=\"$Cfg{\"Sensor.$name.sample_size\"}\" size=3>$fnt2 bytes &nbsp \n";
                   print "    <input type=\"text\" name=\"Sensor.$name.sample_rate\" value=\"$Cfg{\"Sensor.$name.sample_rate\"}\" size=3>$fnt2 samples/min\n";
                   }
                }
              print "</table>\n";
	      }
        else
             {#not expanded - setup as hidden fields
	      foreach $key (sort keys %Cfg)
               {next if ($key !~ /^Sensor/);
                ($tag,$name,$load_cycle) = split('\.',$key);  #Sensor.name.{load|cycle}
                next if ($load_cycle ne "load");
                print "    <input type=\"hidden\" name=\"Sensor.$name.load\" value=\"$Cfg{\"Sensor.$name.load\"}\">\n";
                print "    <input type=\"hidden\" name=\"Sensor.$name.cycle\" value=\"$Cfg{\"Sensor.$name.cycle\"}\">\n";
                if (defined $Cfg{"Sensor.$name.sample_size"})
                  {print " <input type=\"hidden\" name=\"Sensor.$name.sample_size\" value=\"$Cfg{\"Sensor.$name.sample_size\"}\">\n";
                   print " <input type=\"hidden\" name=\"Sensor.$name.sample_rate\" value=\"$Cfg{\"Sensor.$name.sample_rate\"}\">\n";
                   }
                }
	      }
        print "</td></tr>\n";

print <<"EndHTML";
      </table>
</table>
</form>



<!-- Results Block Area -->
EndHTML
if ($arg{'otype'} =~ /plot/i)
  {#print "<iframe name=ResultsIframe src=\"/cgo/pwrmodel-bin/powermodel.pl?cf=$CfgFile&xxotype=plot\" height=350px width=800px frameborder=1>\n";
   print "<img src=\"/cgo/pwrmodel-bin/powermodel.pl?cf=$CfgFile&duration=$arg{'Duration.days'}&otype=$arg{'PlotButton'}\" hspace=155>\n";
   #print "</iframe>\n";
   }
elsif ($arg{'otype'} =~ /csv/i)
  {print "<iframe name=ResultsIframe src=\"/cgo/pwrmodel-bin/powermodel.pl?otype=csv&cf=$CfgFile&duration=$arg{'Duration.days'}\" height=325px width=900px frameborder=1>\n";
   print "</iframe>\n";
   }
  elsif ($arg{'otype'} eq "drawing")
     {print "<img src=/cgo/platforms/Drawings/$Cfg{'Platform.drawing'} hspace=175>\n";
      }
  elsif ($arg{'show'} eq "gmap")
     {print <<"EndHTML";
<table hspace=150><td><iframe width="600" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/?q=$Cfg{'Platform.lat'},$Cfg{'Platform.lon'}&amp;num=1&amp;ie=UTF8&amp;t=h&amp;ll=$Cfg{'Platform.lat'},$Cfg{'Platform.lon'}&amp;z=6&amp;om=1&amp;output=embed"></iframe>
</table>
EndHTML
      }
else
  {print "<iframe name=ResultsIframe src=\"/cgo/pwrmodel-bin/powermodel.pl?otype=ascii&cf=$CfgFile&duration=$arg{'Duration.days'}\" height=525px width=1200px frameborder=1>\n";
   print "</iframe>\n";
   }
print <<"EndHTML";
EndHTML



if ($DEBUG)
  {foreach $key (sort keys %Cfg) {print "$key = $Cfg{$key}<br>\n";}
   foreach $key (sort keys %ENV) {print "$key = $ENV{$key}<br>\n";}
   }

print "</body>\n";
print "</html>\n";


exit;

#
# Subroutines Follow
#
sub load_cfg {my($file) = $_[0];
   my(%Cfg,$line);
   if (!open($CFG_FD,"$file")) {print "***Unable to open config file [$file]";
                                return;
                                }
  while ($line = <$CFG_FD>)
      {next if ($line =~ /^#/ || length($line) < 2); #skip comments & blank lines
       chomp($line);
       my($att,$val) = split("=",$line,2);
       $att =~ s/^(\s+)//; #remove leading spaces
       $att =~ s/(\s+)$//; #remove trailing spaces
       $val =~ s/^(\s+)//; #remove leading spaces
       $val =~ s/(\s+)$//; #remove trailing spaces
       $Cfg{"$att"} = "$val";
       #print "$att=[$val]\n" if $DEBUG;
       }
   close($CFG_FD);
   return %Cfg;
}
