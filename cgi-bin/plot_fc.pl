#!/usr/bin/perl
# plot_fc.pl -  Quick script to generate rt fc data plot for test deployment
#
# Usage: plot_fc.pl pid= deploy= [date=yyyymmdd] [lastN=n days]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      04/10/2014  SL     Create from plot_psc
#      11/24/2014  SL     Ported to oms. Added pid and date arg.
#      12/09/2014  SL     Added plotting fc temp, updated to use get_args
#      02/02/2015  SL     Support Deployment dir
##################################################################################
use strict;
require "cg_util.pl";

my %arg    = &get_args(0);
my $pid    = $arg{'pid'};
my $deploy = $arg{'deploy'};
if ($pid eq "" || $deploy eq "") {print "pid and deploy must be specified\n"; exit(-1);}

my $ddir = "/webdata/cgsn/data/raw/$pid/$deploy/cg_data/pwrsys";
my $day; #yyyymmdd 
if ($arg{'date'} ne "") {$day = $arg{'date'};}
                   else {$day = gmtimestamp_filename(0);}
my $lastN = 2; #lastN files
if ($arg{'lastN'} ne "") {$lastN = $arg{'lastN'};
                          $lastN =~ s/h|H//; #strip-off h if included
                          }

my @full_flist = `cd $ddir; /bin/ls -1 $day*.pwrsys.log`;
my @flist;
my ($i, $line, $date, $time, $fc_status);
for ($i=$lastN;$i>=0;$i--) 
   {my $fname = $full_flist[$#full_flist-$i+1];
    chomp($fname);
    push(@flist,$fname);
    }
#print "FList=[@flist]\n"; exit(-1);


print "<h4>Fuel Cell Data from $day</h4>\n";

my %FC;
my(@fc_data) =  `cd $ddir; grep -hvi No_FC_Data @flist | cut -f1,2,79 -d " "`; 
#@fc_data =  `grep -hvi No_FC_Data @flist | cut -f1,2,79 -d " "`; 
#@fc_data =  `grep -hvi No_FC_Data $ddir/$day\_*.pwrsys.log | cut -f1,2,79 -d " "`; 
#@fc_data =  `cut -f1,2,79 -d " " $ddir/$day\_*.pwrsys.log | grep -v -i "No_FC_Data"`; 
if ($#fc_data <= 0)
  {print "<html><body>&nbsp&nbsp No Fuel Cell data available <br></body></html>\n";
   exit(0);
   }
#print "FC_data=[@fc_data]\n"; exit(-1);


my($html_out) = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<table>
   <td>FC Voltage
       <div id="graphdiv1"
        style="width:500px; height:200px;"></div>
   </td>
   <td>FC Current (Amps)  
       <div id="graphdiv2"
        style="width:500px; height:200px;"></div>
   </td>
   <td>FC State
       <div id="graphdiv22"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>

   <td>Power Out Bat1_Bat2 (watts)
       <div id="graphdiv3" 
        style="width:500px; height:240px;"></div>
   </td>
   <td>Power Out Total (watts)
       <div id="graphdiv4" 
        style="width:500px; height:240px;"></div>
   </td>
   </tr>
   </tr><tr>

   <td>Reform Temp (degC)  
       <div id="graphdiv5"
        style="width:500px; height:200px;"></div>
   </td>
   <td>Reform Press (psi)
       <div id="graphdiv6"
        style="width:500px; height:200px;"></div>
   </td>
   <td>FuelCell Temp (DegC)
       <div id="graphdiv10"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>

   <td>AirPump (%)
       <div id="graphdiv7"
        style="width:500px; height:200px;"></div>
   </td>
   <td>FuelPump (%)
       <div id="graphdiv8"
        style="width:500px; height:200px;"></div>
   </td>
   <td>CoolantPump (%)
       <div id="graphdiv9"
        style="width:500px; height:200px;"></div>
   </td>
   </tr>

</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
EndHTML
    $html_out .= "\"date_time, fc_voltage\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        $FC{'fcvmon_in'} = sprintf("%.3f",$FC{'fcvmon_in'}/1000.0);      #Volts
        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'fcvmon_in'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'fcvmon_in'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
EndHTML
    $html_out .= "\"date_time, fc_current\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        $FC{'fcimon_in'}      = sprintf("%.3f",$FC{'fcimon_in'}/1000.0);      #Amps
        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'fcimon_in'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'fcimon_in'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );



  g22 = new Dygraph(
    document.getElementById("graphdiv22"),
EndHTML
    $html_out .= "\"date_time, sysinfo\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'sys_info'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'sys_info'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      valueRange: [0,5]
     }          // options
  );



  g3 = new Dygraph(
    document.getElementById("graphdiv3"),
EndHTML
    $html_out .= "\"date_time, bat1, bat2\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        $FC{'inbc1_outpwr'}   = sprintf("%7.3f", $FC{'inbc1_outpwr'}/1000.0); #watts
        $FC{'inbc2_outpwr'}   = sprintf("%7.3f", $FC{'inbc2_outpwr'}/1000.0); #watts
        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'inbc1_outpwr'}, $FC{'inbc2_outpwr'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'inbc1_outpwr'}, $FC{'inbc2_outpwr'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g4 = new Dygraph(
    document.getElementById("graphdiv4"),
EndHTML
    $html_out .= "\"date_time, Total_Watts\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        $FC{'tot_outpwr'}   = sprintf("%7.3f", $FC{'inbc1_outpwr'}/1000.0 + $FC{'inbc2_outpwr'}/1000.0); #watts
        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'tot_outpwr'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'tot_outpwr'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g5 = new Dygraph(
    document.getElementById("graphdiv5"),
EndHTML
    $html_out .= "\"date_time, reform_temp\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        $FC{'refblockout_in'} = sprintf("%.1f",$FC{'refblockout_in'}/10.0);   #degrees
        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'refblockout_in'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'refblockout_in'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g6 = new Dygraph(
    document.getElementById("graphdiv6"),
EndHTML
    $html_out .= "\"date_time, reform_press\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        $FC{'fuelpmon_in'}    = sprintf("%.3f",$FC{'fuelpmon_in'}/1000.0);    #psi
        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'fuelpmon_in'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'fuelpmon_in'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g7 = new Dygraph(
    document.getElementById("graphdiv7"),
EndHTML
    $html_out .= "\"date_time, AirPump\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'ap_outdc'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'ap_outdc'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g8 = new Dygraph(
    document.getElementById("graphdiv8"),
EndHTML
    $html_out .= "\"date_time, FuelPump\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'methfuel_outdc'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'methfuel_outdc'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g9 = new Dygraph(
    document.getElementById("graphdiv9"),
EndHTML
    $html_out .= "\"date_time, CoolantPump\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'fan_outdc'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'fan_outdc'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g10 = new Dygraph(
    document.getElementById("graphdiv10"),
EndHTML
    $html_out .= "\"date_time, FC_Temp\\n\" +\n";
    for ($i=0;$i<$#fc_data;$i++)
       {$line = $fc_data[$i];
        chomp($line);
	($date,$time,$fc_status) = split(' ',$line);
	$time = substr($time,0,8);
        ($FC{'dmgr_ver'},$FC{'sys_ver'},$FC{'uptime'},$FC{'fcvmon_in'},$FC{'fcimon_in'},
         $FC{'refblockout_in'},$FC{'pmon_in'},$FC{'exhaustmon_in'},$FC{'fuelpmon_in'},
         $FC{'methfuel_outdc'},$FC{'ap_outdc'},$FC{'fan_outdc'},$FC{'aptach_in'},$FC{'sys_info'},
         $FC{'reformer_fuelremaining'},$FC{'inbc1_outpwr'},$FC{'inbc1_chargertemp'},
         $FC{'inbc2_outpwr'},$FC{'inbc2_chargertemp'},$FC{'bopconverter_outpwr'},$FC{'bopconverter_chargertemp'},
         $FC{'dctodctmon_in'},$FC{'brdtempmon_in'},$FC{'pm_err'},$FC{'pm_errormask'},
         $FC{'reformer_errormask'},$FC{'fccmgr_errormask'}) = split(',',$fc_status);

        $FC{'exhaustmon_in'}  = sprintf("%.1f",$FC{'exhaustmon_in'}/10.0);    #degrees
        if ($i < $#fc_data-1) {$html_out .= "\"$date $time, $FC{'exhaustmon_in'}\\n\" +\n";}
                         else {$html_out .= "\"$date $time, $FC{'exhaustmon_in'}\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

