#!/usr/bin/perl
#
# view_psc_error.pl
#
# Description: Looks up psc error flag and displays messages
#              
# Usage: view_psc_error e=flag1,flag2,flag3
#        where flag1,flag2, and flag3 are hex error codes from psc
#
# History:
#     Date       Who     Description
#     ------     ---     ---------------------------------------------------
#   03/12/2011   SL      Create
#   02/06/2013   SL      Add flag3 for High-power coverter errors
#   06/29/2015   SL      Updated to use cg_util.pl and fault tables from mdf dir
###############################################################################
use strict;
require "cg_util.pl";

my $DEBUG = 0;
my($mdf_dir) = "/webdata/cgsn/data/mdf";

print "Content-type: text/plain\n\n";

my %arg = &get_args(0);
if (!defined $arg{'e'} || $arg{'e'} eq "")
   {print "Usage: view_psc_error <e=flag1,flag2,flag3>\n";
    exit(-1);
    }

#
# Global error flag array
#
my(%ErrorFlag1, %ErrorFlag2, %ErrorFlag3);
my $err_cnt1 = 0;
my $err_cnt2 = 0;
my $err_cnt3 = 0;
my $key;

my $err_code;
my $err_msg;
my $psc_fault_file = "$mdf_dir/psc_faults.txt";

my($err_flag1,$err_flag2,$err_flag3) = split(',',$arg{'e'});
$err_flag1 = hex $err_flag1;
$err_flag2 = hex $err_flag2;
$err_flag3 = hex $err_flag3;

if (!&load_psc_fault_table($psc_fault_file)) {exit(-1);}

if ($DEBUG)
  {foreach $key (sort keys %ErrorFlag1)
     {print "F1 $key: $ErrorFlag1{$key}\n";}
   printf "\n";
   foreach $key (sort keys %ErrorFlag2)
     {print "F2 $key: $ErrorFlag2{$key}\n";}
   printf "\n";
   foreach $key (sort keys %ErrorFlag3)
     {print "F3 $key: $ErrorFlag3{$key}\n";}
   }


#Look up error and print error message(s)
foreach $key (sort keys %ErrorFlag1)
     {if ($err_flag1 & hex $key) {print "F1 $key: $ErrorFlag1{$key}\n";}
      $err_cnt1++;
      }
foreach $key (sort keys %ErrorFlag2)
     {if ($err_flag2 & hex $key) {print "F2 $key: $ErrorFlag2{$key}\n"}
      $err_cnt2++;
      }
foreach $key (sort keys %ErrorFlag3)
     {if ($err_flag3 & hex $key) {print "F3 $key: $ErrorFlag3{$key}\n"}
      $err_cnt3++;
      }

if ($err_flag1 == 0) {printf "F1 0x%08x: No Error\n",$err_flag1;}
else {if ($err_cnt1 == 0) 
        {print "Unknown error - flag1=$err_flag1\n";}
      }
if ($err_flag2 == 0) {printf "F2 0x%08x: No Error\n",$err_flag2;}
else {if ($err_cnt2 == 0) 
        {print "Unknown error - flag2=$err_flag2\n";}
      }
if ($err_flag3 == 0) {printf "F3 0x%08x: No Error\n",$err_flag3;}
else {if ($err_cnt3 == 0) 
        {print "Unknown error - flag3=$err_flag3\n";}
      }




#--------------------------------------------------------------
#load_psc_fault_table
#   Loads ErrorFlag hash arrays
# Returns 1-successful, 0-error
#--------------------------------------------------------------
sub load_psc_fault_table {my($file) = $_[0];
   my($line, $eflag);
   my($err_code,$err_msg);
   my($FD);

  if (!open($FD,"$file")) {print "***Unable to open psc_fault_table [$file]\n";
                           return 0;
                           }

  $eflag = "flag1";
  while ($line = <$FD>)
      {if ($line =~ /^#Begin Flag2/i) {$eflag = "flag2";}
       if ($line =~ /^#Begin Flag3/i) {$eflag = "flag3";}
       next if ($line =~ /^#/); #skip comments
       ($err_code,$err_msg) = split(" ",$line,2);
       $err_code =~ s/^(\s+)//; #remove leading spaces
       $err_code =~ s/(\s+)$//; #remove trailing spaces
       $err_msg  =~ s/^(\s+)//; #remove leading spaces
       $err_msg  =~ s/(\s+)$//; #remove trailing spaces
       if    ($eflag eq "flag1") {$ErrorFlag1{$err_code} = $err_msg;}
       elsif ($eflag eq "flag2") {$ErrorFlag2{$err_code} = $err_msg;}
                            else {$ErrorFlag3{$err_code} = $err_msg;}
       }
   close($FD);

   return 1;
}
