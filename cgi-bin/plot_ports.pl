#!/usr/bin/perl
# plot_ports.pl -  Quick script to generate rt DCL port status
#
# Usage: plot_ports.pl [dcl=dclN]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create
#      08/07/2011  SL     Added dcl arg to plot from cg_data/dclN/syslog
#      07/10/2013  SL     Support STC
#      09/18/2013  SL     Updated to support new STC firmware - v2.07
##################################################################################


#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'QUERY_STRING'} ne "")
 {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }
if ($#ARGV >= 0) {@pairs = @ARGV;}


#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = $value\n";
}

#print "Content-type: text/html\n\n";

my @pstate_data;
my @pcurr_data;

if ($arg{'type'} eq "stc")
  {@pstate_data =  `./view_syslog limit=1500 s=\"stc: \" extract=61,66,71,76,81 label=p1,p2,p3,p5,p7`; 
   @pcurr_data  =  `./view_syslog limit=1500 s=\"stc: \" extract=62,67,72,77,82 label=p1,p2,p3,p5,p7`; 
   }
elsif ($arg{'dcl'} eq "")
  {@pstate_data =  `./view_syslog limit=1500 s=\" dcl: \" extract=29,34,39,44,49,54,59,64 label=p1,p2,p3,p4,p5,p6,p7,p8`; 
   @pcurr_data  =  `./view_syslog limit=1500 s=\" dcl: \" extract=31,36,41,46,51,56,61,66 label=p1,p2,p3,p4,p5,p6,p7,p8`; 
   }
else
  {@pstate_data =  `./view_dcl_syslog limit=1500 s=\" dcl: \" extract=29,34,39,44,49,54,59,64 label=p1,p2,p3,p4,p5,p6,p7,p8`; 
   @pcurr_data  =  `./view_dcl_syslog limit=1500 s=\" dcl: \" extract=31,36,41,46,51,56,61,66 label=p1,p2,p3,p4,p5,p6,p7,p8`; 
   }

my $html_out = <<"EndHTML";
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>
<table>
   <td>All Ports On/Off States
       <table border=0 cellpadding=0 cellspacing=0>
         <td valign="top" align=center><div id="labels1"></div></td>
         </tr><tr>
         <td valign="top"> <div id="graphdiv1"
                            style="width:500px; height:300px;"></div></td>
       </table>
   </td>

   <td>Current (ma)
       <table border=0 cellpadding=0 cellspacing=0>
         <td valign="top" align=center><div id="labels2"></div></td>
         </tr><tr>
         <td valign="top"> <div id="graphdiv2"
                            style="width:500px; height:300px;"></div></td>
       </table>
   </td>
   </tr><tr>
</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
EndHTML
    for ($i=0;$i<$#pstate_data;$i++)
       {$line = $pstate_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#pstate_data-1){$html_out .= "\"$line\\n\" +\n";}
                            else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      fillGraph: false,
      stepPlot: false,
      stackedGraph: true,
      rollPeriod: 1,
      writeStroke: 2.0,
      valueRange: [0,8],
      labelsDiv: "labels1",
      legend: "always"
     }          // options
  );

  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
EndHTML
    for ($i=0;$i<$#pcurr_data;$i++)
       {$line = $pcurr_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#pcurr_data-1){$html_out .= "\"$line\\n\" +\n";}
                            else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels2",
      legend: "always"
     }          // options
  );

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

