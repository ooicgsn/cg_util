#!/usr/bin/perl
# plot_ports.pl -  Quick script to generate rt DCL port status
#
# Usage: plot_ports.pl pid= deploy= [dcl=dclN]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create
#      08/07/2011  SL     Added dcl arg to plot from cg_data/dclN/syslog
#      11/28/2013  SL     Support stc
#      12/01/2013  SL     Added date arg and dlog port stats (tx,rx,log,gd,bd,bb)
#      12/17/2014  SL     Added plotting port voltages and updated to use cg_util.pl
#      02/02/2015  SL     Support Deployent dir
#      03/24/2015  SL     Added plotting port error flag
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
#      08/14/2016  JJP	  Use ScriptAlias /cmn-bin for view_syslog and 
#			  view_dcl_syslog
#      08/19/2016  JJP	  Removed /cmn-bin from path for view_syslog and 
#			  view_dcl_syslog, as they have moved back to cgi-bin/oms
##################################################################################
use strict;
require "../cg_util.pl";

my %arg    = &get_args(0);
my $pid    = $arg{'pid'};
my $deploy = $arg{'deploy'};
my $limit  = 3000;

if ($pid eq "" || $deploy eq "") {print "pid and deploy must be specified\n"; exit(-1);}

#print "Content-type: text/html\n\n";

my (@pstate_data, @pvlt_data, @pcurr_data, @eflag_data);
my (@p1_gdbd_data, @p2_gdbd_data, @p3_gdbd_data, @p4_gdbd_data, @p5_gdbd_data, @p6_gdbd_data, @p7_gdbd_data, @p8_gdbd_data);
my (@p1_txrx_data, @p2_txrx_data, @p3_txrx_data, @p4_txrx_data, @p5_txrx_data, @p6_txrx_data, @p7_txrx_data, @p8_txrx_data);

if ($arg{'type'} =~ /stc/i)
  {#print "PID=$arg{'pid'} dcl=$arg{'dcl'}\n";
   @pstate_data =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\"stc:\" extract=61,66,71,76,81 label=p1,p2,p3,p5,p7 date=$arg{'date'}`;
   @pvlt_data   =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\"stc:\" extract=61,67,72,77,82 label=p1,p2,p3,p5,p7 date=$arg{'date'}`;
   @pcurr_data  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\"stc:\" extract=62,68,73,78,83 label=p1,p2,p3,p5,p7 date=$arg{'date'}`;
   @eflag_data  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\"stc:\" extract=63,69,74,79,84 label=p1,p2,p3,p5,p7 date=$arg{'date'}`;
   }

else 
  {if ($arg{'dcl'} eq "")
     {#print "PID=$arg{'pid'} dcl=$arg{'dcl'}\n";
      @pstate_data =  `./view_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\" dcl: \" extract=29,34,39,44,49,54,59,64 label=p1,p2,p3,p4,p5,p6,p7,p8 date=$arg{'date'}`; 
      @pcurr_data  =  `./view_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\" dcl: \" extract=31,36,41,46,51,56,61,66 label=p1,p2,p3,p4,p5,p6,p7,p8 date=$arg{'date'}`; 
      }
   else
     {@pstate_data =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\" dcl: \" extract=29,34,39,44,49,54,59,64 label=p1,p2,p3,p4,p5,p6,p7,p8 date=$arg{'date'}`; 
      @pvlt_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\" dcl: \" extract=30,35,40,45,50,55,60,65 label=p1,p2,p3,p4,p5,p6,p7,p8 date=$arg{'date'}`; 
      @pcurr_data  =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\" dcl: \" extract=31,36,41,46,51,56,61,66 label=p1,p2,p3,p4,p5,p6,p7,p8 date=$arg{'date'}`; 
      @eflag_data  =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\" dcl: \" extract=32,37,42,47,52,57,62,67 label=p1,p2,p3,p4,p5,p6,p7,p8 date=$arg{'date'}`; 
      @p1_gdbd_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP1 istatus:\" extract=17,19,21 label=GoodRec,BadRec,BB date=$arg{'date'}`; 
      @p2_gdbd_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP2 istatus:\" extract=17,19,21 label=GoodRec,BadRec,BB date=$arg{'date'}`; 
      @p3_gdbd_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP3 istatus:\" extract=17,19,21 label=GoodRec,BadRec,BB date=$arg{'date'}`; 
      @p4_gdbd_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP4 istatus:\" extract=17,19,21 label=GoodRec,BadRec,BB date=$arg{'date'}`; 
      @p5_gdbd_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP5 istatus:\" extract=17,19,21 label=GoodRec,BadRec,BB date=$arg{'date'}`; 
      @p6_gdbd_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP6 istatus:\" extract=17,19,21 label=GoodRec,BadRec,BB date=$arg{'date'}`; 
      @p7_gdbd_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP7 istatus:\" extract=17,19,21 label=GoodRec,BadRec,BB date=$arg{'date'}`; 
      @p8_gdbd_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP8 istatus:\" extract=17,19,21 label=GoodRec,BadRec,BB date=$arg{'date'}`; 
      @p1_txrx_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP1 istatus:\" extract=11,13,15 label=Tx,Rx,Log date=$arg{'date'}`; 
      @p2_txrx_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP2 istatus:\" extract=11,13,15 label=Tx,Rx,Log date=$arg{'date'}`; 
      @p3_txrx_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP3 istatus:\" extract=11,13,15 label=Tx,Rx,Log date=$arg{'date'}`; 
      @p4_txrx_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP4 istatus:\" extract=11,13,15 label=Tx,Rx,Log date=$arg{'date'}`; 
      @p5_txrx_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP5 istatus:\" extract=11,13,15 label=Tx,Rx,Log date=$arg{'date'}`; 
      @p6_txrx_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP6 istatus:\" extract=11,13,15 label=Tx,Rx,Log date=$arg{'date'}`; 
      @p7_txrx_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP7 istatus:\" extract=11,13,15 label=Tx,Rx,Log date=$arg{'date'}`; 
      @p8_txrx_data   =  `./view_dcl_syslog pid=$pid deploy=$deploy dcl=$arg{'dcl'} limit=$limit s=\"DLOGP8 istatus:\" extract=11,13,15 label=Tx,Rx,Log date=$arg{'date'}`; 
      }
   }

my ($i, $line);
my $html_out = <<"EndHTML";
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>
<table border=0>
   <td><table>
   <td>$arg{'dcl'} All Ports On/Off States
       <table border=0 cellpadding=0 cellspacing=0>
         <td valign="top" align=center><div id="labels1"></div></td>
         </tr><tr>
         <td valign="top"> <div id="graphdiv1"
                            style="width:500px; height:300px;"></div></td>
       </table>
   </td>

   <td>Current (ma)
       <table border=0 cellpadding=0 cellspacing=0>
         <td valign="top" align=center><div id="labels2"></div></td>
         </tr><tr>
         <td valign="top"> <div id="graphdiv2"
                            style="width:500px; height:300px;"></div></td>
       </table>
   </td>

   <td>Voltage (v)
       <table border=0 cellpadding=0 cellspacing=0>
         <td valign="top" align=center><div id="labels2_2"></div></td>
         </tr><tr>
         <td valign="top"> <div id="graphdiv2_2"
                            style="width:500px; height:300px;"></div></td>
       </table>
   </td>

   <td>Error Flag
       <table border=0 cellpadding=0 cellspacing=0>
         <td valign="top" align=center><div id="labels2_3"></div></td>
         </tr><tr>
         <td valign="top"> <div id="graphdiv2_3"
                            style="width:500px; height:300px;"></div></td>
       </table>
   </td>

   </table>
   </tr><tr>
   


   
   <td colspan=10><table cellspacing=0 cellpadding=3 border=1>

EndHTML

   if ($#p1_txrx_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port1 Tx/Rx/Log
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels3_1"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv3_1"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
      
   if ($#p2_txrx_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port2 Tx/Rx/Log
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels3_2"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv3_2"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p3_txrx_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port3 Tx/Rx/Log
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels3_3"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv3_3"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p4_txrx_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port4 Tx/Rx/Log
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels3_4"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv3_4"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p5_txrx_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port5 Tx/Rx/Log
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels3_5"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv3_5"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p6_txrx_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port6 Tx/Rx/Log
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels3_6"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv3_6"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p7_txrx_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port7 Tx/Rx/Log
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels3_7"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv3_7"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p8_txrx_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port8 Tx/Rx/Log
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels3_8"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv3_8"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 

$html_out .= <<"EndHTML";
       </table>
       </td>

   </td>
   
   </tr><tr>

   <td colspan=10><table cellspacing=0 cellpadding=3 border=1>

EndHTML

   if ($#p1_gdbd_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port1 Good/Bad
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels4_1"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv4_1"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
      
   if ($#p2_gdbd_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port2 Good/Bad
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels4_2"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv4_2"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p3_gdbd_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port3 Good/Bad
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels4_3"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv4_3"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p4_gdbd_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port4 Good/Bad
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels4_4"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv4_4"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p5_gdbd_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port5 Good/Bad
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels4_5"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv4_5"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p6_gdbd_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port6 Good/Bad
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels4_6"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv4_6"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p7_gdbd_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port7 Good/Bad
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels4_7"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv4_7"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }
	 
   if ($#p8_gdbd_data > 5) 
      {$html_out .= <<"EndHTML";
       <td>Port8 Good/Bad
         <table border=0 cellpadding=0 cellspacing=0>
           <td valign="top" align=center><div id="labels4_8"></div></td>
           </tr><tr>
           <td valign="top"> <div id="graphdiv4_8"
                            style="width:400px; height:150px;"></div></td>
         </table>
       </td>
EndHTML
      }

$html_out .= <<"EndHTML";

</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
EndHTML
    for ($i=0;$i<$#pstate_data;$i++)
       {$line = $pstate_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#pstate_data-1){$html_out .= "\"$line\\n\" +\n";}
                            else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      fillGraph: false,
      stepPlot: false,
      stackedGraph: true,
      rollPeriod: 1,
      writeStroke: 2.0,
      valueRange: [0,8],
      labelsDiv: "labels1",
      legend: "always"
     }          // options
  );

  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
EndHTML
    for ($i=0;$i<$#pcurr_data;$i++)
       {$line = $pcurr_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#pcurr_data-1){$html_out .= "\"$line\\n\" +\n";}
                            else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels2",
      legend: "always"
     }          // options
  );

  g2_2 = new Dygraph(
    document.getElementById("graphdiv2_2"),
EndHTML
    for ($i=0;$i<$#pvlt_data;$i++)
       {$line = $pvlt_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#pvlt_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels2_2",
      legend: "always"
     }          // options
  );

  g2_3 = new Dygraph(
    document.getElementById("graphdiv2_3"),
EndHTML
    for ($i=0;$i<$#eflag_data;$i++)
       {$line = $eflag_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#eflag_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels2_3",
      legend: "always"
     }          // options
  );

EndHTML

$html_out .= <<"EndHTML";
g3_1 = new Dygraph(
    document.getElementById("graphdiv3_1"),
EndHTML
    for ($i=0;$i<$#p1_txrx_data;$i++)
       {$line = "$p1_txrx_data[$i]";
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p1_txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels3_1",
      legend: "always"
     }          // options
  );
EndHTML


$html_out .= <<"EndHTML";
g3_2 = new Dygraph(
    document.getElementById("graphdiv3_2"),
EndHTML
    for ($i=0;$i<$#p2_txrx_data;$i++)
       {$line = "$p2_txrx_data[$i]";
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p2_txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels3_2",
      legend: "always"
     }          // options
  );
EndHTML

$html_out .= <<"EndHTML";
g3_3 = new Dygraph(
    document.getElementById("graphdiv3_3"),
EndHTML
    for ($i=0;$i<$#p3_txrx_data;$i++)
       {$line = "$p3_txrx_data[$i]";
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p3_txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels3_3",
      legend: "always"
     }          // options
  );
EndHTML

$html_out .= <<"EndHTML";
g3_4 = new Dygraph(
    document.getElementById("graphdiv3_4"),
EndHTML
    for ($i=0;$i<$#p4_txrx_data;$i++)
       {$line = "$p4_txrx_data[$i]";
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p4_txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels3_4",
      legend: "always"
     }          // options
  );
EndHTML

$html_out .= <<"EndHTML";
g3_5 = new Dygraph(
    document.getElementById("graphdiv3_5"),
EndHTML
    for ($i=0;$i<$#p5_txrx_data;$i++)
       {$line = "$p5_txrx_data[$i]";
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p5_txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels3_5",
      legend: "always"
     }          // options
  );
EndHTML

$html_out .= <<"EndHTML";
g3_6 = new Dygraph(
    document.getElementById("graphdiv3_6"),
EndHTML
    for ($i=0;$i<$#p6_txrx_data;$i++)
       {$line = "$p6_txrx_data[$i]";
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p6_txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels3_6",
      legend: "always"
     }          // options
  );
EndHTML

$html_out .= <<"EndHTML";
g3_7 = new Dygraph(
    document.getElementById("graphdiv3_7"),
EndHTML
    for ($i=0;$i<$#p7_txrx_data;$i++)
       {$line = "$p7_txrx_data[$i]";
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p7_txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels3_7",
      legend: "always"
     }          // options
  );
EndHTML

$html_out .= <<"EndHTML";
g3_8 = new Dygraph(
    document.getElementById("graphdiv3_8"),
EndHTML
    for ($i=0;$i<$#p8_txrx_data;$i++)
       {$line = "$p8_txrx_data[$i]";
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p8_txrx_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels3_8",
      legend: "always"
     }          // options
  );
EndHTML






$html_out .= "g4_1 = new Dygraph(document.getElementById(\"graphdiv4_1\"),\n";
    for ($i=0;$i<$#p1_gdbd_data;$i++)
       {$line = $p1_gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p1_gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels4_1",
      legend: "always"
     }          // options
  );
EndHTML

$html_out .= "g4_2 = new Dygraph(document.getElementById(\"graphdiv4_2\"),\n";
    for ($i=0;$i<$#p2_gdbd_data;$i++)
       {$line = $p2_gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p2_gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels4_2",
      legend: "always"
     }          // options
  );
EndHTML


$html_out .= "g4_3 = new Dygraph(document.getElementById(\"graphdiv4_3\"),\n";
    for ($i=0;$i<$#p3_gdbd_data;$i++)
       {$line = $p3_gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p3_gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels4_3",
      legend: "always"
     }          // options
  );
EndHTML


$html_out .= "g4_4 = new Dygraph(document.getElementById(\"graphdiv4_4\"),\n";
    for ($i=0;$i<$#p4_gdbd_data;$i++)
       {$line = $p4_gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p4_gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels4_4",
      legend: "always"
     }          // options
  );
EndHTML
  

$html_out .= "g4_5 = new Dygraph(document.getElementById(\"graphdiv4_5\"),\n";
    for ($i=0;$i<$#p5_gdbd_data;$i++)
       {$line = $p5_gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p5_gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels4_5",
      legend: "always"
     }          // options
  );
EndHTML


$html_out .= "g4_6 = new Dygraph(document.getElementById(\"graphdiv4_6\"),\n";
    for ($i=0;$i<$#p6_gdbd_data;$i++)
       {$line = $p6_gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p6_gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels4_6",
      legend: "always"
     }          // options
  );
EndHTML


$html_out .= "g4_7 = new Dygraph(document.getElementById(\"graphdiv4_7\"),\n";
    for ($i=0;$i<$#p7_gdbd_data;$i++)
       {$line = $p7_gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p7_gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels4_7",
      legend: "always"
     }          // options
  );
EndHTML


$html_out .= "g4_8 = new Dygraph(document.getElementById(\"graphdiv4_8\"),\n";
    for ($i=0;$i<$#p8_gdbd_data;$i++)
       {$line = $p8_gdbd_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#p8_gdbd_data-1){$html_out .= "\"$line\\n\" +\n";}
                             else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      fillGraph: true,
      strokeWidth: 2.0,
      showRoller: false,
      rollPeriod: 1,
      labelsDiv: "labels4_8",
      legend: "always"
     }          // options
  );


</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

