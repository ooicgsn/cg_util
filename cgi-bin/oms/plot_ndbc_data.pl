#!/usr/bin/perl
# plot_ndbc_data.pl -  Quick script to plot csv data from ndbc buoy.. Accepts
#                      csv or space-separated.
#
# Usaage: plot_ndbc [bid=] [dtype=] [nrecs=] [skip_amt=] [cols=] [labels=]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      10/05/2011  SL     Create from plot_csv.pl
##################################################################################
use strict;

my($DEBUG) = 0;
my(%arg, $buffer, @pairs, $pair, $key, $value);

# Set default args
$arg{'cols'}       = "";
$arg{'allchecked'} = 0;
$arg{'nrecs'}      = 2000;
$arg{'radio'}      = 1;
$arg{'checkbox'}   = 0;
$arg{'width'}      = 600;
$arg{'height'}     = 300;
$arg{'skip'}       = 1;
$arg{'max_recs'}   = 5000;
$arg{'fill'}       = "false";
$arg{'bid'} = "44008";
$arg{'dtype'} = "txt"; # txt, spec,...
#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'QUERY_STRING'} ne "")
 {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }
if ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
}


if ($arg{'d'} ne "")     {$DEBUG = 1;}
if ($arg{'nrecs'} < 10)  {$arg{'nrecs'} = 10;}
elsif ($arg{'nrecs'} > $arg{'max_recs'}) {$arg{'nrecs'} = $arg{'max_recs'};}
if ($arg{'skip'} < 1) {$arg{'skip'} = 1;}
if    ($arg{'fill'} eq "1") {$arg{'fill'} = "true";}
elsif ($arg{'fill'} eq "0") {$arg{'fill'} = "false";}

if ($arg{'bid'} eq "") {print "Usage: $0 <bid=> [title=] [ylabel=] [cols=c1,c2...] [allchecked=1] [checkbox=1] [radio=1]\n";
                        exit(-1);
                        }

my(@field_names, @field_units, @data, %ColList, $i);


#Sample Realtime NDBC Buoy Data
#YY  MM DD hh mm WVHT  SwH  SwP  WWH  WWP SwD WWD  STEEPNESS  APD MWD
#yr  mo dy hr mn    m    m  sec    m  sec  -  degT     -      sec degT
#2011 10 23 10 00  0.8   MM   MM   MM   MM  MM  MM        N/A  4.7 226

(@data) = `wget -q -t1 -T 3 -O - http://www.ndbc.noaa.gov/data/realtime2/$arg{'bid'}.$arg{'dtype'}`;
if ($#data < 2) {print "Content-type: text/html\n\n";
                  print "No data available from $arg{'bid'} dtype=$arg{'dtype'}\n";
                  exit(-1);
                  }

    $data[0] =~ s/^#//; #Elim leading #
    $data[1] =~ s/^#//; #Elim leading #
    @field_names = split(' ',$data[0]);
    @field_units = split(' ',$data[1]);
    #append units to field names
    for ($i=0; $i<=$#field_names; $i++) {$field_names[$i] .= "_$field_units[$i]";}

    my(@field_name_subset);
    if ($arg{'cols'} ne "")
      {#only put specified cols into field_names
       my(@col_list) = split(',',$arg{'cols'});
       my($col);
       foreach $col (@col_list)
         {push(@field_name_subset,$field_names[$col-1]);
          my($ind) = $col-1;
          $ColList{$ind}++;
          }
       #@field_names = @new_list;
       }

print "Content-type: text/html\n\n";
print << "EndHTML";
   <!DOCTYPE html>
   <HTML>
   <HEAD><TITLE>Plot $arg{'title'}</TITLE>
         <!-- Plot NDBC Data
              (c) WHOI - SL 2011 
         -->
         <STYLE TYPE="text/css">
         <!--
          TH{font-family: Arial; font-size: 10pt;}
          TD{font-family: Arial; font-size: 10pt;}
          -->
         </STYLE>
    
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9"> 
    <!--[if IE]><script src="/cg/excanvas.compiled.js"></script><![endif]-->

         <script type="text/javascript"
            src="/cg/dygraph-combined.js"></script>
         </head>
   </HEAD>
   <BODY vlink=blue><br>
EndHTML


#
# Generate Plots Here
#
my($html_plot) = &gen_csv_plot;
print "$html_plot";

print "</BODY>\n";
print "</HTML>\n";

exit;

#
# Subroutines Follow
#


sub sec2hhmmss {#takes total seconds and returns d hh:mm:ss
        my($sec) = $_[0];
        my($dy,$hr,$mn,$sc);
        $dy = int($sec/86400);
        $hr = int(($sec - ($dy*86400))/3600);
        $mn = int(($sec - ($dy*86400) - ($hr*3600))/60);
        $sc = $sec - ($dy*86400) - ($hr*3600) - ($mn*60);
        if ($hr < 10) {$hr = "0$hr";}
        if ($mn < 10) {$mn = "0$mn";}
        if ($sc < 10) {$sc = "0$sc";}

        if ($dy == 0) {return "$hr:$mn:$sc";}
                 else {return "$dy $hr:$mn:$sc";}
}

sub time2dsltime_str {#takes total seconds and returns yyyy/mm/dd hh:mm:ss.ss
   my($time) = $_[0];
   my($outstr);
   my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdat) = gmtime($time);
   $year += 1900; $mon++;
   $outstr = sprintf("%04d/%02d/%02d %02d:%02d:%02d",$year,$mon,$mday,$hour,$min,$sec);
   return "$outstr";

}


sub gen_csv_plot {#Uses global field_names and data and %ColList and %arg
   my($html_out) = "";
   my($i, $hdr_line, $line, $checked);
   my($visibility) = "";

   $html_out .= "<table><td><div id=\"div_g\" style=\"width:$arg{'width'}px; height:$arg{'height'}px;\"></div></td>\n";
   $html_out .= "       </tr><tr>\n";
   $html_out .= "       <td align=center><div id=\"div_label\" style=\"width:600px; font-size:1.0em; padding-top:5px;\"></div></td>\n";
   $html_out .= "</table>\n";
   $html_out .= "<p>Debug: g.visibility() = <span id=\"visibility\"></span></p>\n" if $DEBUG;
   $html_out .= "<br><form name=\"myform\" method=\"post\">\n";

   my($ind_offset)   = 5; #normally 2 for date/time, but 5 for yy mm dd h m
   my($orig_num_fieldnames);
   if ($arg{'cols'} ne "") {$orig_num_fieldnames = $#field_names;
                            @field_names = ('date', 'time');
                            push(@field_names,@field_name_subset);
                            $ind_offset = 2;
                            }


   $hdr_line = "@field_names"; $hdr_line =~ s/(\s+)/ /sg; $hdr_line =~ s/ /,/g; $hdr_line =~ s/,$//;
   $hdr_line =~ s/YY_yr,MM_mo,DD_dy,hh_hr,mm_mn/date_time/;
   $hdr_line =~ s/date,time/date_time/;

   #print "Fieldnames=[@field_names] nfields=$#field_names subset=[@field_name_subset]\nHeader Line: $hdr_line\n"; exit;

   if ($arg{'checkbox'})
     {$html_out .= "<table cellpadding=8 border=0><td width=100 align=right valign=top><b>Plot:</b></td><td>\n";
      for ($i=0; $i<=$#field_names-$ind_offset; $i++)
        {if ($i==0 || $arg{'allchecked'}) {$checked = "CHECKED";}
                                     else {$checked = "";}
         $html_out .= "<input type=checkbox id=\"$i\" name=\"list\" onClick=\"change_checkbox(this)\" $checked>$field_names[$i+$ind_offset]</input> &nbsp \n";
         if (! (($i+1)%8)) {$html_out .= "<br>";}
        }
         $html_out .= "&nbsp <input type=button value=\"Clear All\" onClick=\"clear_checkbox(document.myform.list)\"></input>\n";
         $html_out .= "&nbsp <input type=button value=\"Set All\" onClick=\"set_checkbox(document.myform.list)\"></input><br>\n";
      $html_out .= "</td></table>\n";
     }

   if ($arg{'radio'})
     {$html_out .= "<table cellpadding=8 border=0><td width=100 align=right valign=top><b>Plot:</b></td><td>\n";
      for ($i=0; $i<=$#field_names-$ind_offset; $i++)
        {if ($i==0) {$checked = "CHECKED";}
               else {$checked = "";}
         my($varname,$ylabel) = split('_',$field_names[$i+$ind_offset]);
         if ($ylabel eq "" || $ylabel eq "-") {$ylabel = $varname;}
         $html_out .= "<input type=radio id=\"$i\" name=\"plot\" onClick=\"change_radio(this,$#field_names,'$ylabel')\" $checked>$varname &nbsp </input>\n";
         if (! (($i+1)%8)) {$html_out .= "<br>";}
        }
      $html_out .= "</td></table>\n";
      }
  $html_out .= "</form>\n";

if (0) {
   for ($i=0; $i<$#field_names-$ind_offset-1; $i++)
      {if ($i==0) {$visibility  = "visibility: [true, ";}
             else {$visibility .= "false, ";}
       }
   $visibility =~ s/, $/\],/; #replace last comma with closing bracket and comma
}


   $html_out .= << "EndHTML";
    <script type="text/javascript">
      g = new Dygraph(
            document.getElementById("div_g"),
EndHTML
     $html_out .= "\"$hdr_line\\n\" +\n";

    for ($i=2;$i<=$#data;$i+=$arg{'skip'})
      {$line = $data[$i];
       next if $line =~/^#/; #skip comments
       chomp($line);
       if ($line !~ /,/)
          {$line =~ s/(\s+)/ /sg; #elim multiple spaces
           $line =~ s/^(\s+)//;   #elim leading spaces
           $line =~ s/ /, /g;
           $line =~ s/, $//; #elim last comma
           }
       my($yy,$mm,$dd,$h,$m,$vars) = split(', ',$line,6);
       my($date) = "$yy/$mm/$dd";
       my($time) = "$h:$m:00";
       next if (length($date) < 8);
       if ($arg{'cols'} ne "")
         {#only print out selected columns
          $html_out .= "\"$date $time, ";
          my($j);
          my(@vlist) = split(',',$vars);
          for ($j=0;$j<=$orig_num_fieldnames;$j++)
            {next if !defined $ColList{$j};
             $html_out .= "$vlist[$j-$ind_offset-3], ";
             }
          chop($html_out); chop($html_out); 
          if ($i < $#data) {$html_out .= "\\n\" +\n";}
                       else {$html_out .= "\\n\" ,\n";}
          }
       else
         {if ($i < $#data){$html_out .= "\"$date $time, $vars\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $vars\\n\" ,\n";}
         }
       }
    $html_out =~ s/\+\n$/,\n/;
    $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: $arg{'fill'},
               title: '$arg{"title"}',
               ylabel: '$arg{"ylabel"}',
               legend: 'always',
               labelsDiv: document.getElementById("div_label"),
               yAxisLabelWidth: 60
               // valueRange: [500,0]
              }          // options
             ); // end-Dygraph
    for (var i=1; i <= $#field_names-$ind_offset; i++)
          {
EndHTML
           if ($arg{'allchecked'}) {$html_out .= "           g.setVisibility(i, true);\n";}
                              else {$html_out .= "           g.setVisibility(i, false);\n";}
      $html_out .= "}\n";
      if ($arg{'radio'}) 
         {#set initial ylabel
	  my($varname,$ylabel) = split('_',$field_names[$ind_offset]);
          if ($ylabel eq "") {$ylabel = $varname;}
	  $html_out .= "g.updateOptions({ ylabel: '$ylabel'});\n";
	  }
$html_out .= <<"EndHTML";
      setStatus();


      function setStatus() {
EndHTML
        $html_out .= "return;" if (! $DEBUG);
my(%ylabel); $ylabel{1} = "DegC"; $ylabel{2} = "TEST"; $ylabel{id} = "Cute";
$html_out .= <<"EndHTML";
        document.getElementById("visibility").innerHTML =
          g.visibility().toString();
      }

      function set_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = true;
           g.setVisibility(i, true);
	   }
        setStatus();
      }

      function clear_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = false;
           g.setVisibility(i, false);
	   }
        setStatus();
      }


      function change_checkbox(el) {
        g.setVisibility(parseInt(el.id), el.checked);
        setStatus();
      }

      function change_radio(el,num_buttons,label) {
        var id = parseInt(el.id);
        g.setVisibility(id, true);
        for (var i=0; i < num_buttons; i++)
          {if (i != id)
             {g.setVisibility(i, false);
              }
           }
         g.updateOptions({ ylabel: label});

        setStatus();
      }


            </script>
EndHTML

    return "$html_out";
}


