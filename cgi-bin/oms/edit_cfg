#!/usr/bin/perl -T
use strict;

$ENV{PATH} = "";

#!/bin/perl
#
# EVT_edit - Steve Lerner 9/95 (created/hacked from GB_edit)
#            Woods Hole Oceanographic Institution
#
#       Ported from SCN_edit
#
#       Description: Edits file via html form
#
#       Usage: DAQ_edit <f=> [ext=] [title=] [rows=] [cols=]
#                    where f is cfg filename to edit (w/o .cfg extension)
#                          ext is ext of filename
#                          title is a title used instead of filename
#                          rows,cols are sizes of textarea to edit
#
#	Output:  
#
# History:
#       Date       Who      Description
#       ----       ---      ---------------------------------------------------
#        9/95       SL      Create/hacked from GB_edit (elim most checking)
#        1/2005     SL      Copied to AlvinFG and changed daq-bin to fg-bin
#        7/2007     SL      Re-written for security purposes - added taint/strict
#        8/2007     SL      Updated for shipdata/rt-display, added username
#       02/01/2010  SL      Modified for OOI/CGO instrument display. Replaced
#                           f input w/ cfg input
#       09/16/2016  SL      Save a copy of the updated cfg file in prevCfgs dir
#	08/26/2016  JJP	    Updated path to Cfg files for new structure
###############################################################################

my(@pairs,$buffer);
my($pair,$key,$value);
my($cols,$Update,$fname,$cfg,$title_str);
my(%arg);
my(@contents);
my($invalid_user);
my($valid_user) = "cgoAdmin";
my($status_line);
my($edit_flag) = 1;

print "Content-Type: text/html\n\n";

#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
   {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
    @pairs = split(/&/, $buffer);
    } 
elsif ($ENV{'REQUEST_METHOD'} eq "GET")
   {@pairs = split(/&/, $ENV{'QUERY_STRING'});
    }
else {if ($#ARGV >= 0) {@pairs = @ARGV};
      }

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $arg{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /; 
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
#    $value =~ tr/\cM/\n/;
    $value =~ s/\cM\cJ /\n/g;
    $value =~ s/\cM\cJ/\n/g;
    $arg{$key} = $value;
    #print "ARG[$key] = $value\n";
}

print "<HTML>\n<HEAD>\n";

#Sanity check rows/cols
if ($arg{'rows'} eq "") {$arg{'rows'} = 40;}
elsif ($arg{'rows'} < 5) {$arg{'rows'} = 5;}
elsif ($arg{'rows'} > 80) {$arg{'rows'} = 80;}
if ($arg{'cols'} eq "") {$arg{'cols'} = 100;}
elsif ($arg{'cols'} < 10) {$arg{'cols'} = 10;}
elsif ($arg{'cols'} > 120) {$arg{'cols'} = 120;}

#Sanitize filename from arg{'f'}
$arg{'cfg'}=~s/[][&;`'\\"|*?~<>^(){}\$\n\r]//g;
$arg{'cfg'}=~s/\.\.//g;
$arg{'cfg'} =~ /(\w+)/;
$cfg = $1; #un-tainted

#print "Cfg=[$cfg]  arg{'cfg'}=$arg{'cfg'}\n";

####TEMP
$fname = "../../../platcon_configs/platform_cfgs/$cfg.cfg";

if ($arg{'UpdateButton'} eq "Update")
   {if ($arg{'username'} eq "$valid_user")
      {if (open(OUT,">$fname"))
        {chomp($arg{'contents'});
         print OUT "$arg{'contents'}\n";
         close(OUT);
         $status_line = "<font color=white>Configuration file $cfg - updated</font>\n";

         # Save a copy to prevCfgs save dir
         my $cfg_lc = $cfg; $cfg_lc =~ tr/A-Z/a-z/;
         my $saveDir = "../../../platcon_configs/platform_cfgs/prevCfgs/$cfg_lc";

         my $tag = &gmtimestamp_file(4);
         if (!-d "$saveDir") {mkdir "$saveDir";}
         `/bin/cp -p $fname $saveDir/$cfg_lc.cfg.$tag`;
         if (! -f "$saveDir/$cfg_lc.cfg.$tag")
           {$status_line = "<font color=white>Configuration file $cfg - updated,</font> <font color=orange>but unable to save backup copy to prevCfgs directory [$saveDir]</font>\n";
            }
         }
       else {#print "<font color=orange><h4>***Error: Unable to update file: $fname</h4></font>\n";
             print "<table hspace=30><td><font color=orange><h4>***Error: Unable to update configuration file [$cfg]</h4></font>\n";
             print "<font color=white>--> Check permissions</font><br><br>\n";
	     print "<A href=\"edit_cfg?cfg=$cfg&title=$arg{'title'}\"><font color=cyan>Return to edit</font></a>\n";
	     print "</table>\n";
             exit;
	     }
       }
    }
if ($arg{'title'} ne "")
     {$title_str = "$arg{'title'}\n";}
else {#$title_str = "Filename: $fname\n";
      $title_str = "<font color=white>Cfg File: $cfg</font>\n";
      }

if ($arg{'username'} eq "")
     {$invalid_user = "&nbsp <font color=orange><b>***username must be specified***</b></font>\n";}
elsif ($arg{'username'} ne "$valid_user")
     {$invalid_user = "&nbsp <font color=orange><b>***Invalid username</b> - try again<br></font>\n";}
else {$invalid_user = "";}	


#-------------------------------------------------------------


open(CFG,"$fname");
@contents = <CFG>;
close(CFG);

print "<table hspace=30><td>\n";
print "<FORM METHOD=\"POST\">\n";
#print "$title_str &nbsp&nbsp&nbsp Username: <input type=password name=username value=\"$arg{'username'}\" size=10> $invalid_user\n";
print "$title_str\n";
print "<P>\n";
print "<INPUT TYPE=\"HIDDEN\" NAME=\"title\" VALUE=\"$arg{'title'}\"></P>\n";
print "<INPUT TYPE=\"HIDDEN\" NAME=\"cfg\" VALUE=\"$arg{'cfg'}\"></P>\n";
print "<INPUT TYPE=\"HIDDEN\" NAME=\"rows\" VALUE=\"$arg{'rows'}\"></P>\n";
print "<INPUT TYPE=\"HIDDEN\" NAME=\"cols\" VALUE=\"$arg{'cols'}\"></P>\n";
print "<P>\n";
print "<TEXTAREA NAME=\"contents\" COLS=\"$arg{'cols'}\" ROWS=\"$arg{'rows'}\" WRAP=\"OFF\">@contents</TEXTAREA>";
print "<br>";
if ($edit_flag)
  {print "<table width=95%><td><font color=#eeeeee>Username: </font><input type=password name=username value=\"$arg{'username'}\" size=10> \n";
   print "       <td>$invalid_user $status_line\n";
   print "       <td><INPUT TYPE=\"SUBMIT\" NAME=\"UpdateButton\" VALUE=\"Update\"><INPUT TYPE=\"RESET\" NAME=\"Cancel\">\n";
   print "</table>\n";
   }
print "</FORM>\n";
print "</table>\n";
print "</center>";
print "</BODY>\n";
print "</HTML>\n";

#
# End of script

#
# Subroutines Follow
#

sub gmtimestamp_file {
        my($format) = $_[0]; #1-daily, 2-hourly, 3-hourly/minute, 4-hhmmss
        my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = gmtime;
        # zero fill hour,min,sec and month
        if ($sec < 10) {$sec = "0$sec";}
        if ($min < 10) {$min = "0$min";}
        if ($hour < 10) {$hour = "0$hour";}
        $mon += 1; #from gmtime defined 0-11, make it 1-12
        if ($mon < 10) {$mon = "0$mon";}
        if ($mday < 10) {$mday = "0$mday";}
        #make year 4-digits
        $year += 1900;
        if ($format == 1)    {return "$year$mon$mday";}
        elsif ($format == 2) {return "$year$mon$mday\_$hour" . "00";}
        elsif ($format == 3) {return "$year$mon$mday\_$hour$min";}
        elsif ($format == 4) {return "$year$mon$mday\_$hour$min$sec";}
        return "$year$mon$mday\_$hour$min";
}
