#!/usr/bin/perl
# plot_csv.pl -  Steven Lerner 10/2011
#
# Description: Quick script to plot csv data using dygraph javascript
#              interface (http://dygraphs.com). Accepts csv or space-separated data.
#              Requires first line of data file to contain field name list
#              and first two columns to contain date (yyyymmdd) and time (hh:mm:ss) 
#              for plotting time axis.
#
# Usaage: plot_csv <f=datafile> [cols=c1,c2...] [nrecs=] [skip=] [title=] [ylabel=] [xlabel=]
#                  [allchecked=0|1] [checkbox=0|1] [radio=0|1] [width=] [height=] [fill=]
#                  [points=0|1] [sample=0]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      10/05/2011  SL     Create
#      12/19/2011  SL     Fixed change_radio last button index bug. Updated to include 
#                         points, sample, xlabel
#      12/23/2014  SL     Updated to use cg_util.pl
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
##################################################################################
use strict;
require "../cg_util.pl";

my($DEBUG) = 0;
my %arg = &get_args(0);


# Set default args
$arg{'cols'}       = "";
$arg{'allchecked'} = 0;
$arg{'nrecs'}      = 2000;
$arg{'radio'}      = 1;
$arg{'checkbox'}   = 0;
$arg{'fileinfo'}   = 0;
$arg{'width'}      = 600;
$arg{'height'}     = 300;
$arg{'skip'}       = 1;
$arg{'max_recs'}   = 5000;
$arg{'fill'}       = "false";
$arg{'points'}     = "false";
$arg{'sample'}     = 1; #Automatically add sample# to first column if no date/time

#Sanitize filename from arg{'f'}
$arg{'f'}=~s/[][&;`'\\"|*?~<>^(){}\$\n\r]//g;
$arg{'f'}=~s/\.\.//g;

my($ddir, $file);
if ($arg{'d'} ne "")     {$DEBUG = 1;}
if ($arg{'ddir'} ne "")  {$ddir  = $arg{'ddir'};}
if ($arg{'f'} ne "")     {$file  = "$arg{'f'}";}
if ($arg{'nrecs'} < 10)  {$arg{'nrecs'} = 10;}
elsif ($arg{'nrecs'} > $arg{'max_recs'}) {$arg{'nrecs'} = $arg{'max_recs'};}
if ($arg{'skip'} < 1) {$arg{'skip'} = 1;}
if    ($arg{'fill'} eq "1") {$arg{'fill'} = "true";}
elsif ($arg{'fill'} eq "0") {$arg{'fill'} = "false";}
if    ($arg{'points'} eq "1") {$arg{'points'} = "true";}
elsif ($arg{'points'} eq "0") {$arg{'points'} = "false";}

if ($file eq "") {print "Usage: $0 <f=in_file> [title=] [ylabel=] [cols=c1,c2...] [allchecked=0|1] [checkbox=0|1] [radio=0|1] [width=] [height=] [fill=] [points=] [sample=]\n";
                  exit(-1);
                  }

if (!-f "$file") {print "Content-type: text/html\n\n";
                  print "File [$file] doesn't exist\n";
                  exit(-1);
                  }

    #Get some status about file
    my($wc_ret) = `/usr/bin/wc $file`;
    my($num_lines) = split(" ", $wc_ret);
    my($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat("$file");
    my($last_mod_time) = time - $mtime;
    my($tstr1) = &time2dsltime_str($mtime);
    my($tstr2) = &sec2hhmmss($last_mod_time);
    my($mbytes) = sprintf("%.2f",$size / 1000000);

    my($file_info);    
    if ($arg{'fileinfo'})
      {$file_info  = "<table bgcolor=#CCCCCC border=1>\n";
       $file_info .= "  <tr><td>Current File</td> <td>$file &nbsp <a href=$file>View</a></td></tr>\n";
       $file_info .= "  <tr><td>File Size</td>    <td>$size bytes &nbsp ($mbytes mb)</td></tr>\n";
       $file_info .= "  <tr><td>Number Lines</td>    <td>$num_lines</td></tr>\n";
       $file_info .= "  <tr><td>Last Updated</td> <td>$tstr1</td></tr>\n";
       $file_info .= "  <tr><td>Time Since Update</td><td>$tstr2</td></tr>\n";
       $file_info .= "</table>\n";
       }


    my(@field_names, @data, %ColList);
    @field_names = split(' ',`head -1 $file`);
    my(@field_name_subset);
    if ($arg{'cols'} ne "")
      {#only put specified cols into field_names
       my(@col_list) = split(',',$arg{'cols'});
       my($col);
       foreach $col (@col_list)
         {push(@field_name_subset,$field_names[$col-1]);
          my($ind) = $col-1;
          $ColList{$ind}++;
          }
       #@field_names = @new_list;
       }
    @data = `tail -$arg{'nrecs'} $file`;

print "Content-type: text/html\n\n";
print << "EndHTML";
   <!DOCTYPE html>
   <HTML>
   <HEAD><TITLE>Plot $arg{'title'}</TITLE>
         <!-- Plot CSV
              (c) WHOI - SL 2011 
         -->
         <STYLE TYPE="text/css">
         <!--
          TH{font-family: Arial; font-size: 10pt;}
          TD{font-family: Arial; font-size: 10pt;}
          -->
         </STYLE>
    
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9"> 
    <!--[if IE]><script src="/cg/excanvas.compiled.js"></script><![endif]-->

         <script type="text/javascript"
            src="/cg/dygraph-combined.js"></script>
         </head>
   </HEAD>
   <BODY vlink=blue><br>
EndHTML

    if ($arg{'fileinfo'})
      {print "$file_info\n<br>\n";
       print "<b>Last Record:</b>\n";
       print "<pre>@field_names\n$data[$#data]</pre>\n";
       }

#
# Generate Plots Here
#
my($html_plot) = &gen_csv_plot;
print "$html_plot";

print "</BODY>\n";
print "</HTML>\n";

exit;

#
# Subroutines Follow
#


sub sec2hhmmss {#takes total seconds and returns d hh:mm:ss
        my($sec) = $_[0];
        my($dy,$hr,$mn,$sc);
        $dy = int($sec/86400);
        $hr = int(($sec - ($dy*86400))/3600);
        $mn = int(($sec - ($dy*86400) - ($hr*3600))/60);
        $sc = $sec - ($dy*86400) - ($hr*3600) - ($mn*60);
        if ($hr < 10) {$hr = "0$hr";}
        if ($mn < 10) {$mn = "0$mn";}
        if ($sc < 10) {$sc = "0$sc";}

        if ($dy == 0) {return "$hr:$mn:$sc";}
                 else {return "$dy $hr:$mn:$sc";}
}

sub time2dsltime_str {#takes total seconds and returns yyyy/mm/dd hh:mm:ss.ss
   my($time) = $_[0];
   my($outstr);
   my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdat) = gmtime($time);
   $year += 1900; $mon++;
   $outstr = sprintf("%04d/%02d/%02d %02d:%02d:%02d",$year,$mon,$mday,$hour,$min,$sec);
   return "$outstr";

}


sub gen_csv_plot {#Uses global field_names and data and %ColList and %arg
   my($html_out) = "";
   my($i, $hdr_line, $line, $checked);
   my($visibility) = "";

   $html_out .= "<table><td><div id=\"div_g\" style=\"width:$arg{'width'}px; height:$arg{'height'}px;\"></div></td>\n";
   $html_out .= "       </tr><tr>\n";
   $html_out .= "       <td align=center><div id=\"div_label\" style=\"width:600px; font-size:1.0em; padding-top:5px;\"></div></td>\n";
   $html_out .= "</table>\n";
   $html_out .= "<p>Debug: g.visibility() = <span id=\"visibility\"></span></p>\n" if $DEBUG;
   $html_out .= "<br><form name=\"myform\" method=\"post\">\n";

   my($orig_num_fieldnames);
   if ($arg{'cols'} ne "") {$orig_num_fieldnames = $#field_names;
                            #@field_names = ('date', 'time');
                            #push(@field_names,@field_name_subset);
                            my($name); my($date_found) = 0;
			    foreach $name (@field_names)
			      {if ($name =~ /^date/i) {@field_names = ('date', 'time'); $date_found=1; last;}
			       }
			    if (!$date_found) {@field_names = @field_name_subset;}
                            else {push(@field_names,@field_name_subset);}
                            }

   my($no_date_time) = 0; #assume first two fields are date time
   my($sample_cnt)   = 0; #used if not date/time
   my($ind_offset)   = 2; #2 for date/time
   #print "Fieldnames=[@field_names] $#field_names subset=[@field_name_subset]\n"; exit;
   $hdr_line = "@field_names"; $hdr_line =~ s/(\s+)/ /sg; $hdr_line =~ s/,//g; $hdr_line =~ s/ /,/g; $hdr_line =~ s/,$//;
   $hdr_line =~ s/Date,Time_GMT/date_time/;
   $hdr_line =~ s/Date,Time/date_time/;
   $hdr_line =~ s/date,time/date_time/;
   $hdr_line =~ s/date_utc,time_utc/datetime_utc/;
   #print "Hdrline=[$hdr_line]\n"; exit;
   if ($hdr_line !~ /^date/i) {$hdr_line = "sample,$hdr_line" if ($arg{'sample'});
                               $no_date_time = 1;
			       if ($arg{'sample'}) {$ind_offset = 0;}
			                      else {$ind_offset = 1;}
                               }

   if ($arg{'checkbox'})
     {$html_out .= "<table cellpadding=8 border=0><td width=100 align=right valign=top><b>Plot:</b></td><td>\n";
      for ($i=0; $i<=$#field_names-$ind_offset; $i++)
        {if ($i==0 || $arg{'allchecked'}) {$checked = "CHECKED";}
                                     else {$checked = "";}
         $html_out .= "<input type=checkbox id=\"$i\" name=\"list\" onClick=\"change_checkbox(this)\" $checked>$field_names[$i+$ind_offset]</input> &nbsp \n";
         if (! (($i+1)%8)) {$html_out .= "<br>";}
        }
         $html_out .= "&nbsp <input type=button value=\"Clear All\" onClick=\"clear_checkbox(document.myform.list)\"></input>\n";
         $html_out .= "&nbsp <input type=button value=\"Set All\" onClick=\"set_checkbox(document.myform.list)\"></input><br>\n";
      $html_out .= "</td></table>\n";
     }

   if ($arg{'radio'})
     {$html_out .= "<table cellpadding=8 border=0><td width=100 align=right valign=top><b>Plot:</b></td><td>\n";
      for ($i=0; $i<=$#field_names-$ind_offset; $i++)
        {if ($i==0) {$checked = "CHECKED";}
               else {$checked = "";}
         my($varname,$ylabel) = split('_',$field_names[$i+$ind_offset]);
         if ($ylabel eq "") {$ylabel = $varname;}
         $html_out .= "<input type=radio id=\"$i\" name=\"plot\" onClick=\"change_radio(this,$#field_names,'$ylabel')\" $checked>$varname &nbsp </input>\n";
         if (! (($i+1)%8)) {$html_out .= "<br>";}
        }
      $html_out .= "</td></table>\n";
      }
  $html_out .= "</form>\n";

if (0) {
   for ($i=0; $i<$#field_names-1; $i++)
      {if ($i==0) {$visibility  = "visibility: [true, ";}
             else {$visibility .= "false, ";}
       }
   $visibility =~ s/, $/\],/; #replace last comma with closing bracket and comma
}


   $html_out .= << "EndHTML";
    <script type="text/javascript">
      g = new Dygraph(
            document.getElementById("div_g"),
EndHTML
     $html_out .= "\"$hdr_line\\n\" +\n";

    for ($i=1;$i<=$#data;$i+=$arg{'skip'})
      {$line = $data[$i];
       next if $line =~/^#/; #skip comments
       chomp($line);
       if ($line !~ /,/)
          {$line =~ s/(\s+)/ /sg; #elim multiple spaces
           $line =~ s/^(\s+)//;   #elim leading spaces
           $line =~ s/ /, /g;
           $line =~ s/, $//; #elim last comma
           }
       my($date,$time,$vars);
       if (! $no_date_time)
         {($date,$time,$vars) = split(', ',$line,3);
          if (length($time) > 8) {$time = substr($time,0,8);} #want hh:mm:ss (ie; elim .nnn sec)
          next if (length($date) < 8);
	  }
       else {$vars = $line;}
       if ($arg{'cols'} ne "")
         {#only print out selected columns
          if ($no_date_time)
	    {$sample_cnt += $arg{'skip'};
	     if ($arg{'sample'}) {$html_out .= "\"$sample_cnt, ";}
	                    else {$html_out .= "\"";}
             my($j);
             my(@vlist) = split(',',$vars);
             for ($j=0;$j<=$orig_num_fieldnames;$j++)
               {next if !defined $ColList{$j};
                #print "COLLIST($j) = $ColList{$j}  vars=[$vars]<br>";
		$html_out .= "$vlist[$j], ";
                }
             chop($html_out); chop($html_out); 
             if ($i < $#data) {$html_out .= "\\n\" +\n";}
                         else {$html_out .= "\\n\" ,\n";}
	     }
          else
	    {$html_out .= "\"$date $time, ";
             my($j);
             my(@vlist) = split(',',$vars);
             for ($j=0;$j<=$orig_num_fieldnames;$j++)
               {next if !defined $ColList{$j};
                $html_out .= "$vlist[$j-2], ";
                }
             chop($html_out); chop($html_out); 
             if ($i < $#data) {$html_out .= "\\n\" +\n";}
                         else {$html_out .= "\\n\" ,\n";}
             }
          }
       else
        {if ($no_date_time)
              {$sample_cnt += $arg{'skip'};
               if ($i < $#data){$html_out .= "\"$sample_cnt, $line\\n\" +\n";}
                          else {$html_out .= "\"$sample_cnt, $line\\n\" ,\n";}
               }
         else {if ($i < $#data){$html_out .= "\"$date $time, $vars\\n\" +\n";}
                          else {$html_out .= "\"$date $time, $vars\\n\" ,\n";}
               }
         }
       }
    $html_out =~ s/\+\n$/,\n/;
    $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: $arg{'points'},
               fillGraph: $arg{'fill'},
               title: '$arg{"title"}',
               ylabel: '$arg{"ylabel"}',
               xlabel: '$arg{"xlabel"}',
               legend: 'always',
               labelsDiv: document.getElementById("div_label"),
               yAxisLabelWidth: 60
               // valueRange: [500,0]
              }          // options
             ); // end-Dygraph
    for (var i=1; i <= $#field_names-$ind_offset; i++)
          {
EndHTML
           if ($arg{'allchecked'}) {$html_out .= "           g.setVisibility(i, true);\n";}
                              else {$html_out .= "           g.setVisibility(i, false);\n";}
      $html_out .= "}\n";
      if ($arg{'radio'}) 
         {#set initial ylabel
	  my($varname,$ylabel) = split('_',$field_names[2]);
          if ($ylabel eq "") {$ylabel = $varname;}
	  $html_out .= "g.updateOptions({ ylabel: '$ylabel'});\n";
	  }
$html_out .= <<"EndHTML";
      setStatus();


      function setStatus() {
EndHTML
        $html_out .= "return;" if (! $DEBUG);
my(%ylabel); $ylabel{1} = "DegC"; $ylabel{2} = "TEST"; $ylabel{id} = "Cute";
$html_out .= <<"EndHTML";
        document.getElementById("visibility").innerHTML =
          g.visibility().toString();
      }

      function set_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = true;
           g.setVisibility(i, true);
	   }
        setStatus();
      }

      function clear_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = false;
           g.setVisibility(i, false);
	   }
        setStatus();
      }


      function change_checkbox(el) {
        g.setVisibility(parseInt(el.id), el.checked);
        setStatus();
      }

      function change_radio(el,num_buttons,label) {
        var id = parseInt(el.id);
        g.setVisibility(id, true);
        for (var i=0; i <= num_buttons; i++)
          {if (i != id)
             {g.setVisibility(i, false);
              }
           }
         g.updateOptions({ ylabel: label});

        setStatus();
      }


            </script>
EndHTML

    return "$html_out";
}


