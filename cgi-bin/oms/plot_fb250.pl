#!/usr/bin/perl
# plot_fb250.pl -  Quick script to generate rt cpm plot
#
# Usage: plot_fb250.pl pid= deploy= type= [date=]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      05/13/2015  SL     Create
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
##################################################################################
use strict;
require "../cg_util.pl";

my $pid    = "pid_not_specified";
my $deploy = "deploy_not_specified";

my %arg = &get_args(0);

if ($arg{'pid'} ne "")    {$pid = $arg{'pid'};}
if ($arg{'deploy'} ne "") {$deploy = $arg{'deploy'};}

my $ddir = "/webdata/cgsn/data/raw/$pid/$deploy/cg_data/fb250";
my $day; #yyyymmdd 
if ($arg{'date'} ne "") {$day = $arg{'date'};}
                   else {$day = gmtimestamp_filename(0);}

if (! -d "$ddir") {print "$ddir does not exist\n";
                   exit(-1);
                   };

my (@flist, @data);
if ($arg{'date'} eq "last_n_days")
    {#grep through last-n fb250 files
     my $last_n_days = 8*10; #note that these are hourly and run typically every 3-hours
     @flist = `cd $ddir; /bin/ls -1t *.fb250.log | head -$last_n_days`;
     }
else {@flist = `cd $ddir; /bin/ls -1 $day*.fb250.log`;
      }

foreach my $f (sort @flist)
     {push(@data, `cd $ddir; grep -hi UP $f`); #only use stats when link is UP
      #push(@data, `cat $ddir/$f`); 
      }

my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>
<table><td>
       <div id="div_g" style="width:800px; height:300px;"></div></td>
       </tr><tr>
       <td align=center><div id="div_label" style="width:600px; font-size:1.0em; padding-top:5px;"></div></td>
</table>
<br><form name="myform" method="post">
<table cellpadding=8 border=0><td width=100 align=right valign=middle><b>Plot:</b></td><td>
   <input type=radio id="0" name="plot" onClick="change_radio(this,9,'snr')" CHECKED>SNR &nbsp </input>
   <input type=radio id="1" name="plot" onClick="change_radio(this,9,'degC')" >Temp &nbsp </input>
   <input type=radio id="2" name="plot" onClick="change_radio(this,9,'nsat')" >NSat &nbsp </input>
<br></td></table>
</form>

<script type="text/javascript">

  g = new Dygraph(
    document.getElementById("div_g"),
EndHTML
    $html_out .= "\"datetime, snr, temp, nsat\\n\" +\n";
    my ($i, $line);
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        #Sample: 2015/05/13, 06:02:11, UP, 65.5, 18, 39.9406, -70.8766, 15/05/13, 06:01:18, 7, 7;36;5;0;6;0, 
        #        7;262.0;1537.070;1540.730;5;143.5;1537.485;1540.825;6;24.9;1537.920;1541.115
        my ($date, $time, $state, $snr, $temp, $lat, $lon, $gps_d, $gps_t, $nsat) = split(', ', $line);
        if ($i < $#data-1){$html_out .= "\"$date $time, $snr, $temp, $nsat\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $snr, $temp, $nsat\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
            { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: true,
               fillGraph: false,
               title: 'FB250 Statistics',
               ylabel: '',
               xlabel: '',
               legend: 'always',
               labelsDiv: document.getElementById("div_label"),
               yAxisLabelWidth: 60
               // valueRange: [500,0]
              }          // options
           ); // end-Dygraph

    for (var i=1; i <= 9-2; i++)
          {
           g.setVisibility(i, false);
}
g.updateOptions({ ylabel: 'SNR'});
      setStatus();


      function setStatus() {
return;        document.getElementById("visibility").innerHTML =
          g.visibility().toString();
      }

      function set_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = true;
           g.setVisibility(i, true);
	   }
        setStatus();
      }

      function clear_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = false;
           g.setVisibility(i, false);
	   }
        setStatus();
      }


      function change_checkbox(el) {
        g.setVisibility(parseInt(el.id), el.checked);
        setStatus();
      }

      function change_radio(el,num_buttons,label) {
        var id = parseInt(el.id);
        g.setVisibility(id, true);
        for (var i=0; i <= num_buttons; i++)
          {if (i != id)
             {g.setVisibility(i, false);
              }
           }
         g.updateOptions({ ylabel: label});

        setStatus();
      }

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

