#!/usr/bin/perl
#
# view_inst.pl - Steven Lerner 10/29/2003
#       Description: Generate html given HTML TEMPLATE file and att/val data
#
#       Usage: view_inst.pl <if=data_file> <t=html_template> [o=outfile] [-d]
#                           [serialNum= depth=]
#	Output: html file
#
# History:
#       Date          Who      Description
#       ---------     ---      ------------------------------------------------
#       10/29/2003    SL       Create
#       02/01/2010    SL       Added CardLineCnt to guarantee order
#                              Renamed from eic2html to view_inst and added
#                              extra inst platform specific args
###############################################################################

require "getopts.pl";
require "flush.pl";

$VERSION = "v1.0";

$DEBUG = 0;
print "Content-type: text/html\n\n";

#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'REQUEST_METHOD'} eq "GET")
     {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }

elsif ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = $value    <br>\n";
}


if ($arg{'if'} eq "" || $arg{'t'} eq "")
   {print STDERR "\n$0 - $VERSION\n";
    print "Usage: $0 <-f data_file> <-t html_template> [-o outfile] [-d]\n";
    print <<"EndUsage";
       where -f data file containing comma-separated data or attr=val pairs
             -t html template file (containing fields: $Card{'attr'})
             -o output html file [default: data_file.html]

EndUsage
    exit;
    }

#
# Setup args
#
$DataFile     = "$arg{'if'}";
$TemplateFile = "$arg{'t'}";
if (!defined ($arg{'o'})) {$outfile = "";}
                     else {$outfile = "$arg{'o'}";} 
if ($arg{'d'}) {$DEBUG = 1;}


#
#make sure TemplateFile and DataFile exist
#
if (! -f "$TemplateFile")
        {print "TemplateFile \'$TemplateFile\' does *not* exist - exiting\n";
	 exit;
         }
if (!open(DAT,"$DataFile"))
        {#print "***Error, unable to open DataFile \'$DataFile\' - exiting\n";
	 print "<table hspace=30><td><font color=white>No Information Available for $arg{'inst'}</font></td></table>";
	 exit;
         }


#
#Read eic text datafile (att=val pairs) and save into Card hash array
#
$linecnt = 1;
while ($line = <DAT>)
   {chomp($line);
    #If line begins with a comment '#', store as Card{'comment.n'}
    if ($line =~ /^#/)
       {$ncomments++;
        $Card{"Comment.$ncomments"} = "$line";
        next;
	}
    ($attr,$val) = split("=",$line,2);
    $Card{"$attr"} = "$val";
    #Added CardLineCnt to keep some items inorder (ie; wng limits & specs)
    $CardLineCnt{"$linecnt.$attr"} = "$val";
    $linecnt++;
    print "Card{\"$attr\"} = $val\n" if $DEBUG;
    }
close(DAT);
$Card{"NumComments"} = $ncomments;

#Add platform specific info here
$Card{'INS.serialNum'} = $arg{'serialNum'};
$Card{'INS.depth'} = $arg{'depth'};


#
#Write out html to either file or to stdout
#
if ($outfile ne "")
  {open (OUT_FD, ">$outfile"); $OUT_FD = OUT_FD;}
else {$OUT_FD = STDOUT;}
do "$TemplateFile";
      
exit;


