#!/usr/bin/perl
# rtsum_csm - Generates status summary info for coastal surface moorings 
#             and outputs either att/val pairs or html
#
# Extracts the following information:
#    Date Time Lat Lon
#    WndSpd  WndDir  SW
#    BP Humid AirTemp  SST 
#    PSC: Volt Curr Watt Bat_Charge Bat_Net
#         Connected
#         PV: WT:
#
#    LoadCell:
#    NSIF: temp cnd press sal
#
#    cpm: uptime
#    dcl11: uptime
#    dcl12: uptime
#    dcl16: uptime
#    Eflags: mpic psc1,psc2
#
# Usage: rtsum_csm pid= deploy= [o=html|html2]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      11/18/2014  SL     Create for cp01cnsm
#      12/23/2014  SL     Updated to use cg_util.pl
#      02/02/2015  SL     Support Deployment dir
#      07/27/2015  SL     Added optional mdf arg
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
#      08/14/2016  JJP	  Use ScriptAlias /cmn-bin in path for view_* files
##################################################################################
use strict;
require "../cg_util.pl";
require "get_lastcomm_times.pl";

#Load cgsn platforms metadata file into hash array
my($metadata_dir)  = "/webdata/cgsn/data/mdf";
my($metadata_file) = "cgsn_platforms_mdf.txt";

my($BLACK)  = "#000000";
my($GREEN)  = "#008800";
my($RED)    = "#aa0000";
my($YELLOW) = "#aa4400";

my %arg = &get_args(0);
my $pid    = "pid_not_specified";
my $deploy = "deploy_not_specified";
if ($arg{'pid'}    ne "") {$pid    = $arg{'pid'};}
if ($arg{'deploy'} ne "") {$deploy = $arg{'deploy'};}
if ($arg{'mdf'}    ne "") {$metadata_file = $arg{'mdf'};}

my($ddir) = "/webdata/cgsn/data/raw/$pid/$deploy/cg_data";
if (! -d "$ddir") {die "DataDir [$ddir] does not exist\n";}

my(%Cfg) = &load_cfg("$metadata_dir/$metadata_file");

#Get MPIC, Pwrsys, GPS info from cpm_status file
my(%CPM)   = &load_cfg("$ddir/syslog/cpm_status.txt");
my(%CPM2)  = &load_cfg("$ddir/cpm2/syslog/cpm_status.txt");
my(%CPM3)  = &load_cfg("$ddir/cpm3/syslog/cpm_status.txt");
my(%DCL11) = &load_cfg("$ddir/dcl11/syslog/dcl_status.txt");
my(%DCL12) = &load_cfg("$ddir/dcl12/syslog/dcl_status.txt");
my(%DCL26) = &load_cfg("$ddir/dcl26/syslog/dcl_status.txt");
my(%DCL27) = &load_cfg("$ddir/dcl27/syslog/dcl_status.txt");
my(%DCL35) = &load_cfg("$ddir/dcl35/syslog/dcl_status.txt");
my(%DCL37) = &load_cfg("$ddir/dcl37/syslog/dcl_status.txt");
my($wake_l, $wake_code, $ir_l, $ir_s, $ir_v, $ir_i, $ir_e, $fwwf_l, $fwwf_s) = split(' ',$CPM{'MPIC.hotel'});

#setup Colors for Eflags
my(%Color);
if ($CPM{'MPIC.eflag'} eq "00000000")     {$Color{'MPIC.eflag'}      = "$BLACK";}
                                     else {$Color{'MPIC.eflag'}      = "$RED";}
if ($CPM2{'MPIC.eflag'} eq "00000000")    {$Color{'cpm2.MPIC.eflag'} = "$BLACK";}
                                     else {$Color{'cpm2.MPIC.eflag'} = "$RED";}
if ($CPM3{'MPIC.eflag'} eq "00000000")    {$Color{'cpm3.MPIC.eflag'} = "$BLACK";}
                                     else {$Color{'cpm3.MPIC.eflag'} = "$RED";}
if ($CPM{'Pwrsys.eflag1'} eq "00000000")  {$Color{'Pwrsys.eflag1'} = "$BLACK";}
                                     else {$Color{'Pwrsys.eflag1'} = "$RED";}
if ($CPM{'Pwrsys.eflag2'} eq "00000000")  {$Color{'Pwrsys.eflag2'} = "$BLACK";}
                                     else {$Color{'Pwrsys.eflag2'} = "$RED";}
if ($CPM{'Pwrsys.eflag3'} eq "00000000")  {$Color{'Pwrsys.eflag3'} = "$BLACK";}
                                     else {$Color{'Pwrsys.eflag3'} = "$RED";}
if ($DCL11{'MPIC.eflag'} eq "00000000")   {$Color{'dcl11.eflag'}   = "$BLACK";}
                                     else {$Color{'dcl11.eflag'}   = "$RED";}
if ($DCL12{'MPIC.eflag'} eq "00000000")   {$Color{'dcl12.eflag'}   = "$BLACK";}
                                     else {$Color{'dcl12.eflag'}   = "$RED";}
if ($DCL26{'MPIC.eflag'} eq "00000000")   {$Color{'dcl26.eflag'}   = "$BLACK";}
                                     else {$Color{'dcl26.eflag'}   = "$RED";}
if ($DCL27{'MPIC.eflag'} eq "00000000")   {$Color{'dcl27.eflag'}   = "$BLACK";}
                                     else {$Color{'dcl27.eflag'}   = "$RED";}
if ($DCL35{'MPIC.eflag'} eq "00000000")   {$Color{'dcl35.eflag'}   = "$BLACK";}
                                     else {$Color{'dcl35.eflag'}   = "$RED";}
if ($DCL37{'MPIC.eflag'} eq "00000000")   {$Color{'dcl37.eflag'}   = "$BLACK";}
                                     else {$Color{'dcl37.eflag'}   = "$RED";}
if ($CPM3{'MPEA.eflag1'} eq "00000000")  {$Color{'MPEA.eflag1'}    = "$BLACK";}
                                     else {$Color{'MPEA.eflag1'}   = "$RED";}
if ($CPM3{'MPEA.eflag2'} eq "00000000")  {$Color{'MPEA.eflag2'}    = "$BLACK";}
                                     else {$Color{'MPEA.eflag2'}   = "$RED";}

#setup Colors for Time Synchronization
my $ntp_thresh = 2; #ms
if (abs($CPM{'NTP.offset'})  > $ntp_thresh || $CPM{'NTP.offset'} eq "")  {$Color{'cpm.ntp.offset'} = "$RED";}
                                                               else {$Color{'cpm.ntp.offset'} = "$GREEN";}
if (abs($CPM2{'NTP.offset'}) > $ntp_thresh || $CPM2{'NTP.offset'} eq "") {$Color{'cpm2.ntp.offset'} = "$RED";}
                                                               else {$Color{'cpm2.ntp.offset'} = "$GREEN";}
if (abs($CPM3{'NTP.offset'}) > $ntp_thresh || $CPM3{'NTP.offset'} eq "") {$Color{'cpm3.ntp.offset'} = "$RED";}
                                                               else {$Color{'cpm3.ntp.offset'} = "$GREEN";}
if (abs($DCL11{'NTP.offset'}) > $ntp_thresh || $DCL11{'NTP.offset'} eq "") {$Color{'dcl11.ntp.offset'} = "$RED";}
                                                                 else {$Color{'dcl11.ntp.offset'} = "$GREEN";}
if (abs($DCL12{'NTP.offset'}) > $ntp_thresh || $DCL12{'NTP.offset'} eq "") {$Color{'dcl12.ntp.offset'} = "$RED";}
                                                                 else {$Color{'dcl12.ntp.offset'} = "$GREEN";}
if (abs($DCL26{'NTP.offset'}) > $ntp_thresh || $DCL26{'NTP.offset'} eq "") {$Color{'dcl26.ntp.offset'} = "$RED";}
                                                                 else {$Color{'dcl26.ntp.offset'} = "$GREEN";}
if (abs($DCL27{'NTP.offset'}) > $ntp_thresh || $DCL27{'NTP.offset'} eq "") {$Color{'dcl27.ntp.offset'} = "$RED";}
                                                                 else {$Color{'dcl27.ntp.offset'} = "$GREEN";}
if (abs($DCL35{'NTP.offset'}) > $ntp_thresh || $DCL35{'NTP.offset'} eq "") {$Color{'dcl35.ntp.offset'} = "$RED";}
                                                                 else {$Color{'dcl35.ntp.offset'} = "$GREEN";}
if (abs($DCL37{'NTP.offset'}) > $ntp_thresh || $DCL37{'NTP.offset'} eq "") {$Color{'dcl37.ntp.offset'} = "$RED";}
                                                                  else {$Color{'dcl37.ntp.offset'} = "$GREEN";}

# Load last communications times
my %LastComms = &get_lastcomm_times($pid, $deploy);


#parse our PSC
my($Connected)   = "";
my($bt_array_t)  = "";
my($bt_array_v)  = "";
my($bt_array_ma) = "";
my($pv_array_ma) = "";
my($wt_array_ma) = "";
my($bt_net_ma)   = 0;
my($bt_net_str)  = "";
my($i);
for ($i=1;$i<=4;$i++)
   {if ($CPM{"Pwrsys.pv$i"} =~ /^1/) {$Connected .= "pv$i ";}
    my($cn,$v,$ma) = split(' ',$CPM{"Pwrsys.pv$i"});
    $pv_array_ma .= " $ma";
    my($bt,$bv,$bma) = split(' ',$CPM{"Pwrsys.b$i"});
    $bt_array_t  .= " $bt";
    $bt_array_v  .= " $bv";
    $bt_array_ma .= " $bma";
    $bt_net_ma += $bma;
    }
if ($bt_net_ma > 0) {$bt_net_str = "Discharging";}
               else {$bt_net_str = "Charging";}
for ($i=1;$i<=2;$i++)
   {if ($CPM{"Pwrsys.wt$i"} =~ /^1/) {$Connected .= "wt$i ";}
    my($cn,$v,$ma) = split(' ',$CPM{"Pwrsys.wt$i"});
    $wt_array_ma .= " $ma";
    }
for ($i=1;$i<=2;$i++)
   {if ($CPM{"Pwrsys.fc$i"} =~ /^1/) {$Connected .= "fc$i ";}
    }
my($psc_watts) = sprintf("%.2f",$CPM{'Pwrsys.main_v'} * ($CPM{'Pwrsys.main_c'}/1000));
my($last_psc_update_str) = &sec2hhmmss($CPM{'Pwrsys.last_update'});


#Get Hydrogen Sensor info
my($hyd1_file)     = `cd $ddir/dcl11/hyd1 && ls -1r *.hyd1.log | head -1`;   chomp($hyd1_file);
my($last_hyd1_rec) = `grep -v 'hyd1' $ddir/dcl11/hyd1/$hyd1_file | tail -1`; chomp($last_hyd1_rec);
my($hyd2_file)     = `cd $ddir/dcl12/hyd2 && ls -1r *.hyd2.log | head -1`;   chomp($hyd2_file);
my($last_hyd2_rec) = `grep -v 'hyd2' $ddir/dcl12/hyd2/$hyd2_file | tail -1`; chomp($last_hyd2_rec);
#2014/09/03 00:01:34.169 *-00000.13 -0.130000 %
my($h1_d,$h1_t,$raw,$hyd1_lel) = split(' ',$last_hyd1_rec);
my($h1_secs_since_update) = time() - &dsltime2sec("$h1_d $h1_t");
if ($h1_secs_since_update > 3600*12) {$Color{'h1.timestamp'} = "$RED";}
                                else {$Color{'h1.timestamp'} = "$BLACK";}
my($h2_d,$h2_t,$raw,$hyd2_lel) = split(' ',$last_hyd2_rec);
my($h2_secs_since_update) = time() - &dsltime2sec("$h2_d $h2_t");
if ($h2_secs_since_update > 3600*12) {$Color{'h2.timestamp'} = "$RED";}
                                else {$Color{'h2.timestamp'} = "$BLACK";}
#Note: CRITICAL Level is 25% LEL
if ($hyd1_lel < 10)   {$Color{'hyd1_lel'} = "$GREEN";}
                 else {$Color{'hyd1_lel'} = "$RED";}
if ($hyd2_lel < 10)   {$Color{'hyd2_lel'} = "$GREEN";}
                 else {$Color{'hyd2_lel'} = "$RED";}

if ($CPM{'Pwrsys.main_v'} < 24 || $CPM{'Pwrsys.main_v'} > 30)  
     {$Color{'Pwrsys.main_v'} = "$RED";}
else {$Color{'Pwrsys.main_v'} = "$GREEN";}

if ($CPM{'Pwrsys.b_chg'} < 70 || $CPM{'Pwrsys.b_chg'} > 100) 
     {$Color{'Pwrsys.b_chg'} = "$RED";}
else {$Color{'Pwrsys.b_chg'} = "$GREEN";}

#Get METBK1 info
#2011/10/01 06:03:48.641 1003.43  80.833  23.900  424.7   41.93  24.600  5.2634    1.2    2.57    6.93  5.2634 12.50
my $metbk1_label = "Metbk";
my($metbk_file, $last_metbk_rec);
my($metbk_dir)  = "$ddir/dcl11/metbk";
if (-d "$metbk_dir") {($metbk_file)  = `cd $metbk_dir && ls -1r *.metbk.log | head -1`;}
               else  {$metbk_dir =  "$ddir/dcl11/metbk1";
                      ($metbk_file)  = `cd $metbk_dir && ls -1r *.metbk1.log | head -1`;
                       $metbk1_label = "Metbk1";
                       }
my($last_metbk_rec) = `tail -1 $metbk_dir/$metbk_file`; chomp($last_metbk_rec);
$last_metbk_rec = &check_metbk_rec("$last_metbk_rec");
my($metbk1_d,$metbk1_t,$bp1,$rel_h1,$air_tmp1,$lw1,$prc1,$sst1,$cond1,$sw1,$wnd_e1,$wnd_n1) = split(' ',$last_metbk_rec);
my($metbk1_secs_since_update)  = time() - &dsltime2sec("$metbk1_d $metbk1_t");
if ($metbk1_secs_since_update > 3600*12) {$Color{'metbk1.timestamp'} = "$RED";}
                                    else {$Color{'metbk1.timestamp'} = "$BLACK";}
my($wndspd1) = sprintf("%.2f",sqrt($wnd_e1 * $wnd_e1  + $wnd_n1 * $wnd_n1));
my($wnddir1) = sprintf("%05.1f &deg;",(atan2($wnd_e1,$wnd_n1) * 180.0/3.14159));
if ($wnddir1 < 0) {$wnddir1 += 360;}

#Get METBK2 info
#2011/10/01 06:03:48.641 1003.43  80.833  23.900  424.7   41.93  24.600  5.2634    1.2    2.57    6.93  5.2634 12.50
my($metbk2_dir)  = "$ddir/dcl12/metbk2";
my($metbk2_d,$metbk2_t,$bp2,$rel_h2,$air_tmp2,$lw2,$prc2,$sst2,$cond2,$sw2,$wnd_e2,$wnd_n2);
my($wndspd2, $wnddir2);
my($metbk2_available) = 0;
if (-d "$metbk2_dir")
  {my($metbk2_file)  = `cd $metbk2_dir && ls -1r *.metbk2.log | head -1`;
   my($last_metbk2_rec) = `tail -1 $metbk2_dir/$metbk2_file`; chomp($last_metbk2_rec);
   $last_metbk2_rec = &check_metbk_rec("$last_metbk2_rec");
   ($metbk2_d,$metbk2_t,$bp2,$rel_h2,$air_tmp2,$lw2,$prc2,$sst2,$cond2,$sw2,$wnd_e2,$wnd_n2) = split(' ',$last_metbk2_rec);
   my($metbk2_secs_since_update)  = time() - &dsltime2sec("$metbk2_d $metbk2_t");
   if ($metbk2_secs_since_update > 3600*12) {$Color{'metbk2.timestamp'} = "$RED";}
                                       else {$Color{'metbk2.timestamp'} = "$BLACK";}
   $wndspd2 = sprintf("%.2f",sqrt($wnd_e2 * $wnd_e2  + $wnd_n2 * $wnd_n2));
   $wnddir2 = sprintf("%05.1f &deg;",(atan2($wnd_e2,$wnd_n2) * 180.0/3.14159));
   if ($wnddir2 < 0) {$wnddir2 += 360;}
   $metbk2_available = 1;
   }
else {#set wndspd and sw to metbk1 values for wt/pv status
      $wndspd2 = $wndspd1; $sw2 = $sw1;
      }

# Put some smarts into evaluating wt/pv array and fwwf status
my ($wt_status, $pv_status, $fwwf_status);
$Color{'psc.wt'} = "$BLACK";
$Color{'psc.pv'} = "$BLACK";
if ($Connected =~ /wt1|wt2/)
     {$wt_status = "Wind Turbines connected";
      my($wt1_ma,$wt2_ma) = split(' ',$wt_array_ma);
      if ($wt1_ma == 0 && $wt2_ma == 0) {$wt_status .= ", generating no power";}
      elsif ($wt1_ma < 100 && $wt2_ma < 100) {$wt_status .= ", generating negligable power";}
      elsif ($wt1_ma < 100) {$wt_status .= ", wt1 generating negligable power";}
      elsif ($wt2_ma < 100) {$wt_status .= ", wt2 generating negligable power";}
      elsif ($wt1_ma > 1000 && $wt2_ma > 1000) {$wt_status .= ", generating reasonable power";}
      elsif ($wt1_ma > 1000) {$wt_status .= ", wt1 generating reasonable power";}
      elsif ($wt2_ma > 1000) {$wt_status .= ", wt2 generating reasonable power";}
      else {$wt_status .= ", generating some power";}
      if ($wndspd1 < 5 && $wndspd2 < 5) {$wt_status .= ", but little wind";} 
                                   else {$wt_status .= ", and there's wind!";}
      #Setup color for WT label
      if    ($wt1_ma < 100 && $wt2_ma < 100 && ($wndspd1 > 5 || $wndspd2 > 5)) {$Color{'psc.wt'} = "$RED";}
      elsif ($wt1_ma > 1000 && $wt2_ma > 1000) {$Color{'psc.wt'} = "$GREEN";}
      }
else {$wt_status = "One or more Wind Turbines Disconnected";}

if ($Connected =~ /pv1|pv2|pv3|pv4/)
     {$pv_status = "PV arrays connected";
      my($pv1_ma,$pv2_ma,$pv3_ma,$pv4_ma) = split(' ',$pv_array_ma);
      if ($pv1_ma < 100 && $pv2_ma < 100 && $pv3_ma < 100 && $pv4_ma < 100) {$pv_status .= ", generating negligable power";}
      elsif (($pv1_ma + $pv2_ma + $pv3_ma + $pv4_ma)/4  < 400) {$pv_status .= ", generating some power";}
      else {$pv_status .= ", generating reasonable power";}
      if    ($sw1 < 200 && $sw2 < 200 ) {$pv_status .= ", but little sunlight";} 
      elsif ($sw1 < 400 && $sw2 < 400 ) {$pv_status .= ", and some sunlight";} 
                                   else {$pv_status .= ", and there's sunshine!";}
      #Setup color for PV label
      if    ($pv1_ma < 100 && $pv2_ma < 100 && $pv3_ma < 100 && $pv4_ma < 100 &&
            ($sw1 > 200 || $sw2 > 200)) {$Color{'psc.pv'} = "$RED";}
      elsif (($pv1_ma + $pv2_ma + $pv3_ma + $pv4_ma)/4  > 600) {$Color{'psc.pv'} = "$GREEN";}
      }
else {$pv_status = "One or more PV Arrays Disconnected";}

if ($fwwf_s  > 0) {$fwwf_status .= "Can conserve power by turning freewave and wifi off";}

#Get NSIF ctdbp info
my($nsif_ctd_file)     = `cd $ddir/dcl27/ctdbp1 && ls -1r *.ctdbp1.log | head -1`; chomp($nsif_ctd_file);
my($last_nsif_ctd_rec) = `tail -2 $ddir/dcl27/ctdbp1/$nsif_ctd_file | head -1`; chomp($last_nsif_ctd_rec);
#2014/08/12 00:00:27.362 # 25.1805,  0.01247,    0.407,   0.0626, 1497.263, 12 Aug 2014 00:00:22,  -2.9526,  6.8,   2.1
$last_nsif_ctd_rec =~ s/,//g;
my($nsif_ctd_d,$nsif_ctd_t,$p,$temp,$cond,$depth,$sal) = split(" ",$last_nsif_ctd_rec);
if (length("$nsif_ctd_d $nsif_ctd_t") == 23)
  {my($nsif_ctd_secs_since_update)  = time() - &dsltime2sec("$nsif_ctd_d $nsif_ctd_t");
   if ($nsif_ctd_secs_since_update > 3600*12) {$Color{'nsif_ctd.timestamp'} = "$RED";}
                                         else {$Color{'nsif_ctd.timestamp'} = "$BLACK";}
  }
else {$Color{'nsif_ctd.timestamp'} = "orange";}

my($secs_since_update)  = time() - &dsltime2sec($CPM{'GPS.timestamp'});
my($last_update_str)    = &sec2hhmmss($secs_since_update);
my($last_update_color)  = $secs_since_update < 12*3600 ? "$GREEN" : "$RED";

$CPM{'last_update_str'}   = &sec2hhmmss(time() - $CPM{'Platform.utime'});
$CPM2{'last_update_str'}  = &sec2hhmmss(time() - $CPM2{'Platform.utime'});
$CPM3{'last_update_str'}  = &sec2hhmmss(time() - $CPM3{'Platform.utime'});
$DCL11{'last_update_str'} = &sec2hhmmss(time() - $DCL11{'Platform.utime'});
$DCL12{'last_update_str'} = &sec2hhmmss(time() - $DCL12{'Platform.utime'});
$DCL26{'last_update_str'} = &sec2hhmmss(time() - $DCL26{'Platform.utime'});
$DCL27{'last_update_str'} = &sec2hhmmss(time() - $DCL27{'Platform.utime'});
$DCL35{'last_update_str'} = &sec2hhmmss(time() - $DCL35{'Platform.utime'});
$DCL37{'last_update_str'} = &sec2hhmmss(time() - $DCL37{'Platform.utime'});

#Setup colors - green if < 12 hours, red otherwise
if ((time() - $CPM{'Platform.utime'}) < 3600*12) {$CPM{"last_update.color"} = "$GREEN";}
                                            else {$CPM{"last_update.color"} = "$RED";}
if ((time() - $CPM2{'Platform.utime'}) < 3600*12) {$CPM2{"last_update.color"} = "$GREEN";}
                                             else {$CPM2{"last_update.color"} = "$RED";}
if ((time() - $CPM3{'Platform.utime'}) < 3600*12) {$CPM3{"last_update.color"} = "$GREEN";}
                                             else {$CPM3{"last_update.color"} = "$RED";}
if ((time() - $DCL11{'Platform.utime'}) < 3600*12) {$DCL11{"last_update.color"} = "$GREEN";}
                                              else {$DCL11{"last_update.color"} = "$RED";}
if ((time() - $DCL12{'Platform.utime'}) < 3600*12) {$DCL12{"last_update.color"} = "$GREEN";}
                                              else {$DCL12{"last_update.color"} = "$RED";}
if ((time() - $DCL26{'Platform.utime'}) < 3600*12) {$DCL26{"last_update.color"} = "$GREEN";}
                                              else {$DCL26{"last_update.color"} = "$RED";}
if ((time() - $DCL27{'Platform.utime'}) < 3600*12) {$DCL27{"last_update.color"} = "$GREEN";}
                                              else {$DCL27{"last_update.color"} = "$RED";}
if ((time() - $DCL35{'Platform.utime'}) < 3600*12) {$DCL35{"last_update.color"} = "$GREEN";}
                                              else {$DCL35{"last_update.color"} = "$RED";}
if ((time() - $DCL37{'Platform.utime'}) < 3600*12) {$DCL37{"last_update.color"} = "$GREEN";}
                                              else {$DCL37{"last_update.color"} = "$RED";}

my $pid_caps = $pid; $pid_caps =~ tr/a-z/A-Z/;
if ($arg{'o'} eq "html")
     {#default format for rt google-map display
      print <<"EndHTML";
<table border=1 cellpadding=3 cellspacing=0 width=100%><td><b>Time:</b> $CPM{'GPS.timestamp'} &nbsp <b>Lat:</b> $CPM{'GPS.lat'} &nbsp <b>Lon:</b> $CPM{'GPS.lon'}
<b>WndSpd:</b> $wndspd1 m/s &nbsp <b>WndDir:</b> $wnddir1 &nbsp <b>SW:</b> $sw1 w/m^2</b> &nbsp <b>RelH:</b> $rel_h1 %
<b>AirTemp:</b> $air_tmp1 degC &nbsp <b>SST:</b> $sst1 degC &nbsp <b>BP:</b> $bp1 mbar 
<b>NSIF</b>  <b>Temp:</b> $temp degC &nbsp <b>Cond:</b> $cond S/m &nbsp <b>Press:</b> $depth dbar &nbsp <b>Sal:</b> $sal psu</td></tr><tr><td><b>Buoy Battery:</b> $CPM{'Pwrsys.main_v'} v &nbsp  $CPM{'Pwrsys.main_c'} mA &nbsp  $psc_watts watts
<b>BatCharge:</b> $CPM{'Pwrsys.b_chg'} % &nbsp <b>BatNet:</b> $bt_net_ma mA &nbsp <b>$bt_net_str</b>
<b>Connected:</b> $Connected  
<b>PV(mA):</b> $pv_array_ma &nbsp <b>WT(mA):</b> $wt_array_ma
<b>Hyd1: </b> <font color=$Color{'hyd1_lel'}>$hyd1_lel % </font> &nbsp as of &nbsp $h1_d $h1_t
<b>Hyd2: </b> <font color=$Color{'hyd2_lel'}>$hyd2_lel % </font> &nbsp as of &nbsp $h2_d $h2_t
</tr><tr><td><b><font color=$last_update_color>Time Since Update</font>:</b> $last_update_str &nbsp&nbsp <b>Uptime:</b> $CPM{'CPU.uptime'} &nbsp 
<b>Eflags:</b> cpm: <font color=$Color{'MPIC.eflag'}>$CPM{'MPIC.eflag'}</font> &nbsp <b>psc:</b> <font color=$Color{'Pwrsys.eflag1'}>$CPM{'Pwrsys.eflag1'}</font>,<font color=$Color{'Pwrsys.eflag2'}>$CPM{'Pwrsys.eflag2'}</font>
<b>Eflags:</b> &nbsp dcl11: <font color=$Color{'dcl11.eflag'}>$DCL11{'MPIC.eflag'}</font> &nbsp dcl12: <font color=$Color{'dcl12.eflag'}>$DCL12{'MPIC.eflag'}</font> &nbsp dcl26: <font color=$Color{'dcl26.eflag'}>$DCL26{'MPIC.eflag'}</font></td></table>
EndHTML
     }

elsif ($arg{'o'} eq "html2")
     {#Alternate format for platform_summary display called by view_syslog 
      print <<"EndHTML";
<table bgcolor=#006688 cellspacing=5 cellpadding=5 xxwidth=100% border=1>
   <td valign=top align=center><img src="/oms-html/images/$pid\_small.png"></td>
   <td valign=top> <table border=0 cellpadding=0 cellspacing=0 width=100% bgcolor=#006688>
     <tr><td>

<table cellspacing=0 cellpadding=0><tr><td colspan=2><font color=white><b>PlatformID:</b> $pid &nbsp <b>DeployID:</b> $deploy &nbsp <b>Timestamp:</b> $CPM{'Platform.time'} </td></tr>
       <tr><td>&nbsp&nbsp&nbsp</td><td>
  <table cellspacing=0 cellpadding=1 border=1 bgcolor=#cccccc><td>
<b>GPS Time:</b> $CPM{'GPS.timestamp'} &nbsp <b>Lat:</b> $CPM{'GPS.lat'} &nbsp <b>Lon:</b> $CPM{'GPS.lon'} <br>
    <table><td valign=top>$metbk1_label &nbsp </td>
           <td valign=top><b>WndSpd:</b> $wndspd1 m/s &nbsp <b>WndDir:</b> $wnddir1 &nbsp <b>SW:</b> $sw1 w/m^2</b> &nbsp <b>RelH:</b> $rel_h1 % &nbsp <b>BP:</b> $bp1 mbar <br>
           <b>AirTemp:</b> $air_tmp1 degC &nbsp <b>SST:</b> $sst1 degC &nbsp as of &nbsp <font color=$Color{'metbk1.timestamp'}>$metbk1_d $metbk1_t</font>
    </table>
EndHTML
    if ($metbk2_available)
      {print "<table><td valign=top>Metbk2 &nbsp </td>\n";
       print "    <td valign=top><b>WndSpd:</b> $wndspd2 m/s &nbsp <b>WndDir:</b> $wnddir2 &nbsp <b>SW:</b> $sw2 w/m^2</b> &nbsp <b>RelH:</b> $rel_h2 % &nbsp <b>BP:</b> $bp2 mbar <br>\n";
       print"    <b>AirTemp:</b> $air_tmp2 degC &nbsp <b>SST:</b> $sst2 degC &nbsp as of &nbsp <font color=$Color{'metbk1.timestamp'}>$metbk2_d $metbk2_t</font>\n";
       print "</table>\n";
       }
print <<"EndHTML";

    <b>NSIF</b>  <b>Temp:</b> $temp degC &nbsp <b>Cond:</b> $cond S/m &nbsp <b>Press:</b> $depth dbar &nbsp <b>Sal:</b> $sal psu &nbsp as of &nbsp <font color=$Color{'nsif_ctd.timestamp'}>$nsif_ctd_d $nsif_ctd_t</font>
</tr>
  </table>
 </table>

<br>
<table><td valign=top>
  <table cellspacing=0 cellpadding=0><tr><td colspan=2><font color=white><b>Power System:</b></font></td></tr>
         <tr><td>&nbsp&nbsp&nbsp</td><td>
    <table cellspacing=0 cellpadding=3 border=1 bgcolor=#cccccc>
       <tr><td><b>Buoy Battery:</b> <font color=$Color{'Pwrsys.main_v'}><b>$CPM{'Pwrsys.main_v'} v</b></font> &nbsp  $CPM{'Pwrsys.main_c'} mA &nbsp  $psc_watts watts &nbsp&nbsp LastUpdate: $last_psc_update_str<br>
        <b>BatCharge:</b> <font color=$Color{'Pwrsys.b_chg'}><b>$CPM{'Pwrsys.b_chg'} %</b></font> &nbsp <b>BatNet:</b> $bt_net_ma mA &nbsp <b>$bt_net_str</b> <br>
        <b>Connected:</b> $Connected   <br>
        <font color=$Color{'psc.pv'}><b>PV(mA):</font></b> $pv_array_ma &nbsp <font color=$Color{'psc.wt'}><b>WT(mA):</font></b> $wt_array_ma <br>
        <b>Hyd1: </b> <font color=$Color{'hyd1_lel'}><b>$hyd1_lel %</b></font> &nbsp as of &nbsp <font color=$Color{'h1.timestamp'}>$h1_d $h1_t</font> <br>
        <b>Hyd2: </b> <font color=$Color{'hyd2_lel'}><b>$hyd2_lel %</b></font> &nbsp as of &nbsp <font color=$Color{'h2.timestamp'}>$h2_d $h2_t</font> <br>
       </tr>
    </table>
   </table>
   <td valign=top><table><tr><td><font color=#dddddd><u><i>Power Generation/Consumption  Notes</i></u></font></td></tr>
              <tr><td><font color=#dddddd>1) Hydrogen levels are $hyd1_lel%, $hyd2_lel% - CRITICAL Level is 25% LEL</font></td></tr>
              <tr><td><font color=#dddddd>2) $wt_status</font></td></tr>
              <tr><td><font color=#dddddd>3) $pv_status</font></td></tr>
EndHTML
              if ($fwwf_status ne "") {print "<tr><td><font color=#dddddd>4) $fwwf_status</font></td></tr>\n";}
   print <<"EndHTML";
       </table>
</table>


<br>
<table><td valign=top>
  <table cellspacing=0 cellpadding=0><tr><td colspan=2><table><td><b><font color=white>Time Since FB250 Update:</font></b> <td bgcolor=$last_update_color><font color=white>$last_update_str </font></table>
         <tr><td>&nbsp&nbsp&nbsp</td><td>
    <table cellspacing=0 cellpadding=1 border=1 bgcolor=#cccccc>
        <tr><td><b><font color=$CPM{"last_update.color"}>CPM1:</font></b> <td align=right>$CPM{'last_update_str'}  <td>&nbsp <b><font color=$DCL11{"last_update.color"}>DCL11:</font></b> <td align=right>$DCL11{'last_update_str'} <td>&nbsp <b><font color=$DCL12{"last_update.color"}>DCL12:</font></b> <td align=right>$DCL12{'last_update_str'} </td></tr>
        <tr><td><b><font color=$CPM2{"last_update.color"}>CPM2:</font></b> <td align=right>$CPM2{'last_update_str'} <td>&nbsp <b><font color=$DCL26{"last_update.color"}>DCL26:</font></b> <td align=right>$DCL26{'last_update_str'} <td>&nbsp <b><font color=$DCL27{"last_update.color"}>DCL27:</font></b> <td align=right>$DCL27{'last_update_str'}</td><tr>
        <tr><td><b><font color=$CPM3{"last_update.color"}>CPM3:</font></b> <td align=right>$CPM3{'last_update_str'} <td>&nbsp <b><font color=$DCL35{"last_update.color"}>DCL35:</font></b> <td align=right>$DCL35{'last_update_str'} <td>&nbsp <b><font color=$DCL37{"last_update.color"}>DCL37:</font></b> <td align=right>$DCL37{'last_update_str'}</td></tr>
    </table>
   </table>

<td valign=top rowspan=2>
  <table cellspacing=0 cellpadding=0><tr><td colspan=2>&nbsp&nbsp&nbsp <font color=white><b>Time Since Last Communications:</b></font></td></tr>
         <tr><td>&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp</td><td>
    <table cellspacing=0 cellpadding=1 border=1 bgcolor=#cccccc>
         <tr><td><b><font color=$LastComms{'fb1.color'}>FB1:</font></b>  </td> <td align=right>$LastComms{'fb1.last_update_str'}  &nbsp </td>
             <td><b><font color=$LastComms{'sbd1.color'}>SBD1:</font></b></td> <td align=right>$LastComms{'sbd1.last_update_str'} &nbsp </td>
             <td><b><font color=$LastComms{'xeos1.color'}>Xeos1:</font></b></td> <td align=right>$LastComms{'xeos1.last_update_str'} &nbsp </td><tr>
         <tr><td><b><font color=$LastComms{'fb2.color'}>FB2:</font></b>  </td> <td align=right>$LastComms{'fb2.last_update_str'}  &nbsp </td>
             <td><b><font color=$LastComms{'sbd2.color'}>SBD2:</font></b></td> <td align=right>$LastComms{'sbd2.last_update_str'} &nbsp </td>
             <td><b><font color=$LastComms{'xeos2.color'}>Xeos2:</font></b></td> <td align=right>$LastComms{'xeos2.last_update_str'} &nbsp </td><tr>
         <tr><td><b><font color=$LastComms{'irid.color'}>Irid:</font></b></td> <td align=right>$LastComms{'irid.last_update_str'} &nbsp </td>
             <td><b><font color=$LastComms{'acom.color'}>Acom:</font></b></td> <td align=right>$LastComms{'acom.last_update_str'} &nbsp </td>
             <td><b><font color=$LastComms{'xeos3.color'}>Xeos3:</font></b></td> <td align=right>$LastComms{'xeos3.last_update_str'} &nbsp </td><tr>
     </table>
   </table>
 </table>


<br>
<table><td valign=top>
  <table cellspacing=0 cellpadding=0><tr><td colspan=2><font color=white><b>CE CPU Uptimes:</b></font></td></tr>
         <tr><td>&nbsp&nbsp&nbsp</td><td>
    <table cellspacing=0 cellpadding=1 border=1 bgcolor=#cccccc>
         <tr><td><b>CPM1:</b> <td align=right>$CPM{'CPU.uptime'}  <td>&nbsp <b>DCL11:</b> <td align=right>$DCL11{'CPU.uptime'} <td>&nbsp <b>DCL12:</b> <td align=right>$DCL12{'CPU.uptime'} </td><tr>
         <tr><td><b>CPM2:</b> <td align=right>$CPM2{'CPU.uptime'} <td>&nbsp <b>DCL26:</b> <td align=right>$DCL26{'CPU.uptime'} <td>&nbsp <b>DCL27:</b> <td align=right>$DCL27{'CPU.uptime'}</td><tr>
         <tr><td><b>CPM3:</b> <td align=right>$CPM3{'CPU.uptime'} <td>&nbsp <b>DCL35:</b> <td align=right>$DCL35{'CPU.uptime'} <td>&nbsp <b>DCL37:</b> <td align=right>$DCL37{'CPU.uptime'}</td></tr>
     </table>
   </table>
<td valign=top>&nbsp&nbsp <font color=white><b>Time Synchronization (offset ms):</b></font><br>
  <table cellspacing=0 cellpadding=0>
         <tr><td>&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp</td><td>
    <table cellspacing=0 cellpadding=1 border=1 bgcolor=#cccccc>
        <tr><td><font color=$Color{'cpm.ntp.offset'}><b>CPM1:</b></font> <td align=right>$CPM{'NTP.offset'}  <td>&nbsp <font color=$Color{'dcl11.ntp.offset'}><b>DCL11:</b></font> <td align=right>$DCL11{'NTP.offset'} <td>&nbsp <font color=$Color{'dcl12.ntp.offset'}><b>DCL12:</b></font> <td align=right>$DCL12{'NTP.offset'} </td></tr>
         <tr><td><font color=$Color{'cpm2.ntp.offset'}><b>CPM2:</b></font> <td align=right>$CPM2{'NTP.offset'} <td>&nbsp <font color=$Color{'dcl26.ntp.offset'}><b>DCL26:</b></font> <td align=right>$DCL26{'NTP.offset'} <td>&nbsp <font color=$Color{'dcl27.ntp.offset'}><b>DCL27:</b></font> <td align=right>$DCL27{'NTP.offset'}</td><tr>
         <tr><td><font color=$Color{'cpm3.ntp.offset'}><b>CPM3:</b></font> <td align=right>$CPM3{'NTP.offset'} <td>&nbsp <font color=$Color{'dcl35.ntp.offset'}><b>DCL35:</b></font> <td align=right>$DCL35{'NTP.offset'} <td>&nbsp <font color=$Color{'dcl37.ntp.offset'}><b>DCL37:</b></font> <td align=right>$DCL37{'NTP.offset'}</td></tr>
    </table>
   </table>
</table>


<br>
<table cellspacing=0 cellpadding=0><tr><td colspan=2><font color=white><b>Error Flags:</b></font></td></tr>
       <tr><td>&nbsp&nbsp&nbsp</td><td>
  <table cellspacing=0 cellpadding=1 border=1 bgcolor=#cccccc>
          <td>
    <table cellspacing=0 cellpadding=1>
       <tr><td><b>PSC:</b></td>
           <td colspan=5><a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_psc_err?e=$CPM{'Pwrsys.eflag1'},$CPM{'Pwrsys.eflag2'},$CPM{'Pwrsys.eflag3'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'Pwrsys.eflag1'}>$CPM{'Pwrsys.eflag1'}</font>,<font color=$Color{'Pwrsys.eflag2'}>$CPM{'Pwrsys.eflag2'}</font>,<font color=$Color{'Pwrsys.eflag3'}>$CPM{'Pwrsys.eflag3'}</font></a> 

       &nbsp&nbsp <b>MPEA:</b> 
           <a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpea_err?e=$CPM3{'MPEA.eflag1'},$CPM3{'MPEA.eflag2'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'MPEA.eflag1'}>$CPM3{'MPEA.eflag1'}</font>,<font color=$Color{'MPEA.eflag2'}>$CPM3{'MPEA.eflag2'}</font></a></td>

       <tr><td><b>CPM1:</b> 
           <td><a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=cpm&e=$CPM{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'MPIC.eflag'}>$CPM{'MPIC.eflag'}</font></a> &nbsp
           <td><b>DCL11:</b> <a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=dcl&e=$DCL11{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'dcl11.eflag'}>$DCL11{'MPIC.eflag'}</font></a> &nbsp
           <td><b>DCL12:</b> <a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=dcl&e=$DCL12{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'dcl12.eflag'}>$DCL12{'MPIC.eflag'}</font></a> &nbsp
        <tr>
           <td><b>CPM2:</b> 
           <td><a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=cpm&e=$CPM2{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'cpm2.MPIC.eflag'}>$CPM2{'MPIC.eflag'}</font></a> &nbsp
           <td><b>DCL26:</b> <a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=dcl&e=$DCL26{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'dcl26.eflag'}>$DCL26{'MPIC.eflag'}</font></a> &nbsp
           <td><b>DCL27:</b> <a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=dcl&e=$DCL27{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'dcl27.eflag'}>$DCL27{'MPIC.eflag'}</font></a>
        <tr>
           <td><b>CPM3:</b> 
           <td><a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=cpm&e=$CPM3{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'cpm3.MPIC.eflag'}>$CPM3{'MPIC.eflag'}</font></a> &nbsp
           <td><b>DCL35:</b> <a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=dcl&e=$DCL35{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'dcl35.eflag'}>$DCL35{'MPIC.eflag'}</font></a> &nbsp
           <td><b>DCL37:</b> <a href=\"javascript: void(0)\" onclick=\"window.open('/cmn-bin/view_mpic_err?t=dcl&e=$DCL37{'MPIC.eflag'}','faultTable','width=600, height=600'); return false;\"><font color=$Color{'dcl37.eflag'}>$DCL37{'MPIC.eflag'}</font></a>
      </td>
    </table>
 </table>
 </table>
<tr><td><br><table width=75%><td valign=top width=5%><font color=white><b>Synopsis:</b> </font></td>
    <td bgcolor=#cccccc valign=top><font color=black>$Cfg{"$pid.$deploy.synopsis"}</font></td></tr></table>
</table>
</table>
EndHTML
     }
else {print <<"EndTXT";
PlatformID: $pid_caps  DeployID: $deploy  LastUpdate: $last_update_str  Timestamp: $CPM{'Platform.time'}
GPS Time: $CPM{'GPS.timestamp'}  Lat: $CPM{'GPS.lat'}  Lon: $CPM{'GPS.lon'}
WndSpd: $wndspd1 m/s  WndDir: $wnddir1  SW: $sw1 w/m^2  
AirTemp: $air_tmp1 degC  SST: $sst1 degC  BP: $bp1 mbar   RelH: $rel_h1 %  
NSIF  Temp: $temp degC   Cond: $cond S/m  Press: $depth dbar  Sal: $sal psu
Buoy Battery: $CPM{'Pwrsys.main_v'} v  $CPM{'Pwrsys.main_c'} mA   $psc_watts watts
BatCharge: $CPM{'Pwrsys.b_chg'} %  BatNet: $bt_net_ma mA  $bt_net_str
Connected: $Connected  PV(ma): $pv_array_ma  WT(ma): $wt_array_ma
Hyd1: $hyd1_lel %  as of  $h1_d $h1_t
Hyd2: $hyd2_lel %  as of  $h2_d $h2_t
Time Since Update: $last_update_str   Uptime: $CPM{'CPU.uptime'}  
Eflags: cpm: $CPM{'MPIC.eflag'}  psc: $CPM{'Pwrsys.eflag1'},$CPM{'Pwrsys.eflag2'}
Eflags: dcl11: $DCL11{'MPIC.eflag'}  dcl12: $DCL12{'MPIC.eflag'}  dcl26: $DCL26{'MPIC.eflag'}
EndTXT
     }

exit;

#
# Subroutines Follow
#
sub check_metbk_rec {my $metbk_rec = $_[0];
       #Check if record contains error flag
       if ($metbk_rec =~/\[metbk/i || $metbk_rec =~ /BAD_DATA/)
         {#If correct number of fields is present, strip-off [metbkN:DLOGPN]: error message field
          $metbk_rec =~ s/]:/]: /; #sometimes there's no space before barometric pressure
          my ($date,$time,@vars) = split(' ', $metbk_rec);
          if ($#vars == 12)
            {$metbk_rec =~ s/\[([^\[\]]|(?0))*]://g; #strip-off [metbkN:DLOGPN]: error message field
             }
          }
       return $metbk_rec;
}
