#!/usr/bin/perl
#
#gen_mmp_psum2.pl - Generate mmp profile summary. Takes in McClane profile mmp 
#                   engineering E*.DAT files and generates CSV min/max/ave records 
#                   of start_time, end_time, duration, batC_ma, batV_V, press_dbar 
#                   for each profile.
#
# Usage: gen_mmp_psum2.pl [i=|d=|pid=] [force=1]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      12/07/2013  SL     Create from gen_mmp_psum.pl
#      08/19/2016  JJP	  Fixed the path to bin/mpp_unpack, it moved up one level 
##################################################################################
require "timelocal.pl";
use strict;

my($DEBUG) = 0;
my(%arg,$buffer,@pairs,$pair,$key,$value);
my($max_vars) = 100;
my($minmax_flag) = 1;
$arg{'force'} = 0;

#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'QUERY_STRING'} ne "")
 {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }
if ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = $value\n";
}

if ($arg{'i'} eq "" && $arg{'d'} eq "" && $arg{'pid'} eq "") 
     {print "Usage: $0 <i=in_file d=dir | pid=pid> [force=1]\n";
      exit(-1);
      }

if ($arg{'cols'} eq "") {$arg{'cols'} = "1,2,3,4,5";}

my($fd,$out_fd, @recs,@vars, @vmin, @vmax, @vave, @vtot, @vrcnt, $date, @dates, $date_time, $hour, $minute, $var);
my($mmp_eng_dir, @flist, $file, $outfile);
my($i,$j);
my(@col_list) = split(',',$arg{'cols'});

if ($arg{'i'} ne "") {$flist[0] = substr($arg{'i'},rindex($arg{'i'},"/")+1);
                      if ($arg{'i'} =~ /\//)
                           {$mmp_eng_dir = substr($arg{'i'},0,rindex($arg{'i'},"/"));}
                      else {$mmp_eng_dir = ".";}
                      }
elsif ($arg{'d'} ne "")
     {$mmp_eng_dir = "$arg{'d'}";
      if (! -d "$arg{'d'}") {die "***Unable to readdir [$arg{'d'}], exiting\n";}
      }
else {#use default dir: /webdata/cgsn/data/raw/PID/last_deploy/imm/mmp
      my($ddir) = "/webdata/cgsn/data/raw/$arg{'pid'}/last_deploy/imm/mmp";
      if (! -d "$ddir") {die "***Unable to readdir [$ddir], exiting\n";}
      $mmp_eng_dir = "$ddir";
      }

print "MMP_ENG_DIR=[$mmp_eng_dir]\n" if ($DEBUG);
if (!opendir($fd,"$mmp_eng_dir"))
       {print "***Unable to readdir [$mmp_eng_dir], exiting\n";
        exit;
        }
  if ($arg{'i'} eq "")
       {(@flist) = sort grep (/^E/,readdir($fd)); #want E*.DAT files
        }
  closedir($fd);

print "Flist=[@flist]\n" if ($DEBUG);

  #setup outfile name
  my $outdir  = "/webdata/cgsn/data/proc/$arg{'pid'}/mmp_eng";
  $outfile = "profile_summary.csv";

  my($start_profile) = -1;
  my($bypass_hdr) = 0;
  if ($arg{'force'} == 0 && -f "$outdir/$outfile")
    {#read existing outfile and get last profile
     my($last_rec) = `tail -1 $outdir/$outfile`;
     ($start_profile) = split(" ",$last_rec);
     $bypass_hdr = 1;
     open($out_fd,">>$outdir/$outfile") || die "***Unable to open [$outdir/$outfile], exiting\n";
     }
  else {#just create outfile
        open($out_fd,">$outdir/$outfile") || die "***Unable to open [$outdir/$outfile], exiting\n";
        }

  #initialize vmin, vmax, vtot - currently handle up to n-variables
  for ($i=0;$i<$max_vars;$i++) {$vmin[$i] = 9999999; $vmax[$i] = -9999999; $vtot[$i]=0;}

  my($rec_cnt) = 0;
  my($first_time) = 1;
  foreach $file (@flist)
   {next if ($file !~ /^E/); #make sure E*.dat
    #Extract profile number from filename Ennnnnn.DAT
    my($pnum); $pnum = substr($file,1,length($file)-5);
    open($fd,"../../bin/mmp_unpack stdout $mmp_eng_dir/$file|") || die "***Unable to open/unpack [$mmp_eng_dir/$file], exiting\n";
    (@recs) = <$fd>;
    close($fd);

    next if ($start_profile != -1 && $pnum <= $start_profile);
    print "Processing $arg{'pid'} - $file...\n";
    my $hdr_rec_num = 7;
    my $start_data_rec = $hdr_rec_num + 1;
    my $end_data_rec   = $#recs - 6;
    my($hdr_line) = "$recs[$hdr_rec_num]"; chomp($hdr_line);
    $hdr_line = "date time batC_ma batV_v press_dbar par_mv scatSig chlSig CDOMSig";
    if ($arg{'cols'} ne "")
      {#only put specified cols into hdr_line
       my($col);
       my($field_name_subset) = "";
       my(@hdr_fields) = split(' ',$hdr_line);
       foreach $col (@col_list)
         {$field_name_subset .= "$hdr_fields[$col-1] ";
          }
       $hdr_line = $field_name_subset;
       }

    #Update hdr_line to include min/max cols
    my(@hdr_fields) = split(' ',$hdr_line);
    my($new_hdr_line) = "profile sdate stime edate etime dur_min ";
    if ($minmax_flag)
       {for ($j=2;$j<=$#hdr_fields;$j++)
          {$new_hdr_line .= "min-$hdr_fields[$j]  max-$hdr_fields[$j]  ave-$hdr_fields[$j]  ";
           }
        $hdr_line = "$new_hdr_line";
	}

    if ($first_time && !$bypass_hdr) {$first_time = 0; print $out_fd "$hdr_line\n";}

    for ($i=$start_data_rec;$i<=$end_data_rec;$i++)
      {next if ($recs[$i] =~ /^#/); #skip comments
       next if ($recs[$i] =~ /\[/); #skip metadata
       next if ($recs[$i] =~ /BAD_DATA/); #skip BAD Data
       next if ($recs[$i] =~ /NaN NaN NaN NaN NaN NaN NaN/i);
       chomp($recs[$i]);

       $recs[$i] =~ s/(\s+)/ /gs; #remove extra spaces
       $recs[$i] =~ s/^ //; #remove leading space
       (@vars) = split(' ',$recs[$i]);
       $rec_cnt++;

       my ($mm,$dd,$yyyy) = split('/',$vars[0]);
       $date      = "$yyyy/$mm/$dd";
       $date_time = "$date $vars[1]";
       ($hour,$minute) = split(":",$vars[1]);
    
       for ($j=2;$j<=$#vars;$j++)
         {$vars[$j] =~ s/^\*//; #eliminate leading *
          next if ($vars[$j] =~ /nan/i);
	  next if ($vars[$j] == 0); #don't average in 0-value nor use for min/max
          if ($vars[$j] < $vmin[$j]) {$vmin[$j] = $vars[$j];}
          if ($vars[$j] > $vmax[$j]) {$vmax[$j] = $vars[$j];}
          $vtot[$j] += $vars[$j];
	  if ($vars[$j] != 0) {$vrcnt[$j]++;}
          }
       }
    
       #calculate the averages
       for ($j=2;$j<=$#vars;$j++) 
         {if ($vrcnt[$j] > 0) {$vave[$j] = $vtot[$j]/$vrcnt[$j];}
                         else {$vave[$j] = "nan";}
          }
             
       #print "$recs[$i]\n";

       
   #output profile summary
   my($temp_date,$temp_time,$dd,$mm,$yyyy);
   my($stime) = substr($recs[$start_data_rec],0,20);
   ($temp_date,$temp_time) = split(' ',$stime);
   ($mm,$dd,$yyyy) = split('/',$temp_date);
   $temp_date = "$yyyy/$mm/$dd";
   $stime = "$temp_date $temp_time";
   my($etime) = substr($recs[$end_data_rec],0,20);
   ($temp_date,$temp_time) = split(' ',$etime);
   ($mm,$dd,$yyyy) = split('/',$temp_date);
   $temp_date = "$yyyy/$mm/$dd";
   $etime = "$temp_date $temp_time";
   print "START TIME = [$stime]\n";
   print "END TIME   = [$etime]\n";

   my($dur_sec)   = &dsltime2sec($etime) - &dsltime2sec($stime);
   my($dur_min) = sprintf("%.3f",$dur_sec/60.0);
   print $out_fd "$pnum $stime $etime $dur_min ";
   if ($arg{'cols'} eq "")
               {for ($j=2;$j<$#vars+1;$j++) 
                  {print $out_fd "$vmin[$j] $vmax[$j] " if $minmax_flag;
                   printf $out_fd "%.6f  ", $vave[$j];
                   }
               }
   else
              {#output only selected columns
               for ($j=2;$j<=$#col_list;$j++)
                 {print $out_fd "$vmin[$col_list[$j]-1] $vmax[$col_list[$j]-1] " if $minmax_flag;
                  printf $out_fd "%.6f  ", $vave[$col_list[$j]-1];
                  }
               }
   print $out_fd "\n";




   #clear stats
   $rec_cnt = 0;
   for ($j=0;$j<$max_vars;$j++) {$vmin[$j] = 9999999; $vmax[$j] = -9999999; $vtot[$j]=0; $vrcnt[$j] = 0;}

    } #end for-each file

   close($out_fd);



exit;

#
# Subroutines Follow
#
sub dsltime2sec {#takes yyyy/mm/dd hh:mm:ss and returns total seconds
   my($dsltime) = $_[0];
   my($date_str,$time_str);
   my($sec,$min,$hour,$mday,$mon,$year);
   ($date_str,$time_str) = split(' ',$dsltime);
   ($year,$mon,$mday) = split('/',$date_str); $mon--;
   ($hour,$min,$sec) = split(':',$time_str);
   return(timegm($sec,$min,$hour,$mday,$mon,$year));
}
