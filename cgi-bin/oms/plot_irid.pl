#!/usr/bin/perl
# plot_irid.pl -  Quick script to generate rt cpm plot
#
# Usage: plot_irid.pl pid= deploy= [limit=] [date=]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      04/29/2015  SL     Create
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
#      08/14/2016  JJP	  Use ScriptAlias /cmn-bin in path for view_syslog
#      08/19/2016  JJP	  Removed /cmn-bin from path for view_syslog, as it has
#			  moved back to cgi-bin/oms
##################################################################################
use strict;
require "../cg_util.pl";

my $pid    = "pid_not_specified";
my $deploy = "deploy_not_specified";
my $limit  = 3000;

my %arg = &get_args(0);

if ($arg{'pid'}    ne "") {$pid    = $arg{'pid'};}
if ($arg{'deploy'} ne "") {$deploy = $arg{'deploy'};}
if ($arg{'limit'}  ne "") {$limit  = $arg{'limit'};}

my @irid_data  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" C_IRID sent \" extract=6,8,10,12,14,16,18,20 label=sent_files,recv_files,tx_bytes,rx_bytes,txRate_BPS,rxRate_BPS,ltime_sec,ctime_sec date=$arg{'date'}`; 


my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<table><td>
       <div id="div_g" style="width:800px; height:300px;"></div></td>
       </tr><tr>
       <td align=center><div id="div_label" style="width:600px; font-size:1.0em; padding-top:5px;"></div></td>
</table>
<br><form name="myform" method="post">
<table cellpadding=8 border=0><td width=100 align=right valign=middle><b>Plot:</b></td><td>
   <input type=radio id="0" name="plot" onClick="change_radio(this,9,'files')" CHECKED>sent &nbsp </input>
   <input type=radio id="1" name="plot" onClick="change_radio(this,9,'files')" >recv &nbsp </input>
   <input type=radio id="2" name="plot" onClick="change_radio(this,9,'bytes')" >tx &nbsp </input>
   <input type=radio id="3" name="plot" onClick="change_radio(this,9,'bytes')" >rx &nbsp </input>
   <input type=radio id="4" name="plot" onClick="change_radio(this,9,'BPS')" >txRate &nbsp </input>
   <input type=radio id="5" name="plot" onClick="change_radio(this,9,'BPS')" >rxRate &nbsp </input>
   <input type=radio id="6" name="plot" onClick="change_radio(this,9,'sec')" >ltime &nbsp </input>
   <input type=radio id="7" name="plot" onClick="change_radio(this,9,'sec')" >ctime &nbsp </input>
<br></td></table>
</form>

<script type="text/javascript">

  g = new Dygraph(
    document.getElementById("div_g"),
EndHTML
    my ($line, $i);
    for ($i=0;$i<$#irid_data;$i++)
       {$line = $irid_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#irid_data-1){$html_out .= "\"$line\\n\" +\n";}
                          else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
            { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: true,
               fillGraph: false,
               title: 'Iridium Statistics',
               ylabel: '',
               xlabel: '',
               legend: 'always',
               labelsDiv: document.getElementById("div_label"),
               yAxisLabelWidth: 60
               // valueRange: [500,0]
              }          // options
           ); // end-Dygraph

    for (var i=1; i <= 9-2; i++)
          {
           g.setVisibility(i, false);
}
g.updateOptions({ ylabel: 'files'});
      setStatus();


      function setStatus() {
return;        document.getElementById("visibility").innerHTML =
          g.visibility().toString();
      }

      function set_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = true;
           g.setVisibility(i, true);
	   }
        setStatus();
      }

      function clear_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = false;
           g.setVisibility(i, false);
	   }
        setStatus();
      }


      function change_checkbox(el) {
        g.setVisibility(parseInt(el.id), el.checked);
        setStatus();
      }

      function change_radio(el,num_buttons,label) {
        var id = parseInt(el.id);
        g.setVisibility(id, true);
        for (var i=0; i <= num_buttons; i++)
          {if (i != id)
             {g.setVisibility(i, false);
              }
           }
         g.updateOptions({ ylabel: label});

        setStatus();
      }

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

