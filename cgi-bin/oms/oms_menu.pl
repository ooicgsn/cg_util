#!/usr/bin/perl
###############################################################################
#
# oms_menu.pl - SL 11/2011
#
# Description: Displays OOI/CGSN platform/deployment menu bar
#
# Usage: oms_menu.pl
#
# History:
#       Date       Who     Description
#       ----       ---     ----------------------------------------------------
#     10/17/2011   SL      Create
#     10/17/2012   SL      Removed references to sealion. CGO platform
#                          and pwr model relocated under /webdata/cgsn/omc
#     10/18/2012   SL      Updated links to use oms instead of omc. Relocated
#                          data dir to /webdata/cgsn/data. Display status based
#                          on PID.Type
#     02/02/2015   SL      Addd cg_util.pl, misc clean-up, and updated to display 
#                          based on array type instead of by year - use oms_menu_mdf.txt
#     07/20/2015   SL      Added optional omc arg (whoi|osu). Removed OOI Related Links that
#                          are no-longer working or applicable.
#     07/26/2016   CE      Removed references to UCSD; Replaced UCSD logo with Rutgers logo
#     08/09/2016   JJP	   Correct path to cg_util.pl for restructure in Bitbucket
#     08/14/2016   JJP	   Changed ScriptAlias from /cgo/platforms-bin to 
#			   /oms-bin
#     08/19/2016  JJP	   Removed /cmn-bin from path for view_syslog, as it has
#			   moved back to cgi-bin/oms
#     10/29/2016  JJP	   Removed Status & Data link under Test Moorings -> EOMTST 
#                          Updated version to 2.0
###############################################################################
require "../cg_util.pl";
use strict;

my $VERSION = "v2.0-20161029";
my $DEBUG   = 0;
my $metadata_dir  = "/webdata/cgsn/data/mdf";
my $oms_menu_mdf  = "oms_menu_mdf.txt";
my $cgsn_platforms_mdf = "cgsn_platforms_mdf.txt";
my $omc = "";
my $omc_str = "";

print "Content-type: text/html\n\n";

my %arg = &get_args(0);
if (defined $arg{'omc'}) 
    {$omc = "$arg{'omc'}";
     $oms_menu_mdf       =~ s/txt$/$omc.txt/;
     $cgsn_platforms_mdf =~ s/txt$/$omc.txt/;
     $omc_str = "omc=$omc";
      }

#
#--------------------------------------------------------------
#Set-up ARGS
#
my @expand_list;
my %expand;

if ($arg{'expand'} ne "") 
  {my $value;
   my $i;
   $arg{'expand'} =~ s/,,/,/g; $arg{'expand'} =~ s/^,//g;
   $value = $arg{'expand'};
   (@expand_list) = split(',',$value);
   foreach $i (@expand_list)
         {$expand{$i} = 1;}
   }


#
# Read all cgsn platform IDs/array types from metadata file
#
my %MDF_Cfg;
if (-f "$metadata_dir/$oms_menu_mdf") {%MDF_Cfg = &load_cfg("$metadata_dir/$oms_menu_mdf");}
                                  else {%MDF_Cfg = &load_cfg("$oms_menu_mdf");}
my %ArrayType;
foreach my $type (split(',',$MDF_Cfg{'platform.array_types'}))
  {foreach my $pid (split(',',$MDF_Cfg{"platform.$type"}))
     {$ArrayType{"$type"} .= "$pid ";
      }
   }


print <<"EndHTML";
<HTML>
<HEAD><TITLE>OOI CGSN/OMS Menu</TITLE>
<!--
   OOI CGSN OMS System
   (c) Deep Submergence Laboratory - SL
   Woods Hole Oceanographic Institution
-->
</HEAD>
        <BODY bgcolor=#004466 xxbgcolor=#0088aa OnLoad=window.focus()>

<table cellspacing=0 cellpadding=3 width=250>
  <tr><td align=center><table bgcolor=white>
	<td><img src="/oms-html/images/ooi_logo.png" border=0  height=40>
        <td><img src="/oms-html/images/CGSN_logo_icon.png" border=0  height=40>
      <xximg src="/oms-html/images/whoi-logo-rvideo.gif">
      </table>
      </td>   
  </tr>
  <tr><td align=center><font face=sans-serif color=white size=2><b>$arg{'title'} OOI/CGSN OMS System</b><br>$VERSION</font>
  </tr>

  <tr><td>      
       <a href="oms_splash.pl" target=Main><font face=sans-serif color=cyan size=2>Home</a> <br> 
      </td>
</table>


<table cellspacing=0 cellpadding=0 width=100%>
<form method=post>
    <xxinput type=hidden name=title value="$arg{'title'}">
    <xxinput type=hidden name=d     value="$arg{'d'}">
    <input type=hidden name=expand value="$arg{'expand'}">

</tr>
</table>


<table align=center border=0 width=100% cellspacing=0 cellpadding=0>
<tr><td valign=top>
    <table border=0 cellpadding=0 cellspacing=5 width=100%>
EndHTML

print "<tr><td><font face=sans-serif size=2 color=white>Status: </font>\n";
print "        <a href=view_summary_rpt?mdf=$cgsn_platforms_mdf target=Main><font face=sans-serif size=2 color=cyan><b>Summary</b></font></a> &nbsp \n";
print "        <a href=status_report?mdf=$cgsn_platforms_mdf&otype=html target=Main><font face=sans-serif size=2 color=cyan><b>Report</b></font></a> &nbsp \n";
print "        <a href=plot_gmap?mdf=$cgsn_platforms_mdf&zoom=4 target=Main><font face=sans-serif size=2 color=cyan><b>Map</b></font></a>\n";
print "</td></tr>\n";

#
#For each array_type, list clickable options for each platform id
#
my $first_time = 1; my $first_time2 = 1;
my ($type, $prev_type);
foreach my $type (split(',',$MDF_Cfg{'platform.array_types'}))
   {#Special case - if no type is  to be expanded, expand first one 
    #if (!defined $arg{'expand'} && $first_time) {$arg{'expand'} = $type;
    #                                             $expand{"$type"} = 1;
    #                                             $first_time = 0;
    #                                             print "<input type=hidden name=expand value=\"$arg{'expand'}\">\n";
    #                                             }
    if ($type ne $prev_type)
        {if ($first_time2)
         {print "<tr><td>\n";
          $first_time2 = 0;
          }
         else {print "<tr><td>\n";}
         if ($expand{"$type"})
           {my $expand_list_str;
            ($expand_list_str = $arg{'expand'}) =~ s/$type//;
            print "<a href=\"oms_menu.pl?$omc_str&DisplayChoice=$arg{'DisplayChoice'}&expand=$expand_list_str&title=$arg{'title'}&d=$arg{'d'}\"><img src=/oms-html/images/down_white.gif border=0 align=top>";
            }
         else
           {print "<a href=\"oms_menu.pl?$omc_str&DisplayChoice=$arg{'DisplayChoice'}&expand=$arg{'expand'},$type&title=$arg{'title'}&d=$arg{'d'}\"><img src=/oms-html/images/right_white.gif border=0 align=top>";
           }
         print "<font face=sans-serif color=orange><b>$type</b></a></td></tr><tr>";
	 }
    $prev_type = $type;

    next if (! $expand{"$type"});


    my $font_color = "#00eeaa";
    my $ptype_html = "";

    #setup status/plot url links based on platform type (eg; CPSM, CPPM)
    my $font_style = "<font color=cyan face=sans-serif size=1>";
    my @pid_list = split(' ',$ArrayType{$type});
    my $pid;
    foreach $pid (sort @pid_list)
      {#Get list of deployments
       my @deploy_dirs = `cd /webdata/cgsn/data/raw/$pid; /bin/ls -1d D*`;
       for (my $i=0; $i<=$#deploy_dirs; $i++)  {chomp($deploy_dirs[$i]);}

       my $pid_caps = $pid; $pid_caps =~ tr/a-z/A-Z/;
       $ptype_html .= "<table border=0 cellspacing=1 cellpadding=1 hspace=20 width=100%>\n";
       $ptype_html .= "<td><a href=\"view_syslog?pid=$pid&deploy=$deploy_dirs[$#deploy_dirs]&view=platform_status&mdf=$cgsn_platforms_mdf\" Target=\"Main\"><font color=$font_color face=sans-serif size=3><b>$pid_caps</b></a>&nbsp - &nbsp</font>\n";
       $ptype_html .= "<a href=\"/oms-bin/view_platform.pl?cfg=$pid.cfg\" Target=\"Main\">$font_style<b>Configuration</b></font></a></td>\n";


       # -----------------------------------------------------------------------------------------------
       # Handle any customized PIDs here
       # -----------------------------------------------------------------------------------------------
       if ($pid_caps eq "EOMTST")
	 {$ptype_html .= "<tr><td>&nbsp&nbsp\n";
          $ptype_html .= "</table><br>\n";
          next;
         }
       # ------------------------------------------------------------------------------------------------


       $ptype_html .= "<tr><td>&nbsp&nbsp\n";
       $ptype_html .= "  <a href=\"view_rtstatus?pid=$pid&deploy=$deploy_dirs[$#deploy_dirs]\" Target=\"Main\">$font_style<b>RT Status</b></font></a>&nbsp\n";
       $ptype_html .= "  <a href=\"view_syslog?pid=$pid&deploy=$deploy_dirs[$#deploy_dirs]&plot=watchcircle&mdf=$cgsn_platforms_mdf\" Target=\"Main\">$font_style<b>Watch Circle</b></font></a>&nbsp\n";
       $ptype_html .= "<a href=\"view_syslog?pid=$pid&deploy=$deploy_dirs[$#deploy_dirs]&plot=xeos&mdf=$cgsn_platforms_mdf\" Target=\"Main\">$font_style<b>Xeos</b></font></a>&nbsp \n";
      my $plot_type;
      if ($pid =~ /pm/i) {$plot_type = "CPPM";}
                    else {$plot_type = "CPSM";}
      #$ptype_html .= "<a href=\"view_data_plots?pid=$pid&deploy=$deploy_dirs[$#deploy_dirs]&type=$plot_type\" Target=\"Main\">$font_style<b>DP</b></font></a>\n";
      $ptype_html .= "<a href=\"view_syslog?pid=$pid&deploy=$deploy_dirs[$#deploy_dirs]&type=$plot_type&plot=idata&show_platform_inst=1&mdf=$cgsn_platforms_mdf\" Target=\"Main\">$font_style<b>DP</b></font></a>\n";
      #$ptype_html .= "<td>&nbsp&nbsp\n";
      #$ptype_html .= "</td></tr><tr>\n<td>&nbsp&nbsp <font color=#dddddd face=sans-serif size=1><b>Data:</b></font>\n";
      #$ptype_html .= "<a href=\"view_syslog?pid=$pid&deploy=$deploy_dirs[$#deploy_dirs]&rtf=data_format.txt\" Target=\"Main\">$font_style<b>Fmt</b></font></a>&nbsp\n";
      #$ptype_html .= "<a href=\"/webdata/cgsn/data/raw/$pid/$deploy_dirs[$#deploy_dirs]\" Target=\"Main\">$font_style<b>Raw</b></font></a>&nbsp\n";
      #$ptype_html .= "<a href=\"/webdata/cgsn/data/proc/$pidi/$deploy_dirs[$#deploy_dirs]\" Target=\"Main\">$font_style<b>Proc</b></font></a>\n";
      
      #Show deployment dirs
      $ptype_html .= "<tr><td><table cellspacing=0 cellpadding=0 hspace=15><td>";
      foreach my $deploy (@deploy_dirs)
        {$ptype_html .= "<a href=\"view_syslog?pid=$pid&deploy=$deploy&view=platform_status\" Target=\"Main\">$font_style<b>$deploy</b></font></a>&nbsp\n";
         }
      $ptype_html .= "</table>\n";
      $ptype_html .= "</table><br>\n";
     } #end-foreach pid w/in Array type


    print <<"EndHTML";
      <tr>
      <td>
          $ptype_html
      </td>
      </tr>
EndHTML
    }



#
#Add Related Links...
#
#print "<tr><td><br></td></tr>\n";
if ($expand{"related"})
    {my $expand_list_str;
     ($expand_list_str = $arg{'expand'}) =~ s/related//;
     print "<tr><td><a href=\"oms_menu.pl?$omc_str&DisplayChoice=$arg{'DisplayChoice'}&expand=$expand_list_str&title=$arg{'title'}&d=$arg{'d'}\"><img src=/oms-html/images/down_white.gif border=0 align=top>";
     print "<font face=sans-serif color=cyan><b>Related Links</b></a></td></tr>";

     print "<tr><td>&nbsp&nbsp&nbsp&nbsp \n";
     print "<a href=http://www.ndbc.noaa.gov?lat=40.705628&lon=-71.411133&zoom=7&type=h&status=r&pgm=&op=&ls=false Target=Main>\n";
     print "    <font size=2 face=sans-serif color=#00eeaa>NDBC</a> &nbsp \n";
     print "<a href=plot_ndbc_data.pl?bid=44008&cols=9,10,11,12,13,14,15,18&width=800&title=NDBC%2044008 Target=Main>\n";
     print "    <font size=2 face=sans-serif color=#00eeaa>44008</a></td></tr>\n";

     print "<tr><td>&nbsp&nbsp&nbsp&nbsp \n";
     print "<font size=2 face=sans-serif color=#00eeaa>CI</a> - \n";
     print "<a href=http://ooi.whoi.edu/thredds/ooi_cgsn.html Target=Main>\n";
     print "    <font size=2 face=sans-serif color=#00eeaa>Thredds Server</a> &nbsp \n";
     print "<a href=http://ion-beta.oceanobservatories.org/ Target=Main>\n";
     print "    <font size=2 face=sans-serif color=#00eeaa>ION</a></td></tr>\n";
 
 
     print "<tr><td>&nbsp&nbsp&nbsp&nbsp \n";
     print "<a href=/cgo/pwrmodel-bin/pmodel_gui.pl Target=Main>\n";
     print "    <font size=2 face=sans-serif color=#00eeaa>CGSN Buoy Power Model</a></td></tr>\n";
 
     print "<tr><td>&nbsp&nbsp&nbsp&nbsp \n";
     print "<a href=/oms-bin/view_platform.pl Target=Main>\n";
     print "    <font size=2 face=sans-serif color=#00eeaa>CGSN Platform & Instruments</a></td></tr>\n";
 
     print "<tr><td>&nbsp&nbsp&nbsp&nbsp \n";
     print "<a href=http://www.oceanobservatories.org Target=Main>\n";
     print "    <font size=2 face=sans-serif color=#00eeaa>Ocean Observatories Initiative</a></td></tr>\n";
     }

else {print "<tr><td><a href=\"oms_menu.pl?$omc_str&DisplayChoice=$arg{'DisplayChoice'}&expand=$arg{'expand'},related&title=$arg{'title'}&d=$arg{'d'}\"><img src=/oms-html/images/right_white.gif border=0 align=top>";
      print "<font face=sans-serif color=cyan><b>Related Links</b></a></td></tr><tr>";
      }


print "</form></table>\n</body>\n</html>\n";

print <<"EndHTML";
  <table bgcolor=#004466 vspace=50 align=center width=100%>
      <td align=center>
      <img src="/oms-html/images/nsf_logo.gif" border=0 height=52>
      <img src="/oms-html/images/whoilogo-small-trans.gif" border=0 height=50>
      <img src="/oms-html/images/rutgers_logo3.jpg" border=0 valign=top height=50>
      <img src="/oms-html/images/osu_logo.png" border=0 height=50>
  </table>
EndHTML



exit;
