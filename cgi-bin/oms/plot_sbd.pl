#!/usr/bin/perl
# plot_sbd.pl -  Quick script to generate sbd plot
#
# Usage: plot_sbd.pl <pid=> <deploy=> [limit=] [date=]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/25/2015  SL     Create
#      04/14/2015  SL     Updated to support mpic_cpm and mpic_stc sbd messages
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
##################################################################################
use strict;
require "../cg_util.pl";
use Scalar::Util qw(looks_like_number);

my %arg = &get_args(0);
my $deploy = "deploy_not_specified";
my $limit  = 1500;
if ($arg{'deploy'} ne "") {$deploy = $arg{'deploy'};}
if ($arg{'limit'}  ne "") {$limit  = $arg{'deploy'};}

if (!defined $arg{'pid'})
  {print "content-type/html\n\n";
   print "Error: pid must be specified\n";
   exit(-1);
   }

# Read-in SBD file(s)
my $ddir = "/webdata/cgsn/data/raw/$arg{'pid'}/$deploy/irid_sbd";
if (! -d "$ddir") {die "ddir [$ddir] does not exist\n";}

my @sbd_files = `cd $ddir; /bin/ls -1 *.log | grep -v _`; 
#print "FILES=[@sbd_files]\n";

my %Mon;
$Mon{'Jan'} = "01";
$Mon{'Feb'} = "02";
$Mon{'Mar'} = "03";
$Mon{'Apr'} = "04";
$Mon{'May'} = "05";
$Mon{'Jun'} = "06";
$Mon{'Jul'} = "07";
$Mon{'Aug'} = "08";
$Mon{'Sep'} = "09";
$Mon{'Oct'} = "10";
$Mon{'Nov'} = "11";
$Mon{'Dec'} = "12";

#Sample format of SBD data
# MPIC_CPM
# MOMSN=2135, Wed Mar 25 05:58:52 2015, 00 - Transfer OK, bytes=207, Lat: -42.92112 Lon: -42.57366 CEPradius: 3, 
#    mpic_cpm: 24.7 518.0 0.0 0.0 00400000 t 26.4 24.0 h 6.7 p 14.8 0.0 0.0 0.0 ndata ndata ndata pv 0 wt 0 fc 0 sbd 5 0 
#    gf f ld 3 hb 1 125 2 wake 01 ir 0 fwwf 3 gps 1 pps 0 dcl 23 wtc 0.00 wpc 1 esw 3 dsl 1 33a2
# MOMSN=2132, Wed Mar 25 04:58:49 2015, 00 - Transfer OK, bytes=207, Lat: -42.93620 Lon: -42.46828 CEPradius: 2, 
#    mpic_cpm: 24.6 517.0 0.0 0.0 00400000 t 26.5 24.0 h 6.7 p 14.8 0.0 0.0 0.0 ndata ndata ndata pv 0 wt 0 fc 0 sbd 5 0 
#    gf f ld 3 hb 1 125 2 wake 01 ir 0 fwwf 3 gps 1 pps 0 dcl 23 wtc 0.00 wpc 1 esw 3 dsl 1 33a1
# MOMSN=2129, Wed Mar 25 03:58:53 2015, 02 - Transfer OK Bad Location, bytes=207, Lat: -42.94944 Lon: -41.63354 CEPradius: 809, 
#    mpic_cpm: 24.7 513.0 0.0 0.0 00400000 t 26.6 24.0 h 6.7 p 14.8 0.0 0.0 0.0 ndata ndata ndata pv 0 wt 0 fc 0 sbd 2 0 
#    gf f ld 3 hb 1 125 2 wake 01 ir 0 fwwf 3 gps 1 pps 0 dcl 23 wtc 0.00 wpc 1 esw 3 dsl 1 339f

# MPIC_STC
# MOMSN=20296, Tue Apr  7 17:05:34 2015, 00 - Transfer OK, bytes=273, Lat: 41.51084 Lon: -70.65567 
#    CEPradius: 7, mpic_stc: 20.8 470.8 00000400 t 16.7 15.1 h 2.8 p 13.8 gf f ld 3 1226 1233 
#    hb 1 125 2 wake 01 ir 0 0.0 0.0 0 fwwf 3 12.0 334.8 0 gps 1 sbd 5 pps 0 imm 1 wtc 0.00 wpc 1 
#    esw 0 dsl 0 00000000 p1 0 0.0 0.0 0 p2 1 20.7 17.1 0 p3 1 12.1 4.9 0 p5 1 12.0 39.1 0 
#    p7 0 0.0 0.0 0 3bc2
#

my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>
EndHTML


foreach my $sbd_file (@sbd_files)
  {chomp($sbd_file);
   my $fd; 
   open($fd, "<$ddir/$sbd_file") || warn "Unable to open $ddir/$sbd_file\n";

   my @sbd_data = <$fd>; 
   my (@IMEI, @timestamp, @mainV, @mainC, @t1, @t2, @humid, @press, $dummy, $label, @err_flag, @mainW);
   my $imei_cnt = 0;
   my $line_cnt = 0;
   my $i;
   my $line;

   foreach $line (@sbd_data)
     {if ($line =~ /SBD IMEI\:/) {($dummy,$dummy,$dummy,$IMEI[++$imei_cnt]) = split(' ',$line);
                                  next;
                                  }
      elsif ($line =~ /Transfer OK,/)
          {chomp($line);
           my($momsn,$time_string, $status, $bytes, $lat_lon_radius, $mpic_str) = split(',', $line);
           my($day,$month,$mday,$time,$yyyy) = split(' ',$time_string);
           $mday = sprintf("%02d",$mday);
           $timestamp[$line_cnt] = "$yyyy/$Mon{$month}/$mday $time";
           if ($line =~ /mpic_cpm/)
             {($dummy, $mainV[$line_cnt], $mainC[$line_cnt], $dummy, $dummy, $err_flag[$line_cnt], $label, $t1[$line_cnt], $t2[$line_cnt], 
              $label, $humid[$line_cnt], $label, $press[$line_cnt]) = split(' ',$mpic_str);
              }
           elsif ($line =~ /mpic_stc/)
             {($dummy, $mainV[$line_cnt], $mainC[$line_cnt], $err_flag[$line_cnt], $label, $t1[$line_cnt], $t2[$line_cnt], 
              $label, $humid[$line_cnt], $label, $press[$line_cnt]) = split(' ',$mpic_str);
              }
           next if ($mainV[$i] !~ /^[+-]?\d+(\.\d+)?$/ || $mainC[$i] !~ /^[+-]?\d+(\.\d+)?$/);
           $mainW[$line_cnt] = sprintf("%.2f", $mainV[$line_cnt] * $mainC[$line_cnt]/1000.0);
           #print "LINE_CNT=$line_cnt mainV_cnt=$#mainV mainV[$line_cnt] = $mainV[$line_cnt]\n\nline = [$line]\nmpic_str = [$mpic_str]\n";
           $line_cnt++;
           }
      }



my $imei = $sbd_file; $imei =~ s/\.log//;

   $html_out .= <<"EndHTML";
<!-- Platform ID: $arg{'pid'}  IMEI: $imei<br>
-->
<br>
<b>SBD IMEI: $imei<br></b>
<table>
   <td>MPIC Main Voltage
       <div id="graphdiv1_$imei"
        style="width:500px; height:200px;"></div>
   </td>
   <td>MPIC Main Current (mA) 
       <div id="graphdiv2_$imei"
        style="width:500px; height:200px;"></div>
   </td>
   <td>MPIC Main Watts 
       <div id="graphdiv3_$imei"
        style="width:500px; height:200px;"></div>
   </td>
   </tr><tr>
   <td>MPIC Temperature 
       <div id="graphdiv4_$imei"
        style="width:500px; height:200px;"></div>
   </td>
   <td>MPIC Humidity
       <div id="graphdiv5_$imei"
        style="width:500px; height:200px;"></div>
   </td>
   <td>MPIC Pressure
       <div id="graphdiv6_$imei"
        style="width:500px; height:200px;"></div>
   </td>
</table>

<script type="text/javascript">

  g1_$imei = new Dygraph(
    document.getElementById("graphdiv1_$imei"),
"timestamp, Volts \\n" +
EndHTML
    for ($i=0;$i<$#mainV;$i++)
       {next if !(looks_like_number($mainV[$i])); # or use something like if $val =~ /^[+-]?\d+(\.\d+)?$/
        if ($i < $#mainV-1){$html_out .= "\"$timestamp[$i], $mainV[$i] \\n\" +\n";}
                      else {$html_out .= "\"$timestamp[$i], $mainV[$i] \\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      valueRange: [0,35]
     }          // options
  );

  g2_$imei = new Dygraph(
    document.getElementById("graphdiv2_$imei"),
"timestamp, mA \\n" +
EndHTML
    for ($i=0;$i<$#mainC;$i++)
       {next if !(looks_like_number($mainC[$i])); # or use something like if $val =~ /^[+-]?\d+(\.\d+)?$/
        if ($i < $#mainC-1){$html_out .= "\"$timestamp[$i], $mainC[$i] \\n\" +\n";}
                      else {$html_out .= "\"$timestamp[$i], $mainC[$i] \\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g3_$imei = new Dygraph(
    document.getElementById("graphdiv3_$imei"),
"timestamp, Watts \\n" +
EndHTML
    for ($i=0;$i<$#mainW;$i++)
       {next if !(looks_like_number($mainW[$i])); # or use something like if $val =~ /^[+-]?\d+(\.\d+)?$/
        if ($i < $#mainW-1){$html_out .= "\"$timestamp[$i], $mainW[$i] \\n\" +\n";}
                      else {$html_out .= "\"$timestamp[$i], $mainW[$i] \\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g4_$imei = new Dygraph(
    document.getElementById("graphdiv4_$imei"),
"timestamp, DegC \\n" +
EndHTML
    for ($i=0;$i<$#t1;$i++)
       {next if !(looks_like_number($t1[$i])); # or use something like if $val =~ /^[+-]?\d+(\.\d+)?$/
        if ($i < $#mainW-1){$html_out .= "\"$timestamp[$i], $t1[$i] \\n\" +\n";}
                      else {$html_out .= "\"$timestamp[$i], $t1[$i] \\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      fillGraph: true,
      valueRange: [0,45]
     }          // options
  );

  g5_$imei = new Dygraph(
    document.getElementById("graphdiv5_$imei"),
"timestamp, Humidity \\n" +
EndHTML
    for ($i=0;$i<$#humid;$i++)
       {next if !(looks_like_number($humid[$i])); # or use something like if $val =~ /^[+-]?\d+(\.\d+)?$/
        if ($i < $#mainW-1){$html_out .= "\"$timestamp[$i], $humid[$i] \\n\" +\n";}
                      else {$html_out .= "\"$timestamp[$i], $humid[$i] \\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      fillGraph: true
     }          // options
  );

  g6_$imei = new Dygraph(
    document.getElementById("graphdiv6_$imei"),
"timestamp, Humidity \\n" +
EndHTML
    for ($i=0;$i<$#press;$i++)
       {next if !(looks_like_number($humid[$i])); # or use something like if $val =~ /^[+-]?\d+(\.\d+)?$/
        if ($i < $#mainW-1){$html_out .= "\"$timestamp[$i], $press[$i] \\n\" +\n";}
                      else {$html_out .= "\"$timestamp[$i], $press[$i] \\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
     { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      fillGraph: true
     }          // options
  );
</script>
<br>
EndHTML

} #end-foreach sbd_file

print "$html_out";
print "</body>\n</html>\n";
exit;

