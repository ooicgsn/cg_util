#!/usr/bin/perl
# plot_psc.pl -  Quick script to generate rt psc data plot
#
# Usage: plot_psc.pl <pid=> <deploy=>
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      03/31/2011  SL     Create from plot_psc.html, but now use in-line data
#      08/03/2011  SL     Updated to plot currents for bt, pv, wt. Added plot
#                         for power generation sources
#      10/18/2011  SL     Added battery charge and re-arranged plots
#      10/28/2012  SL     Added pid as arg
#      11/16/2013  SL     Added 300V high-voltage converter
#      12/23/2014  SL     Updated to use cg_util.pl
#      02/02/2015  SL     Support Deployment dir
#      03/19/2015  SL     Added plotting watts
#      05/12/2015  SL     Added plotting battery temperature
#      05/26/2015  SL     Added plotting 300V Cvtr watts
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
#      08/14/2016  JJP    Use ScriptAlias /cmn-bin for path to view_syslog
#      08/19/2016  JJP	  Removed /cmn-bin from path for view_syslog, as it has
#			  moved back to cgi-bin/oms
##################################################################################
require "../cg_util.pl";

my %arg    = &get_args(0);
my $pid    = $arg{'pid'};
my $deploy = $arg{'deploy'};
my $limit  = 3000;

if ($pid eq "" || $deploy eq "") {print "pid and deploy must be specified\n"; exit(-1);}


my @vmain_data =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=6   label=Vmain date=$arg{'date'}`; 
my @imain_data =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=7   label=Imain date=$arg{'date'}`; 
my @bat_data   =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=47,51,55,59  label=b1,b2,b3,b4 date=$arg{'date'}`; 
my @bat_tdata  =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=45,49,53,57  label=bt1,bt2,bt3,bt4 date=$arg{'date'}`; 
my @pv_data    =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=15,19,23,27 label=pv1,pv2,pv3,pv4 date=$arg{'date'}`; 
my @wt_data    =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=31,35 label=wt1,wt2 date=$arg{'date'}`; 
my @src_data   =  `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=13,17,21,25,29,33,37,41 label=pv1,pv2,pv3,pv4,wt1,wt2,fc1,fc2 date=$arg{'date'}`; 
my @bcharge    =   `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=8 label=Charge date=$arg{'date'}`; 
my @cvtr_v     =   `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=75 label=cvt_v date=$arg{'date'}`; 
my @cvtr_i     =   `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=76 label=cvt_i date=$arg{'date'}`; 
my @cvtr_vi     =   `./view_syslog pid=$pid deploy=$deploy limit=$limit s=\" psc: \" extract=75,76 label=cvt_v,cvt_i date=$arg{'date'}`; 

my ($i, $line);
my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>
<!-- Platform ID: $pid <br>
-->
<table>
   <td>PSC Vmain
       <div id="graphdiv1"
        style="width:500px; height:240px;"></div>
   </td>
   <td>PSC Imain (ma)  
       <div id="graphdiv6"
        style="width:500px; height:240px;"></div>
   </td>
   <td>PSC Main Watts  
       <div id="graphdiv10"
        style="width:500px; height:240px;"></div>
   </td>
   </tr><tr>
   <td>Battery Charge (%)  
       <div id="graphdiv7"
        style="width:500px; height:240px;"></div>
   </td>
   <td>Battery 1-4 ma
       <div id="graphdiv2"
        style="width:500px; height:240px;"></div>
   </td>
   <td>BTemp DegC
       <div id="graphdiv11"
        style="width:500px; height:240px;"></div>
   </td>
   </tr><tr>
   <td>PV 1-4 ma
       <div id="graphdiv3"
        style="width:500px; height:240px;"></div>
   </td>
   <td>WT 1-2 ma
       <div id="graphdiv4"
        style="width:500px; height:240px;"></div>
   </td>
   <td>Sources
       <div id="graphdiv5"
        style="width:500px; height:240px;"></div>
   </td>
   </tr><tr>
   <td>300V Cvtr - volts
       <div id="graphdiv8" 
        style="width:500px; height:240px;"></div>
   </td>
   <td>300V Cvtr - ma
       <div id="graphdiv9" 
        style="width:500px; height:240px;"></div>
   </td>
   <td>300V Cvtr - watts
       <div id="graphdiv12" 
        style="width:500px; height:240px;"></div>
   </td>
   </tr>

</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?s=%22psc:%22&extract=6&label=Vmain", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#vmain_data;$i++)
       {$line = $vmain_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#vmain_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      valueRange: [20,34]
     }          // options
  );

  g6 = new Dygraph(
    document.getElementById("graphdiv6"),
EndHTML
    for ($i=0;$i<$#imain_data;$i++)
       {$line = $imain_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#imain_data-1){$html_out .= "\"$line\\n\" +\n";}
                           else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g10 = new Dygraph(
    document.getElementById("graphdiv10"),
EndHTML
    $html_out .= "\"timestamp, watts \\n\" +\n";
    for ($i=0;$i<$#imain_data;$i++)
       {$line = $imain_data[$i];  #yyyy/mm/dd hh:mm:ss, val
        next if $line =~/content-type/i || length($line) <= 2;
        next if $line =~/vmain/i || $line =~/imain/i;
        my($timestamp,$vmain,$imain,$watts);
        ($timestamp,$vmain) = split(',',$vmain_data[$i]);
        ($timestamp,$imain) = split(',',$imain_data[$i]);
        $watts = sprintf("%.2f",$vmain * $imain/1000.0);
#print "vmain=$vmain, imain=$imain, watts=$watts<br>\n";
        if ($i < $#imain_data-1){$html_out .= "\"$timestamp, $watts \\n\" +\n";}
                           else {$html_out .= "\"$timestamp, $watts \\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g7 = new Dygraph(
    document.getElementById("graphdiv7"),
EndHTML
    for ($i=0;$i<$#bcharge;$i++)
       {$line = $bcharge[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#bcharge-1){$html_out .= "\"$line\\n\" +\n";}
                        else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?s=%22psc:%22&extract=45,49,53,57&label=bt1,bt2,bt3,bt4", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#bat_data;$i++)
       {$line = $bat_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#bat_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g11 = new Dygraph(
    document.getElementById("graphdiv11"),
EndHTML
    for ($i=0;$i<$#bat_tdata;$i++)
       {$line = $bat_tdata[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#bat_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g3 = new Dygraph(
    document.getElementById("graphdiv3"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?s=%22psc:%22&extract=14,18,22,26&label=pv1,pv2,pv3,pv4", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#pv_data;$i++)
       {$line = $pv_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#pv_data-1){$html_out .= "\"$line\\n\" +\n";}
                        else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g4 = new Dygraph(
    document.getElementById("graphdiv4"),
    // "http://dcl9.whoi.edu/cg-bin/view_syslog?s=%22psc:%22&extract=30,34&label=wt1,wt2", // url or path to CSV file
EndHTML
    for ($i=0;$i<$#wt_data;$i++)
       {$line = $wt_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#wt_data-1){$html_out .= "\"$line\\n\" +\n";}
                        else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g5 = new Dygraph(
    document.getElementById("graphdiv5"),
EndHTML
    for ($i=0;$i<$#src_data;$i++)
       {$line = $src_data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#src_data-1){$html_out .= "\"$line\\n\" +\n";}
                         else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      stackedGraph: true,
      writeStroke: 2.0,
      valueRange: [0,8]
     }          // options
  );

  g8 = new Dygraph(
    document.getElementById("graphdiv8"),
EndHTML
    for ($i=0;$i<$#cvtr_v;$i++)
       {$line = $cvtr_v[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#cvtr_v-1){$html_out .= "\"$line\\n\" +\n";}
                       else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
     }          // options
  );


  g9 = new Dygraph(
    document.getElementById("graphdiv9"),
EndHTML
    for ($i=0;$i<$#cvtr_i;$i++)
       {$line = $cvtr_i[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#cvtr_i-1){$html_out .= "\"$line\\n\" +\n";}
                       else {$html_out .= "\"$line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
     }          // options
  );


  g12 = new Dygraph(
    document.getElementById("graphdiv12"),
EndHTML
    $html_out .= "\"timestamp, watts \\n\" +\n";
    for ($i=0;$i<$#cvtr_vi;$i++)
       {$line = $cvtr_vi[$i];  #yyyy/mm/dd hh:mm:ss, volt, curr
        next if $line =~/content-type/i || length($line) <= 2;
        next if $line =~/cvt/i;
        my($timestamp,$vmain,$imain,$watts);
        ($timestamp,$volts,$current) = split(',',$cvtr_vi[$i],$cvtr_vi[$i]);
        $watts = sprintf("%.2f",$volts * $current/1000.0);
        if ($i < $#cvtr_vi-1) {$html_out .= "\"$timestamp, $watts \\n\" +\n";}
                         else {$html_out .= "\"$timestamp, $watts \\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
     }          // options
  );
</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

