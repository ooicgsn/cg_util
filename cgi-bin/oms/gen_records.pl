#!/usr/bin/perl
#
#gen_records.pl - Takes in csv input (or space seperated) and generates 
#                 min/max/ave records at hourly or daily intervals.
#
# Usage: gen_records.pl [i=|d=|pid=] [t=daily|hourly|minute] [minmax=1] [cols=1,2,...] [force=1]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      10/01/2011  SL     Create
#      10/30/2011  SL     Added cols and update minmax header. Note Cols must
#                         include 1,2 which are date, time respectively
#                         Added force flag, when set process all files. If not
#                         set (default), read current file until last day, then
#                         re-process that day.
##################################################################################
use strict;

my($DEBUG) = 0;
my(%arg,$buffer,@pairs,$pair,$key,$value);
my($max_vars) = 100;
$arg{'force'} = 0;
my($bypass_hdr) = 0;

#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      }
elsif ($ENV{'QUERY_STRING'} ne "")
 {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }
if ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
    #print "arg{$key} = $value\n";
}

my($minute_flag) = 0;
my($hourly_flag) = 1;
my($daily_flag)  = 0;
my($minmax_flag) = 0;

if ($arg{'i'} eq "" && $arg{'d'} eq "" && $arg{'pid'} eq "") 
     {print "Usage: $0 <i=in_file d=dir | pid=pid> [t=hourly|daily|minute] [minmax=1] [cols=1,2,...] [force=1]\n";
      exit(-1);
      }

if ($arg{'minmax'}) {$minmax_flag = 1;}
if    ($arg{'t'} eq "hourly") {$hourly_flag = 1; $daily_flag = 0; $minute_flag = 0;}
elsif ($arg{'t'} eq "daily")  {$hourly_flag = 0; $daily_flag = 1; $minute_flag = 0;}
elsif ($arg{'t'} eq "minute") {$hourly_flag = 0; $daily_flag = 0; $minute_flag = 1;}

my($fd,$out_fd, @recs,@vars, @vmin, @vmax, @vave, @vtot, $date, @dates, $date_time, $hour, $minute, $var);
my(@dlist, $dir, @flist, $file, $outfile);
my($i,$j,$prev_date,$prev_hour,$prev_minute);
my(@col_list) = split(',',$arg{'cols'});

if ($arg{'i'} ne "") {$flist[0] = substr($arg{'i'},rindex($arg{'i'},"/")+1);
                      if ($arg{'i'} =~ /\//)
                           {$dlist[0] = substr($arg{'i'},0,rindex($arg{'i'},"/"));}
                      else {$dlist[0] = ".";}
                      }
elsif ($arg{'d'} ne "")
     {$dlist[0] = "$arg{'d'}";
      if (! -d "$arg{'d'}") {die "***Unable to readdir [$arg{'d'}], exiting\n";}
      }
else {#search through pid dir and foreach subdir, get list of 2*.csv files
      my($ddir) = "/webdata/cgsn/proc";
      my(@tmp_dlist, $i);
      opendir($fd,"$ddir/$arg{'pid'}") || die "***Unable to readdir [$ddir/$arg{'pid'}], exiting\n";
      (@tmp_dlist) = sort grep(!/^.\.?$/,readdir($fd)); #elim . & .. files
      closedir($fd);
      for ($i=0; $i<$#tmp_dlist; $i++) {$dlist[$i] = "$ddir/$arg{'pid'}/$tmp_dlist[$i]";}
      }

print "Dlist=[@dlist]\n" if ($DEBUG);
foreach $dir (@dlist)
 {if (!opendir($fd,"$dir"))
       {print "***Unable to readdir [$dir], skipping\n";
        next;
        }
  if ($arg{'i'} eq "")
       {(@flist) = sort grep (/^(2|C|A|E|MC)/,readdir($fd)); #want 2*.csv files
        }
  closedir($fd);

print "Flist=[@flist]\n" if ($DEBUG);

  #setup outfile name
  my($dname) = substr($dir,rindex($dir,"/")+1);
  if    ($daily_flag)  {$outfile = "$dname.daily.csv";} 
  elsif ($hourly_flag) {$outfile = "$dname.hourly.csv";} 
  elsif ($minute_flag) {$outfile = "$dname.minute.csv";} 
  if ($minmax_flag)    {$outfile =~ s/\.csv/_minmax.csv/;}

  my($start_date) = "";
  if ($arg{'force'} == 0 && -f "$dir/$outfile")
    {#read existing outfile, get last day of data, and strip-off last day from outfile
     open($fd,"<$dir/$outfile") || die "***Unable to open [$dir/$outfile] for reading, exiting\n";
     (@recs) = <$fd>;
     close($fd);
     open($out_fd,">$dir/$outfile") || die "***Unable to open outfile [$dir/$outfile], exiting\n";
     $start_date = substr($recs[$#recs],0,10);
     for ($i=0;$i<=$#recs;$i++) 
        {last if ($recs[$i] =~ /^$start_date/);
         print $out_fd "$recs[$i]";
         }
     $bypass_hdr = 1;
     }
  else {#just create outfile
        open($out_fd,">$dir/$outfile") || die "***Unable to open [$dir/$outfile], exiting\n";
        }

  #initialize vmin, vmax, vtot - currently handle up to n-variables
  for ($i=0;$i<$max_vars;$i++) {$vmin[$i] = 9999999; $vmax[$i] = -9999999; $vtot[$i]=0;}
  $prev_date   = "-1";
  $prev_hour   = "-1";
  $prev_minute = "-1";


  my($rec_cnt) = 0;

  foreach $file (@flist)
   {next if ($file !~ /\.csv$/); #make sure ends with .csv
    open($fd,"<$dir/$file") || die "***Unable to open [$dir/$file], exiting\n";
    (@recs) = <$fd>;
    close($fd);
    next if ($start_date ne "" && substr($recs[1],0,10) lt $start_date);
    print "Processing $arg{'pid'} - $file...\n";
    my($hdr_line) = "$recs[0]"; chomp($hdr_line);
    if ($arg{'cols'} ne "")
      {#only put specified cols into hdr_line
       my($col);
       my($field_name_subset) = "";
       my(@hdr_fields) = split(' ',$hdr_line);
       foreach $col (@col_list)
         {$field_name_subset .= "$hdr_fields[$col-1] ";
          }
       $hdr_line = $field_name_subset;
       }
    if ($minmax_flag)
      {#Update hdr_line to include min/max cols
       my(@hdr_fields) = split(' ',$hdr_line);
       my($new_hdr_line) = "date time ";
       for ($j=2;$j<=$#hdr_fields;$j++)
          {$new_hdr_line .= "min-$hdr_fields[$j]  max-$hdr_fields[$j]  ave-$hdr_fields[$j]  ";
           }
       $hdr_line = "$new_hdr_line";
       }

    for ($i=1;$i<=$#recs;$i++)
      {next if ($recs[$i] =~ /^#/); #skip comments
       next if ($recs[$i] =~ /\[/); #skip metadata
       next if ($recs[$i] =~ /BAD_DATA/); #skip BAD Data
       chomp($recs[$i]);

       (@vars) = split(' ',$recs[$i]);
       $rec_cnt++;
       $date      = "$vars[0]";
       $date_time = "$vars[0] $vars[1]";
       ($hour,$minute) = split(":",$vars[1]);
    
       for ($j=2;$j<=$#vars;$j++)
         {$vars[$j] =~ s/^\*//; #eliminate leading *
          next if ($vars[$j] =~ /nan/i);
          if ($vars[$j] < $vmin[$j]) {$vmin[$j] = $vars[$j];}
          if ($vars[$j] > $vmax[$j]) {$vmax[$j] = $vars[$j];}
          $vtot[$j] += $vars[$j];
          }
    
       if ($daily_flag)
         {if ($date ne "$prev_date" && $prev_date ne "-1")
            {#calculate the averages
             for ($j=2;$j<=$#vars;$j++) {$vave[$j] = $vtot[$j]/$rec_cnt;}
             
             #output daily record
             print $out_fd "$prev_date 00:00:00 ";
             if ($arg{'cols'} eq "")
               {for ($j=2;$j<$#vars+1;$j++) 
                  {print $out_fd "$vmin[$j] $vmax[$j] " if $minmax_flag;
                   printf $out_fd "%.6f  ", $vave[$j];
                   }
               }
             else
              {#output only selected columns
               for ($j=2;$j<=$#col_list;$j++)
                 {print $out_fd "$vmin[$col_list[$j]-1] $vmax[$col_list[$j]-1] " if $minmax_flag;
                  printf $out_fd "%.6f  ", $vave[$col_list[$j]-1];
                  }
               }
             print $out_fd "\n";

             #clear stats
  	     $rec_cnt = 0;
 	     for ($j=0;$j<$max_vars;$j++) {$vmin[$j] = 9999999; $vmax[$j] = -9999999; $vtot[$j]=0;}
	     }
           if ($prev_date eq "-1") {print $out_fd "$hdr_line\n" unless $bypass_hdr;}
           }

        if ($hourly_flag)
         {if ($hour ne "$prev_hour" && $prev_hour ne "-1")
            {#calculate the averages
             for ($j=2;$j<=$#vars;$j++) {$vave[$j] = $vtot[$j]/$rec_cnt;}
             
             #output hourly record
             print $out_fd "$prev_date $prev_hour:00:00 ";
             if ($arg{'cols'} eq "")
               {for ($j=2;$j<$#vars+1;$j++) 
                  {print $out_fd "$vmin[$j] $vmax[$j] " if $minmax_flag;
                   printf $out_fd "%.6f  ", $vave[$j];
                   }
               }
             else
              {#output only selected columns
               for ($j=2;$j<=$#col_list;$j++)
                 {print $out_fd "$vmin[$col_list[$j]-1] $vmax[$col_list[$j]-1] " if $minmax_flag;
                  printf $out_fd "%.6f  ", $vave[$col_list[$j]-1];
                  }
               }
             print $out_fd "\n";

             #clear stats
             $rec_cnt = 0;
             for ($j=0;$j<$max_vars;$j++) {$vmin[$j] = 9999999; $vmax[$j] = -9999999; $vtot[$j]=0;}
             }
           if ($prev_hour eq "-1") {print $out_fd "$hdr_line\n" unless $bypass_hdr;}
          }

       if ($minute_flag)
         {if ($minute ne "$prev_minute" && $prev_minute ne "-1")
            {#calculate the averages
             for ($j=2;$j<=$#vars;$j++) {$vave[$j] = $vtot[$j]/$rec_cnt;}
             
             #output minute record
             print $out_fd "$prev_date $prev_hour:$prev_minute:00 ";
             if ($arg{'cols'} eq "")
               {for ($j=2;$j<$#vars+1;$j++) 
                  {print $out_fd "$vmin[$j] $vmax[$j] " if $minmax_flag;
                   printf $out_fd "%.6f  ", $vave[$j];
                   }
               }
             else
              {#output only selected columns
               for ($j=2;$j<=$#col_list;$j++)
                 {print $out_fd "$vmin[$col_list[$j]-1] $vmax[$col_list[$j]-1] " if $minmax_flag;
                  printf $out_fd "%.6f  ", $vave[$col_list[$j]-1];
                  }
               }
             print $out_fd "\n";

	     #clear stats
	     $rec_cnt = 0;
	     for ($j=0;$j<$max_vars;$j++) {$vmin[$j] = 9999999; $vmax[$j] = -9999999; $vtot[$j]=0;}
	     }
          if ($prev_minute eq "-1") {print $out_fd "$hdr_line\n" unless $bypass_hdr;}
          }

       #For profiler
       if ($recs[$i] =~ /NaN NaN NaN NaN NaN NaN NaN/i) {print $out_fd "$recs[$i]\n";}
       #print "$recs[$i]\n";

       $prev_date = "$date";
       $prev_hour = "$hour";
       $prev_minute = "$minute";
       }
    } #end for-each file

   #
   # print out remaining stats
   #
   if ($rec_cnt > 0)
     {if    ($daily_flag)  {print $out_fd "$prev_date 00:00:00 ";}
      elsif ($hourly_flag) {print $out_fd "$prev_date $prev_hour:00:00 ";}
      elsif ($minute_flag) {print $out_fd "$prev_date $prev_hour:$prev_minute:00 ";}
      #calculate the averages
      for ($j=2;$j<=$#vars;$j++) {$vave[$j] = $vtot[$j]/$rec_cnt;}
      if ($arg{'cols'} eq "")
            {for ($j=2;$j<$#vars+1;$j++) 
                {print $out_fd "$vmin[$j] $vmax[$j] " if $minmax_flag;
                 printf $out_fd "%.6f  ", $vave[$j];
                 }
             }
      else
            {#output only selected columns
             for ($j=2;$j<=$#col_list;$j++)
                {print $out_fd "$vmin[$col_list[$j]-1] $vmax[$col_list[$j]-1] " if $minmax_flag;
                 printf $out_fd "%.6f  ", $vave[$col_list[$j]-1];
                 }
             }
      print $out_fd "\n";
      }
      
   close($out_fd);
  } #end for-each dir



