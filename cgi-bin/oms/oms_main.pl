#!/usr/bin/perl
###############################################################################
#
# oms_main.pl - SL 11/2011
#
# Description: Top-level menu
#
# Web Usage: oms_main.pl
#
# History:
#       Date       Who     Description
#       ----       ---     ----------------------------------------------------
#     11/17/2011   SL      Create
#     10/18/2012   SL      Updated links to use oms instead of omc
#     07/20/2016   SL      Added optional arg omc
#     08/09/2016   JJP	   Correct path to cg_util.pl for restructure in Bitbucket
###############################################################################
require "../cg_util.pl";
require "flush.pl";

print "Content-type: text/html\n\n";
print "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n";
flush(STDOUT);

my %arg = &get_args(0);
my $omc = "";
if (defined $arg{'omc'}) {$omc = "$arg{'omc'}";}

$menu_url   = "/oms-bin/oms_menu.pl";
$splash_url = "/oms-bin/oms_splash.pl";
$title      = "CGSN OMS";
$col_width  = 300;

print <<"EndHTML";

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<TITLE>$title</TITLE>
<!--
   WHOI OOI/CGSN OMS System
   (c) Deep Submergence Laboratory - SL
   Woods Hole Oceanographic Institution
-->
</HEAD>

<FRAMESET COLS="$col_width,*" border=0>
EndHTML
if ($omc eq "")
     {print "<FRAME SRC=\"$menu_url\"  NAME=\"Menu\">\n";}
else {print "<FRAME SRC=\"$menu_url?omc=$omc\"  NAME=\"Menu\">\n";}
print <<"EndHTML";
        <FRAME SRC="$splash_url" NAME="Main">
    </FRAMESET>

    <NOFRAMES>
        <BODY>
        <P>
        </P>
        </BODY>
    </NOFRAMES>
</FRAMESET>
</HTML>

EndHTML
