#!/usr/bin/perl
# view_summary_rpt - Reads in cgsn_history.txt file and generates
#                    html output
#
# Usage: view_summary_rpt
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      02/03/2015  SL     Create
#      05/13/2015  SL     Added last comm times
#      06/03/2015  SL     Added on-line edit capability
#      07/20/2015  SL     Added optional arg mdf
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
#      08/14/2016  JJP	  Use ScriptAlias /cmn-bin in path for view_syslog
#      08/19/2016  JJP	  Removed /cmn-bin from path for view_syslog, as it has
#			  moved back to cgi-bin/oms
##################################################################################
use strict;
require "../cg_util.pl";

my $DEBUG = 0;

my($BLACK)  = "#000000";
my($GREEN)  = "#008800";
my($RED)    = "#aa0000";
my($BLUE)   = "#002266";
my($YELLOW) = "#aa4400";
my($TABLE_BGCOLOR)  = "#004466";
my($TABLE_BGCOLOR2) = "#224444";

my($metadata_dir)  = "/webdata/cgsn/data/mdf";
my($metadata_file) = "cgsn_platforms_mdf.txt";

my %arg = &get_args(0);
if (defined $arg{'mdf'}) 
    {$metadata_file = "$arg{'mdf'}";
     }

print "Content-type: text/html\n\n";
print "<html><body bgcolor=#222222 text=#eeeeee>\n";
print "<font face=sans-serif>\n";

#Load cgsn_history file into hash array
my(%Cfg) = &load_cfg("$metadata_dir/$metadata_file");

#
# Get PIDs - including Active and NonActive platform IDs
#
my (%PIDs, %ActivePID, %NonActivePID);
foreach my $key (sort keys %Cfg)
  {my ($pid,$deploy,$tag) = split('\.',$key);
   #print "pid=$pid, deploy=$deploy, tag=$tag  key=$key\n" if $DEBUG;
   next if ($tag ne "status");
   if ($Cfg{"$key"} =~ /Active/i) {$ActivePID{$pid} = 1;}
                          else {$NonActivePID{$pid} = 1;}
   $PIDs{$pid} = 1;
   }


#
# Get Number of Deployments for each platform ID
#
my (%NDeploy);
my ($prev_pid, $prev_deploy);
foreach my $key (sort keys %Cfg)
  {my ($pid,$deploy,$tag) = split('\.',$key);
   if ($prev_pid ne $pid || $deploy ne $prev_deploy) {$NDeploy{$pid}++;}
   $prev_pid = $pid;
   $prev_deploy = $deploy;
   }

#
#Get number of active platfors for each type
#
my %ArrayType_active;
foreach my $type (split(',',$Cfg{'platform.array_types'}))
   {foreach my $active_pid (sort keys %ActivePID)
      {next if ($Cfg{"platform.$type"} !~ $active_pid);
       $ArrayType_active{$type}++;
       }
   }


#------------------------------------------------------------------
# For Debugging
#------------------------------------------------------------------
if ($DEBUG)
  {print "ActivePIDs = ";
   foreach my $pid (sort keys %ActivePID)
      {print "$pid ";
      }
   print "<br>\n";
   print "Non-ActivePIDs = ";
   foreach my $pid (sort keys %NonActivePID)
     {print "$pid ";
      }
   print "<br>\n";
   print "Number of Deploys = ";
   foreach my $pid (sort keys %NDeploy)
     {print "$pid-$NDeploy{$pid}  ";
      }
   print "<br>\n";
   foreach my $type (split(',',$Cfg{'platform.array_types'}))
     {print "$type has $ArrayType_active{$type} active pids<br>\n";
      }
   }
#------------------------------------------------------------------
  
# Get PSS status (ping/rsync)
my $pss_status = `./check_pss mdf=$metadata_file`;
if ($pss_status =~ /ping_OK/i && $pss_status =~ /rsync_OK/i) 
     {$pss_status = "<font color=#00ff00>$pss_status</font>";}
else {$pss_status = "<font color=#ffaa00>$pss_status</font>";}

#------------------- Display Active Platforms by array types --------------------
print "<table border=0 bgcolor=$TABLE_BGCOLOR cellpadding=3 cellspacing=0 width=100% align=center>\n";
print "  <tr><th colspan=6 bgcolor=gray><table width=100%>";
print "      <th align=left width=15%>$pss_status</th>\n";
print "      <th align=center>Summary of Active Platforms</th>\n";
print "    <td align=right width=120><a href=\"edit_status?cfg=$metadata_file\"><font color=#0000cc>Edit StatusFile</font></a></th></table></tr>\n";
foreach my $type (split(',',$Cfg{'platform.array_types'}))
  { next if !defined ($ArrayType_active{$type}); #skip array if there are no active platforms
    print "<tr><td><br><table border=1 bgcolor=$TABLE_BGCOLOR2 cellpadding=3 cellspacing=0 width=90% align=center>\n";
    print "<tr><th colspan=6 bgcolor=#004444>$type</th></tr>\n";
    print "  <tr><th>PlatformID</th><th>DeployID</th><th>Status</th><th>Last Comms</th><th width=20%>Deployment Start Time</th><th width=50%>Synopsis</th></tr>\n";

    foreach my $active_pid (sort keys %ActivePID)
      {#skip acive pid unless it's in the array type being displayed
       next if ($Cfg{"platform.$type"} !~ $active_pid);

       foreach my $key (sort keys %Cfg)
         {my ($pid,$deploy,$tag) = split('\.',$key);
          next if ($Cfg{"$pid.$deploy.status"} ne "Active");
          next if ($pid ne $active_pid || $tag ne "synopsis");
          print "<tr><td valign=top><b><font color=cyan>$pid</font></b><br><font color=#dddddd><i>$Cfg{\"platform.$pid.name\"}</i></font></td>\n";
          print     "<td valign=top align=center><a href=view_syslog?pid=$pid&deploy=$deploy&view=platform_status><font color=cyan>$deploy</a></td> \n";
          print     "<td valign=top align=center>$Cfg{\"$pid.$deploy.status\"}\n";
          my $last_comms = "--:--:--";
          my $dummy;
          if (-e "rtsum_$pid") {($dummy,$last_comms) = split(":",`./rtsum_$pid  pid=$pid deploy=$deploy o=ascii|grep "Time Since Update"`,2);
                                 if ($last_comms =~ /Uptime/) {($last_comms) = split("Uptime",$last_comms),1};
                                 }
          print     "<td valign=top align=center>$last_comms</td>\n";
          print     "<td valign=top><table cellspacing=0 cellpadding=0 width=100%>\n";
          print     "   <tr bgcolor=$GREEN><td>Start: </td><td>$Cfg{\"$pid.$deploy.start\"}</td></tr>\n";
          if ($Cfg{"$pid.$deploy.end"} !~ /Present/ && $Cfg{"$pid.$deploy.end"} ne "")
               {print     "   <tr bgcolor=$BLUE><td>End:   </td><td>$Cfg{\"$pid.$deploy.end\"}</td></tr>\n";
                }
          my $duration = "";
          if ($Cfg{"$pid.$deploy.start"} =~ /^2/ && ($Cfg{"$pid.$deploy.end"} =~ /^2/ || $Cfg{"$pid.$deploy.end"} =~ /Present/i))
             {my $start_sec = &dsltime2sec($Cfg{"$pid.$deploy.start"});
              my $end_sec;
              if ($Cfg{"$pid.$deploy.end"} =~ /Present/i) {$end_sec = time();}
                                                     else {$end_sec = &dsltime2sec($Cfg{"$pid.$deploy.end"});}
              my $dtime = $end_sec - $start_sec;
              if ($dtime >= 0) {$duration = &sec2hhmmss($dtime);}
                          else {$duration = "--:--:--";}
              }
          print     "   <tr bgcolor=#666666><td>Duration:   </td><td>$duration</td></tr>\n";
          print     "</table></td>\n";
          print     "<td valign=top>$Cfg{\"$pid.$deploy.synopsis\"}&nbsp</td>\n";
          print "</tr>";
          }
       } #end-forach active pid
    print "</table><br>\n";
    } #end-foreach array type
print "</table>\n";
#-----------------------------------------------------------------


#------------------- Display Status for All Platforms --------------------
print "<br><br><br>\n";
print "<table border=0 bgcolor=$TABLE_BGCOLOR cellpadding=3 cellspacing=0 width=100% align=center>\n";
print "<tr><th xxcolspan=6 bgcolor=gray>Summary of All CGSN Platforms</th></tr>\n";
foreach my $type (split(',',$Cfg{'platform.array_types'}))
  { print "<tr><td><br><table border=1 bgcolor=$TABLE_BGCOLOR2 cellpadding=3 cellspacing=0 width=90% align=center>\n";
    print "<tr><th colspan=5 bgcolor=#004444>$type</th></tr>\n";
    print "  <tr><th>PlatformID</th><th>DeployID</th><th>Status</th><th width=20%>Deployment Start/End Times</th><th width=50%>Synopsis</th></tr>\n";
   foreach my $my_pid (sort keys %PIDs)
    {#skip the pid unless it's in the array type being displayed
     next if ($Cfg{"platform.$type"} !~ $my_pid);


     foreach my $key (sort keys %Cfg)
       {my ($pid,$deploy,$tag) = split('\.',$key);
        next if ($pid ne $my_pid || $tag ne "synopsis");
        if ($deploy eq "D0001" || $deploy eq "D00001")
            {print "<tr><td valign=top rowspan=$NDeploy{$pid}><b><font color=cyan>$pid</font></b><br><font color=#dddddd><i>$Cfg{\"platform.$pid.name\"}</i></font></td>\n";}
        else
          {print "<tr>\n";}
        print     "<td valign=top align=center><a href=view_syslog?pid=$pid&deploy=$deploy&view=platform_status><font color=cyan>$deploy</a></td> \n";
        print     "<td valign=top>$Cfg{\"$pid.$deploy.status\"}</td> \n";
        print     "<td valign=top><table cellspacing=0 cellpadding=0 width=100%>\n";
        print     "   <tr bgcolor=$GREEN><td>Start: </td><td>$Cfg{\"$pid.$deploy.start\"}</td></tr>\n";
        print     "   <tr bgcolor=$BLUE><td>End:   </td><td>$Cfg{\"$pid.$deploy.end\"}</td></tr>\n";
          my $duration = "";
          if ($Cfg{"$pid.$deploy.start"} =~ /^2/ && ($Cfg{"$pid.$deploy.end"} =~ /^2/ || $Cfg{"$pid.$deploy.end"} =~ /Present/i))
             {my $start_sec = &dsltime2sec($Cfg{"$pid.$deploy.start"});
              my $end_sec;
              if ($Cfg{"$pid.$deploy.end"} =~ /Present/i) {$end_sec = time();}
                                                     else {$end_sec = &dsltime2sec($Cfg{"$pid.$deploy.end"});}
              my $dtime = $end_sec - $start_sec;
              if ($dtime >= 0) {$duration = &sec2hhmmss($dtime);}
                          else {$duration = "--:--:--";}
              }
          print     "   <tr bgcolor=#666666><td>Duration:   </td><td>$duration</td></tr>\n";
        print     "</table></td>\n";
        print     "<td valign=top>$Cfg{\"$pid.$deploy.synopsis\"}&nbsp</td>\n";
        print "</tr>";
        }
     } #end-forach non-active pid
   print "</table><br>\n";
   } #end-foreach array type
print "</table>\n";
print "</body>\n</html>\n";
#-----------------------------------------------------------------
#print "<iframe name=DisplayIframe src=\"edit_cfg?cfg=$metadata_file\" height=900px width=850px scrolling=auto frameborder=0></iframe>\n";

exit;

