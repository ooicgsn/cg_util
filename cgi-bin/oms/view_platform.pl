#!/usr/bin/perl 
#!c:/Perl64/bin/perl 
###############################################################################
#
# view_platform.pl - Steven Lerner 01/2010
#                    Woods Hole Oceanographic Institution
#
# Description: Web-based user interface for OOI/CGO Platforms/Instruments
#
# Web Usage: view_platform.pl?cfg=platform_cfg_file
#
# History:
#       Date       Who    Description
#       ----       ---    ----------------------------------------------------
#     01/29/2010   SL     Create
#     08/14/2012   SL     Updated to delineate sensors on buoy, nsif, mfn,
#                         profiler, dock
#     10/03/2012   SL     Added support for sensors on inductive modem (imm)
#                         Use new updated platform configuration files (v3)
#     10/17/2012   SL     Relocated to /webdata/cgsn/omc/platforms
#     08/16/2016   JJP	  Updated path to Config files
#     08/31/2016   JJP	  Updated path to Instruments
#     09/26/2016   JJP    Removed oms_sw from path to platcon_configs
#     09/27/2016   JJP    Made path to platcon_configs relative
#     04/10/2017   CC     Removed AUV Dock code
###############################################################################
require "getopts.pl";
require "flush.pl";
require "timelocal.pl";

my($VERSION) = "v3.0-20121003";
my($DEBUG)   = 0;



#
# Create key/value parameter pairs based on the request method
#
if ($ENV{'REQUEST_METHOD'} eq "POST")
     {read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
      @pairs = split(/&/, $buffer);
      } 
elsif ($ENV{'REQUEST_METHOD'} eq "GET")
     {@pairs = split(/&/, $ENV{'QUERY_STRING'});
      $buffer = "$ENV{'QUERY_STRING'}";
      }

elsif ($#ARGV >= 0) {@pairs = @ARGV;}

#
# Put the keys and values in an associative array.
# The value of a form variable can then be referenced
# as $form{variable_name}
#
foreach $pair (@pairs) {
    ($key, $value) = split(/=/, $pair,2);
    $key =~ tr/+/ /;
    $key =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $key =~ tr/\cM/\n/;
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9]{2})/pack("C", hex($1))/eg;
    $value =~ tr/\cM/\n/;
    $arg{$key} = $value;
}

#
#Check args and give usage
#
if (0 &&! $arg{'cfg'})
   {print "Content-type: text/html\n\n";
    print "$0 - $VERSION\n";
    print "Usage: $0 <cfg=cfg_file>\n";
    exit;
    }
if ($arg{'debug'} eq "1") {$DEBUG = 1;}

print "Content-type: text/html\n\n";

#Give list of availabe CFGs
opendir(DIR,"../../../platcon_configs/platform_cfgs");
(@flist) = sort grep(/\.cfg$/,readdir(DIR));
closedir(DIR);
if ($arg{'cfg'} eq "") {#default to first on the list
                        $arg{'cfg'} = "$flist[0]";
			}
$cfg_list = "";
foreach $f (@flist)
      {chomp($f);
       my($name) = $f; $name =~ s/\.cfg//;
       if ($f eq $arg{'cfg'})
            {$cfg_list .= "<OPTION value=\"$f\" SELECTED>$name</OPTION>\n";}
       else {$cfg_list .= "<OPTION value=\"$f\">$name</OPTION>\n";}
       }

#print "Content-type: text/html\n\n";
print "<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">\n";
print <<"EndComment";
<!-- 
        #Interactive OOI/CGO Platform Instruments Display
        #(c)2010 Deep Submergence Laboratory - SL
        #Woods Hole Oceanographic Institution
-->
EndComment
flush(STDOUT);

#
#Handle buttons here
#


#
# Read configuration file - use last customized one if available
#
my(%CfgLine); #to get att/val inorder when necessary
$CfgFile = "../../../platcon_configs/platform_cfgs/$arg{'cfg'}";
%Cfg = load_cfg("$CfgFile");
if (!defined %Cfg) {exit(-1);}


#Load in the instrument port mappings
my(%InstPort) = map_inst_ports();



$fnt1 = "<font color=#eeeeee face=sans-serif size=2><b>";
#$fnt2 = "<font color=#0088aa face=sans-serif size=2><b>";
$fnt2 = "<font color=#00aadd face=sans-serif size=2><b>";
$fnt3 = "<font color=#00aadd face=sans-serif size=2><b>";
$fnt4 = "<font color=#dddddd face=sans-serif size=1><b>";

print <<"EndHTML";
<html>
<head>
<title>CGO Platform and Instrument Display</title>
<script language="JavaScript">
    function updateCfgForm(form)
       {cfgFile = form.ConfigFile.options[form.ConfigFile.selectedIndex].value;
        window.location.href = "?cfg=" + cfgFile;
        }
</script>

<style>
a:link{ text-decoration: none;
	color: #00aadd;
        }
	
a:visited{ text-decoration: none;
	  color: #00aadd;
          }

a:hover{ text-decoration: underline;
	color: #dddd00;
	font-weight: bolder;
	letter-spacing: 2px;
        }
</style>
</head>

<body bgcolor=#0088aa xxxOnLoad=window.focus()>

<form method="post">

<center>
<table bgcolor=#004466 border=0 cellspacing=2 cellpadding=0 width=90% height=800>
        <tr><td colspan=2 align=center>$fnt3 CGSN Platforms and Instruments - $VERSION</td>
            <td rowspan=3 valign=top><img src=/oms-html/images/whoilogo-small-trans.gif>
	</tr>
  <tr><td valign=top>
       <table cellspacing=0 cellpadding=0 border=0 width=95%>
          <td><table width=160><td>$fnt1 Select Platform:</font> <td>
<SELECT NAME="ConfigFile" onChange="updateCfgForm(this.form)">
     $cfg_list
              </table>
</SELECT>
</form>
     <td>   <table border=1><td>
              <table><tr><td>$fnt1 ID: </font> $fnt3 $Cfg{'Platform.id'}</font> &nbsp
                                    $fnt1 Desc: </font> $fnt3 $Cfg{'Platform.desc'}</font> &nbsp </td>
                         <td>$fnt1 Location: </font> $fnt3 $Cfg{'Deploy.location'}</font></td></tr> 
                   <tr><td>$fnt1 Lat: </font> $fnt3 $Cfg{'Deploy.lat'}</font> &nbsp 
                           $fnt1 Lon: </font> $fnt3 $Cfg{'Deploy.lon'}</font> &nbsp
                           $fnt1 WDepth: </font> $fnt3 $Cfg{'Deploy.wdepth'} m</font></td>
                       <td>$fnt1 Status: </font> $fnt3 $Cfg{'Deploy.status'}</font></td></tr>
              </table>
            </table>
          </td>
  </tr>	  
  <tr>
  <td colspan=1><table cellspacing=0 cellpadding=0>
        <td>$fnt1 View: </font> &nbsp 
        <td>$fnt1 <a href=?cfg=$arg{'cfg'}&show=drawing> <u>Drawing</u></a> &nbsp 
            <a href=?cfg=$arg{'cfg'}&show=cfg> <u>Cfg</u></a>  &nbsp 
	    <br><a href=?cfg=$arg{'cfg'}&show=gmap> <u>Map</u></a>
	    </font>
	</table>
  </td>
  
<!--
   <td colspan=10>&nbsp &nbsp &nbsp $fnt1 Inst: </font> $fnt2 $arg{'inst'} </font>
               &nbsp &nbsp $fnt1 SerialNumber:</font> $fnt2 $Cfg{"Sensor.$arg{'inst'}.serialNum"}$Cfg{"Hotel.$arg{'inst'}.serialNum"} </font>
	       &nbsp &nbsp $fnt1 Depth: </font> $fnt2 $Cfg{"Sensor.$arg{'inst'}.depth"}$Cfg{"Hotel.$arg{'inst'}.depth"}  </font>
-->

  </tr>

<tr><td width=225 valign=top>
<div id="Instruments" style="position:relative; height:1000px; width:175px; top:15px; left:20px; overflow: auto">
<xxtable bgcolor=#004466 border=1 cellspacing=3 cellpadding=0 width=100%>
EndHTML


        #List each HotelLoad specified in the config file
	print "<table border=1 cellspacing=0 cellpadding=1 width=100%><td>\n";
        print "<table cellspacing=0 cellpadding=0 border=0 width=100%>\n";
        print "<tr><th bgcolor=gray colspan=2>Hotel</th></tr>\n";
        (@buoy_hotel_list) = split(',',$Cfg{'Platform.buoy.hotel_cfg'});
        (@nsif_hotel_list) = split(',',$Cfg{'Platform.nsif.hotel_cfg'});
        (@mfn_hotel_list)  = split(',',$Cfg{'Platform.mfn.hotel_cfg'});
        foreach $name (sort @buoy_hotel_list)
              {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$name>$name</a> </td><td>$fnt4 <i>(buoy)</i></td></tr>\n";
               }
        foreach $name (sort @nsif_hotel_list)
              {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$name>$name</a> </td><td>$fnt4 <i>(nsif)</i></td></tr>\n";
               }
        foreach $name (sort @mfn_hotel_list)
              {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$name>$name</a> </td><td>$fnt4 <i>(mfn)</i></td></tr>\n";
               }
         print "</table>\n";
         print "</table>\n";



	#----------------------------------------------------------------
        #List each Instrument specified (buoy, nsif, mfn, imm) in the config file
	#----------------------------------------------------------------
	
	print "<br><table border=1 cellspacing=0 cellpadding=1 width=100%>\n";
	print "<th bgcolor=gray>Instruments</th>\n";
        #List each Instrument (buoy) specified in the config file
        (@buoy_inst_list) = split(',',$Cfg{'Platform.buoy.inst_cfg'});
        (@nsif_inst_list) = split(',',$Cfg{'Platform.nsif.inst_cfg'});
        (@mfn_inst_list)  = split(',',$Cfg{'Platform.mfn.inst_cfg'});
        (@wfp_inst_list)  = split(',',$Cfg{'Platform.wfp.inst_cfg'});
        (@imm_inst_list)  = split(',',$Cfg{'Platform.imm.inst_cfg'});
	if ($#buoy_inst_list >= 0)
          {print "<tr><td>";
	   print "<table cellspacing=1 cellpadding=0 border=0 width=100%>\n";
           print "<tr><th>$fnt1 Buoy</th></tr>\n";
           print "<td>\n";
           foreach $name (sort @buoy_inst_list)
              {my($port_str) = $InstPort{$name};
	       if ($port_str ne "") {$port_str = "($port_str)";}
	       print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$name>$name</a></td><td>$fnt4 <i>$port_str</i></td></tr>\n";
               }
           print "</tr><tr><td></table>\n";
	   }

	if ($#nsif_inst_list >= 0)
          {print "<tr><td>";
           print "<table cellspacing=1 cellpadding=0 border=0 width=100%>\n";
           print "<tr><th>$fnt1 NSIF</th></tr>\n";
           print "<td>\n";
           foreach $name (sort @nsif_inst_list)
              {my($port_str) = $InstPort{$name};
	       if ($port_str ne "") {$port_str = "($port_str)";}
               print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$name>$name</a></td><td>$fnt4 <i>$port_str</i></td></tr>\n";
               }
           print "</tr><tr><td></table>\n";
	   }


	if ($#imm_inst_list >= 0)
          {print "<tr><td>";
           print "<table cellspacing=1 cellpadding=0 border=0 width=100%>\n";
           print "<tr><th>$fnt1 IMM</th></tr>\n";
           print "<td>\n";
           foreach $name (sort @imm_inst_list)
              {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$name>$name</a></td></tr>\n";
               }
           print "</tr><tr><td></table>\n";
	   }

	if ($#wfp_inst_list >= 0)
          {print "<tr><td>";
           print "<table cellspacing=1 cellpadding=0 border=0 width=100%>\n";
           print "<tr><th>$fnt1 Profiler</th></tr>\n";
           print "<td>\n";
           foreach $name (sort @wfp_inst_list)
              {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$name>$name</a></td></tr>\n";
               }
           print "</tr><tr><td></table>\n";
	   }

	if ($#mfn_inst_list >= 0)
          {print "<tr><td>";
           print "<table cellspacing=1 cellpadding=0 border=0 width=100%>\n";
           print "<tr><th>$fnt1 MFN</th></tr>\n";
           print "<td>\n";
           foreach $name (sort @mfn_inst_list)
              {my($port_str) = $InstPort{$name};
	       if ($port_str ne "") {$port_str = "($port_str)";}
               print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$name>$name</a></td><td>$fnt4 <i>$port_str</i></td></tr>\n";
               }
           print "</tr><tr><td></table>\n";
	   }
	# End Instrument Section
	print "</table>\n";
	#--------------------------------------------------------------------------


        #List each Power Generation unit specified in the config file
	print "<br><table border=1 cellspacing=0 cellpadding=1 width=100%><td>\n";
        print "<table cellspacing=0 cellpadding=0 border=0 width=100%>\n";
        print "<tr><th bgcolor=gray>Power Gen</th></tr>\n";
        print "<td>\n";
        if ($Cfg{'Platform.power_cfg'} =~ /wind/i && $Cfg{'PowerCfg.WindPwr.mfg'} ne "")
                 {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$Cfg{'PowerCfg.WindPwr.mfg'}>Wind Turbine</a></td></tr>\n";
                  }
        if ($Cfg{'Platform.power_cfg'} =~ /solar/i && $Cfg{'PowerCfg.SolarPwr.mfg'} ne "")
                 {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$Cfg{'PowerCfg.SolarPwr.mfg'}>Solar PV</a></td></tr>\n";
                  }
        if ($Cfg{'Platform.power_cfg'} =~ /battery/i && $Cfg{'PowerCfg.BattPwr.mfg'} ne "")
                 {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$Cfg{'PowerCfg.BattPwr.mfg'}>Battery</a></td></tr>\n";
                  }
        if ($Cfg{'Platform.power_cfg'} =~ /fuelcell/i && $Cfg{'PowerCfg.FuelCell.mfg'} ne "")
                 {print "<tr><td>$fnt2 <a href=?cfg=$arg{'cfg'}&inst=$Cfg{'PowerCfg.FuelCell.mfg'}>FuelCell</a></td></tr>\n";
                  }
        print "</tr>\n";


print <<"EndHTML";
      </table>
</table>
</div>

<td valign=top colspan=2>
<!--
   <xxdiv id="Results" style="position:relative; height:200px; width:300px; top:15px; left:0px; xxoverflow: auto"
-->


EndHTML

  if ($arg{'inst'} ne "")
     {print "<iframe name=DisplayIframe src=\"view_inst.pl?inst=$arg{'inst'}&if=../../../platforms/Instruments/$arg{'inst'}/inst_info.cfg&t=INST_VIEW.TEMPLATE\" height=525px width=750px scrolling=auto frameborder=0></iframe>\n";
      }
  elsif ($arg{'show'} eq "gmap")
     {print <<"EndHTML";
<iframe width="600" height="480" frameborder="0" scrolling="no" marginheigh
t="0" marginwidth="0" src="http://maps.google.com/?q=$Cfg{'Deploy.lat'},$Cfg{'Deploy.lon'}&amp;num=1&amp;ie=UTF8&amp;t=h&amp;ll=$Cfg{'Deploy.lat'},$Cfg{'Deploy.lon'}&amp;z=6&amp;om=1&amp;output=embed"></iframe>
EndHTML
      }
  elsif ($arg{'show'} eq "drawing" || $arg{'show'} eq "")
     {print "<a href=/cgo/platforms/Drawings/$Cfg{'Platform.drawing'}><img src=/cgo/platforms/Drawings/$Cfg{'Platform.drawing'} width=600 border=0></a>\n";
      }
  elsif ($arg{'show'} eq "cfg" || $arg{'show'} eq "")
     {print "<iframe name=DisplayIframe src=\"edit_cfg?cfg=$arg{'cfg'}\" height=900px width=850px scrolling=auto frameborder=0></iframe>\n";
      }
   print "</iframe>\n";
print <<"EndHTML";
</div>
EndHTML



if ($DEBUG)
  {foreach $key (sort keys %Cfg) {print "$key = $Cfg{$key}<br>\n";}
   foreach $key (sort keys %ENV) {print "$key = $ENV{$key}<br>\n";}
   }

print "</body>\n";
print "</html>\n";


exit;

#
# Subroutines Follow
#
sub load_cfg {my($file) = $_[0];
   my(%Cfg,$line);
   if (!open($CFG_FD,"$file")) {print "***Unable to open config file [$file]";
                                return;
                                }
  while ($line = <$CFG_FD>)
      {next if ($line =~ /^#/ || length($line) < 2); #skip comments & blank lines
       chomp($line);
       my($att,$val) = split("=",$line,2);
       ($val) = split("#",$val); #remove end-comments
       $att =~ s/^(\s+)//; #remove leading spaces
       $att =~ s/(\s+)$//; #remove trailing spaces
       $val =~ s/^(\s+)//; #remove leading spaces
       $val =~ s/(\s+)$//; #remove trailing spaces
       $Cfg{"$att"} = "$val";
       #print "$att=[$val]\n" if $DEBUG;
       }
   close($CFG_FD);
   return %Cfg;
}

sub map_inst_ports {
   my($key,%InstPort);
   #Loop through all dcl/stc ports and map inst name to dcl/stc port number
   foreach $key (sort keys %Cfg) 
     {next if ($key !~ /^stc/i && $key !~ /^dcl/i);
      next if ($key !~ /inst$/); #only want dcl/stcN.portN.inst
      my($dcl_stc,$port_num) = split('\.',$key);
      ($port_num) =~ s/port//;  #strip-off "port"
      $InstPort{$Cfg{$key}} = "$dcl_stc\P$port_num";
      #print "$key = [$Cfg{$key}] &nbsp Inst[$Cfg{$key}]=$InstPort{$Cfg{$key}}<br>\n";
      }
    return %InstPort;

}
