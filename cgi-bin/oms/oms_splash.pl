#!/usr/bin/perl
###############################################################################
#
# oms_splash.pl - sl 6/2005
#
# Description: Displays ship splash page
# Usage: om_splash.pl ship=[ship_name] [vehicle=veh_name]
#
# History:
#       Date       Who     Description
#       ----       ---     ----------------------------------------------------
#     06/25/2005   SL      Create
#     10/17/2005   SL      Renamed from dv_splash.pl to om_splash.pl
#     01/23/2006   SL      Cloned from om_splash
#     02/07/2012   SL      Added UW map credit for basemap
#     10/18/2012   SL      Updated links to use oms instead of omc
#     02/05/2015   SL      Added active deployment statistics
#     08/09/2016  JJP	   Updated path to cg_util.pl for restructure in Bitbucket
###############################################################################
require "../cg_util.pl";
require "flush.pl";

my $metadata_file = "/webdata/cgsn/data/mdf/cgsn_platforms_mdf.txt";

#Load cgsn_history file into hash array
my(%Cfg) = &load_cfg("$metadata_file");

#
# Get PIDs - including Active and NonActive platform IDs
#
my (%PIDs, %ActivePID, %NonActivePID);
foreach my $key (sort keys %Cfg)
  {my ($pid,$deploy,$tag) = split('\.',$key);
   #print "pid=$pid, deploy=$deploy, tag=$tag  key=$key\n" if $DEBUG;
   next if ($tag ne "status");
   if ($Cfg{"$key"} =~ /Active/i) {$ActivePID{$pid} = 1;}
                          else {$NonActivePID{$pid} = 1;}
   $PIDs{$pid} = 1;
   }


#
# Get Number of Deployments for each platform ID
#
my (%NDeploy);
my ($prev_pid, $prev_deploy);
foreach my $key (sort keys %Cfg)
  {my ($pid,$deploy,$tag) = split('\.',$key);
   if ($prev_pid ne $pid || $deploy ne $prev_deploy) {$NDeploy{$pid}++;}
   $prev_pid = $pid;
   $prev_deploy = $deploy;
   }

#
#Get number of active platfors for each type
#
my %ArrayType_active;
foreach my $type (split(',',$Cfg{'platform.array_types'}))
   {foreach my $active_pid (sort keys %ActivePID)
      {next if ($Cfg{"platform.$type"} !~ $active_pid);
       $ArrayType_active{$type}++;
       }
   }


my $stat_str = "";
foreach my $type (split(',',$Cfg{'platform.array_types'}))
 {my $type_str = $type; $type_str =~ s/_/ /; #elim underscore
  if (defined($ArrayType_active{$type})) {$stat_str .= "CGSN $type_str has $ArrayType_active{$type} active moorings<br>\n";}
  }


#$image_url  = "/oms-html/images/ooi_station_map.jpg";
$image_url  = "/oms-html/images/splash_as02.png";
$map_url    = "/oms-bin/plot_gmap";
$search_url = "om_search_oms";
$title      = "OOI/CGSN OMS System";

print "Content-type: text/html\n\n";

print <<"EndHTML";

<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<HTML>
<HEAD><TITLE>OOI/CGSN OMS Splash Page</TITLE>
<!--
   WHOI OOI/CGSN OMS System
   (c) Deep Submergence Laboratory - SL
   Woods Hole Oceanographic Institution
-->
</HEAD>
<body bgcolor=#0088aa>
<center><font color=white size=+1>$title</font><br>
        <font color=#004466><b>$stat_str</b></center><br>
	
<font face=sans-serif color=222222 size=2>
<i>
These data were collected by the Ocean Observatory Initiative (OOI) project purely for internal system <br>
development purposes during the construction phase of the project and are offered for release to the public <br>
with no assurance of data quality, consistency, or additional support.  The OOI Program assumes no liability <br>
resulting from the use of these data for other than the intended purpose.
</i>
<br><br>
<font face=sans-serif color=white size=2>
<b>
From the menu on the left, select an Array type and click on the Platform's Detail Status link to view status and data.

<br><br><br>
<table><td valign=top><font face=sans-serif color=white size=2><b>Note: </b></font>
       <td valign=top><font face=sans-serif color=white size=2><b>It is <i>strongly</i> recommended that either Firefox, Safari, or the 
          Chrome web-browser be used as the <br>interactive data plots may not render properly with Internet Explorer</b></font>
</table>
<br><br>
<img src="$image_url" height=800 hspace=120>
<!-- 
   <iframe name=main width="100%" height="90%" frameborder=0 src=$map_url></iframe>
 -->

<br>
<center><table width=80%><td><font color=222222 size=1>Map graphic courtesy of the OOI Regional Scale Nodes
program and the Center for Environmental Visualization, University of Washington.</font>
</td></table>
</center>
<br>
<font color=white size=1> This website and contents copyrighted (c) 2011 Woods Hole Oceanographic Institution. All rights reserved.</font>
</body>
</html>

EndHTML

flush(STDOUT);

