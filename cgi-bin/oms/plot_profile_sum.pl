#!/usr/bin/perl
# plot_profile_sum.pl -  Quick script to generate mmp eng sum 
#
# Usage: plot_profiler_sum.pl pid= deploy= [limit=] [date=]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      05/20/2015  SL     Create
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
#      08/19/2016  JJP	  Fixed path to bin/mpp_unpack, it moved up one level
##################################################################################
use strict;
require "../cg_util.pl";

my $pid    = "pid_not_specified";
my $deploy = "deploy_not_specified";
my $limit  = 3000;

my %arg = &get_args(0);

if ($arg{'pid'}    ne "") {$pid    = $arg{'pid'};}
if ($arg{'deploy'} ne "") {$deploy = $arg{'deploy'};}
if ($arg{'limit'}  ne "") {$limit  = $arg{'limit'};}

my @data = &gen_mmp_psum($pid,$deploy,0);

my $NUM_FIELDS = 14;
my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>

<table><td>
       <div id="div_g" style="width:800px; height:300px;"></div></td>
       </tr><tr>
       <td align=center><div id="div_label" style="width:600px; font-size:1.0em; padding-top:5px;"></div></td>
</table>
<br><form name="myform" method="post">
<table cellpadding=8 border=0><td width=100 align=right valign=middle><b>Plot:</b></td><td>
   <input type=radio id="0" name="plot" onClick="change_radio(this,$NUM_FIELDS,'profile')" CHECKED>profile &nbsp </input>
<!--
   <input type=radio id="1" name="plot" onClick="change_radio(this,$NUM_FIELDS,'sdate')" >sdate &nbsp </input>
   <input type=radio id="2" name="plot" onClick="change_radio(this,$NUM_FIELDS,'stime')" >stime &nbsp </input>
   <input type=radio id="3" name="plot" onClick="change_radio(this,$NUM_FIELDS,'edate')" >edate &nbsp </input>
   <input type=radio id="4" name="plot" onClick="change_radio(this,$NUM_FIELDS,'etime')" >etime &nbsp </input>
-->
   <input type=radio id="5" name="plot" onClick="change_radio(this,$NUM_FIELDS,'Minutes')" >dur &nbsp </input>
   <input type=radio id="6" name="plot" onClick="change_radio(this,$NUM_FIELDS,'mA')" >min-batC &nbsp </input>
   <input type=radio id="7" name="plot" onClick="change_radio(this,$NUM_FIELDS,'mA')" >max-batC &nbsp </input>
   <input type=radio id="8" name="plot" onClick="change_radio(this,$NUM_FIELDS,'mA')" >ave-batC &nbsp </input>
   <input type=radio id="9" name="plot" onClick="change_radio(this,$NUM_FIELDS,'Volts')" >min-batV &nbsp </input>
   <input type=radio id="10" name="plot" onClick="change_radio(this,$NUM_FIELDS,'Volts')" >max-batV &nbsp </input>
   <input type=radio id="11" name="plot" onClick="change_radio(this,$NUM_FIELDS,'Volts')" >ave-batV &nbsp </input>
   <input type=radio id="12" name="plot" onClick="change_radio(this,$NUM_FIELDS,'dbar')" >min-press &nbsp </input>
   <input type=radio id="13" name="plot" onClick="change_radio(this,$NUM_FIELDS,'dbar')" >max-press &nbsp </input>
<!--
   <input type=radio id="14" name="plot" onClick="change_radio(this,$NUM_FIELDS,'dbar')" >ave-press &nbsp </input>
-->
<br></td></table>
</form>

<script type="text/javascript">

  g = new Dygraph(
    document.getElementById("div_g"),
EndHTML
    my $hdr_line = $data[0]; $hdr_line =~ s/(\s+)/ /g; $hdr_line =~ s/ /,/g; $hdr_line =~ s/,$//;
    $html_out .= "\"sample,$hdr_line\\n\" +\n";
    my ($line, $i);
    for ($i=1;$i<$#data;$i++)
       {$line = $data[$i];
        $line =~ s/(\s+)/ /g; $line =~ s/ /, /g; $line =~ s/, $//;
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        if ($i < $#data-1){$html_out .= "\"$i, $line\\n\" +\n";}
                          else {$html_out .= "\"$i, $line\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
            { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: true,
               fillGraph: false,
               title: 'Profile Summary',
               ylabel: '',
               xlabel: 'Profile Number',
               legend: 'always',
               labelsDiv: document.getElementById("div_label"),
               yAxisLabelWidth: 60
               // valueRange: [500,0]
              }          // options
           ); // end-Dygraph

    for (var i=1; i <= $NUM_FIELDS; i++)
          {
           g.setVisibility(i, false);
}
g.updateOptions({ ylabel: 'Profile Number'});
      setStatus();


      function setStatus() {
return;        document.getElementById("visibility").innerHTML =
          g.visibility().toString();
      }

      function set_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = true;
           g.setVisibility(i, true);
	   }
        setStatus();
      }

      function clear_checkbox(el) {
	for (var i=0; i < el.length; i++)
	  {el[i].checked = false;
           g.setVisibility(i, false);
	   }
        setStatus();
      }


      function change_checkbox(el) {
        g.setVisibility(parseInt(el.id), el.checked);
        setStatus();
      }

      function change_radio(el,num_buttons,label) {
        var id = parseInt(el.id);
        g.setVisibility(id, true);
        for (var i=0; i <= num_buttons; i++)
          {if (i != id)
             {g.setVisibility(i, false);
              }
           }
         g.updateOptions({ ylabel: label});

        setStatus();
      }

</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

#
# Subroutines Follow
#


#gen_mmp_psum2.pl - Generate mmp profile summary. Takes in McClane profile mmp 
#                   engineering E*.DAT files and generates CSV min/max/ave records 
#                   of start_time, end_time, duration, batC_ma, batV_V, press_dbar 
#                   for each profile.
sub gen_mmp_psum {
   my($pid)    = $_[0];
   my($deploy) = $_[1];
   my($DEBUG)  = $_[2];

   my($max_vars) = 100;
   my($minmax_flag) = 1;

   my $cols = "1,2,3,4,5";

   my($fd, @recs, @vars, @vmin, @vmax, @vave, @vtot, @vrcnt, $date, @dates, $date_time, $hour, $minute, $var);
   my($mmp_eng_dir, @flist, $file);
   my($i,$j);
   my(@col_list) = split(',',$cols);
   my @out_fd;

   my $mmp_eng_dir = "/webdata/cgsn/data/raw/$pid/$deploy/imm/mmp";
   if (! -d "$mmp_eng_dir") {die "***Unable to readdir [$mmp_eng_dir], exiting\n";}

   if (!opendir($fd,"$mmp_eng_dir"))
       {print "***Unable to readdir [$mmp_eng_dir], exiting\n";
        exit;
        }
   (@flist) = sort grep (/^E/,readdir($fd)); #want E*.DAT files
   closedir($fd);

   print "Ddir[$mmp_eng_dir], Flist=[@flist]\n" if ($DEBUG);

   my($start_profile) = -1;

   #initialize vmin, vmax, vtot - currently handle up to n-variables
   for ($i=0;$i<$max_vars;$i++) {$vmin[$i] = 9999999; $vmax[$i] = -9999999; $vtot[$i]=0;}

   my($rec_cnt) = 0;
   my($first_time) = 1;
   foreach $file (@flist)
     {next if ($file !~ /^E/); #make sure E*.dat
      #Extract profile number from filename Ennnnnn.DAT
      my($pnum); $pnum = substr($file,1,length($file)-5);
      open($fd,"../../bin/mmp_unpack stdout $mmp_eng_dir/$file|") || die "***Unable to open/unpack [$mmp_eng_dir/$file], exiting\n";
      (@recs) = <$fd>;
      close($fd);

      next if ($start_profile != -1 && $pnum <= $start_profile);
      print "Processing $arg{'pid'} - $file...\n" if $DEBUG;
      my $hdr_rec_num = 7;
      my $start_data_rec = $hdr_rec_num + 1;
      my $end_data_rec   = $#recs - 6;
      my($hdr_line) = "$recs[$hdr_rec_num]"; chomp($hdr_line);
      $hdr_line = "date time batC_ma batV_v press_dbar par_mv scatSig chlSig CDOMSig";
      if ($cols ne "")
        {#only put specified cols into hdr_line
         my($col);
         my($field_name_subset) = "";
         my(@hdr_fields) = split(' ',$hdr_line);
         foreach $col (@col_list)
           {$field_name_subset .= "$hdr_fields[$col-1] ";
            }
         $hdr_line = $field_name_subset;
         }

      #Update hdr_line to include min/max cols
      my(@hdr_fields) = split(' ',$hdr_line);
      my($new_hdr_line) = "profile sdate stime edate etime dur_min ";
      if ($minmax_flag)
         {for ($j=2;$j<=$#hdr_fields;$j++)
            {$new_hdr_line .= "min-$hdr_fields[$j]  max-$hdr_fields[$j]  ave-$hdr_fields[$j]  ";
             }
          $hdr_line = "$new_hdr_line";
	  }

      if ($first_time) {$first_time = 0; push(@out_fd, "$hdr_line\n");}

      for ($i=$start_data_rec;$i<=$end_data_rec;$i++)
        {next if ($recs[$i] =~ /^#/); #skip comments
         next if ($recs[$i] =~ /\[/); #skip metadata
         next if ($recs[$i] =~ /BAD_DATA/); #skip BAD Data
         next if ($recs[$i] =~ /NaN NaN NaN NaN NaN NaN NaN/i);
         chomp($recs[$i]);

         $recs[$i] =~ s/(\s+)/ /gs; #remove extra spaces
         $recs[$i] =~ s/^ //; #remove leading space
         (@vars) = split(' ',$recs[$i]);
         $rec_cnt++;

         my ($mm,$dd,$yyyy) = split('/',$vars[0]);
         $date      = "$yyyy/$mm/$dd";
         $date_time = "$date $vars[1]";
         ($hour,$minute) = split(":",$vars[1]);
    
         for ($j=2;$j<=$#vars;$j++)
           {$vars[$j] =~ s/^\*//; #eliminate leading *
            next if ($vars[$j] =~ /nan/i);
	    next if ($vars[$j] == 0); #don't average in 0-value nor use for min/max
            if ($vars[$j] < $vmin[$j]) {$vmin[$j] = $vars[$j];}
            if ($vars[$j] > $vmax[$j]) {$vmax[$j] = $vars[$j];}
            $vtot[$j] += $vars[$j];
	    if ($vars[$j] != 0) {$vrcnt[$j]++;}
            }
         }
    
       #calculate the averages
         for ($j=2;$j<=$#vars;$j++) 
           {if ($vrcnt[$j] > 0) {$vave[$j] = $vtot[$j]/$vrcnt[$j];}
                           else {$vave[$j] = "nan";}
            }
             
         #print "$recs[$i]\n";

       
     #output profile summary
     my($temp_date,$temp_time,$dd,$mm,$yyyy);
     my($stime) = substr($recs[$start_data_rec],0,20);
     ($temp_date,$temp_time) = split(' ',$stime);
     ($mm,$dd,$yyyy) = split('/',$temp_date);
     $temp_date = "$yyyy/$mm/$dd";
     $stime = "$temp_date $temp_time";
     my($etime) = substr($recs[$end_data_rec],0,20);
     ($temp_date,$temp_time) = split(' ',$etime);
     ($mm,$dd,$yyyy) = split('/',$temp_date);
     $temp_date = "$yyyy/$mm/$dd";
     $etime = "$temp_date $temp_time";
     print "START TIME = [$stime]\n" if $DEBUG;
     print "END TIME   = [$etime]\n" if $DEBUG;

     my($dur_sec) = &dsltime2sec($etime) - &dsltime2sec($stime);
     my($dur_min) = sprintf("%.3f",$dur_sec/60.0);
     my $str = "$pnum $stime $etime $dur_min ";
     if ($cols eq "")
               {for ($j=2;$j<$#vars+1;$j++) 
                  {$str .= "$vmin[$j] $vmax[$j] " if $minmax_flag;
                   $str .= sprintf("%.6f  ", $vave[$j]);
                   }
               }
     else
              {#output only selected columns
               for ($j=2;$j<=$#col_list;$j++)
                 {$str .= "$vmin[$col_list[$j]-1] $vmax[$col_list[$j]-1] " if $minmax_flag;
                  $str .= sprintf("%.6f  ", $vave[$col_list[$j]-1]);
                  }
               }
     push(@out_fd, "$str\n");


     #clear stats
     $rec_cnt = 0;
     for ($j=0;$j<$max_vars;$j++) {$vmin[$j] = 9999999; $vmax[$j] = -9999999; $vtot[$j]=0; $vrcnt[$j] = 0;}

      } #end for-each file

   return @out_fd;

   } #end-gen_mmp_sum

