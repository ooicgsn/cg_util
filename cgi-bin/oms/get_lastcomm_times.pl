#!/usr/bin/perl
###############################################################################
# Library for rtsum routines to get last update times for fb2550, irid, sbd,
# and xeos communications links. Returns LastComms hash array
# 
# History:
#     Date      Who    Description
#    -------    ---    ----------------------------------
#   06/10/2015  SL     Create
#######################################################################################
use strict;

1;

#
# Subroutines Follow
#

sub get_lastcomm_times {my $pid    = $_[0];
                        my $deploy = $_[1];
                        my $debug  = $_[2];

    my($BLACK)  = "#000000";
    my($GREEN)  = "#008800";
    my($RED)    = "#aa0000";
    my($YELLOW) = "#aa4400";
    my($ddir)   = "/webdata/cgsn/data/raw/$pid/$deploy";
    if (-d "$ddir/cg_data") {$ddir .= "/cg_data";}
    my (%LastComms, $status_line, $timestamp, $secs_since_update);
    my(%PCfg) = &load_cfg("$ddir/cfg_files/platform.cfg");

    $LastComms{'fb1.last_update_str'}   = "Not Available"; $LastComms{'fb1.color'}   = "$BLACK";
    $LastComms{'fb2.last_update_str'}   = "Not Available"; $LastComms{'fb2.color'}   = "$BLACK";
    $LastComms{'sbd1.last_update_str'}  = "Not Available"; $LastComms{'sbd1.color'}  = "$BLACK";
    $LastComms{'sbd2.last_update_str'}  = "Not Available"; $LastComms{'sbd2.color'}  = "$BLACK";
    $LastComms{'irid.last_update_str'}  = "Not Available"; $LastComms{'irid.color'}  = "$BLACK";
    $LastComms{'acom.last_update_str'}  = "Not Available"; $LastComms{'acom.color'}  = "$BLACK";
    $LastComms{'xeos1.last_update_str'} = "Not Available"; $LastComms{'xeos1.color'} = "$BLACK";
    $LastComms{'xeos2.last_update_str'} = "Not Available"; $LastComms{'xeos2.color'} = "$BLACK";
    $LastComms{'xeos3.last_update_str'} = "Not Available"; $LastComms{'xeos3.color'} = "$BLACK";

    #
    #Get last fb250 communcations time
    #
    if (defined $PCfg{"Telem.fb1.ip"} || $PCfg{"Telem.fb2.ip"} ne "")
      {foreach my $fb_link ("fb1", "fb2")
        {$status_line = `./view_syslog pid=$pid deploy=$deploy s=\"$fb_link link is UP\" extract=6,7,8,9 date=last_n_days | tail -1`; 
         chomp($status_line);
         if ($status_line =~ /timestamp/)
           {$LastComms{"$fb_link.last_update_str"} = "> 10 Days";
            $LastComms{"$fb_link.color"} = "$RED";
            }
         else
           {($timestamp) = split(', ',$status_line);
            my($secs_since_update) = time() - &dsltime2sec($timestamp);
            $LastComms{"$fb_link.last_update_str"}  = &sec2hhmmss($secs_since_update);
            if ($secs_since_update < 3600*12) {$LastComms{"$fb_link.color"} = "$GREEN";}
                                         else {$LastComms{"$fb_link.color"} = "$RED";}
            }
         }
       }
    
    #
    #Get last iridium communcations time
    #
    $status_line = `./view_syslog pid=$pid deploy=$deploy s=\"Begin FGET\" extract=4,5,6 date=last_n_days | tail -1`; 
    chomp($status_line);
    if ($status_line =~ /timestamp/)
      {$LastComms{'irid.last_update_str'} = "> 10 Days";
       $LastComms{"irid.color"} = "$RED";
       }
    else
      {($timestamp) = split(', ',$status_line);
       my($secs_since_update) = time() - &dsltime2sec($timestamp);
       $LastComms{"irid.last_update_str"}  = &sec2hhmmss($secs_since_update);
       if ($secs_since_update < 3600*12) {$LastComms{"irid.color"} = "$GREEN";}
                                    else {$LastComms{"irid.color"} = "$RED";}
       }
    

    #
    #Get last SBD/Xeos communcations time
    #
    my %Mon;
    $Mon{'Jan'} = "01";
    $Mon{'Feb'} = "02";
    $Mon{'Mar'} = "03";
    $Mon{'Apr'} = "04";
    $Mon{'May'} = "05";
    $Mon{'Jun'} = "06";
    $Mon{'Jul'} = "07";
    $Mon{'Aug'} = "08";
    $Mon{'Sep'} = "09";
    $Mon{'Oct'} = "10";
    $Mon{'Nov'} = "11";
    $Mon{'Dec'} = "12";
    foreach my $item ("sbd1","sbd2","xeos1","xeos2","xeos3")
      {if (defined $PCfg{"Telem.$item.imei"} && $PCfg{"Telem.$item.imei"} ne "")
         {my $sbd_file;
          if    ($item =~ /sbd/)  {if (-d "$ddir/irid_sbd")
                                        {$sbd_file = "$ddir/irid_sbd/$PCfg{\"Telem.$item.imei\"}.log";}
                                   else {$sbd_file = "$ddir/../irid_sbd/$PCfg{\"Telem.$item.imei\"}.log";}
                                   }
          elsif ($item =~ /xeos/) {if (-d "$ddir/xeos_sbd")
                                        {$sbd_file = "$ddir/xeos_sbd/$PCfg{\"Telem.$item.imei\"}.log";}
                                   else {$sbd_file = "$ddir/../xeos_sbd/$PCfg{\"Telem.$item.imei\"}.log";}
                                   }
          $LastComms{"$item.sbd_file"} = "$sbd_file";
          if (-f "$sbd_file")
            {$status_line = `grep "Transfer OK," $sbd_file|tail -1`; 
             chomp($status_line);
             #MOMSN=5193, Tue Jun 9 13:35:55 2015, 00 - Transfer OK, bytes=208, Lat: 40.13980 Lon: -70.76314 CEPradius: 2, mpic_cpm: 24.7 693.0
              my($momsn,$time_string, $status, $bytes, $lat_lon_radius, $mpic_str) = split(',', $status_line);
              my($day,$month,$mday,$time,$yyyy) = split(' ',$time_string);
              $mday = sprintf("%02d",$mday);
              $timestamp = "$yyyy/$Mon{$month}/$mday $time";
              #print "TIMESTAMP=[$timestamp]\nLine=$status_line\n";
             if (length($timestamp) != 19)
               {$LastComms{"$item.last_update_str"} = "$timestamp";
                $LastComms{"$item.color"} = "$RED";
                }
             else
               {my($secs_since_update) = time() - &dsltime2sec($timestamp);
                $LastComms{"$item.last_update_str"}  = &sec2hhmmss($secs_since_update);
                my $threshold = 3600 * 12; #12 hours
                if ($item =~ /xeos/) {$threshold = 3600 * 24;}
                if ($secs_since_update < $threshold) {$LastComms{"$item.color"} = "$GREEN";}
                                                else {$LastComms{"$item.color"} = "$RED";}
                }
             }
          else {$LastComms{"$item.color"} = "$RED";}
          }
       }

    if ($debug == 1)
      {#print-out LastComms status
       foreach my $key (sort keys %LastComms)
         {#next if ($key =~ /color/);
          print "LastComms[$key] = $LastComms{\"$key\"}\n";
          }
       }

    return %LastComms;
} 

