#!/usr/bin/perl
# plot_mpea.pl -  Quick script to generate rt psc/mpea mpm data plot
#
# Usage: plot_mpea.pl <pid=> <deploy=>
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      05/18/2015  SL     Create
#      08/10/2016  JJP	  Updated path to cg_util.pl for restructure in Bitbucket
##################################################################################
use strict;
require "../cg_util.pl";

my($pid)    = "pid_not_specified"; 
my($deploy) = "deploy_not_specified"; 
my $limit   = 3000;

my %arg = &get_args(0);


if ($arg{'pid'} ne "")    {$pid = $arg{'pid'};}
if ($arg{'deploy'} ne "") {$deploy = $arg{'deploy'};}

my $ddir = "/webdata/cgsn/data/raw/$pid/$deploy/cg_data/cpm3/pwrsys";
if (! -d "$ddir") {print "$ddir does not exist\n";
                   exit(-1);
                   };

my $day; #yyyymmdd 
if ($arg{'date'} ne "") {$day = $arg{'date'};}
                   else {$day = &gmtimestamp_filename(0);}


my (@flist, @data);
if ($arg{'date'} eq "last_n_days")
    {#grep through last-n fb250 files
     my $last_n_days = 10; 
     @flist = `cd $ddir; /bin/ls -1t *.pwrsys.log | head -$last_n_days`;
     }
else {@flist = `cd $ddir; /bin/ls -1 $day.pwrsys.log`;
      }

foreach my $f (sort @flist)
     {push(@data, `cd $ddir; grep -hi mpm $f`);
      #push(@data, `cat $ddir/$f`); 
      }

#Sample MPEA/mpm Data
#  2015/05/18 00:00:54.071 MPEA mpm: 372.31 232.42 80400000 0C183060 cv1 1 24.10 1421.54 cv2 1 24.05 427.69 
#     cv3 0 0.00 0.00 cv4 0 0.00 0.00 cv5 0 0.00 0.00 cv6 0 0.00 0.00 cv7 0 0.00 0.00 
#     aux 0 0.00 1 htl 5.19 108.72 17.37 -6.37 287b
#
my $i;
my $line;
my $html_out = <<"EndHTML";
<html>
<head>
<script type="text/javascript"
  src="/cg/dygraph-combined.js"></script>
</head>
<body>
<!-- Platform ID: $pid <br>
-->
<table>
   <td>MPEA Volts
       <div id="graphdiv1"
        style="width:500px; height:240px;"></div>
   </td>
   <td>MPEA Current (mA)  
       <div id="graphdiv2"
        style="width:500px; height:240px;"></div>
   </td>
   <td>MPEA Main Watts  
       <div id="graphdiv3"
        style="width:500px; height:240px;"></div>
   </td>
   </tr><tr>
   <td>CVT 1-2 States
       <div id="graphdiv4"
        style="width:500px; height:240px;"></div>
   </td>
   <td>CVT 1-2 Volts
       <div id="graphdiv5"
        style="width:500px; height:240px;"></div>
   </td>
   <td>CVT 1-2 Current (mA)
       <div id="graphdiv6"
        style="width:500px; height:240px;"></div>
   </td>
   <td>CVT 1-2 Watts
       <div id="graphdiv7"
        style="width:500px; height:240px;"></div>
   </td>
   </tr><tr>
   <td>Htl Volts
       <div id="graphdiv8"
        style="width:500px; height:240px;"></div>
   </td>
   <td>Htl Current (mA)
       <div id="graphdiv9"
        style="width:500px; height:240px;"></div>
   </td>
   <td>Temperature (degC)
       <div id="graphdiv10"
        style="width:500px; height:240px;"></div>
   </td>
   <td>Humidity %
       <div id="graphdiv11"
        style="width:500px; height:240px;"></div>
   </td>
   </tr>

</table>

<script type="text/javascript">

  g1 = new Dygraph(
    document.getElementById("graphdiv1"),
EndHTML
    $html_out .= "\"timestamp, volts \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $volts\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $volts\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      //strokeWidth: 1.0,
      //drawPoints: true,
      //valueRange: [0,400]
     }          // options
  );

  g2 = new Dygraph(
    document.getElementById("graphdiv2"),
EndHTML
    $html_out .= "\"timestamp, current \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $current\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $current\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g3 = new Dygraph(
    document.getElementById("graphdiv3"),
EndHTML
    $html_out .= "\"timestamp, watts \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        my $watts = sprintf("%.2f",$volts * $current/1000.0);
        if ($i < $#data-1){$html_out .= "\"$date $time, $watts\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $watts\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g4 = new Dygraph(
    document.getElementById("graphdiv4"),
EndHTML
    $html_out .= "\"timestamp, cv1_s, cv2_s \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current,$e1,$e2,$cv1_l,$cv1_s,$cv1_v,$cv1_i,$cv2_l,$cv2_s,$cv2_v,$cv2_i,
            $cv3_l,$cv3_s,$cv3_v,$cv3_i,$cv4_l,$cv4_s,$cv4_v,$cv4_i,$cv5_l,$cv5_s,$cv5_v,$cv5_i,
            $cv6_l,$cv6_s,$cv6_v,$cv6_i,$cv7_l,$cv7_s,$cv7_v,$cv7_4,$aux_l,$aux_s,$aux_v,$aux_i,
            $htl_l, $htl_v, $htl_i, $htl_24v, $htl_h) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $cv1_s, $cv2_s\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $cv1_s, $cv2_s\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1,
      stackedGraph: true,
      writeStroke: 2.0,
      valueRange: [0,4]
     }          // options
  );

  g5 = new Dygraph(
    document.getElementById("graphdiv5"),
EndHTML
    $html_out .= "\"timestamp, cv1_v, cv2_v \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current,$e1,$e2,$cv1_l,$cv1_s,$cv1_v,$cv1_i,$cv2_l,$cv2_s,$cv2_v,$cv2_i,
            $cv3_l,$cv3_s,$cv3_v,$cv3_i,$cv4_l,$cv4_s,$cv4_v,$cv4_i,$cv5_l,$cv5_s,$cv5_v,$cv5_i,
            $cv6_l,$cv6_s,$cv6_v,$cv6_i,$cv7_l,$cv7_s,$cv7_v,$cv7_4,$aux_l,$aux_s,$aux_v,$aux_i,
            $htl_l, $htl_v, $htl_i, $htl_24v, $htl_h) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $cv1_v, $cv2_v\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $cv1_v, $cv2_v\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g6 = new Dygraph(
    document.getElementById("graphdiv6"),
EndHTML
    $html_out .= "\"timestamp, cv1_i, cv2_i \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current,$e1,$e2,$cv1_l,$cv1_s,$cv1_v,$cv1_i,$cv2_l,$cv2_s,$cv2_v,$cv2_i,
            $cv3_l,$cv3_s,$cv3_v,$cv3_i,$cv4_l,$cv4_s,$cv4_v,$cv4_i,$cv5_l,$cv5_s,$cv5_v,$cv5_i,
            $cv6_l,$cv6_s,$cv6_v,$cv6_i,$cv7_l,$cv7_s,$cv7_v,$cv7_4,$aux_l,$aux_s,$aux_v,$aux_i,
            $htl_l, $htl_v, $htl_i, $htl_24v, $htl_h) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $cv1_i, $cv2_i\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $cv1_i, $cv2_i\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g7 = new Dygraph(
    document.getElementById("graphdiv7"),
EndHTML
    $html_out .= "\"timestamp, cv1_w, cv2_w \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current,$e1,$e2,$cv1_l,$cv1_s,$cv1_v,$cv1_i,$cv2_l,$cv2_s,$cv2_v,$cv2_i,
            $cv3_l,$cv3_s,$cv3_v,$cv3_i,$cv4_l,$cv4_s,$cv4_v,$cv4_i,$cv5_l,$cv5_s,$cv5_v,$cv5_i,
            $cv6_l,$cv6_s,$cv6_v,$cv6_i,$cv7_l,$cv7_s,$cv7_v,$cv7_4,$aux_l,$aux_s,$aux_v,$aux_i,
            $htl_l, $htl_v, $htl_i, $htl_24v, $htl_h) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        my $watts1 = sprintf("%.2f",$cv1_v * $cv1_i/1000.0);
        my $watts2 = sprintf("%.2f",$cv2_v * $cv2_i/1000.0);
        if ($i < $#data-1){$html_out .= "\"$date $time, $watts1, $watts2\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $watts1, $watts2\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );


  g8 = new Dygraph(
    document.getElementById("graphdiv8"),
EndHTML
    $html_out .= "\"timestamp, volts \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current,$e1,$e2,$cv1_l,$cv1_s,$cv1_v,$cv1_i,$cv2_l,$cv2_s,$cv2_v,$cv2_i,
            $cv3_l,$cv3_s,$cv3_v,$cv3_i,$cv4_l,$cv4_s,$cv4_v,$cv4_i,$cv5_l,$cv5_s,$cv5_v,$cv5_i,
            $cv6_l,$cv6_s,$cv6_v,$cv6_i,$cv7_l,$cv7_s,$cv7_v,$cv7_4,$aux_l,$aux_s,$aux_v,$aux_i,
            $htl_l, $htl_v, $htl_i, $htl_24v, $htl_h) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $htl_v\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $htl_v\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g9 = new Dygraph(
    document.getElementById("graphdiv9"),
EndHTML
    $html_out .= "\"timestamp, current \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current,$e1,$e2,$cv1_l,$cv1_s,$cv1_v,$cv1_i,$cv2_l,$cv2_s,$cv2_v,$cv2_i,
            $cv3_l,$cv3_s,$cv3_v,$cv3_i,$cv4_l,$cv4_s,$cv4_v,$cv4_i,$cv5_l,$cv5_s,$cv5_v,$cv5_i,
            $cv6_l,$cv6_s,$cv6_v,$cv6_i,$cv7_l,$cv7_s,$cv7_v,$cv7_4,$aux_l,$aux_s,$aux_v,$aux_i,
            $htl_l, $htl_v, $htl_i, $htl_24v, $htl_h) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $htl_i\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $htl_i\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { delimeter: ' ',
      errorBars: false,
      showRoller: false,
      rollPeriod: 1
     }          // options
  );

  g10 = new Dygraph(
    document.getElementById("graphdiv10"),
EndHTML
    $html_out .= "\"timestamp, degC \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current,$e1,$e2,$cv1_l,$cv1_s,$cv1_v,$cv1_i,$cv2_l,$cv2_s,$cv2_v,$cv2_i,
            $cv3_l,$cv3_s,$cv3_v,$cv3_i,$cv4_l,$cv4_s,$cv4_v,$cv4_i,$cv5_l,$cv5_s,$cv5_v,$cv5_i,
            $cv6_l,$cv6_s,$cv6_v,$cv6_i,$cv7_l,$cv7_s,$cv7_v,$cv7_4,$aux_l,$aux_s,$aux_v,$aux_i,
            $htl_l, $htl_v, $htl_i, $htl_24v, $htl_h) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $htl_24v\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $htl_24v\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
     }          // options
  );


  g11 = new Dygraph(
    document.getElementById("graphdiv11"),
EndHTML
    $html_out .= "\"timestamp, humidity \\n\" +\n";
    for ($i=0;$i<$#data;$i++)
       {$line = $data[$i];
        next if $line =~/content-type/i || length($line) <= 2;
        chomp($line);
        my ($date,$time,$l1,$l2,$volts,$current,$e1,$e2,$cv1_l,$cv1_s,$cv1_v,$cv1_i,$cv2_l,$cv2_s,$cv2_v,$cv2_i,
            $cv3_l,$cv3_s,$cv3_v,$cv3_i,$cv4_l,$cv4_s,$cv4_v,$cv4_i,$cv5_l,$cv5_s,$cv5_v,$cv5_i,
            $cv6_l,$cv6_s,$cv6_v,$cv6_i,$cv7_l,$cv7_s,$cv7_v,$cv7_4,$aux_l,$aux_s,$aux_v,$aux_i,
            $htl_l, $htl_v, $htl_i, $htl_24v, $htl_h) = split(' ',$line);
        ($time) = split('\.',$time); #strip-off ms
        if ($i < $#data-1){$html_out .= "\"$date $time, $htl_h\\n\" +\n";}
                     else {$html_out .= "\"$date $time, $htl_h\\n\" ,\n";}
        }
$html_out .= <<"EndHTML";
    { errorBars: false,
      showRoller: false,
      rollPeriod: 1,
     }          // options
  );


</script>
</body>
</html>
EndHTML

print "$html_out";
exit;

