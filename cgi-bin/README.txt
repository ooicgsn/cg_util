This folder was created to capture the cgi-bin files that are common between platcon_sw/cg_util/cgi-bin, 
oms_sw/cg_util/cgi-bin and/or pss_sw/cg_util/cgi-bin.

All files that are specific to either OMS, PlatCon or PSS should be captured in their respective folder:
	OMS     -> cg_util/cgi-bin/oms
	PlatCon -> cg_util/cgi-bin/platcon
	PSS     -> cg_util/cgi-bin/pss

cgi-bin/
   bcmd*              used by oms_sw & platcon_sw
   cg_util.pl*        used by oms_sw & platcon_sw
   edit_cfg*          used by platcon_sw & pss_sw; oms-unique version in cgi-bin/oms
   plot_cpm.pl*       used by oms_sw & platcon_sw & pss_sw
   plot_cpu.pl*       used by oms_sw & platcon_sw & pss_sw
   plot_dcl.pl*       used by oms_sw & platcon_sw & pss_sw
   plot_fc.pl*        used by oms_sw & platcon_sw
   plot_gflt.pl*      used by platcon_sw
   plot_idata.pl*     used by platcon_sw & pss_sw, oms-unique version in cgi-bin/oms
   plot_ntp.pl*       used by oms_sw & platcon_sw & pss_sw
   plot_portN.pl*     used by platcon_sw & pss_sw; oms-unique version in cgi-bin/oms
   plot_ports.pl*     used by platcon_sw & pss_sw; oms-unique version in cgi-bin/oms
   plot_psc.pl*       used by platcon_sw & pss_sw; oms-unique version in cgi-bin/oms
   plot_stc.pl*       used by oms_sw & platcon_sw & pss_sw
   view_cpm_status*   used by oms_sw & platcon_sw & pss_sw
   view_dcl_status*   used by oms_sw & platcon_sw & pss_sw
   view_dcl_syslog*   all are server-unique, see oms, platcon and pss folders for these files. 
		      NOTE: platcon and pss files should be reviewed to see if they can be merged.
   view_idata*        all are server unique, see oms, platcon and pss folders for these files
		      NOTE: platcon and pss files should be reviewed to see if they can be merged.
   view_istatus*      used by oms_sw & platcon_sw & pss_sw
   view_leak_detect*  used by oms_sw & platcon_sw
   view_mpea_err*     used by oms_sw & platcon_sw
   view_mpic_err*     used by oms_sw & platcon_sw & pss_sw
   view_psc_err*      used by oms_sw & platcon_sw & pss_sw
   view_psc_override* used by oms_sw & platcon_sw & pss_sw
   view_stc_status*   used by oms_sw & platcon_sw & pss_sw
   view_sys_stat*     used by oms_sw & platcon_sw & pss_sw
   view_syslog*       all are server-unique, see oms, platcon and pss folders for these files.
                      NOTE: platcon and pss files should be reviewed to see if they can be merged.
   view_wake_codes*   used by oms_sw & platcon_sw & pss_sw
