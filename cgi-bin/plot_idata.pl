#!/usr/bin/perl
# plot_idata.pl -  Quick script to plot platform instrument data
#
# Usaage: plot_idata pid= deploy= [inst=inst_dir_name] [nrecs=]
#
# History:
#       Date       Who    Description
#       ----       ---    --------------------------------------------------------
#      09/08/2011  SL     Create - support metbk, sbe16, sbe37, loadcell, rreflect,
#                         aquadopp 
#      09/14/2011  SL     Support eco_dfls, eco_vfsf
#      09/14/2011  ME     Added labels and units
#      09/25/2011  SL     Updated plots for aquadopp, 3dmgx2, 3dmgx3. Skip
#                         superv as part of infrastructure data subdir.
#      10/23/2012  SL     added mmp acm, ctd, eng data
#      09/24/2013  SL     Support imm sdir (for listing dirs only, currently do not support
#                         plotting raw imm/{mcat,mmp,phsen). Elim display embedded CEs - cpmN/dclN
#      09/30/2013  SL     Support plotting imm/mmp engineering data E*/C*, mcat, adcp 
#      10/09/2013  SL     Added velpt, flort, nutnr, pco2w
#      11/13/2013  SL/JKOB Support imm/mmp A*
#      11/20/2013  SL     Added pco2a, phsen, wavss, updated velpt, ctdpb, spikr (tbd)
#      11/30/2013  SL     Added pull-down file menu list, added dosta
#      12/09/2013  SL     Added spkir
#      07/01/2014  SL     Support plotting raw imm/ctdmo (mcat) and added
#                         hydrogen plots (hyd)
#      07/28/2014  SL     Support plotting optaa
#      09/03/2014  SL     Added Critical Level comment to plot title for hyd sensors,
#                         support plotting fdchp
#      09/15/2014  SL/BW  Swapped metbk wnd_e and wnd_n labels\
#      11/18/2014  SL     Updated wavss to handle additional NMEA strings
#      12/23/2014  SL     Updated to use cg_util.pl
#      02/02/2015  SL     Support Deployment dir
#      03/16/2015  SL     Support CTDBP format change which dropped Salinity
#      03/28/2015  SL/DG  Added zplsc (work in progress)
#      05/20/2015  SL     Added link to view data file. Try to plot metbk data in the
#                         case where 1 or more sensors may be bad.
#      06/01/2015  SL     Added show_platform_inst
#      07/24/2015  SL     ctdmo - removed using water depth for max depth - either keep track
#                         of max CTD depth or plot full dynamic range
#      08/18/2016  JJP	  Added TODO there is a reference to view_inst_plots
#			  which does not exist in the common area. This should
#			  be investigated - either add the file to common 
#			  or remove this reference.
##################################################################################
require "cg_util.pl";
my($DEBUG) = 0;

my($RED)    = "#ff0000";
my($YELLOW) = "#ffff00";
my($GREEN)  = "#00dd00";
my($GRAY)   = "#cccccc";
my($BLUE)   = "#0000dd";
my($LBLUE)  = "#aaaaff";

my($num_recs) = 2000;
my %arg = &get_args(0);
my $buffer = "$ENV{'QUERY_STRING'}";

my($pid) = $arg{'pid'}; #eg; as02cppm
my $deploy = "deploy_not_specified";
if ($arg{'deploy'} ne "") {$deploy = $arg{'deploy'};}
my($ddir) = "/webdata/cgsn/data/raw/$pid/$deploy";
if (-d "$ddir/cg_data") {$ddir .= "/cg_data";}

if ($arg{'d'} ne "") {$DEBUG = 1;}
if ($arg{'ddir'}  ne "") {$ddir = $arg{'ddir'};}
if ($arg{'dcl'}   ne "") {$ddir = "$ddir/$arg{'dcl'}";}
if ($arg{'nrecs'} ne "") {$num_recs = $arg{'nrecs'};}
if ($num_recs < 10)      {$num_recs = 10;}
elsif ($num_recs > 15000) {$num_recs = 15000;}
if (!defined $arg{'nrecs'} || $arg{'nrecs'} eq "") {$arg{'fileinfo'} = 1;} # 1-all 2 don't display inst list


#
#if ($arg{'inst'} eq "")
#  {print "***Error: Usage $0 <inst=instrument>\n\tWhere instrument - metbk, 3dmgx2, aquadop, ...\n";
#   exit;
#   }

# Look in ddir (either /data/cg_data or /data/cg_data/dclN) for instrument dirs

#Give list of instrument data to plot based on dirs in data dir
# Look in ddir (either /data/cg_data or /data/cg_data/dclN) for instrument dirs
opendir(DIR,"$ddir") || warn "***Unable to readdir [$ddir]\n";
my(@dir_list) = sort readdir(DIR);
closedir(DIR);
my($dir);
foreach $dir (@dir_list)
     {#skip infrastructure data subdirs
      chomp($dir);
      next if (! -d "$ddir/$dir");
      next if ($dir =~ /^\./ || $dir eq "cfg_files" || $dir eq "fb250" || $dir eq "irid" ||
               $dir eq "irid2shore" || $dir =~ /^syslog/ || $dir =~ /^dcl/ || $dir =~ /^rsync/ ||
               $dir eq "superv" || $dir eq "uplink_logs" || $dir eq "from_shore" ||
	       $dir =~ /^cpm(\d)/ || $dir =~ /^dcl(\d)/ || $dir =~ "irid_sbd" || $dir =~ "xeos_sbd");
      #If directory is IMM, pushd instrument subdirs on list
      if ($dir =~ /imm/)
          {opendir(DIR,"$ddir/$dir") || warn "***Unable to readdir [$ddir/$dir]\n";
           my(@sdir_list) = sort readdir(DIR);
           closedir(DIR);
           foreach my $sdir (@sdir_list)
             {#skip immlog and dot dirs
              chomp($sdir);
              next if (! -d "$ddir/$dir/$sdir");
              next if ($sdir =~ /^\./ || $sdir eq "imm_log");
              next if ($sdir =~ /^mmp_|^adcp_out/); #skip unpacked mmp_xxx dir (note: these shouln't be here)
              #If subdir is mmp, add mmp/e* and mmp/c*
              if ($sdir eq "mmp")
                   {push(@inst_dlist,"$dir/$sdir/E*");
                    push(@inst_dlist,"$dir/$sdir/C*");
                    push(@inst_dlist,"$dir/$sdir/A*");
                    }
              else {push(@inst_dlist,"$dir/$sdir");}
	      }
	   }
      else 
        {push(@inst_dlist,$dir);
	 }
      }

if ($arg{'inst'} eq "" && !$arg{'show_platform_inst'}) {$arg{'inst'} = $inst_dlist[0];}

#Get most recent instrument data file
    if ($arg{'inst'} eq "mmp_eng")
         {if ($arg{'file'} eq "") {$file = `cd $ddir/$arg{'inst'}; /bin/ls -1r E* | head -1`; chomp($file);}
          }
    elsif ($arg{'inst'} eq "mmp_acm")
         {if ($arg{'file'} eq "") {$file = `cd $ddir/$arg{'inst'}; /bin/ls -1r A* | head -1`; chomp($file);}
          }
    elsif ($arg{'inst'} eq "mmp_ctd")
         {if ($arg{'file'} eq "") {$file = `cd $ddir/$arg{'inst'}; /bin/ls -1r C* | head -1`; chomp($file);}
          }
    elsif ($arg{'inst'} =~ /imm\/mmp/)
         {$file = `cd $ddir; /bin/ls -1t $arg{'inst'} | head -1`; chomp($file);
	  $file =~ s#imm/mmp/##;
          }
    else {#$file = `cd $ddir; /bin/ls -1t $arg{'inst'} | head -1`; chomp($file);
          $file = `/bin/ls -1t $ddir/$arg{'inst'} | head -1`; chomp($file);
          }
    my($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks);
    if ($arg{'file'} ne "")
     {$file = $arg{'file'};
      if ($arg{'inst'} =~ /mmp\/E*/ || $arg{'inst'} =~ /mmp\/C*/ || $arg{'inst'} =~ /mmp\/A*/)
           {($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat("$ddir/imm/mmp/$file");}
      else {($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat("$ddir/$arg{'inst'}/$file");}
      }
    else
      {if ($arg{'inst'} =~ /mmp\/E*/ || $arg{'inst'} =~ /mmp\/C*/ || $arg{'inst'} =~ /mmp\/A*/)
         {($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat("$ddir/imm/mmp/$file");}
       else
         {($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat("$ddir/$arg{'inst'}/$file");}
       }
    my($last_mod_time) = time - $mtime;
    my($tstr1) = &time2dsltime_str($mtime);
    my($tstr2) = &sec2hhmmss($last_mod_time);
    my($mbytes) = sprintf("%.2f",$size / 1000000);
    my($tcolor) = "$GREEN";
    if    ($last_mod_time > 3600 * 24 * 2) {$tcolor = $RED;}
    elsif ($last_mod_time > 3600 * 24)     {$tcolor = $YELLOW;}
    $inst_info  = "<table bgcolor=$GRAY border=1>\n";
    $inst_info .= "  <tr><td>Instrument</td>   <td><b>$arg{'inst'}</b></td></tr>\n";
    $inst_info .= "  <tr><td>Current File</td> <td>$file</td></tr>\n";
    $inst_info .= "  <tr><td>File Size</td>    <td>$size bytes &nbsp ($mbytes mb)</td></tr>\n";
    $inst_info .= "  <tr><td>Last Updated</td> <td>$tstr1</td></tr>\n";
    $inst_info .= "  <tr><td>Time Since Update</td> <td bgcolor=$tcolor>$tstr2</td></tr>\n";
    if ($arg{'inst'} eq "aquadopp")
      {#dump last n-record
       @recent_data = `../bin/aqua_dump -ce < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} eq "3dmgx2" || $arg{'inst'} eq "mopak")
      {#dump last n-record
       @recent_data = `../bin/3dmgx_dump -a < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} eq "3dmgx3" || $arg{'inst'} eq "mopak")
      {#dump last n-record
       @recent_data = `../bin/3dmgx_dump < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /^imm\/mmp\/(A|C|E)/)
      {#dump last n-record
       @recent_data = `../bin/mmp_unpack stdout $ddir/imm/mmp/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} eq "imm/mcat" || $arg{'inst'} =~ /imm\/ctdmo/)
      {#dump last n-record
       @recent_data = `../bin/mcat_dump stdout $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /imm\/adcp/)
      {#dump last n-record
       @recent_data = `../bin/adcp_dump stdout $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /imm\/phsen/)
      {#dump last n-record
       @recent_data = `../bin/sunburst_dump -ce -t2 -i -q < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /^velpt/)
      {#dump last n-record
       @recent_data = `../bin/nortek_dump -ce < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /^pco2w/)
      {#dump last n-record
       @recent_data = `../bin/sunburst_dump -ce -t1 -i < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /^phsen/)
      {#dump last n-record
       @recent_data = `../bin/sunburst_dump -ce -t2 -i < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /^spkir/)
      {#dump last n-record
       @recent_data = `../bin/spkir_dump -ce < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /^optaa/)
      {#dump last n-record
       #Get timestamp from filename
       my($yyyymmdd_hhmmss) = split('\.',$file);
       $yyyymmdd_hhmmss =~ s/_//; #elim '_'
       @recent_data = `../bin/optaa_dump -ce -t $yyyymmdd_hhmmss < $ddir/$arg{'inst'}/$file | tail -$num_recs`;
       }
    elsif ($arg{'inst'} =~ /^fdchp/)
      {@recent_data = `tail -$num_recs $ddir/$arg{'inst'}/$file | grep -v fdchp`;
       }
    else {@recent_data = `tail -$num_recs $ddir/$arg{'inst'}/$file`;
          }
    $inst_info .= "</table>\n";

print "Content-type: text/html\n\n";
print << "EndHTML";
   <HTML>
   <HEAD><TITLE>Plot Idata</TITLE>
         <STYLE TYPE="text/css">
         <!--
          TH{font-family: Arial; font-size: 10pt;}
          TD{font-family: Arial; font-size: 10pt;}
          -->
         </STYLE>

    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9"> 
    <!--[if IE]><script src="/cg/excanvas.compiled.js"></script><![endif]-->

         <script type="text/javascript"
            src="/cg/dygraph-combined.js"></script>

         <script language="JavaScript">
            function updateForm(form)
               {fname = form.fileSel.options[form.fileSel.selectedIndex].value;
                window.location.href = "?$buffer&file=" + fname;
                }
         </script>
         </head>
   </HEAD>
   <BODY vlink=blue>
EndHTML

if ($arg{'show_platform_inst'}) # TODO view_inst_plots does not exist in common area. This should be fixed.
  {open(IN,"./view_inst_plots pid=$pid deploy=$deploy date=$arg{'date'}|") || warn "***Unable to read view_inst_plots\n";
    my @show_platform_inst  = <IN>;
   close(IN);
   print "@show_platform_inst";
   if ($arg{'inst'} eq "") {exit(1);}
   }

print "<br>";

if ($arg{'fileinfo'} > 0) 
  {if ($arg{'fileinfo'} == 1)
    {if ($arg{'dcl'} ne "") {print "<b>$arg{'dcl'} instruments:</b> ";}
                       else {print "<b>Instruments:</b> ";}
     foreach $inst (sort @inst_dlist)
         {print "<a href=?pid=$pid&plot=idata&dcl=$arg{'dcl'}&inst=$inst&nrecs=$num_recs>$inst </a> &nbsp\n";
         }
     print "<br><br>";
     }
   print "$inst_info\n<br>\n";
   print "<b>Last Few Records:</b>\n";
   if (-f "$ddir/$arg{'inst'}/$arg{'inst'}.hdr")
    {my(@hdr,$fd);
     open($fd,"$ddir/$arg{'inst'}/$arg{'inst'}.hdr");
     $hdr = <$fd>; close($fd);
     print "<pre>Fields: $hdr\n";
     print "$recent_data[$#recent_data]</pre>\n";
     }
  else
    {print "<pre>\n$recent_data[$#recent_data-4]$recent_data[$#recent_data-3]$recent_data[$#recent_data-2]\n";
     print "$recent_data[$#recent_data-1]$recent_data[$#recent_data]</pre>\n";
     }
  print "<b>Recent Data:</b><br>\n";
  }

else {#print "<b>$arg{'inst'}</b> - file: $file<br>\n";
      #generate file list
      my($fd);
      print "<form method=\"post\">\n";
      my ($file_sel) = "<SELECT Name=\"fileSel\" onChange=\"updateForm(this.form)\">\n";
      if ($arg{'inst'} =~ /^imm\/mmp/)
           {if    ($arg{'inst'} =~ /A\*/) {open($fd,"cd $ddir/imm/mmp; /bin/ls -1t A*|");}
            elsif ($arg{'inst'} =~ /C\*/) {open($fd,"cd $ddir/imm/mmp; /bin/ls -1t C*|");}
            elsif ($arg{'inst'} =~ /E\*/) {open($fd,"cd $ddir/imm/mmp; /bin/ls -1t E*|");}
            }
      else {open($fd,"/bin/ls -1t $ddir/$arg{'inst'}|");}
      my($fname); my($SELECTED);
      while ($fname = <$fd>)
       {chomp($fname);
        next if ($fname =~ /\.hdr$/); #skip header file
        #If imm/mmp/{A*|C*|E*} then only list those types of files
        if ($arg{'inst'} =~ /^imm\/mmp/) 
             {my($f) = $arg{'inst'}; chop($f); #elim asterik
              next if ("/imm/mmp/$fname" !~ /$f/);
              }
        if ($fname eq "$file") {$SELECTED = "SELECTED";}
                          else {$SELECTED = "";}
        $file_sel .=  "<OPTION value=\"$fname\" $SELECTED>$fname</OPTION>\n";
        }
      $file_sel .= "</SELECT>\n";
      print "<b>$arg{'inst'}</b> - Plot File: $file_sel &nbsp <a href=view_syslog?pid=$pid&deploy=$deploy&view=idata&dcl=$arg{'dcl'}&inst=$arg{'inst'}&nrecs=1000&file=$file&show_platform_inst=$arg{'show_platform_inst'}><font size=2>View File</font></a><br>\n";
      print "</form>\n";
      }

#
# Generate Plots Here
#
$html_plot = &gen_inst_plot($arg{'inst'});


print "$html_plot";
print "</BODY>\n";
print "</HTML>\n";

exit;

#
# Subroutines Follow
#

sub gen_inst_plot {my($inst) = "$_[0]";
   my($html_out) = "";
   my(%plot_item, $itemi, $i, $gid);

   if ($inst =~ /aquadopp/i)
      {#Date,ErrCode,SoundSpeed,Heading,Pitch,Roll,PressureMSB,Status,PressureLSW,Temp,E,N,U,Amp1,Amp2,Amp3
       #06/25/11 07:51:08,0,1530.10,147.80,34.20,32.90,0,60,849,23.29,-294,-266,266,26,23,24,
       $plot_item{"1.Heading (deg)"} = 2;
       $plot_item{"2.Pitch (deg)"}   = 3;
       $plot_item{"3.Roll (deg)"}    = 4;
       $plot_item{"4.Temp (degC)"}    = 8;
       $plot_item{"5.E,N,U"}     = "9,10,11";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:200px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 3 || $ind == 6) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[aquadopp/i || $line =~ /BAD_DATA/);
             #next if ($line =~ /date/i); #Skip header line
             if ($line =~ /date/i) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if aquadopp

   elsif ($inst =~ /3dmgx/i || $inst =~ /mopak/)
      {#3DM-GX CB_AARM ax: -0.053948 ay: 0.075111 az: -1.054216 rx: 0.149715 ry: 0.127701 rz: -0.105600 mx: 0.235052 my: -0.118783 mz: 0.484943 t: 1799.44
       $plot_item{"1.accel (g's)"} = "3,5,7";
       $plot_item{"2.rate (r/s)"}  = "9,11,13";
       $plot_item{"3.mag"}   = "15,17,19";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"n,x,y,z\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[3dmgx/i || $line =~ /BAD_DATA/);
             # Need to calculate date based on filename and rec_cnt
             # if ($line =~ /date/i) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             (@vars) = split(' ',$line);
             ($one,$two,$three) = split(",",$plot_item{"$item"});
             if ($i < $#recent_data-1){$html_out .= "\"$i,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                 else {$html_out .= "\"$i,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
#print "GOT HERE - 1=$one,2=$two,3=$three [$plot_item{\"$item\"}]\n"; 
#print "[@vars] <br> $html_out"; exit(-1);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             #$line =~ s/,/ /g; #get rid of commas
             #($date,$time,@vars) = split(' ', $line);
             #if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
             #                    else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: true,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if 3dmgx


   elsif ($inst =~ /velpt/i)
      {#AQVEL: date_utc time_utc, ErrFlag, sndspd_m/s, hdg_deg, pitch_deg, roll_deg, pressMSB, StatusFlag, pressLSW, 
       #       temp_degC, velE_mm/s, velN_mm/s, velU_mm/s, ampB1, ampB2, ampB3,
       #2013/10/09 12:59:16, 0, 1497.00, 123.30, 29.20, -30.00, 0, 32, 80, 12.07, 572, 4294963557, 4294962736, 
       #        59, 56, 56, 0.080
       $plot_item{"1.SndSpd (m/s)"} = "1";
       $plot_item{"2.Temp (degC)"}  = "8";
       $plot_item{"3.VelE (mm/s)"}  = "9";
       $plot_item{"4.VelN (mm/s)"}  = "10";
       $plot_item{"5.VelU (mm/s)"}  = "11";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:256px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 2) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             #next if ($line =~ /BAD_DATA/);
             #next if ($line =~ /date/i); #Skip header line
             if ($line =~ /^AQVEL/i) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true 
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if velpt


   elsif ($inst =~ /flort/i)
      {#Date      Time     N/U sig N/U sig N/U sig Therm
       #2013/10/09 00:00:16.773 07/12/13        00:00:00        700     4130    695     1059    460     4130    554
       #2013/10/09 00:00:17.911 07/12/13        00:00:01        700     4130    695     1060    460     4130    553
       $plot_item{"1.Sig1"}  = "3";
       $plot_item{"2.Sig2"}  = "5";
       $plot_item{"3.Sig3"}  = "7";
       $plot_item{"4.Therm"} = "8";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($gid == 3) {$html_out .= "</tr><tr>\n";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[flort/i || $line =~ /BAD_DATA/);
             #next if ($line =~ /date/i); #Skip header line
             if ($line =~ /^AQVEL/i) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  $time = substr($time,0,8); #elim ms
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		   $time = substr($time,0,8); #elim ms
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if flort


   elsif ($inst =~ /dosta/i)
      {#Date      Time   f1. f2. O2_uM, AirSat_%, Temp_degC
       #2013/12/06 10:30:41.644 4831	131	321.595	96.072	12.246	32.225	32.225	37.171	4.945	1067.0	1097.8	323.1
       #2013/12/06 10:30:46.643 4831	131	321.552	96.059	12.246	32.227	32.227	37.173	4.947	1067.1	1097.2	323.1
       $plot_item{"1.O2 (uM)"}     = "2";
       $plot_item{"2.AirSat (%)"}  = "3";
       $plot_item{"3.Temp (degC)"} = "4";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[nutnr/i || $line =~ /BAD_DATA/);
             #next if ($line =~ /date/i); #Skip header line
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  $time = substr($time,0,8); #elim ms
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		   $time = substr($time,0,8); #elim ms
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if dosta


   elsif ($inst =~ /nutnr/i)
      {#Date      Time   instID, date, time, nitrate_uMol/L,aux1,aux2,aux3,RMS error
       # 2013/10/03 12:08:54.332 SATNLC0243,2013276,12.148282,5.60,-39.02,64.77,0.46,0.000379
       # 2013/10/03 12:08:55.579 SATNLC0243,2013276,12.148653,6.34,-44.45,73.72,0.46,0.000308
       $plot_item{"1.Nitrate (uMol/L)"} = "3";
       $plot_item{"2.Error (RMS)"}      = "7";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[nutnr/i || $line =~ /BAD_DATA/);
             #next if ($line =~ /date/i); #Skip header line
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  $time = substr($time,0,8); #elim ms
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		   $time = substr($time,0,8); #elim ms
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if nutnr
	
   elsif ($inst =~ /wavss/i)
      {# $TSPWA, Date, Time, Serial, BuoyID, Latitude, Longitude, Number of Zero Crossings, Havg (Average Wave Height), 
       # Tz (Mean Spectral Period), Hmax (Maximum Wave Height), Hsig (Significant Wave Height), Tsig (Significant Period), 
       # H10 (average height of highest tenth of waves), T10 (average period of H10 waves), Tavg (Mean Wave Period), 
       # TP (Peak Period), TP5, HMO, Mean Direction, Mean Spread *cs
       # 2012/12/22 00:40:23.250 $TSPWA,20121221,191901,04581,buoyID,,,132,0.00,8.0,0.00,0.00,11.6,0.00,10.9,6.3,28.6,30.3,
       # 0.00,297.3,75.4*6D
       #$plot_item{"1.Ave Wave Height (m)"} = "6";
       #$plot_item{"3.Significant Wave Height (m)"} = "9";
       $plot_item{"1.Wave Height Ave, Sig, Max (m)"} = "6,9,8";
       $plot_item{"2.Period Ave, Sig, Peak (sec)"} = "13,10,14";
       $plot_item{"3.Mean Direction (deg)"} = "17";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:256px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($gid == 2) {$html_out .= "</tr><tr><td><br></td></tr><tr>\n";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<=$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[wavss/i || $line =~ /BAD_DATA/);
             #next if ($line =~ /Record/i); #Skip header line
             next if ($line !~ /\$TSPWA/);  #Format change 11/2014 to log more NMEA strings - only plot $TSPWA for now
             chomp($line);

             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  $time = substr($time,0,8); #elim ms
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  #print "date=$date, time=$time, on=$one, two=$two, three=$three wave=$vars[$one],$vars[$two]<br>";
                  if ($i < $#recent_data){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                    else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		   $time = substr($time,0,8); #elim ms
                   if ($i < $#recent_data){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                     else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true 
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if wavvs

   elsif ($inst =~ /pco2a/i)
      {#YYYY/MM/DD HH:MM:SS M aaaaa bbbbb ccc.ccc dd.d ee.eeee ff.ffff gggg hh.h ii.i A|W
       #  M                       Begin Measurement
       #  aaaaa                   Zero A/D [counts] from most recent autozero sequence
       #  bbbbb                   Current A/D [counts]
       #  ccc.ccc                 Measured CO2 [ppm]
       #  dd.d                    Average IRGA temperature [°C]
       #  ee.eeee                 Humidity [mbar]
       #  ff.ffff                 Humidity sensor temperature [°C]
       #  gggg                    Gas stream pressure [gas tension]
       #  hh.h                    IRGA detector temperature [°C]
       #  ii.i                    IRGA source temperature [°C]
       #  A|W                     Water or atmospheric measurement
       #2012/10/10 11:59:30.738 #2012/10/10 11:59:18, M,46197,42788,531.5,42.8,14.507,30.160,1013,42.4,43.3,W
       $plot_item{"1.PCO2 Air (ppm)"} = "5";
       $plot_item{"2.Ave IRGA Temp (DegC)"} = "6";
       $plot_item{"3.PCO2 Water (ppm)"} = "5";
       $plot_item{"4.Ave IRGA Temp (DegC)"} = "6";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:256px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($gid == 2) {$html_out .= "</tr><tr><td><br></td></tr><tr>\n";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[pco2/i || $line =~ /BAD_DATA/);
             #next if ($line =~ /Record/i); #Skip header line
             chomp($line);

             #IF gid is < 2, plot Air 'A' records, otherwise plot Water 'W' records
             next if ($gid < 2 && $line =~ /W$/);
             next if ($gid >= 2 && $line =~ /A$/);

             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  $time = substr($time,0,8); #elim ms
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		   $time = substr($time,0,8); #elim ms
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true 
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if pco2a

   elsif ($inst =~ /pco2w/i)
      {#PCO2: Record, RawTime, DrkRf, DrkSg, 434Rf, 434Sg, 620Rf, 620Sg, 434Rt, 620Rt, DrkRf, DrkSg, 434Rf, 434Sg, 620Rf, 620Sg,
       #Bat_r, tmp_r, InstDate  InstTime,  Bat_V, Tem_C, PCO2_ppm
       #PC02: 2013/10/08 00:10:25.397 *5E2704CE78FDF90031003409D3029F07CD01C3100A0D3B0032003209D4029F07CA01C80C8B068AE6,
       #  3464035833, 00049, 00052, 02515, 00671, 01997, 00451, 04106, 03387, 00050, 00050, 02516, 
       #  00671, 01994, 00456, 03211, 01674, 201
       $plot_item{"1.PCO2 (ppm)"} = "22";
       $plot_item{"2.Temp (DegC)"} = "21";
       $plot_item{"3.Battery (v)"} = "20";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[pco2/i || $line =~ /BAD_DATA/);
             next if ($line =~ /Record/i); #Skip header line
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             $line =~ s/^PCO2: |^PC02: //; #note oh vs zero
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  $time = substr($time,0,8); #elim ms
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		   $time = substr($time,0,8); #elim ms
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if pco2w


   elsif ($inst =~ /phsen/i)
      {#PHSEN: Record, RawTime, RawTemp1, Blanks[4][4], Measurements[23][4], RawBattery, RawTemp2, InstDate  InstTime, Tmp1_C, pH, Bat_V, Tmp2_C
       #PHSEN: 2013/11/18 02:00:21.249 *CFE70ACEAF2538083D0D0608A80B560D9A0D0608A40B570D9A0D0608A70B560D980D0308A70B550D9A0D0708A80
       #      B560D9D0D0708890B570D9A0D0A076C0B550D930D0704FD0B590D530D0A02E50B580CB80D0A01B30B560BFB0D0901340B570B6E0D0601170B560B190D0
       #      701300B550AFD0D08016C0B570B180D0601C60B580B4E0D0602360B520B930D0B02B70B560BD80D05034A0B540C200D0903DB0B550C5D0D0604710B530
       #      C970D0904FA0B540CC20D0805780B560CE70D0805EE0B560D0A0D07064E0B540D220D0506A70B520D360D0506EF0B540D460D06072C0B540D5200000C8
       #      2083FA0, 3467584824, 02109, 03334, 02216, 02902, 03482, 03334, 02212, 02903, 03482, 03334, 02215, 02902, 03480, 03331, 
       #      02215, 02901, 03482, 03335, 02216, 02902, 03485, 03335, 02185, 02903, 03482, 03338, 01900, 02901, 03475, 03335, 01277, 
       #      02905, 03411, 03338, 00741, 02904, 03256, 03338, 00435, 02902, 03067, 03337, 00308, 02903, 02926, 03334, 00279, 02902, 
       #      02841, 03335, 00304, 02901, 02813, 03336, 00364, 02903, 02840, 03334, 00454, 02904, 02894, 03334, 00566, 02898, 02963, 
       #      03339, 00695, 02902, 03032, 03333, 00842, 02900, 03104, 03337, 00987, 02901, 03165, 03334, 01137, 02899, 03223, 03337, 
       #      01274, 02900, 03266, 03336, 01400, 02902, 03303, 03336, 01518, 02902, 03338, 03335, 01614, 02900, 03362, 03333, 01703, 
       #      02898, 03382, 03333, 01775, 02900, 03398, 03334, 01836, 02900, 03410, 03202, 02111, 2013/11/18 02:00:24, 10.29, 3.91, 
       #      11.73, 10.25,
       $plot_item{"1.pH"} = "116";
       $plot_item{"2.Temp1 (DegC)"} = "115";
       $plot_item{"3.Temp2 (DegC)"} = "118";
       $plot_item{"4.Battery (v)"} = "117";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($gid == 3) {$html_out .= "</tr><tr>\n";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[phsen/i || $line =~ /BAD_DATA/);
             next if ($line =~ /Record/i); #Skip header line
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             $line =~ s/^PHSEN: //; #note oh vs zero
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  $time = substr($time,0,8); #elim ms
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		   $time = substr($time,0,8); #elim ms
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if phsen


   elsif ($inst =~ /spkir/i)
      {#Date Time,Inst,SN,Timer(s),Delay(ms),C1(uW/cm^2/nm),C2(uW/cm^2/nm),C3(uW/cm^2/nm),C4(uW/cm^2/nm),C5(uW/cm^2/nm),C6(uW/cm^2/nm),C7(uW/cm^2/nm),Vin(V),Va(V),Temp(C),Frame
       #2013/12/10 00:00:15.299,SATDI7,0238,0000004.90,-133,0.001127,0.014123,0.014123,-0.002580,-0.020874,0.010456,-0.011848,8.22,5.34,15.00,0
       #2013/12/10 00:00:16.304,SATDI7,0238,0000005.99,-133,0.001082,0.014274,0.014256,-0.002505,-0.021066,0.010433,-0.011785,8.34,5.34,15.00,1
       $plot_item{"01.C1 (uW/cm^2/nm)"} = "4";
       $plot_item{"02.C2 (uW/cm^2/nm)"} = "5";
       $plot_item{"03.C3 (uW/cm^2/nm)"} = "6";
       $plot_item{"04.C4 (uW/cm^2/nm)"} = "7";
       $plot_item{"05.C5 (uW/cm^2/nm)"} = "8";
       $plot_item{"06.C6 (uW/cm^2/nm)"} = "9";
       $plot_item{"07.C7 (uW/cm^2/nm)"} = "10";
       $plot_item{"08.Vin (v)"}         = "11";
       $plot_item{"09.Va (v)"}          = "12";
       $plot_item{"10.Temp (degC)"}     = "13";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:256px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 4 || $ind == 7) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=1;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             #next if ($line =~ /BAD_DATA/);
             #next if ($line =~ /date/i); #Skip header line
             #if ($line =~ /^SPKIR/i) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		   $time = substr($time,0,8); #strip-off sub-seconds
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }


          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true 
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if spkir


    elsif ($inst =~ /metbk/i)
      {#date, time, BP (mbar)  RH    RH Temp  LW    PRC     SST  Cond     SW WND E WND N MUX BAT
       #2011/08/27 00:03:29.797 1013.15  70.017  20.388  427.9   -0.08  -5.000  0.0000    2.7  -12.31    4.76  0.0000 12.50
       $plot_item{"01.BP (mbar)"}      = 0;
       $plot_item{"02.Rel Humid %"}    = 1;
       $plot_item{"03.Rel Temp (degC)"}= 2;
       $plot_item{"04.PRC (mm)"}       = 4;
       $plot_item{"05.SST (degC)"}     = 5;
       $plot_item{"06.Cond (s/m)"}     = 6;
       $plot_item{"07.LW (w/m^2)"}     = 3;
       $plot_item{"08.SW (w/m^2)"}     = 7;
       $plot_item{"09.wnd_e (m/s)"}    = 8;
       $plot_item{"10.wnd_n (m/s)"}    = 9;
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:325px; height:200px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 3 || $ind == 6) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             #next if ($line =~/\[metbk/i || $line =~ /BAD_DATA/);
             if ($line =~/\[metbk/i || $line =~ /BAD_DATA/)
                  {#try to plot anyways - assuming correct number of fields is present
                   $line =~ s/]:/]: /; #sometimes there's no space before barometric pressure
	           ($date,$time,@vars) = split(' ', $line);
                   next if ($#vars != 12);
                   $line =~ s/\[([^\[\]]|(?0))*]://g; #strip-off [metbkN:DLOGPN]: error message field
                   }
             if ($i == 0) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             $line =~ s/(\s+)/ /gs; #get rid of extra spaces
             #Convert date/time to m/d/y hr:mn:ss
	     ($date,$time,@vars) = split(' ', $line);
	     my($y,$m,$d)    = $date =~ /^(\d+)\/(\d+)\/(\d+)/;
	     my($hr,$mm,$ss) = $time =~ /^(\d+)\:(\d+)\:(\d+)/;
             $ss = substr($ss,0,2);
             next if ($plot_item{"$item"} eq ""); #get rid of any blank fields
             if ($i < $#recent_data-1){$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                 else {$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}

             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true
              }          // options  - valueRange: [x,y]
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if metbk


    elsif ($inst =~ /loadcell/i)
      {#Loadcell: date, time, raw, lbs
       #          2011/08/30 00:00:17.175 *+00074.48 -54.91
       $plot_item{"1.loadcell (lbs)"} = 1;
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:640px; height:320px;\"></div>\n";
          $html_out .= "  </td>\n";
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[loadcell/i || $line =~ /BAD_DATA/);
             if ($i == 0) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             $line =~ s/(\s+)/ /gs; #get rid of extra spaces
             #Convert date/time to m/d/y hr:mn:ss
	     ($date,$time,@vars) = split(' ', $line);
	     my($y,$m,$d)    = $date =~ /^(\d+)\/(\d+)\/(\d+)/;
	     my($hr,$mm,$ss) = $time =~ /^(\d+)\:(\d+)\:(\d+)/;
             $ss = substr($ss,0,2);
             next if ($plot_item{"$item"} eq ""); #get rid of any blank fields
             if ($i < $#recent_data-1){$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                 else {$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}

             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if loadcell


    elsif ($inst =~ /rreflect/i)
      {#Loadcell: date, time, raw
       #          2011/09/09 00:00:17.175 *+02205.48
       $plot_item{"1.rreflect (raw)"} = 0;
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:640px; height:320px;\"></div>\n";
          $html_out .= "  </td>\n";
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[rreflect/i || $line =~ /BAD_DATA/);
             if ($i == 0) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             $line =~ s/(\s+)/ /gs; #get rid of extra spaces
             #Convert date/time to m/d/y hr:mn:ss
	     ($date,$time,@vars) = split(' ', $line);
	     my($y,$m,$d)    = $date =~ /^(\d+)\/(\d+)\/(\d+)/;
	     my($hr,$mm,$ss) = $time =~ /^(\d+)\:(\d+)\:(\d+)/;
             $ss = substr($ss,0,2);
             $vars[$plot_item{"$item"}] =~ s/^\*\+//; #eliminate leading *+
             next if ($plot_item{"$item"} eq ""); #get rid of any blank fields
             if ($i < $#recent_data-1){$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                 else {$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}

             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if rreflect



    elsif ($inst =~ /^sbe16/ || $inst =~ /^ctdbp/)
      {#sbe16 and sbe37: date, time, # temp, ...
       #          2011/09/09 00:00:05.520 # 23.4509, 0.00003,   -0.037,   0.0114, 09 Sep 2011, 00:00:03
       #Support CTDBP format change which dropped Salinity 3/2015
       #          2015/03/16 00:00:05.413 # 13.7090,  4.09786,   13.233, 16 Mar 2015 00:00:01
       $plot_item{"1.temp (degC)"}  = 1;
       $plot_item{"2.Conductivity (S/m)"} = 2;
       $plot_item{"3.Pressure (decibars)"} = 3;
       #Check to see if salinity is present. If is isn't, don't plot it
       if (split(' ',$recent_data[$#recent_data-3]) > 10) {$plot_item{"4.Salinity (psu)"} = 4;}
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:640px; height:200px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 2) {$html_out .= "</tr><tr><td><br></td></tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[sbe16/i || $line =~ /BAD_DATA/ || $line =~ /\[ctd/i);
             if ($i == 0) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             $line =~ s/,/ /g; #get rid of commas
             $line =~ s/(\s+)/ /gs; #get rid of extra spaces
             #Convert date/time to m/d/y hr:mn:ss
	     ($date,$time,@vars) = split(' ', $line);
	     my($y,$m,$d)    = $date =~ /^(\d+)\/(\d+)\/(\d+)/;
	     my($hr,$mm,$ss) = $time =~ /^(\d+)\:(\d+)\:(\d+)/;
             $ss = substr($ss,0,2);
             next if ($plot_item{"$item"} eq ""); #get rid of any blank fields
             if ($i < $#recent_data-1){$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                 else {$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}

             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true
              }          // options  - valueRange: [x,y]
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if either sbe16


    elsif ($inst =~ /eco_dfls1/i || $inst =~ /eco_vsfs3/i)
      {#1 and 3 chan fluorom: date, time, ...
       # 1 chan: 2011/09/14 04:29:59.490 40800	12141	3443
       # 3 chan: 2011/09/14 04:29:58.890 40800	12140	4095	4095	4095	871
       # $plot_item{"1.Date (Julian)"} = 0;
       $plot_item{"2.Time (fraction of day)"} = 1;
       if ($inst =~ /eco_dfls1/i)
         {$plot_item{"3.Signal (raw)"} = 2;}
       elsif ($inst =~ /eco_vsfs3/i)
         {$plot_item{"3.100-degree LED (raw)"} = 2;
	  $plot_item{"4.125-degree LED (raw)"} = 3;
          $plot_item{"5.150-degree LED (raw)"} = 4;
          $plot_item{"6.LED intesity ref. (raw)"} = 5;
	 }
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:500px; height:200px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 3) {$html_out .= "</tr><tr><td><br></td></tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[eco_/i || $line =~ /BAD_DATA/);
             if ($i == 0) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             $line =~ s/,/ /g; #get rid of commas
             $line =~ s/(\s+)/ /gs; #get rid of extra spaces
             #Convert date/time to m/d/y hr:mn:ss
	     ($date,$time,@vars) = split(' ', $line);
	     my($y,$m,$d)    = $date =~ /^(\d+)\/(\d+)\/(\d+)/;
	     my($hr,$mm,$ss) = $time =~ /^(\d+)\:(\d+)\:(\d+)/;
             $ss = substr($ss,0,2);
             next if ($plot_item{"$item"} eq ""); #get rid of any blank fields
             if ($i < $#recent_data-1){$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                 else {$html_out .= "\"$m/$d/$y $hr:$mm:$ss, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}

             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true
              }          // options  - valueRange: [x,y]
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if eco_dfls1 and eco_vsfs3


   elsif ($inst =~ /mmp_ctd/i || $inst =~ /imm\/mcat/ || $inst =~ /imm\/mmp\/C/)
      {###NOTE - no timestamp on data get profile time from engineering meta file
       #Sal mmho/cm, Temp(C), Depth (dbars), DO2 Hz
       #+35.3624  +06.7874  +0468.230  2667
       $plot_item{"1.Depth (dbars)"}   = 2;
       $plot_item{"2.Temp (degC)"}     = 1;
       $plot_item{"3.Sal (mmho/cm)"}   = 0;
       $plot_item{"4.DO2 (Hz)"}        = 3;
       #$plot_item{"1.Depth (m)"}    = 2;
       #$plot_item{"2.Depth/Temp"}   = "1,2";
       #$plot_item{"3.Depth/DO2"}    = "3,2";
       $fields_item{"1"}    = "Sample,Depth";
       $fields_item{"2"}    = "Sample,Temp";
       $fields_item{"3"}    = "Sample,mmho/cm";
       $fields_item{"4"}    = "Sample,DO2";
       $gid = 0;
       $html_out .= "<table cellspacing=10 cellpadding=10>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:500px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 2 || $ind == 4) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          if ($fields_item{"$ind"} ne "") 
               {$html_out .= "\"$fields_item{$ind}\\n\" +\n";
                }
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~ /^#/ || $line =~/content-type/i || length($line) <= 20; #Note 20 to avoid metadata
             chomp($line);
             $line =~ s/,//g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {(@vars) = split(' ',$line);
                  ($one,$two) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$vars[$one], $vars[$two]\\n\" +\n";}
                                      else {$html_out .= "\"$vars[$one], $vars[$two]\\n\" ,\n";}
                  }
             else {(@vars) = split(' ', $line);
                   if ($item =~ /depth \(m\)/i) 
                       {if ($i < $#recent_data-1){$html_out .= "\"$i, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$i, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                       }
                   else
                       {if ($i < $#recent_data-1){$html_out .= "\"$i, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$i, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                       }
                   }
             }
          $html_out =~ s/\+\n$/,\n/;
          if ($item =~ /depth \(dbars\)/i) {#max depth should be based on platform - temp commented out
                                            #$valueRange = "valueRange:[500,0],";
                                            }
                                  else {$valueRange = "";}
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: true,
               $valueRange
               fillGraph: false
              // valueRange: [500,0]
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if mmp_ctd


   elsif ($inst =~ /mmp_acm/i)
      {###NOTE - no timestamp on data get profile time from engineering meta file
       #VA(cm/s)  VB(cm/s)  VC(cm/s)  VD(cm/s)  MX(norm)  MY(norm)  MZ(norm)  PT(deg)  RL(deg)
       #-183.72 -183.72 -183.72 -183.72    nan    nan    nan 0.0 0.1
       $plot_item{"1.VA,VB,VC,VD (cm/s)"}   = "0,1,2,3";
       $plot_item{"2.MX,MY,MZ (norm)"}      = "4,5,6";
       $plot_item{"3.Pitch(deg)"}    = 7;
       $plot_item{"4.Roll (deg)"}    = 8;
       #$plot_item{"1.Depth (m)"}    = 2;
       #$plot_item{"2.Depth/Temp"}   = "1,2";
       #$plot_item{"3.Depth/DO2"}    = "3,2";
       $fields_item{"1"}    = "Sample, VA,VB,VC,VD";
       $fields_item{"2"}    = "Sample,MX,MY,MZ";
       $fields_item{"3"}    = "Sample,pitch";
       $fields_item{"4"}    = "Sample,roll";
       $gid = 0;
       $html_out .= "<table cellspacing=10 cellpadding=10>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:500px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 2) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          if ($fields_item{"$ind"} ne "") 
               {$html_out .= "\"$fields_item{$ind}\\n\" +\n";
                }
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~ /^#/ || $line =~/content-type/i || $line =~/ACM/i ||
	             length($line) <= 20; #Note 20 to avoid metadata
             chomp($line);
             $line =~ s/,//g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {(@vars) = split(' ',$line);
		  if ($item =~ /VA/)
                    {($one,$two,$three,$four) = split(",",$plot_item{"$item"});
		     #next if ($vars[$one] eq "nan" || $vars[$two] eq "nan");
                     if ($i < $#recent_data-1){$html_out .= "\"$i, $vars[$one], $vars[$two], $vars[$three], $vars[$four]\\n\" +\n";}
                                         else {$html_out .= "\"$i, $vars[$one], $vars[$two], $vars[$three], $vars[$four]\\n\" ,\n";}
		     }
		  elsif ($item =~ /MX/)
                    {($one,$two,$three) = split(",",$plot_item{"$item"});
		     next if ($vars[$one] eq "nan" || $vars[$two] eq "nan");
                     if ($i < $#recent_data-1){$html_out .= "\"$i, $vars[$one], $vars[$two], $vars[$three]\\n\" +\n";}
                                         else {$html_out .= "\"$i, $vars[$one], $vars[$two], $vars[$three]\\n\" ,\n";}
		     }
                  }
             else {(@vars) = split(' ', $line);
                   if ($i < $#recent_data-1){$html_out .= "\"$i, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$i, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: true,
               legend: 'always',
               fillGraph: false
              // valueRange: [500,0]
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if mmp_acm


    elsif ($inst =~ /mmp_eng/i || $inst =~ /imm\/mmp\/E/)
      {#mm dd yy hh mm ss mA  batV depth(dbar) Par(mV) scatSig chlSig CDOMSig 
       # 09 25 2011 08 26 43    191   10.8    467.120         0.00      304       52       82
       if ($arg{'poption'} eq "inst_only")
            {$plot_item{"01.Par (mV)"}      = 3;
             $plot_item{"02.ScatSig"}       = 4;
             $plot_item{"03.ChlSig"}        = 5;
             $plot_item{"04.CDOMSig"}       = 6;
             }
       elsif ($arg{'poption'} eq "eng_only")
            {$plot_item{"01.Current (mA)"}  = 0;
             $plot_item{"02.Batt (V)"}      = 1;
             $plot_item{"03.Press (dbar)"}  = 2;
             }
       else {$plot_item{"01.Current (mA)"}  = 0;
             $plot_item{"02.Batt (V)"}      = 1;
             $plot_item{"03.Press (dbar)"}  = 2;
             $plot_item{"04.Par (mV)"}      = 3;
             $plot_item{"05.ScatSig"}       = 4;
             $plot_item{"06.ChlSig"}        = 5;
             $plot_item{"07.CDOMSig"}       = 6;
             }
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          if ($arg{'plot'} eq "inst_only")
               {$html_out .= "      <div id=\"graphdiv$gid\" style=\"width:500px; height:300px;\"></div>\n";}
          else {$html_out .= "      <div id=\"graphdiv$gid\" style=\"width:450px; height:256px;\"></div>\n";}
          $html_out .= "  </td>\n";
          if ($ind == 2 && $arg{'plot'} eq "inst_only")  {$html_out .= "</tr><tr>";}
          if ($ind == 3 && $arg{'plot'} ne "inst_only")  {$html_out .= "</tr><tr>";}
          if ($ind == 5) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if (substr($line,3,1) ne "/"); #skip meta-data records (ie; doesn't start with date str)
             chomp($line);
             $line =~ s/(\s+)/ /gs; #get rid of extra spaces
             #Convert date/time to m/d/y hr:mn:ss - only if written with no metadata
             #($m, $d, $y, $hr, $mm, $ss, @vars) = split(' ', $line);
             #$date = "$m/$d/$y"; $time="$hr:$mm:$ss";
             ($date,$time,@vars) = split(' ', $line);
             $ss = substr($ss,0,2);
             next if ($plot_item{"$item"} eq ""); #get rid of any blank fields
             if ($item =~ /press/i)
               {next if ($vars[$plot_item{"$item"}] == 0);
                if ($i < $#recent_data-1){$html_out .= "\"$date $time, -$vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                    else {$html_out .= "\"$date $time, -$vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                }
             else
               {if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                    else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                }

             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true
              }          // options  - valueRange: [x,y]
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if mmp_eng


   elsif ($inst =~ /imm\/ctdmo/i)
      {#yyyy/mm/dd hh:mm:ss, Temp(C), Cd(S/m), P(db)
       #2014/07/01 12:11:07, 21.9089, 0.00005, -0.023
       $plot_item{"1.Temp (degC)"}   = 0;
       $plot_item{"2.Cd (S/m)"}      = 1;
       $plot_item{"3.Depth (dbars)"} = 2;
       $fields_item{"1"} = "Timestamp,Temp";
       $fields_item{"2"} = "Timestamp,Cd";
       $fields_item{"3"} = "Timestamp,Depth";
       $gid = 0;

       # Removed water depth for max depth - either keep track
       # of max CTD depth or plot full dynamic range
       my $max_depth = 10;

       $html_out .= "<table cellspacing=10 cellpadding=10>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:500px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 2 || $ind == 4) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          if ($fields_item{"$ind"} ne "") 
               {$html_out .= "\"$fields_item{$ind}\\n\" +\n";
                }
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~ /^#/ || $line =~/content-type/i || length($line) <= 20; #Note 20 to avoid metadata
             chomp($line);
             $line =~ s/,//g; #get rid of commas
             ($date,$time,@vars) = split(' ', $line);
             if ($item =~ /depth \(/i) 
               {my $depth = $vars[$plot_item{"$item"}];
	        if ($depth > $max_depth) {$max_depth = $depth;}
	        if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                    else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                }
             else
               {if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                    else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                }
             }
          $html_out =~ s/\+\n$/,\n/;
          if ($item =~ /depth \(/i) {#$valueRange = "valueRange:[$max_depth,0],";
                                     }
                                else {$valueRange = "";}
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: true,
               $valueRange
               fillGraph: false
              // valueRange: [500,0]
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if imm/ctdmo


    elsif ($inst =~ /imm\/adcp/i)
      {#Processing file: adcp_20130914_205545.DAT
       #Date_Time,Ensemble,Heading,Pitch,Roll,Temp(C),Pressure(kPa)
       #2013/09/14 16:15:00.00,195,152.12,-34.00,-36.32,22.75,  4.61
       #Bin,East,North,Up,Err
       #01,1,41,3,-32768
       #02,-32768,-32768,-32768,-32768
       #03,-32768,-32768,-32768,-32768
       #04,-32768,-32768,-32768,-32768

       #Plot heading pitch/roll, temp, press
       $plot_item{"1.Heading (deg)"}    = "1";
       $plot_item{"2.Pitch(deg),Roll(deg)"} = "2,3";
       $plot_item{"3.Temp (degC)"}      = "4";
       $plot_item{"4.Pressure(kPa)"}    = "5";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
	  if ($gid == 3) {$html_out .= "</tr><tr>\n";}
          }

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line !~/\//); #must contain a 'slash' for date
             chomp($line);
             $line =~ s/,/ /g; #get rid of commas
	     my($yyyy,$mm,$dd,$time);
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                  ($one,$two) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" +\n";}
                                      else {$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                   if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
	  
       #Plot Bin #1 velocities
       #Bin,East,North,Up,Err
       #01,1,41,3,-32768
       #02,-32768,-32768,-32768,-32768
       $gid = 5;
       $html_out .= "  <td>Bin #1 - Velocities (E,N,U,Err)\n";
       $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
       $html_out .= "  </td>\n";
       $html_out .= "</table>\n";

       $html_out .= "<script type=\"text/javascript\">\n";
       $html_out .= "g$gid = new Dygraph(\n";
       $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
       $html_out .= "\"i,east,north,up,err\\n\" +\n";
       my $cnt = 0;
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line !~/^01,/); #must start with 01,
             chomp($line);
             $line =~ s/,/ /g; #get rid of commas
	     my($yyyy,$mm,$dd,$time);
             $cnt++;
	     (@vars) = split(' ',$line);
                  if ($i < $#recent_data-1){$html_out .= "\"$cnt,$vars[1],$vars[2],$vars[3],$vars[4]\\n\" +\n";}
                                      else {$html_out .= "\"$cnt,$vars[1],$vars[2],$vars[3],$vars[4]\\n\" ,\n";}
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
        } #End-if adcp

   elsif ($inst =~ /imm\/mmp\/A/)
      {#MM-DD-YYYY HH:MM:SS,TmpC,Heading,Pitch,Roll,Vel[0,0],Vel[1,0],Vel[2,0],Amp[0,0],Amp[1,0],Amp[2,0]
       #11-01-2013 22:10:17,14.4500,51.0000,0.0000,-0.3000,-0.14300,-0.25920,-0.08110,164,164,166
       $plot_item{"1.Heading (deg)"} = 1;
       $plot_item{"2.Pitch (deg)"}   = 2;
       $plot_item{"3.Roll (deg)"}    = 3;
       $plot_item{"4.Temp (degC)"}   = 0;
       $plot_item{"5.Vel0-2 (m/s)"}  = "4,5,6";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:200px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 3 || $ind == 6) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          if ($label =~ /Vel/i)
               {$html_out .= "\"date_time, Vel0, Vel1, Vel2\\n\" +\n";}
          else {$html_out .= "\"date_time, $label\\n\" +\n";}
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[aquadopp/i || $line =~ /BAD_DATA/);
             #next if ($line =~ /date/i); #Skip header line
             if ($line =~ /date/i) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
             #if ($i < $#recent_data-1){$html_out .= "\"$line\\n\" +\n";}
             #                    else {$html_out .= "\"$line\\n\" ,\n";}
             $line =~ s/,/ /g; #get rid of commas
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
                  #convert date mm-dd-yyyy hh:mm:ss to mm/dd/yyyy
                  my($mm,$dd,$yyyy) = split('-',$date);
                  $date = "$mm/$dd/$yyyy";
                  next if ($yyyy !~ /^2/);
                  ($one,$two,$three) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                      else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
                   #convert date mm-dd-yyyy hh:mm:ss to mm/dd/yyyy
                   my($mm,$dd,$yyyy) = split('-',$date);
                   next if ($yyyy !~ /^2/);
                   $date = "$mm/$dd/$yyyy";
                   if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if $inst =~ /imm\/mmp\/A/


    elsif ($inst =~ /adcp/i)
      {#2013/11/22 00:04:18.804 Hdg: 34.7 Pitch: 0.0 Roll: -1.2
       #2013/11/22 00:04:18.837 Temp: 14.1 SoS: 1506 BIT: 00
       #2013/11/22 00:04:18.920 Bin    Dir    Mag     E/W     N/S    Vert     Err   Echo1  Echo2  Echo3  Echo4
       #2013/11/22 00:04:19.004   1  187.2  135.1     -17    -134       9      15    203    207    204    205
       #2013/11/22 00:04:19.088   2  190.5  125.1     -23    -123      19       5    206    208    207    208
       # ... to bin 30

       #Plot heading pitch/roll, temp, press
       $plot_item{"1.Heading (deg)"}    = "1";
       $plot_item{"2.Pitch(deg),Roll(deg)"} = "3,5";
       $plot_item{"3.Temp (degC)"}      = "7";
       $plot_item{"4.SoundVel (m/s)"}   = "9";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
	  if ($gid == 3) {$html_out .= "</tr><tr>\n";}
          }

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line !~/\//); #must contain a 'slash' for date
             next if ($line !~ /Hdg:/);
             chomp($line);
             #It's a hdg line, append next line which contains temp/sos
             if ($recent_data[$i+1] =~ /Temp:/) 
                {my($d,$t,$t_l,$temp,$s_l,$sndspd) = split(' ',$recent_data[$i+1]);
                 $line .= "temp: $temp sndspd: $sndspd";
                 }
             $line =~ s/,/ /g; #get rid of commas
	     my($yyyy,$mm,$dd,$time);
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                  ($one,$two) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" +\n";}
                                      else {$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                   if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
	  
       #Plot Bin #1 velocities E/W N/S Vert 
       # 2013/11/22 02:04:19.778 Bin    Dir    Mag     E/W     N/S    Vert     Err   Echo1  Echo2  Echo3  Echo4
       # 2013/11/22 02:04:19.852   1  213.6  151.4     -84    -126      19     -30    190    196    194    192
       $gid = 5;
       $html_out .= "  <td>Bin #1 - Velocities (E,N,U,Err)\n";
       $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
       $html_out .= "  </td>\n";
       $html_out .= "</table>\n";

       $html_out .= "<script type=\"text/javascript\">\n";
       $html_out .= "g$gid = new Dygraph(\n";
       $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
       $html_out .= "\"i,east,north,up,err\\n\" +\n";
       my $cnt = 0;
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             my($d,$t,$bin) = split(' ',$line);
             next if ($bin ne "1"); #only want bin 1
             chomp($line);
             $cnt++;
	     (@vars) = split(' ',$line);
                  if ($i < $#recent_data-1){$html_out .= "\"$cnt,$vars[5],$vars[6],$vars[7],$vars[8]\\n\" +\n";}
                                      else {$html_out .= "\"$cnt,$vars[5],$vars[6],$vars[7],$vars[8]\\n\" ,\n";}
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
        } #End-if adcp



    elsif ($inst =~ /presf/i)
      {#2013/12/10 00:00:12.683 [presf:DLOGP2]:Instrument Started [Power On]
       # 2013/12/10 00:05:24.374 tide: start time = 10 Dec 2013 00:00:26, p = 208.0130, pt = 13.797, t = 13.6434
       # 2013/12/10 00:05:25.370 wave: start time = 10 Dec 2013 00:05:28
       # 2013/12/10 00:05:25.398 wave: ptfreq = 171754.281
       # 2013/12/10 00:05:27.415  208.0155
       # 2013/12/10 00:05:28.415  208.0189
       $plot_item{"1.Tide Pressure (psi)"} = "10";
       $plot_item{"2.PT (degC), T (degC)"} = "13,16";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
	  if ($gid == 3) {$html_out .= "</tr><tr>\n";}
          }

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line !~ /tide:/);
             chomp($line);
             $line =~ s/,/ /g; #get rid of commas
	     my($yyyy,$mm,$dd,$time);
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                  ($one,$two) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" +\n";}
                                      else {$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                   if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
	  
       # 2013/12/10 00:05:25.370 wave: start time = 10 Dec 2013 00:05:28
       # 2013/12/10 00:05:25.398 wave: ptfreq = 171754.281
       # 2013/12/10 00:05:27.415  208.0155
       # ...
       #2013/12/10 00:05:46.454 wave: end burst
       $gid = 5;
       $html_out .= "  <td>Wave Pressure (psi)\n";
       $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
       $html_out .= "  </td>\n";
       $html_out .= "</table>\n";

       $html_out .= "<script type=\"text/javascript\">\n";
       $html_out .= "g$gid = new Dygraph(\n";
       $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
       $html_out .= "\"date_time,press\\n\" +\n";
       for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             #skip all lines that are not wave pressure data
             next if ($line =~ /wave|tide|presf/);
             chomp($line);
	     my($yyyy,$mm,$dd,$time);
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                  ($one,$two) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" +\n";}
                                      else {$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                   if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: true
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
        } #End-if presf


   elsif ($inst =~ /hyd/i)
      {#yyyy/mm/dd hh:mm:ss raw %lel unit
       #2014/07/02 18:00:08.807 *+00003.99 3.990000 %
       $plot_item{"1.LEL (%) - CRITICAL Level is at 25%"} = 1;
       $fields_item{"1"}    = "date_time,LEL";
       $gid = 0;
       $html_out .= "<table cellspacing=10 cellpadding=10>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:500px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 2 || $ind == 4) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          if ($fields_item{"$ind"} ne "") 
               {$html_out .= "\"$fields_item{$ind}\\n\" +\n";
                }
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             #Normally use next line instead of line below it - commented-out to eliminate connected points in plots
             #next if ($line =~/\[hyd/i || $line =~ /BAD_DATA/); #Skip meta data msgs
             next if (($line =~/\[hyd/i && $line !~ /Stopped/) || $line =~ /BAD_DATA/); #Skip meta data msgs
             next if $line =~ /^#/ || $line =~/content-type/i || length($line) <= 5;
             chomp($line);
             $line =~ s/,//g; #get rid of commas
             ($date,$time,@vars) = split(' ', $line);
	      $time = substr($time,0,8); #strip-off sub-seconds
              if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                  else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: true,
               fillGraph: false
              // valueRange: [0,100]
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if hydr


   elsif ($inst =~ /optaa/i)
      {#Note - currently optaa dump program only puts out temperatures
       #PType, MType, SerNum, RawExTemp, RawInTemp, Time(ms), Date Time, ExtTemp(C), IntTemp(C)
       # 05, 53, 000099, 7bcd, b0ff, 000028ac, 2014/07/30 00:00:27.412, 21.791497, 23.055998
       $plot_item{"1.ExtTemp (degC)"} = 8;
       $plot_item{"2.IntTemp (degC)"} = 9;
       $fields_item{"1"}    = "date_time,ExtTemp";
       $fields_item{"2"}    = "date_time,IntTemp";
       $gid = 0;
       $html_out .= "<table cellspacing=10 cellpadding=10>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:500px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
          if ($ind == 2 || $ind == 4) {$html_out .= "</tr><tr>";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          if ($fields_item{"$ind"} ne "") 
               {$html_out .= "\"$fields_item{$ind}\\n\" +\n";
                }
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if ($line =~/\[optaa/i || $line =~ /BAD_DATA/); #Skip meta data msgs
             next if $line =~ /^#/ || $line =~/content-type/i || length($line) <= 5;
             chomp($line);
             $line =~ s/,//g; #get rid of commas
             (@vars) = split(' ', $line);
              $date = $vars[6]; next if ($date !~ /^2/);
              $time = $vars[7];
	      $time = substr($time,0,8); #strip-off sub-seconds
              if ($i < $#recent_data-1){$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                  else {$html_out .= "\"$date $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: true,
               fillGraph: false
              // valueRange: [0,100]
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if optaa


   elsif ($inst =~ /fdchp/i)
      {#2014/09/02 14:21:51.302 FLUXDATA 1409652060.058,1.3,000001,  0.11,  0.12,  0.11,206.17,  0.46,  0.47,  0.29,  0.18,  2.75,  2.17,  1.36,207.07, -2.31, -1.69, -2.04,205.21, -0.33,  0.08,  9.83,  0.04,  0.03,  0.01 -0.12,  0.24,  9.91, -0.54, -0.07,  9.74  0.00, -0.01, -0.00,  0.00,  0.00,  0.00,  0.02,  0.00,  0.02, -0.01, -0.03, -0.02,  4.25, -0.00,  0.02,  0.00,  0.00,  0.00, -2.03,  0.00,  0.02, -2.05, -0.00,  0.02, -0.14,  0.03,  0.11, -0.14,  0.03,  0.11,  0.17, -0.07, -0.01,  0.03  0.03
       $plot_item{"1.wndUVW (m/s)"} = "2,3,4";
       $plot_item{"2.accel (m/s)"}  = "18,19,20";
       $plot_item{"3.rate (deg/s)"}   = "30,31,32";
       $plot_item{"4.hdg/pitch/roll (deg)"}   = "40,41,42";
       $fields_item{"1"}    = "date_time,U,V,W";
       $fields_item{"2"}    = "date_time,x,y,z";
       $fields_item{"3"}    = "date_time,x,y,z";
       $fields_item{"4"}    = "date_time,hdg,pitch,roll";
       my $Rad2Deg = 180.0/3.1415926536;
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
	  if ($gid >= 3) {$html_out .= "</tr><tr>\n";}
          }
       $html_out .= "</table>\n";

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          if ($fields_item{"$ind"} ne "")
               {$html_out .= "\"$fields_item{$ind}\\n\" +\n";
                }
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[fdchp/i || $line =~ /BAD_DATA/);
             # if ($line =~ /date/i) {$html_out .= "\"date_time, $label\\n\" +\n"; next;}
             chomp($line);
	     my $date_time_flux_secs;
             ($date_time_flux_secs,@vars) = split(',',$line);
	     ($date,$time) = split(' ',$date_time_flux_secs);
	     $time = substr($time,0,8); #elim ms
             ($one,$two,$three) = split(",",$plot_item{"$item"});
	     if ($item =~ /hdg/) {$vars[$one] *= $Rad2Deg; $vars[$two] *= $Rad2Deg; $vars[$three] *= $Rad2Deg;}
             if ($i < $#recent_data-1){$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" +\n";}
                                 else {$html_out .= "\"$date $time,$vars[$one],$vars[$two],$vars[$three]\\n\" ,\n";}
#print "GOT HERE - 1=$one,2=$two,3=$three [$plot_item{\"$item\"}]\n"; 
#print " <br>VARS=[$vars[$one]], $vars[$two], $vars[$three]\n<br>\n";
#print "[@vars] <br> $html_out"; exit(-1);
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
        } #End-if fdchp



    elsif ($inst =~ /zplsc/i)
      {#Field 	Type	Description
       #-----	----	-----------
       #1	string	Header: @Dyyyymmddhhmmss!@P (Contains timestamp)
       #2 	uint 	Instrument Serial Number 
       #3 	Int 	Phase 
       #4 	int 	Burst number (1  65535) 
       #5 	int 	Number of stored frequencies 1  4 
       #6 	int 	N1 = Number of bins for frequency 1 
       #7** 	int 	N2 = Number of bins for frequency 2 
       #8** 	int 	N3 = Number of bins for frequency 3 
       #9** 	int 	N4 = Number of bins for frequency 4 
       #10 	uint 	Minimum value in the data subtracted out 
       #11** 	uint 	Minimum value in the data subtracted out 
       #12** 	uint 	Minimum value in the data subtracted out 
       #13** 	uint 	Minimum value in the data subtracted out 
       #14 	string 	Date of burst, YYMMDDHHMMSSHH 
       #15 	double 	Tilt X 
       #16 	double 	Tilt Y 
       #17 	double 	Battery voltage 
       #18 	double 	Temperature 
       #19 	double 	Pressure (valid value if sensor is available) 
       #20 	Uchar 	Channel 1 board number always 0 
       #21 	Uint 	Channel 1 frequency 
       #N1 val 	uint 	Channel 1 values minus minimum value 
       #N1+1 	Uchar 	Channel 2 board number always 1 
       #N1+2 	uint 	Channel 2 frequency 
       #N2 val 	uint 	Channel 2 values minus minimum value (available) 
       #N2+1 	Uchar 	Channel 3 board number always 2 
       #N2+2 	uint 	Channel 3 frequency 
       #N3 val 	uint 	Channel 3 values minus minimum value (available) 
       #N3+1 	Uchar 	Channel 4 board number always 3 
       #N3+2 	uint 	Channel 4 frequency 
       #N4 val 	uint 	Channel 4 values minus minimum value (available) 
       #Last 	char 	Literal !\n 

       #example: 
       #yyyy/mm/dd hh:mm:ss.sss,@D20150312094853!@P,55080,1,2714,4,275,275,275,275,9192,14736,11504,11480,15031209484767,
       #32.5,-16.3,10.4,21.9,99.0,0,38,53536,31744,320,304,64,280,288,288,280,288,200,232,280,288,288,280,288,40,296,


       #Plot TiltX, TiltY, , temp, press
       $plot_item{"1.TiltX(deg),TiltY(deg"} = "14,15";
       $plot_item{"2.Battery(v)"}  = "16";
       $plot_item{"3.Temp (degC)"} = "17";
       $plot_item{"4.Pressure(?)"} = "18";
       $gid = 0;
       $html_out .= "<table>\n";
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "  <td>$label\n";
          $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
          $html_out .= "  </td>\n";
	  if ($gid == 3) {$html_out .= "</tr><tr>\n";}
          }

       $gid = 0;
       foreach $item (sort keys %plot_item)
         {($ind,$label) = split('\.',$item);
          $gid++;
          $html_out .= "<script type=\"text/javascript\">\n";
          $html_out .= "g$gid = new Dygraph(\n";
          $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
          $html_out .= "\"date_time, $label\\n\" +\n";
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line =~/\[zplsc/i || $line =~ /BAD_DATA/);
             chomp($line);
             $line =~ s/,/ /g; #get rid of commas
	     my($yyyy,$mm,$dd,$time);
             if ($plot_item{"$item"} =~ /,/)
                 {($date,$time,@vars) = split(' ',$line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                  ($one,$two) = split(",",$plot_item{"$item"});
                  if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" +\n";}
                                      else {$html_out .= "\"$mm/$dd/$yyyy $time,$vars[$one],$vars[$two]\\n\" ,\n";}
                  }
             else {($date,$time,@vars) = split(' ', $line);
		  ($yyyy,$mm,$dd) = split('/',$date);
		  $time = substr($time,0,8); #strip-off sub-seconds
                   if ($i < $#recent_data-1){$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" +\n";}
                                       else {$html_out .= "\"$mm/$dd/$yyyy $time, $vars[$plot_item{\"$item\"}]\\n\" ,\n";}
                   }
             }

          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
          }
	  


       #Plot channel 1-4 frequencies
       $gid = 5;
       $html_out .= "  <td>Chan1 (frequency))\n";
       $html_out .= "      <div id=\"graphdiv$gid\" style=\"width:480px; height:300px;\"></div>\n";
       $html_out .= "  </td>\n";
       $html_out .= "</table>\n";

       $html_out .= "<script type=\"text/javascript\">\n";
       $html_out .= "g$gid = new Dygraph(\n";
       $html_out .= "document.getElementById(\"graphdiv$gid\"),\n";
       $html_out .= "\"i,east,north,up,err\\n\" +\n";
       my $cnt = 0;
          for ($i=0;$i<$#recent_data;$i++)
            {$line = $recent_data[$i];
             next if $line =~/content-type/i || length($line) <= 2;
             next if ($line !~/^01,/); #must start with 01,
             chomp($line);
             $line =~ s/,/ /g; #get rid of commas
	     my($yyyy,$mm,$dd,$time);
             $cnt++;
	     (@vars) = split(' ',$line);
                  if ($i < $#recent_data-1){$html_out .= "\"$cnt,$vars[1],$vars[2],$vars[3],$vars[4]\\n\" +\n";}
                                      else {$html_out .= "\"$cnt,$vars[1],$vars[2],$vars[3],$vars[4]\\n\" ,\n";}
             }
          $html_out =~ s/\+\n$/,\n/;
          $html_out .= <<"EndHTML";
             { errorBars: false,
               showRoller: false,
               rollPeriod: 1,
               strokeWidth: 1.0,
               drawPoints: false,
               fillGraph: false
              }          // options
             ); // end-Dygraph
            </script>
EndHTML
        } #End-if zplsc


    return "$html_out";
}


